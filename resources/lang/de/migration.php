<?php

return [
    'headline' => 'Vielen Dank für Deine Profil-Bestätigung',
    'text' => 'Herzlich Willkommen auf Deiner neuen Online-Mentoring Plattform!',
    'next' => 'Weiter zum Login'
];