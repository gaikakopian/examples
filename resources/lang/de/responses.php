<?php

return [
    'confirmed' => [
        'title' => 'Super! Vielen Dank!',
        'text' => 'Als nächstes, kannst du zurück auf unsere Plattform gehen!',
    ],
    'invalidlink' => [

        'title' => 'Dieser Link ist nicht mehr gültig!',
        'text' => 'Bitte entschuldige, aber dieser Link ist nicht mehr gültig. Bitte kontaktiere uns per E-Mail!',
    ],

    'toofast' => [
        'title' => 'Dieser Link ist noch nicht aktiv',
        'text' => 'Bitte warten Sie zumindest 5 Minuten nach E-Mail versandt, bis dieser Link aktiviert wurde!',
    ],


    'appointment' => [ // could be mereged;
        'confirmed' => [
            'title' => 'Okay!',
            'text' => 'Vielen Dank für die Rückmeldung!',
        ]
    ],
    'next' => 'Zurück zur Plattform'
];