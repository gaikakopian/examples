<?php

return [
    'confirmed' => [

        'title' => 'Great! Thank you!',
        'text' => 'Now please go back to the platform and follow the next steps!',
    ],
    'invalidlink' => [

        'title' => 'This link is not valid anymore!',
        'text' => 'Sorry, but the link is not valid anymore. Most probably it was already clicked. Please contact us if this seems to be wrong.',
    ],
    'toofast' => [
        'title' => 'This link is not ready yet',
        'text' => 'Please wait at least 5 minutes after you received the email to confirm this link!',
    ],

    'appointment' => [
        'confirmed' => [
            'title' => 'Okay!',
            'text' => 'Thank you for your response!',
        ]
    ],
    'next' => 'Continue to the platform'
];