<html>
<head>
    <title>@yield('title')</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
</head>
<body>
<style>
    *, body {
        font-family: 'Open Sans', sans-serif;
        font-size: 13px;
    }

    body {
        background: #fafafa;
    }
    pre {
        border: 1px #eee solid;
        background:#d3e1ec;
        padding:10px;
        font-family: "Times New Roman";
    }

    .container {
        background-color: #fff;
        border-radius: 3px;
        padding: 20px;
        margin: 50px auto;
        width: 924px;
        max-width: 100%;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.07);
    }

    a {
        color: #00b1a7;
        text-decoration: none;
    }

    a:hover {
        text-decoration: underline;
    }

    .muted {
        color: #a0a0a0;;

    }

    .inner-container {
        padding: 20px;
        min-height: 300px;
        overflow: auto;
    }

    h1 {
        font-family: "Open Sans", Arial, sans-serif !important;
        font-weight: 800;
        color: #454545;
        margin-top:0;
        font-size: 30px;

    }
</style>
<div class="container">
    <div class="inner-container">

        @yield('content')

        @yield('nextsteps')
    </div>
</div>
</body>
</html>