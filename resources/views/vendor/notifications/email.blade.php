@component('mail::message')


@if (isset($brand))
    <img style="margin-bottom:20px; " src="{{$brand['logo']}}" height="50" alt="{{$brand['name']}}">
@else
    <p>eMentoring Software</p>

@endif


    {{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level == 'error')
# Whoops!
@else

@endif
@endif

{{-- Intro Lines --}}
<div>
@foreach ($introLines as $line)
    {!! $line !!}
@endforeach
</div>
{{-- Action Button --}}
@isset($actionText)
<?php
    switch ($level) {
        case 'success':
            $color = 'green';
            break;
        case 'error':
            $color = 'red';
            break;
        default:
            $color = 'blue';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@endisset

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{{ $line }}

@endforeach


@isset($brand['email_signature'])
@slot('footer')
    @component('mail::footer')

{{ ($brand['email_signature']) }}

    @endcomponent
@endslot
@endisset

{{-- Subcopy --}}
@isset($actionText)
@component('mail::subcopy')
If you’re having trouble clicking the "{{ $actionText }}" button, copy and paste the URL below
into your web browser: [{{ $actionUrl }}]({{ $actionUrl }})
@endcomponent
@endisset
@endcomponent
