@extends('errors.default')

@section('title', 'The given data was wrong')


@section('content')

    <p><b>{{$exception->getMessage()}}</b></p>

    <pre>{{print_r($exception->toArray())}}</pre>


@endsection


@section('nextsteps')
    <br/><br/><br/>

    <p> Next steps:</p>
    <a href="javascript:history.back()"> &raquo; Back to last page </a><br/>
    <a href="mailto:info@volunteer-vision.com">&raquo; Contact us</a><br/>
    <a href="mailto:simon.fakir@volunteer-vision.com">&raquo; Fire the IT guy</a><br/>

@endsection
