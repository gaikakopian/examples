@extends('layouts.errorlayout')

@section('title', 'Page Title')


@section('content')
    <p> We are sorry, something went wrong... <br/>
        Please read the following message and contact us if the problem was not
        solved after a second try.</p>
<br/>


    <pre>{{$exception->getMessage()}}</pre>

    <pre>{{get_class($exception)}}</pre>


@endsection


@section('nextsteps')
    <br/><br/><br/>

    <p> Next steps:</p>
    <a href="javascript:history.back()"> &raquo; Back to the last page </a><br/>
    <a href="mailto:info@volunteer-vision.com">&raquo; Contact us</a><br/>
    <a href="mailto:simon.fakir@volunteer-vision.com">&raquo; Fire the IT guy ;)</a><br/>

@endsection
