@extends('layouts.errorlayout')

@section('title', 'Page Title')


@section('content')
    <h1>Ohh no!</h1>

    <p> Something went wrong in the authentication process</p>

    <p><b>{{$message}}</b></p>





    <pre>{{$errors}}</pre>




@endsection


@section('nextsteps')
    <br/><br/><br/>

    <p> Next steps:</p>
    <a href="javascript:history.back()"> &raquo; Back to last page </a><br/>
    <a href="mailto:info@volunteer-vision.com">&raquo; Contact us</a><br/>
    <a href="mailto:simon.fakir@volunteer-vision.com">&raquo; Fire the IT guy of Volunteer Vision ;) </a><br/>

@endsection
