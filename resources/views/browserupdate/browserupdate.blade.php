@extends('layouts.bootstrap')

@section('title', 'Browser update')

@section('content')

    <div id="content">


        <div class="box" style="margin-top:100px">


            <div class="row">
                <div class="col-md-12 text-center">
                    <img src="/images/compatiblebrowser.jpg" width="60%">
                    <hr/>
                </div>

            </div>

            <div class="row">
                <div class="col-sm">

                    <p class=" text-center">

                        <span class="famfamfam-flags de"></span>
                        <b>Browser inkompatibel</b></p>



                    <p>Sehr geehrter Benutzer,</p>
                    <p>Dein Browser ist leider nicht mehr aktuell oder nicht kompatibel.</p>
                    <p>Wenn du ein geschäftliches Gerät nutzt, nimm Kontakt <a href="mailto:info@volunteer-vision.com">
                            zu uns auf</a>, um Chrome oder Firefox zu installieren.


                    </p>



                    <p>Wenn du ein privates Gerät nutzt, installiere die aktuelle Version von
                        <a href="https://www.google.com/chrome/" target="_blank" class="text-black"> Google Chrome</a>
                        oder
                        <a href="https://www.mozilla.org/de/firefox/new/" target="_blank" class="text-black"> Mozilla
                            Firefox</a>.

                    </p>


                    <p>Für Fragen kontaktieren Sie bitte

                        <a href="mailto:info@volunteer-vision.com">
                            info@volunteer-vision.com</a>.</p>


                </div>
                <div class="col-sm">
                    <p class=" text-center">

                    <span class="famfamfam-flags en"></span>
                    <b>Browser incompatible</b></p>

                    <p>Dear user,</p>
                    <p>Your browser is outdated or incompatible.</p>
                    <p> If you are using a corporate device, please <a href="mailto:info@volunteer-vision.com"> ask us for help</a> to install Google Chrome or Firefox.
                    </p>
                    <p>If you are using a private device, please install a new version of
                        <a href="https://www.google.com/chrome/" target="_blank" class="text-black"> Google Chrome</a>
                        or
                        <a href="https://www.mozilla.org/de/firefox/new/" target="_blank" class="text-black"> Mozilla
                            Firefox</a>.

                    </p>

                    <p>For further questions please contact:
                        <a href="mailto:info@volunteer-vision.com"> info@volunteer-vision.com</a>.</p>


                </div>
            </div>


            <br/><br/>
            <div class="row">
                <div class="col-md-6">

                    <p> Wenn Sie einen anderen Browser installiert haben, kopieren Sie bitte diese Adresse in den
                        anderen Browser:</p>
                </div>
                <div class="col-md-6">
                    <p> In case you have another browser installed, please copy the following address:</p>

                </div>
                <div class="col-md-12 text-center">

                    <input type="form-control" style="width:60%; margin:20px auto;" id="requesturl"
                           value="{{$frontendUrl}}"/>
                </div>
            </div>

            <div class="row text-center">
                <div class="alert alert-info ">
                    <span> Detected browser: <span id="browser">{{$browser}}</span></span>
                </div>

             </div>


        </div>

    </div>





@endsection