<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8"/>
    <style>

        @page {
            size: A4;
            margin: 0;
        }

        html {
            margin:0 auto;
            /*width: 210mm;*/
            /*height: 297mm;*/
        }

        #header {
            margin-bottom: 2cm;
        }

        .container {
            margin: 0 auto;
            width: 15cm;
        }

        .certificate {
            padding: 1.5cm;
            width: 14cm;
            background-color: #F7F7F7;
            background-image: url('http://api2.staging.volunteer-vision.com/images/vv_sign_large.png');
            {{--background-image: url('{{asset('images/vv_sign_large.png')}}');--}}
            background-size: contain;
            background-repeat: no-repeat;
            background-position: center center;

        }

        #footer p, .certificate p, .certificate li, .certificate h1, .certificate h2, .certificate h3 {
            font-size:10pt;
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif
        }

        .text-block {

        }

        .text-block p {
            text-align: justify;
        }

        .text-block h1, .text-block h2, .text-block h3 {
            margin: 0.1cm 0;

            text-align: center;
            font-weight: normal;

        }

        .text-block h1 {
            font-size: 22pt;
            margin-bottom:0.5cm;
        }

        .text-block h2 {
            font-size: 14pt;
        }

        .text-block h3 {
            font-size: 14pt;
            font-weight: bold;
            margin-bottom: 2cm;
        }

        #footer {
            /*position: fixed;*/
            /*bottom: 0;*/
            /*height: 2cm;*/
            margin-top: 2cm;
            width: 100%;
            text-align: center;
        }

        #footer p {
            text-align: center;
        }

        #signature {
            margin-top: 2cm;
            text-align: center;;
        }

        #signature img {
            width: 120px;
            position:relative;
            top:20px;
            margin: 0 auto;
        }

        .logo {
            width:100%;

            text-align: right;
        }

        .logo img {
            margin-top:2cm;
            width: 5cm;
        }
    </style>
</head>
<body>


<div id="header" class="container">
    <div class="logo">
        <img src="{{ $brand->logo }}">
    </div>

</div>
<div class="certificate container">


    <div class="text-block">
        <h1>{{$participation->enrollment->user->displayName}}</h1>

        {!! $text !!}



        @if ($enrollment->role === 'mentee')
            <p>
                {{ __('certificate.nameOfMentor') }}:
                {{ $match->getMentorParticipation()->enrollment->user->displayName }}

            </p>
            @if ($match->mentor_comment)
                <p class="mentorComment">{{$match->mentor_comment}}</p>
            @endif
        @else
            <p>
                {{ __('certificate.nameOfMentee') }}:
                {{ $match->getMenteeParticipation()->enrollment->user->displayName }}
            </p>

        @endif


    </div>


    <p id="signature">
        {{ __('certificate.munich') }}, {{ $participation->match->match_done_at->formatLocalized('%d %B %Y')  }}
        <br/><br/><br/><br/>

        {{--<img src="{{asset('images/signature_suska_short.png')}}" width="40"><br/>--}}
        <img src="http://api2.staging.volunteer-vision.com/images/signature_suska_short.png" ><br/>


        Dr. Suska Dreesbach-Bundy<br/>
        CEO Volunteer Vision

    </p>

</div>


<div id="footer" class="container">
    <p>

        @if ($enrollment->program->cooperating_organization)
            {{ __('certificate.inCooperationWith') }}  {{$enrollment->program->cooperating_organization}} |
        @endif

        www.volunteer-vision.com
    </p>
</div>
</body>
</html>
