@extends('layouts.singlepage')

@section('title', 'Appointment confirmed')

@section('content')


    <section class="section">

    <div class="box">
        <form action="{{ route('reporting.bvLogin') }}" method="GET">
            <div class="field">
                <label class="label is-medium">Login</label>

                <p class="control">

                    <input class="input" type="password" placeholder="Password" name="password">

                </p>

                @if ($warning)
                    <p class="help is-danger">This password is invalid</p>
                @endif



            </div>
            <div class="field">
                <p class="control">
                    <button class="button is-success">
                        Login
                    </button>
                </p>
            </div>


        </form>

    </div>
@endsection