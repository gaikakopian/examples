@extends('layouts.singlepage')

@section('title', 'Appointment confirmed')

@section('content')

    <section class="section">

        <div class="box">
            <h1>
                {{ __('responses.confirmed.title') }}
            </h1>

            <p>
                {{ __('responses.confirmed.text') }}

                <br/><br/><br/>
                @if (!empty($link))
                    <a class="btn btn-primary" href="{{$link}}">{{ __('responses.next') }} </a>
                @endif
            </p>
        </div>
    </section>
@endsection
