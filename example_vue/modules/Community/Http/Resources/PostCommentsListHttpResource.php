<?php

namespace Modules\Community\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Http\Resources\UserPublicHttpResource;

/**
 * Class PostCommentsListHttpResource
 *
 * @package Modules\Community\Http\Resources
 */
class PostCommentsListHttpResource extends HttpResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'username' => e($this->user()->first()->first_name) . ' ' . e($this->user()->first()->last_name),
            'body' => e($this->body),
            'parent_id' => $this->parent_id,
            'comments_count' => $this->comments()->count(),
            'user' => new UserPublicHttpResource($this->whenLoaded('user')),
            'created_at' => $this->created_at->toIso8601String(),
        ];
    }
}
