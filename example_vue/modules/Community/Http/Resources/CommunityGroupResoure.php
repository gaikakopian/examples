<?php

namespace Modules\Community\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Http\Resources\UserPublicHttpResource;

class CommunityGroupResoure extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => ($this->name ? $this->name : 'GroupName Placeholder'),
            'createdAt' => $this->created_at->toIso8601String(),
            'updatedAt' => $this->updated_at->toIso8601String(),
            'users' => UserPublicHttpResource::collection($this->whenLoaded('users')),
            // @todo: maybe registration codes;
            'organizationId' => $this->organization_id,
        ];
    }
}
