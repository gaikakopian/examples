<?php

namespace Modules\Community\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Community\Domain\Models\Post;
use Modules\Community\Domain\Services\PostService;
use Modules\Community\Http\Resources\PostsListHttpResource;

/**
 * Class ListPostsAction
 *
 * @package Modules\Community\Http\Actions
 */
class ListUserPostsAction extends Action
{
    /** @var ResourceResponder */
    private $resourceResponder;

    /**
     * @var PostService
     */
    private $postService;

    /**
     * ListPostCommentsAction constructor.
     *
     * @param ResourceResponder $resourceResponder
     * @param PostService $postService
     */
    public function __construct(ResourceResponder $resourceResponder, PostService $postService)
    {
        $this->resourceResponder = $resourceResponder;
        $this->postService = $postService;
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function __invoke(Request $request)
    {
        $userId = auth()->id();
        $limit = $request->get('limit');
        $posts = $this->postService->getUserAllPosts($userId, $limit);

        return $this->resourceResponder->send($posts, PostsListHttpResource::class);
    }
}
