<?php

namespace Modules\Community\Http\Actions;

use Illuminate\Http\Request;
use Modules\Community\Domain\Services\ChatroomService;
use Modules\Community\Http\Requests\AddChatRequest;
use Modules\Community\Http\Resources\ChatroomHttpResource;
use Modules\Core\Http\Resources\CommentHttpResource;
use Modules\Core\Http\Requests\CommentRequest;
use App\Infrastructure\Http\ResourceResponder;
use App\Infrastructure\Http\Action;
use Modules\Core\Domain\Models\Comment;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\Client\NotAuthorizedException;

class AddChatroomAction extends Action
{
    private $chatroomService;

    public function __construct(ResourceResponder $responder, ChatroomService $chatroomService)
    {
        $this->responder = $responder;
        $this->chatroomService = $chatroomService;
    }

    public function __invoke($id, AddChatRequest $request)
    {
        $myId = Auth::id();

        $users = [$id, $myId];

        $chatid = $request->input('external_room_id');


        $chatroom = $this->chatroomService->createChatRoomForUsers($users, $chatid);

        return $this->responder->send($chatroom, ChatroomHttpResource::class);
    }
}
