<?php

namespace Modules\Community\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Community\Http\Resources\GroupCommunityHttpResource;
use Modules\Core\Domain\Services\GroupService;
use Modules\Community\Http\Requests\ListMyGroupsRequest;

/**
 * Class ListMyGroupsAction
 *
 * @package Modules\Community\Http\Actions
 */
class ListMyGroupsAction extends Action
{
    /** @var GroupService */
    protected $groupService;

    /** @var ResourceResponder */
    protected $responder;

    /**
     * ListMyGroupsAction constructor.
     *
     * @param GroupService $groupService
     * @param ResourceResponder $responder
     */
    public function __construct(GroupService $groupService, ResourceResponder $responder)
    {
        $this->groupService = $groupService;
        $this->responder = $responder;
    }

    /**
     * @param ListMyGroupsRequest $request
     *
     * @return mixed
     */
    public function __invoke(ListMyGroupsRequest $request)
    {
        $page = $request->get('page', 1);
        $perPage = $request->get('per_page', 5);

        $groups = $this->groupService->listUserPublicGroups(Auth::id(), $page - 1, $perPage, ['users']);

        return $this->responder->send($groups, GroupCommunityHttpResource::class);
    }
}
