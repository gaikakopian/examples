<?php

/*
|---------- ----------------------------------------------------------------
| Module routes
|--------------------------------------------------------------------------
|
| Here's the place to register API routes for the current module. These routes
| are loaded by the RouteServiceProvider within a group which is assigned
| the "api" middleware group and a version prefix, eg. `/api/v1`.
|
 */

use App\Http\Middleware\MapCommentable;

/**
 * Private Scope
 */
Route::group(['middleware' => ['auth', 'verify-community-enabled']], function () {
    Route::group(['prefix' => '/community'], function () {
        Route::get('/groups/{id}', 'Actions\ShowPublicGroupAction');
        Route::get('/users/{id}', 'Actions\ShowPublicProfileAction');
        Route::post('/users/{id}/chatroom', 'Actions\AddChatroomAction');
        Route::get('/users/{id}/chatroom', 'Actions\ShowChatroomAction');

        Route::post('/users/me/joincommunity', 'Actions\JoinCommunityAction');

        Route::group(['prefix' => '/users/me/groups'], function () {
            Route::post('join/{id}', 'Actions\JoinGroupAction');
            Route::post('/{id}', 'Actions\LeaveGroupAction');
            Route::get('/', 'Actions\ListMyGroupsAction');
        });

        Route::group(['prefix' => '/users/me/posts'], function () {
            Route::post('', 'Actions\CreatePostAction');
            Route::get('', 'Actions\ListPostsAction');
            Route::get('all', 'Actions\ListUserPostsAction');
            Route::get('{id}', 'Actions\GetPostAction');
            Route::put('/{id}', 'Actions\UpdatePostAction')->where('id', '[0-9]+');
            Route::delete('/{id}', 'Actions\DeletePostAction')->where('id', '[0-9]+');

            Route::group(['prefix' => '/{id}/likes'], function () {
                Route::post('', 'Actions\LikePostAction');
                Route::delete('', 'Actions\DislikePostAction');
            });

            Route::group(['prefix' => '/{id}/comments'], function () {
                Route::post('', 'Actions\CreatePostCommentAction');
                Route::get('', 'Actions\ListPostCommentsAction');
                Route::put('/{commentId}', 'Actions\UpdatePostCommentAction')->where('id', '[0-9]+');
                Route::delete('/{commentId}', 'Actions\DeletePostCommentAction')->where('id', '[0-9]+');
            });
        });
    });
});
