<?php

namespace Modules\Community\Domain\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Domain\Models\User;

/**
 * Class PostComment
 *
 * @package Modules\Community\Domain\Models
 */
class PostComment extends Model
{
    /** @var string */
    protected $table = 'post_comments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_id',
        'user_id',
        'body',
        'parent_id',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(PostComment::class, 'parent_id', 'id');
    }
}
