<?php

namespace Modules\Community\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Community\Domain\Models\Post;
use Modules\Core\Domain\Models\Group;

/**
 * Class ListPostsTest
 *
 * @package Modules\Community\Tests\Http
 */
class ListUserPostsTest extends EndpointTest
{
    public function test_it_returns_error_when_group_does_not_exist()
    {
        $this->user->community_enabled = true;
        $response = $this->actingAs($this->user)->getJson('/api/v2/community/users/me/posts?group_id[]=0');

        self::assertEquals(404, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'message' => 'Group with id 0 could not be found',
        ]);
    }

    public function test_it_returns_all_posts()
    {
        $this->user->community_enabled = true;

        $group = factory(Group::class)->create([
            'organization_id' => 1,
            'privacy' => Group::PRIVACY_PUBLIC,
        ]);

        factory(Post::class)->create([
            'user_id' => $this->user->id,
            'group_id' => $group->id,
            'body' => 'Post #1',
        ]);

        $response = $this
            ->actingAs($this->user)
            ->getJson('/api/v2/community/users/me/posts/all');

        self::assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJsonStructure([
            'data' => [
                [
                    [
                    'id',
                    'user_id',
                    'username',
                    'body',
                    'comments_count',
                    'likes_count',
                    'created_at',
                    ]
                ]
            ],
        ]);
    }
}
