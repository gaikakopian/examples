<?php

namespace Modules\Community\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Group;

/**
 * Class JoinGroupActionTest
 *
 * @package Modules\Community\Tests\Http
 */
class JoinGroupTest extends EndpointTest
{
    public function test_it_returns_error_when_group_does_not_exist()
    {
        $this->user->community_enabled = true;
        $response = $this->actingAs($this->user)->postJson('/api/v2/community/users/me/groups/join/0');

        self::assertEquals(404, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'message' => 'Group with id 0 could not be found',
        ]);
    }

    public function test_it_returns_error_when_group_is_not_public()
    {
        $this->user->community_enabled = true;
        $organization = factory(\Modules\Core\Domain\Models\Organization::class)->create([
            'name' => 'Test Volunteer Vision',
            'type' => 'company',
        ]);

        $group = factory(Group::class)->create([
            'name' => 'Private group',
            'organization_id' => $organization->id,
            'privacy' => Group::PRIVACY_PRIVATE
        ]);

        $response = $this->actingAs($this->user)->postJson('/api/v2/community/users/me/groups/join/' . $group->id);

        $response->assertJson([
            'message' => 'User can join only public groups',
        ]);
    }

    public function test_it_does_not_attach_already_attached_user_to_group()
    {
        $this->user->community_enabled = true;
        $organization = factory(\Modules\Core\Domain\Models\Organization::class)->create([
            'name' => 'Test Volunteer Vision',
            'type' => 'company',
        ]);

        $group = factory(Group::class)->create([
            'name' => 'Test Group',
            'organization_id' => $organization->id,
            'privacy' => Group::PRIVACY_PUBLIC
        ]);

        $group->users()->attach($this->user->id);

        $response = $this->actingAs($this->user)->postJson('/api/v2/community/users/me/groups/join/' . $group->id);

        self::assertEquals(201, $response->getStatusCode(), $response->getContent());
        self::assertEquals(1, $group->users()->where('id', $this->user->id)->count());
    }

    public function test_it_attaches_user_to_group()
    {
        $this->user->community_enabled = true;
        $organization = factory(\Modules\Core\Domain\Models\Organization::class)->create([
            'name' => 'Test Volunteer Vision',
            'type' => 'company',
        ]);

        $group = factory(Group::class)->create([
            'name' => 'Test group',
            'organization_id' => $organization->id,
            'privacy' => Group::PRIVACY_PUBLIC
        ]);

        $response = $this->actingAs($this->user)->postJson('/api/v2/community/users/me/groups/join/' . $group->id);

        self::assertEquals(201, $response->getStatusCode(), $response->getContent());
        self::assertNotNull($group->users()->find($this->user->id));
    }
}
