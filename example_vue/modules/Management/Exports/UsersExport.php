<?php

namespace Modules\Management\Exports;


use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromArray, WithHeadings
{
    public function getQuery()
    {
        return 'SELECT
	enrollments.ROLE,
	u.ID AS user_id,
	u.first_name,
	u.last_name,
	o.NAME,
	u.email,
	concat ( u.phone_number_prefix, u.phone_number ) AS phone_number,
	u.STATE AS user_state,
	P.title AS program_name,
	enrollments.ID AS enrollment_id,
	participations.ID AS participation_id,
	enrollments.STATE AS enrollment_state,
	enrollments.last_state_change_at AS enrollment_state_change,
	( date_part( \'epoch\' :: TEXT, age( enrollments.last_state_change_at :: TIMESTAMP WITH TIME ZONE, now())) / 86400 :: DOUBLE PRECISION ) :: INTEGER AS days_since_enrollment_state,
	matches.ID AS match_id,
	matches.STATE AS match_state,
	matches.last_state_change_at AS match_state_change,
	( date_part( \'epoch\' :: TEXT, age( matches.last_state_change_at :: TIMESTAMP WITH TIME ZONE, now())) / 86400 :: DOUBLE PRECISION ) :: INTEGER AS days_since_match_state,
	matches.licence_activated,
	matches.lections_completed 
FROM
	users u
	LEFT JOIN enrollments ON u.ID = enrollments.user_id
	LEFT JOIN participations ON enrollments.ID = participations.enrollment_id
	LEFT JOIN matches ON participations.match_id = matches.
	ID JOIN organizations o ON u.organization_id = o.
	ID JOIN programs P ON enrollments.program_id = P.ID 
WHERE
	u.count_for_reporting = TRUE 
	AND participations.deleted_at IS NULL   
	AND u.deleted_at IS NULL 
	AND enrollments.deleted_at IS NULL;';
    }

    public function array(): array
    {
        return DB::select(DB::raw($this->getQuery()));
    }

    public function headings(): array
    {
        return [
            'role',
            'user_id at',
            'first_name',
            'last_name',
            'name',
            'email',
            'phone_number',
            'user_state',
            'program_name',
            'enrollment_id',
            'participation_id',
            'enrollment_state',
            'enrollment_state_change',
            'days_since_enrollment_state',
            'match_id',
            'match_state',
            'match_state_change',
            'days_since_match_state',
            'license_activated',
            'lections_completed',
        ];
    }
}
