<?php

namespace Modules\Management\Tests\Http;


use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\File;
use Modules\Core\Domain\Models\Role;

class GetUsersWithProgramsLiistTest extends EndpointTest
{
    public function test_it_lists_users_with_programs()
    {
        $managerRole = Role::whereName(Role::ROLES['manager'])->first();
        $this->user->roles()->attach($managerRole);



        factory(File::class)->create(['category' => 'video']);
        $response = $this->actingAs($this->user)->getJson(route('management.getUsersWithPrograms'));
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' =>
                [
                    [
                        'id',
                        'displayName',
                        'firstName',
                        'lastName',
                        'email',
                        'gender',
                        'primaryRole',
                        'phoneNumber',
                        'phoneNumberPrefix',
                        'role',
                        'licence_activated',
                        'lections_completed',
                        'whatsappSetup',
                        'whatsappNumber',
                        'whatsappNumberPrefix',
                        'address',
                        'state',
                        'city',
                        'postcode',
                        'country',
                        'countryOfOrigin',
                        'birthday',
                        'brand',
                        'language',
                        'acceptSms',
                        'acceptEmail',
                        'acceptPush',
                        'preferredChannel',
                        'countForReporting',
                        'avatar',
                        'points',
                        'position',
                        'aboutMe',
                        'askForTosAcceptance',
                        'pointsNextLevel',
                        'level',
                        'emailOptinAt',
                        'createdAt',
                    ]
                ]
        ]);
    }
}
