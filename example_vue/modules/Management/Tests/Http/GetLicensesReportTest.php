<?php

namespace Modules\Management\Tests\Http;


use App\Infrastructure\AbstractTests\EndpointTest;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Core\Domain\Models\Role;
use Modules\Management\Exports\UsersExport;

class GetLicensesReportTest extends EndpointTest
{
    public function test_it_get_reports_for_licenses()
    {
        $managerRole = Role::whereName(Role::ROLES['manager'])->first();
        $this->user->roles()->attach($managerRole);



        $response = $this->actingAs($this->user)->getJson(route('management.getLicensesReport'));
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'all',
                'activated',
                'open',
                'waiting'
            ]
        ]);
    }
}
