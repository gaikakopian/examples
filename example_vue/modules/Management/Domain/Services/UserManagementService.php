<?php

namespace Modules\Management\Domain\Services;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

class UserManagementService
{
    /**
     * @var Model
     */
    protected $model;

    protected $allowedTables = ['users', 'appointments', 'matches'];

    /**
     * UserService constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function getUsersQuery($organizationId)
    {
        return $this->model
            ->where('users.organization_id', '=', $organizationId)
            ->where('users.state', User::STATES['REGISTERED'])
            ->where('users.count_for_reporting', true);
    }

    /**
     * @param int $organizationId
     * @return mixed
     */
    public function getUsers(int $organizationId)
    {
        return $this->getUsersQuery($organizationId)->get();
    }

    /**
     * @param int $organizationId
     * @param int $limit
     * @return mixed
     */
    public function getLatestUsers(int $organizationId, int $limit)
    {
        return $this->getUsersQuery($organizationId)
            ->orderBy('users.created_at', 'desc')
            ->take($limit)
            ->get();
    }

    /**
     * @param int $organizationId
     * @return mixed
     */
    public function getUsersCount(int $organizationId)
    {
        return $this->getUsersQuery($organizationId)->count();
    }

    /**
     * @param int $organizationId
     * @return mixed
     */
    public function getUsersCountriesCount(int $organizationId)
    {
        return $this->getUsers($organizationId)->groupBy('country')->count();
    }

    /**
     * @param string $table
     * @return string
     * @throws \Exception
     */
    public function getYearExtractQueryString(string $table = 'users')
    {
        if (!in_array($table, $this->allowedTables)) {
            throw new \Exception('Not valid table');
        }

        return 'EXTRACT ( YEAR FROM ' . $table . '.created_at )';
    }

    /**
     * @param string $table
     * @return string
     * @throws \Exception
     */
    public function getQuarterExtractQueryString(string $table = 'users')
    {
        if (!in_array($table, $this->allowedTables)) {
            throw new \Exception('Not valid table');
        }

        return 'EXTRACT ( QUARTER FROM ' . $table . '.created_at )';
    }

    /**
     * @param int $organizationId
     * @return Collection|\Illuminate\Support\Collection
     * @throws \Exception
     */
    public function getUsersProgress(int $organizationId)
    {
        $users = $this->getUsersQuery($organizationId)
            ->orderBy(DB::raw($this->getYearExtractQueryString()), 'DESC')
            ->orderBy(DB::raw($this->getQuarterExtractQueryString()), 'DESC')
            ->get();

        return $this->getCountAndPercentageProgressByQuarter($users, 'users');
    }

    /**
     * @param int $organizationId
     * @return Collection|\Illuminate\Support\Collection
     * @throws \Exception
     */
    public function getAppointmentsProgress(int $organizationId)
    {
        $appointments = Appointment::query()
            ->join('matches', 'appointments.match_id', '=', 'matches.id')
            ->join('participations', 'participations.match_id', '=', 'matches.id')
            ->join('enrollments', 'enrollments.id', '=', 'participations.enrollment_id')
            ->join('users', 'users.id', '=', 'enrollments.user_id')
            ->where('users.organization_id', $organizationId)
            ->where('appointments.state', Appointment::STATES['SUCCESS'])
            ->select('appointments.*')
            ->orderBy(DB::raw($this->getYearExtractQueryString('appointments')), 'DESC')
            ->orderBy(DB::raw($this->getQuarterExtractQueryString('appointments')), 'DESC')
            ->get();

        return $this->getCountAndPercentageProgressByQuarter($appointments, 'appointments');
    }

    /**
     * @param int $organizationId
     * @return Collection|\Illuminate\Support\Collection
     * @throws \Exception
     */
    public function getActivatedLicensesProgress(int $organizationId)
    {
        $appointments = Match::query()
            ->join('participations', 'participations.match_id', '=', 'matches.id')
            ->join('enrollments', 'enrollments.id', '=', 'participations.enrollment_id')
            ->join('users', 'users.id', '=', 'enrollments.user_id')
            ->where('users.organization_id', $organizationId)
            ->whereNotNull('matches.state')
            ->select('matches.*')
            ->orderBy(DB::raw($this->getYearExtractQueryString('matches')), 'DESC')
            ->orderBy(DB::raw($this->getQuarterExtractQueryString('matches')), 'DESC')
            ->get();

        return $this->getCountAndPercentageProgressByQuarter($appointments, 'activated_licenses');
    }

    /**
     * @param int $organizationId
     * @return Collection|\Illuminate\Support\Collection
     * @throws \Exception
     */
    public function getProgress(int $organizationId)
    {
        $usersProgress = $this->getUsersProgress($organizationId);
        $appointmentsProgress = $this->getAppointmentsProgress($organizationId);
        $activatedLicenses = $this->getActivatedLicensesProgress($organizationId);

        return $usersProgress
            ->map(function ($item, $index) use ($appointmentsProgress) {
                $appointment = $appointmentsProgress->get($index);

                return array_merge($item, [
                    'appointments_count_progress' => !is_null($appointment) ? $appointment['appointments_count_progress'] : null,
                    'appointments_percent_progress' => !is_null($appointment) ? $appointment['appointments_percent_progress'] : null
                ]);
            })
            ->map(function ($item, $index) use ($activatedLicenses) {
                $license = $activatedLicenses->get($index);

                return array_merge($item, [
                    'activated_licenses_count_progress' => !is_null($license) ? $license['activated_licenses_count_progress'] : null,
                    'activated_licenses_percent_progress' => !is_null($license) ? $license['activated_licenses_percent_progress'] : null
                ]);
            });
    }

    /**
     * @param Collection $collection
     * @param $column
     * @return Collection|\Illuminate\Support\Collection
     */
    public function getCountAndPercentageProgressByQuarter(Collection $collection, $column)
    {
        $progress = $collection->groupBy(function ($item) {
            return $item->created_at->quarter . $item->created_at->year;
        })->take(3);

        $cursor = $progress->getIterator();
        $progress = $progress->map(function ($group) use ($cursor, $column) {
            $count = $group->count();
            $next = next($cursor);
            $percent = $next ? ($count - $next->count()) * 100 / $count : null;

            return [
                'quarter' => 'Q' . $group[0]->created_at->quarter . ' / ' . $group[0]->created_at->year,
                $column . '_count_progress' => $count,
                $column . '_percent_progress' => $percent
            ];
        });

        return $progress;
    }

    /**
     * @param int $organizationId
     * @param string $searchText
     * @return \Illuminate\Database\Eloquent\Builder[]|Collection|Model[]
     */
    public function getUsersWithEnrollmentsAndMatches(int $organizationId, string $searchText = null)
    {
        $query = $this->getUsersQuery($organizationId)
            ->select('users.*', 'matches.lections_completed', 'matches.licence_activated', 'enrollments.role', 'enrollments.state')
            ->join('enrollments', 'enrollments.user_id', '=', 'users.id')
            ->join('participations', 'participations.enrollment_id', '=', 'enrollments.id')
            ->rightJoin('matches', 'participations.match_id', '=', 'matches.id');

        if ($searchText) {
            $query->where(function ($q) use ($searchText) {
                return $q->where('users.first_name', 'like', '%' . $searchText . '%')
                    ->orWhere('users.last_name', 'like', '%' . $searchText . '%')
                    ->orWhere('users.address', 'like', '%' . $searchText . '%')
                    ->orWhere(DB::raw("CONCAT(users.first_name, ' ', users.last_name)"), 'like', '%' . $searchText . '%');
            });
        }

        $users = $query->get();

        return $users;
    }

}
