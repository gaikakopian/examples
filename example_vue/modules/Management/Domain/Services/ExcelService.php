<?php

namespace Modules\Management\Domain\Services;


use Maatwebsite\Excel\Facades\Excel;
use Modules\Management\Exports\UsersExport;

class ExcelService
{
    /**
     * @var string
     */
    protected $usersFilePath = 'users.xlsx';

    public function exportUsers()
    {
        return Excel::download(new UsersExport, $this->usersFilePath);
    }
}
