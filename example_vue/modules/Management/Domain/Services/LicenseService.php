<?php

namespace Modules\Management\Domain\Services;


use Illuminate\Database\Eloquent\Model;
use Modules\Matching\Domain\Models\Match;

class LicenseService
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * LicenseService constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param int $organizationId
     * @return mixed
     */
    public function getLicensesQuery(int $organizationId)
    {
        return $this->model
            ->join('participations', 'matches.id', '=', 'participations.match_id')
            ->join('enrollments', 'participations.enrollment_id', '=', 'enrollments.id')
            ->join('users', 'enrollments.user_id', '=', 'users.id')
            ->where('users.organization_id', '=', $organizationId);
    }

    /**
     * @param int $organizationId
     * @return mixed
     */
    public function getAllLicensesCount(int $organizationId)
    {
        return $this->getLicensesQuery($organizationId)
            ->selectRaw('COUNT(DISTINCT(matches.id)) as data')
            ->get()[0]['data'];
    }

    /**
     * @param int $organizationId
     * @return mixed
     */
    public function getActivatedLicensesCount(int $organizationId)
    {
        return $this->getLicensesQuery($organizationId)
            ->whereNotNull('matches.licence_activated')
            ->selectRaw('COUNT(DISTINCT(matches.id)) as data')
            ->get()[0]['data'];
    }

    /**
     * @param int $organizationId
     * @return mixed
     */
    public function getOpenLicensesCount(int $organizationId)
    {
        return $this->getLicensesQuery($organizationId)
            ->whereNull('matches.licence_activated')
            ->where('matches.state', Match::STATES['UNCONFIRMED'])
            ->selectRaw('COUNT(DISTINCT(matches.id)) as data')
            ->get()[0]['data'];
    }

}
