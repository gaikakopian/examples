<?php

namespace Modules\Management\Http\Actions;


use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Modules\Core\Http\Resources\UserHttpResource;
use Modules\Management\Domain\Services\UserManagementService;
use Modules\Management\Http\Resources\UserManagementMinimalResource;

class GetNewestUsersAction extends Action
{
    /**
     * @var UserManagementService
     */
    protected $userService;

    /**
     * @var ResourceResponder
     */
    private $resourceResponder;

    /**
     * ManagementDashboardAction constructor.
     * @param UserManagementService $userService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(UserManagementService $userService, ResourceResponder $resourceResponder)
    {
        $this->userService = $userService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function __invoke(Request $request)
    {
        $organizationId = auth()->user()->organization_id;
        $limit = $request->get('limit', 15);

        $newestUsers = $this->userService->getLatestUsers($organizationId, $limit);

        return $this->resourceResponder->send($newestUsers, UserManagementMinimalResource::class);
    }
}
