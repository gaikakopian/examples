<?php

namespace Modules\Management\Http\Actions;


use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Services\RegistrationCodeService;
use Modules\Core\Http\Resources\UserHttpResource;
use Modules\Management\Domain\Services\UserManagementService;
use Modules\Management\Http\Resources\RegistrationCodeManagementHttpResource;

class GetRegistrationLinksAction extends Action
{
    /**
     * @var RegistrationCodeService
     */
    protected $registrationCodeService;

    /**
     * @var ResourceResponder
     */
    private $resourceResponder;

    /**
     * ManagementDashboardAction constructor.
     * @param UserManagementService $registrationCodeService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(RegistrationCodeService $registrationCodeService, ResourceResponder $resourceResponder)
    {
        $this->registrationCodeService = $registrationCodeService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        $user = Auth::user();

        $organization = $user->organization;
//        $organization = Organization::find($organizationId);
        $codes = $this->registrationCodeService->findCodesForOrganization($organization);


        return $this->resourceResponder->send($codes, RegistrationCodeManagementHttpResource::class);
    }
}
