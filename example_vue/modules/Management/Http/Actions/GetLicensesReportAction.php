<?php

namespace Modules\Management\Http\Actions;


use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\PlainHttpResource;
use App\Infrastructure\Http\ResourceResponder;
use Modules\Management\Domain\Services\LicenseService;

class GetLicensesReportAction extends Action
{
    /**
     * @var GetLicensesReportAction
     */
    protected $licenseService;

    /**
     * @var ResourceResponder
     */
    private $resourceResponder;

    /**
     * GetLicensesListAction constructor.
     * @param LicenseService $licenseService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(LicenseService $licenseService, ResourceResponder $resourceResponder)
    {
        $this->licenseService = $licenseService;

        $this->resourceResponder = $resourceResponder;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        $organizationId = auth()->user()->organization_id;

        $allLicenses = $this->licenseService->getAllLicensesCount($organizationId);
        $activatedLicenses = $this->licenseService->getActivatedLicensesCount($organizationId);
        $openLicenses = $this->licenseService->getOpenLicensesCount($organizationId);
        $waitingLicenses = $allLicenses - ($activatedLicenses + $openLicenses);

        $data = [
            'all' => $allLicenses,
            'activated' => $activatedLicenses,
            'open' => $openLicenses,
            'waiting' => $waitingLicenses
        ];

        return $this->resourceResponder->send($data, PlainHttpResource::class);
    }
}
