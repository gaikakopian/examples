<?php

namespace Modules\Management\Http\Actions;


use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\PlainHttpResource;
use App\Infrastructure\Http\ResourceResponder;
use Modules\Admin\Http\Resources\UserFeedbackHttpResource;
use Modules\Core\Http\Resources\UserHttpResource;
use Modules\Management\Domain\Services\MentorShipService;
use Modules\Management\Domain\Services\UserFeedbackService;
use Modules\Management\Domain\Services\UserManagementService;

class ManagementDashboardAction extends Action
{
    /**
     * @var UserManagementService
     */
    protected $userService;

    /**
     * @var UserFeedbackService
     */
    protected $userFeedbackService;

    /**
     * @var MentorShipService
     */
    protected $mentorShipService;

    /**
     * @var ResourceResponder
     */
    private $resourceResponder;

    /**
     * ManagementDashboardAction constructor.
     * @param UserManagementService $userService
     * @param UserFeedbackService $userFeedbackService
     * @param MentorShipService $mentorShipService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(
        UserManagementService $userService,
        UserFeedbackService $userFeedbackService,
        MentorShipService $mentorShipService,
        ResourceResponder $resourceResponder
    )
    {
        $this->userService = $userService;
        $this->userFeedbackService = $userFeedbackService;
        $this->mentorShipService = $mentorShipService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        $organizationId = auth()->user()->organization_id;

        $engagedUsersCount = $this->userService->getUsersCount($organizationId);
        $countriesCount = $this->userService->getUsersCountriesCount($organizationId);

        $happyFeedbacksPercentage = $this->userFeedbackService->getHappinessFeedbacksPercent($organizationId);

        $mentoringHours = $this->mentorShipService->getMentoringHours($organizationId);

        $data = [
            'engagedUsersCount' => $engagedUsersCount,
            'countriesCount' => $countriesCount,
            'happyFeedbacksPercentage' => $happyFeedbacksPercentage,
            'mentoringHours' => $mentoringHours,
        ];

        return $this->resourceResponder->send($data, PlainHttpResource::class);
    }
}
