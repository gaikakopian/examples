<?php

namespace Modules\Management\Http\Actions;


use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Modules\Core\Http\Resources\UserHttpResource;
use Modules\Management\Domain\Services\UserManagementService;
use Modules\Management\Http\Resources\UserManagementHttpResource;

class GetRegisteredUsersAction extends Action
{
    /**
     * @var UserManagementService
     */
    protected $userService;

    /**
     * @var ResourceResponder
     */
    private $resourceResponder;

    /**
     * ManagementDashboardAction constructor.
     * @param UserManagementService $userService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(UserManagementService $userService, ResourceResponder $resourceResponder)
    {
        $this->userService = $userService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        $organizationId = auth()->user()->organization_id;
        $newestUsers = $this->userService->getUsers($organizationId);

        return $this->resourceResponder->send($newestUsers, UserManagementHttpResource::class);
    }
}
