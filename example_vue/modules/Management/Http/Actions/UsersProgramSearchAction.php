<?php

namespace Modules\Management\Http\Actions;


use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Modules\Management\Domain\Services\UserManagementService;
use Modules\Management\Http\Resources\UserManagementHttpResource;

class UsersProgramSearchAction extends Action
{
    /**
     * @var UserManagementService
     */
    protected $userService;

    /**
     * @var ResourceResponder
     */
    private $resourceResponder;

    /**
     * UsersSearchAction constructor.
     * @param UserManagementService $userService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(UserManagementService $userService, ResourceResponder $resourceResponder)
    {
        $this->userService = $userService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function __invoke(Request $request)
    {
        $organizationId = auth()->user()->organization_id;
        $users = $this->userService->getUsersWithEnrollmentsAndMatches($organizationId, $request->get('q'));

        return $this->resourceResponder->send($users, UserManagementHttpResource::class);
    }
}
