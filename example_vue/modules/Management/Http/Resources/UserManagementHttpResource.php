<?php

namespace Modules\Management\Http\Resources;


use App\Infrastructure\Http\HttpResource;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ActivityService;

class UserManagementHttpResource extends HttpResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'displayName' => $this->first_name . ' ' . $this->last_name,
            'firstName' => $this->first_name,
            'lastName' => $this->last_name,
            'email' => $this->email,
            'gender' => $this->gender,
            'primaryRole' => $this->primary_role,
            'phoneNumber' => $this->phone_number,
            'phoneNumberPrefix' => $this->phone_number_prefix,
            'role' => $this->role,
            'licence_activated' => !is_null($this->licence_activated),
            'lections_completed' => $this->lections_completed,

            'whatsappSetup' => $this->whatsapp_setup,
            'whatsappNumber' => $this->whatsapp_number,
            'whatsappNumberPrefix' => $this->whatsapp_number_prefix,
            'address' => $this->address,
            'state' => $this->state,
            'city' => $this->city,
            'postcode' => $this->postcode,
            'country' => $this->country,
            'countryOfOrigin' => $this->country_of_origin,
            'birthday' => $this->birthday,
            'brand' => $this->brand,
            'language' => $this->language,
            'acceptSms' => $this->accept_sms,
            'acceptEmail' => $this->accept_email,
            'acceptPush' => $this->accept_push,
            'preferredChannel' => $this->preferred_channel,
            'countForReporting' => $this->count_for_reporting,
            'avatar' => $this->avatar,
            'points' => $this->points,
            'position' => $this->position,
            'aboutMe' => $this->about_me,
            'askForTosAcceptance' => $this->tos_accepted_at === null,
            'pointsNextLevel' => ActivityService::getPointsForNextLevel($this->points),
            'level' => (isset(User::GAMIFICATION_LEVELS[$this->level]) ? User::GAMIFICATION_LEVELS[$this->level] : 'DEFAULT'),
            'emailOptinAt' => $this->email_optin_at ? $this->email_optin_at->toIso8601String() : $this->email_optin_at,
            'createdAt' => $this->created_at ? $this->created_at->toIso8601String() : $this->created_at,
        ];
    }
}
