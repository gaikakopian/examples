<?php

namespace Modules\Management\Http\Resources;


use App\Infrastructure\Http\HttpResource;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ActivityService;

class FeedbackManagementHttpResource extends HttpResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'questionCode' => $this->question_code,
            'responseScalar' => $this->response_scalar,
            'responseText' => $this->response_text,
            'createdAt' => $this->created_at->toIso8601String(),
            'user' => new UserManagementMinimalResource($this->whenLoaded('user'))
        ];
    }
}
