<?php

namespace Modules\Core\Reminder;

use App\Exceptions\Client\ClientException;


use App\Notifications\Match\MatchNotConfirmedReminder2Notification;
use App\Notifications\Match\MatchNotConfirmedReminder3Notification;
use App\Notifications\Match\MatchNotConfirmedReminderNotification;
use App\Reminders\Handler\AbstractReminderHandler;
use http\Exception\InvalidArgumentException;
use Illuminate\Support\Carbon;
use Kreait\Firebase\Exception\Messaging\InvalidArgument;
use Modules\Admin\Domain\Services\CoordinatorTodoService;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

class MatchNotConfirmedReminderHandler extends AbstractReminderHandler
{


    protected $coordinatorTodoService;

    public function __construct(CoordinatorTodoService $coordinatorTodoService)
    {
        parent::__construct();
        $this->coordinatorTodoService = $coordinatorTodoService;

        $this->setDateCallback(function ($match) {
            return $match->last_state_change_at;
        });

        $this->registerEvents();
    }

    public function registerEvents()
    {
        $this->registerEvent(24 * 3 , [$this, 'firstReminder']);
        $this->registerEvent(24 * 6, [$this, 'secondReminder']);
        $this->registerEvent(24 * 9, [$this, 'thirdReminder']);
        $this->registerEvent(24 * 12, [$this, 'rejectMatch']);

    }

    /**
     * @param Match $match
     */
    public function firstReminder($match)
    {
        $this->sendReminder($match, 'first');
    }

    public function secondReminder($match)
    {
        $this->sendReminder($match, 'second');
    }

    public function thirdReminder($match)
    {
        $this->sendReminder($match, 'third');
    }

    public function rejectMatch(Match $match)
    {

        $enrollment = $match->getMentorParticipation()->enrollment;

        // no need to create a todo, because we create anyway one on rejection
//        $data = [
//            'meta' => ['enrollment_id' => $enrollment->id],
//            'type' => CoordinatorTodo::TYPES['MATCH_AUTO_REJECTED'],
//            'customer_id' => $enrollment->user->id,
//            'duedate' => Carbon::now(),
//        ];
//        $todo = new CoordinatorTodo($data);
//        $this->coordinatorTodoService->createTodoIfNotExistsWithinDays($todo, 7);

        $match->user_comment = '[autoreject] because the mentor did not confirm it within a long time';
        $match->save();

        try {
            $match->transition('autoreject');
        } catch (ClientException $e) {
            report($e);
        }


    }

    private function sendReminder(Match $match, $type)
    {
        /**
         * @var Enrollment $enrollment
         * @var User $user
         */
        $enrollment = $match->getMentorParticipation()->enrollment;
        $user = $enrollment->user;

        if (!$user) {
            throw new InvalidArgument("Mentor of match could not be found!");
        }
        // tdb: needed?
        $user->last_reminder = Carbon::now();
        $user->save();

        switch ($type) {
            case 'first':
                $notification = new MatchNotConfirmedReminderNotification($user->brand, $match, $enrollment);
                break;
            case 'second':
                $notification = new MatchNotConfirmedReminder2Notification($user->brand, $match, $enrollment);
                break;

            case 'third';
                $notification = new MatchNotConfirmedReminder3Notification($user->brand, $match, $enrollment);
                break;
            default:
                throw new InvalidArgumentException("type is invalid" . $type);
        }

        $user->notify($notification);

    }
}