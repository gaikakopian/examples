<?php

namespace Modules\Core\Reminder;

use App\Notifications\Enrollment\EnrollmentIncompleteReminder2Notification;
use App\Notifications\Enrollment\EnrollmentIncompleteReminder3Notification;
use App\Notifications\Enrollment\EnrollmentIncompleteReminderNotification;

use App\Notifications\Webinar\WebinarChoiceReminder2Notification;
use App\Notifications\Webinar\WebinarChoiceReminder3Notification;
use App\Notifications\Webinar\WebinarChoiceReminderNotification;
use App\Reminders\Handler\AbstractReminderHandler;
use Illuminate\Support\Carbon;
use Modules\Admin\Domain\Models\CoordinatorTodo;
use Modules\Admin\Domain\Services\CoordinatorTodoService;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\WebinarService;
use Modules\Matching\Domain\Models\Match;

class WebinarReminderHandler extends AbstractReminderHandler
{


    protected $coordinatorTodoService;


    public function __construct(CoordinatorTodoService $coordinatorTodoService)
    {
        parent::__construct();
        $this->coordinatorTodoService = $coordinatorTodoService;


        $this->setDateCallback(function ($enrollment) {
            return $enrollment->last_state_change_at;
        });

        $this->registerEvents();
    }

    public function registerEvents()
    {
        $this->registerEvent(24 * 14, [$this, 'firstReminder']);
        $this->registerEvent(24 * 30, [$this, 'secondReminder']);
        $this->registerEvent(24 * 60, [$this, 'thirdReminder']);
        $this->registerEvent(24 * 67, [$this, 'coordinatorAction']);

    }

    /**
     * @param Match $match
     */
    public function firstReminder($enrollment)
    {
        $this->sendReminder($enrollment, WebinarChoiceReminderNotification::class);
    }

    public function secondReminder($enrollment)
    {
        $this->sendReminder($enrollment, WebinarChoiceReminder2Notification::class);
    }

    public function thirdReminder($enrollment)
    {
        $this->sendReminder($enrollment, WebinarChoiceReminder3Notification::class);

    }

    public function noReply($enrollment)
    {
        /** @var Enrollment $enrollment */
        $enrollment->transition('noreply');
    }

    public function coordinatorAction(Enrollment $enrollment)
    {

        if (!$enrollment->user) {
            // mh.. enrollment exists without user :/
            return false;
        }
        $data = [
            'meta' => ['enrollment_id' => $enrollment->id],
            'type' => CoordinatorTodo::TYPES['WEBINAR_REMINDER'],
            'customer_id' => $enrollment->user->id,
            'duedate' => Carbon::now(),
        ];
        $todo = new CoordinatorTodo($data);
        $this->coordinatorTodoService->createTodoIfNotExistsWithinDays($todo, 7);

    }

    private function sendReminder($enrollment, $class)
    {
        /**
         * @var Enrollment $enrollment
         * @var User $user
         */
        $user = $enrollment->user;
        if (!$user) { // if user was deleted
            return;
        }

        // tdb: needed?
        $user->last_reminder = Carbon::now();
        $user->save();

        // interface for: WebinarChoiceReminderNotification
        /** @var WebinarService $webinarService */
        $webinarService = app(WebinarService::class);
        $webinars = $webinarService->listForEnrollment($enrollment);

        $notification = new $class($user->brand, $user, $enrollment, $webinars);


        $user->notify($notification);

    }
}