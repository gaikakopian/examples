<?php

namespace Modules\Core\Reminder;

use App\Exceptions\Client\ClientException;
use App\Notifications\UserInvitation\UserLeadInvitationNotification;
use App\Notifications\UserInvitation\UserLeadReminder2Notification;
use App\Notifications\UserInvitation\UserLeadReminderNotification;
use App\Reminders\Handler\AbstractReminderHandler;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Notification;

use Modules\Admin\Domain\Models\CoordinatorTodo;
use Modules\Admin\Domain\Services\CoordinatorTodoService;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserLead;

class UserLeadReminderHandler extends AbstractReminderHandler
{


    protected $coordinatorTodoService;

    public function __construct(CoordinatorTodoService $coordinatorTodoService)
    {
        parent::__construct();
        $this->coordinatorTodoService = $coordinatorTodoService;


        // this has to be the 'reason date'  -> created At
        $this->setDateCallback(function ($lead) {
            return $lead->created_at;
        });

        $this->registerEvents();
    }

    public function registerEvents()
    {
        $this->registerEvent(1, [$this, 'inviteMail']);
        $this->registerEvent(24 * 5, [$this, 'firstReminder']);
        $this->registerEvent(24 * 14, [$this, 'secondReminder']);
        $this->registerEvent(24 * 21, [$this, 'createTodo']);
    }

    /**
     * @param UserLead $lead
     */
    public function firstReminder($lead)
    {
        $this->sendReminder($lead, UserLeadReminderNotification::class);
    }

    public function secondReminder($lead)
    {
        $this->sendReminder($lead, UserLeadReminder2Notification::class);
    }

    public function inviteMail(UserLead $lead)
    {
        $this->sendReminder($lead, UserLeadInvitationNotification::class);
    }


    /**
     * @param UserLead $lead
     * @throws \App\Exceptions\Server\ServerException
     */
    public function createTodo(UserLead $lead)
    {
        // @todo: customer id is required
//        $data = [
//            'meta' => ['lead_id' => $lead->id],
//            'type' => CoordinatorTodo::TYPES['USER_INVITE_NO_RESPONSE'],
//            'customer_id' => false,
//            'duedate' => Carbon::now()->addDays(2),
//        ];
//        $todo = new CoordinatorTodo($data);
//        $this->coordinatorTodoService->createTodoIfNotExistsWithinDays($todo, 7);


    }

    private function sendReminder(UserLead $lead, $notificationClass)
    {
        if ($lead->transitionAllowed('invite')) {
            $lead->transition('invite');
        }

        $notification = new $notificationClass($lead->registrationCode->brand, $lead);

        Notification::route('mail', $lead->email)
            ->notify($notification);

    }
}