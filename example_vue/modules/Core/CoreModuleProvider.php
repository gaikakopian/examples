<?php

namespace Modules\Core;

use App\Infrastructure\Providers\ModuleServiceProvider;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\DatabaseNotification;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Activity;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\File;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\ProfileField;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\SecurityIncident;
use Modules\Core\Domain\Models\SmsLog;
use Modules\Core\Domain\Models\Tenant;
use Modules\Core\Domain\Models\Terms;
use Modules\Core\Domain\Models\Translation;
use Modules\Core\Domain\Models\User;

use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Models\UserInvitation;
use Modules\Core\Domain\Models\UserLead;
use Modules\Core\Domain\Models\Webinar;
use Modules\Core\Domain\Services\BrandedDocumentService;
use Modules\Core\Domain\Services\BrandService;
use Modules\Core\Domain\Services\ActivityService;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\FileService;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Domain\Services\NotificationsService;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Core\Domain\Services\ProfileFieldService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Domain\Services\RegistrationCodeService;
use Modules\Core\Domain\Services\SecurityIncidentService;
use Modules\Core\Domain\Services\SmslogService;
use Modules\Core\Domain\Services\TenantService;
use Modules\Core\Domain\Services\TranslationService;
use Modules\Core\Domain\Services\TermsService;

use Modules\Core\Domain\Services\UserFeedbackService;
use Modules\Core\Domain\Services\UserInvitationService;
use Modules\Core\Domain\Services\UserLeadService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Domain\Services\WebinarService;

class CoreModuleProvider extends ModuleServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(EnrollmentService::class)->needs(Model::class)->give(Enrollment::class);
        $this->app->when(FileService::class)->needs(Model::class)->give(File::class);
        $this->app->when(GroupService::class)->needs(Model::class)->give(Group::class);
        $this->app->when(OrganizationService::class)->needs(Model::class)->give(Organization::class);
        $this->app->when(ParticipationService::class)->needs(Model::class)->give(Participation::class);
        $this->app->when(ProfileFieldService::class)->needs(Model::class)->give(ProfileField::class);
        $this->app->when(ProgramService::class)->needs(Model::class)->give(Program::class);
        $this->app->when(RegistrationCodeService::class)->needs(Model::class)->give(RegistrationCode::class);
        $this->app->when(TermsService::class)->needs(Model::class)->give(Terms::class);
        $this->app->when(UserService::class)->needs(Model::class)->give(User::class);
        $this->app->when(OrganizationService::class)->needs(Model::class)->give(Organization::class);
        $this->app->when(WebinarService::class)->needs(Model::class)->give(Webinar::class);
        $this->app->when(UserLeadService::class)->needs(Model::class)->give(UserLead::class);

        $this->app->when(BrandService::class)->needs(Model::class)->give(Brand::class);
        $this->app->when(TenantService::class)->needs(Model::class)->give(Tenant::class);

        $this->app->when(TranslationService::class)->needs(Model::class)->give(Translation::class);
        $this->app->when(SecurityIncidentService::class)->needs(Model::class)->give(SecurityIncident::class);
        $this->app->when(BrandedDocumentService::class)->needs(Model::class)->give(BrandedDocument::class);
        $this->app->when(UserFeedbackService::class)->needs(Model::class)->give(UserFeedback::class);
        $this->app->when(UserInvitationService::class)->needs(Model::class)->give(UserInvitation::class);
        $this->app->when(SmslogService::class)->needs(Model::class)->give(SmsLog::class);
        $this->app->when(ActivityService::class)->needs(Model::class)->give(Activity::class);
        $this->app->when(UserInvitationService::class)->needs(Model::class)->give(UserInvitation::class);
        $this->app->when(NotificationsService::class)->needs(Model::class)->give(DatabaseNotification::class);
    }
}
