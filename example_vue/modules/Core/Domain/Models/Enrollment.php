<?php

namespace Modules\Core\Domain\Models;

use App\Domain\Models\BaseModel;
use App\Infrastructure\Traits\Statable;
use App\Reminders\Traits\Remindable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Matching\Domain\Models\Match;

class Enrollment extends BaseModel
{
    use SoftDeletes, Statable, Remindable;

    /*
    * State Machine Configuration
    */

    const HISTORY_MODEL = EnrollmentStateLog::class;
    const SM_CONFIG = 'enrollment'; // the SM graph to use

    /*
    |--------------------------------------------------------------------------
    | Constants
    |--------------------------------------------------------------------------
    */


    /**
     * All possible states that this model can have.
     *
     * @var array
     */
    const STATES = [
        'NEW' => 'NEW',
        'INCOMPLETE' => 'INCOMPLETE',
        'TRAINING' => 'TRAINING',
        'WEBINAR' => 'WEBINAR',

        'WAITINGLIST' => 'WAITINGLIST',

        'ACTIVE' => 'ACTIVE',
        'QUIT' => 'QUIT',

        'EXCLUDED' => 'EXCLUDED',
        'NOREPLY' => 'NOREPLY',

        'DONE' => 'DONE',
    ];
    const DEFAULT_STATE = 'NEW';
    /**
     * All possible state transitions that can be performed on the model.
     *
     * @var array
     */
    const TRANSITIONS = [
        'start_training' => [
            'from' => ['NEW', 'INCOMPLETE', 'TRAINING'],
            'to' => 'TRAINING'
        ],
        'register_training' => [
            'from' => ['TRAINING'],
            'to' => 'WEBINAR'
        ],
        'missed_training' => [
            'from' => ['WEBINAR', 'TRAINING'],
            'to' => 'TRAINING'
        ],
        'complete_training' => [
            'from' => ['TRAINING', 'WEBINAR'],
            'to' => 'ACTIVE'
        ],
        'complete_video_training' => [
            'from' => ['TRAINING', 'WEBINAR'],
            'to' => 'ACTIVE'
        ],
        'participate' => [
            'from' => ['ACTIVE'],
            'to' => 'ACTIVE'
        ],
        'overbooked' => [
            'from' => ['NEW', 'ACTIVE'],
            'to' => 'WAITINGLIST'
        ],
        'reactivate' => [
            'from' => ['WAITINGLIST'],
            'to' => 'NEW'
        ],
        'finish' => [
            'from' => ['ACTIVE'],
            'to' => 'DONE'
        ],
        'quit' => [
            'from' => ['TRAINING', 'ACTIVE', 'DONE'],
            'to' => 'QUIT'
        ],
        'exclude' => [
            'from' => ['QUIT', 'TRAINING', 'ACTIVE', 'DONE'],
            'to' => 'EXCLUDED'
        ],
        'noreply' => [
            'from' => ['NEW', 'TRAINING', 'WEBINAR'],
            'to' => 'NOREPLY',
        ],
        'return_to_active' => [
            'from' => ['QUIT', 'DONE'],
            'to' => 'ACTIVE'
        ],
        'return_to_training' => [
            'from' => ['QUIT'],
            'to' => 'TRAINING'
        ],

    ];

    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'program_id',
        'role',
        'state',
        'training_completed_at',
        'last_state_change_at',
        'postponed_until',
        'last_reminder',
        'last_activity_at',
        'last_request_accepted_at',
        'last_reminder_type'
    ];

    protected $attributes = [
        'state' => 'NEW'
    ];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['deleted_at'];

    protected $casts = [
        'last_state_change_at' => 'datetime',
        'last_request_accepted_at' => 'datetime',
        'last_reminder' => 'datetime',
        'last_activity_at' => 'datetime',
        'training_completed_at' => 'datetime',
    ];


    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    /**
     * An Enrollment belongs to a Program.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function program()
    {
        return $this->belongsTo(Program::class);
    }

    /**
     * An Enrollment belongs to a User.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * An Enrollment can have many Participations.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function participations()
    {
        return $this->hasMany(Participation::class);
    }

    /**
     * An Enrollment can have many Webinars.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function webinars()
    {
        return $this->belongsToMany(Webinar::class)->withPivot('attended');
    }

    /*
    |--------------------------------------------------------------------------
    | Helpers & Computed Attributes
    |--------------------------------------------------------------------------
    */

    /**
     * Check whether the Enrollments role is 'mentee'.
     *
     * @return boolean
     */
    public function isMentee()
    {
        return $this->role === 'mentee';
    }

    public function hasApprovedRequestWithinDays($days = 21)
    {
        if (!$this->last_request_accepted_at) {
            return false;
        }
        if (Carbon::now()->subDays($days)->gt($this->last_request_accepted_at)) {
            return false;

        }
        return true;
    }


    /**
     * Check whether the Enrollments role is 'mentor'.
     *
     * @return boolean
     */
    public function isMentor()
    {
        return $this->role === 'mentor';
    }

    public function hasActiveMatch()
    {
        /**
         * @var Participation $participation
         * @var Match $match
         *
         */
        $participations = $this->participations;
        foreach ($participations as $participation){

            $match = $participation->match;

            if ($match && $match->isActive()) {
                return true;
            }
        }
        return false;
    }
}
