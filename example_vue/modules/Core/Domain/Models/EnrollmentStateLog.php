<?php

namespace Modules\Core\Domain\Models;

use App\Infrastructure\Traits\LogsState;
use Illuminate\Database\Eloquent\Model;

class EnrollmentStateLog extends Model
{
    use LogsState;

    protected $fillable = ['enrollment_id', 'actor_id', 'transition', 'from', 'to'];

    public function enrollment()
    {
        return $this->belongsTo(Enrollment::class);
    }
}
