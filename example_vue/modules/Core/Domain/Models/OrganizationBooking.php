<?php

namespace Modules\Core\Domain\Models;

use Illuminate\Database\Eloquent\Model;

class OrganizationBooking extends Model
{

    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];
}
