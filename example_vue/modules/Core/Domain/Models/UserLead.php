<?php

namespace Modules\Core\Domain\Models;

use App\Domain\Models\BaseModel;
use App\Infrastructure\Traits\Statable;
use App\Interfaces\Notifications\HasNotificationSettings;
use App\Reminders\Traits\Remindable;
use App\Services\MultiTenant\BelongsToTenant;
use App\Services\MultiTenant\TenantScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Modules\Community\Domain\Models\Chatroom;
use Modules\Core\Domain\Models\Traits\Commentable;
use Modules\Core\Domain\Models\Traits\HasImageAttachments;
use Modules\Matching\Domain\Interfaces\HasAvailabilityInterface;
use Modules\Matching\Domain\Models\Traits\HasAvailability;

class UserLead extends BaseModel
{

    use Statable;
    use Remindable;
    /**
     * All possible states that this model can have.
     *
     * @var array
     */
    const STATES = [
        'NEW' => 'NEW',
        'CONTACTED' => 'CONTACTED',
        'ACTIVATED' => 'ACTIVATED',
        'DISABLED' => 'DISABLED',//
    ];


    const HISTORY_MODEL = UserLeadStateLog::class;
    const SM_CONFIG = 'userlead'; // the SM graph to use





    const TRANSITIONS = [
        'invite' => [
            'from' => ['NEW'],
            'to' => 'CONTACTED'
        ],
        'register' => [
            'from' => ['NEW','CONTACTED'],
            'to' => 'ACTIVATED'
        ],
        'activate' => [ //manually
            'from' => ['NEW','CONTACTED'],
            'to' => 'ACTIVATED'
        ],
        'disable' => [
            'from' => ['NEW','CONTACTED'],
            'to' => 'DISABLED'
        ],
    ];

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'language',
        'state',
        'source',
        'code',
        'last_reminder',
        'last_state_change_at',
        'registration_code_id',
        'last_reminder_type'
    ];

    public $searchable = [
        'first_name',
        'last_name',
        'email'
    ];


    protected $casts = [
        'last_reminder' => 'datetime',
        'last_state_change_at' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',

    ];
    /**
     * A User can be member of a Brand.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function registrationCode()
    {
        return $this->belongsTo(RegistrationCode::class);
    }
    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower($value);
    }
    public function emailSecret($action)
    {
        return strtolower(substr(sha1($this->id . $this->email . $action), 0, 10));
    }






}
