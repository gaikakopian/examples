<?php

namespace Modules\Core\Domain\Models;

use App\Services\MultiTenant\BelongsToTenant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


use Modules\Community\Domain\Models\Post;
use Modules\Matching\Domain\Interfaces\HasAvailabilityInterface;
use Modules\Matching\Domain\Models\Traits\HasAvailability;

class Group extends Model
{
    use BelongsToTenant;
    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
    */

    const PRIVACY_PUBLIC = '1';
    const PRIVACY_PRIVATE = '2';
    const PRIVACY_INVITE = '3';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'organization_id',
        'has_availability',
        'scheduling_comment',
        'name',
        'privacy',
        'description',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];


    /**
     * used by the eloquent service to fuzzy search entities
     * @var array
     */
    public $searchable = [
        'name'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['deleted_at'];

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    /**
     * A Group can have many Users.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('role');
    }
    /**
     * A Group can have many Users.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function members()
    {
        return $this->belongsToMany(User::class)->withPivot('role')->where('role',  'member');
    }

    /**
     * A Group can have many Users.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function supervisors()
    {
        return $this->belongsToMany(User::class)->withPivot('role')->where('role', Role::ROLES['supervisor']);
    }

    /**
     * A Group can have many registration codes;
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function registrationCodes()
    {
        return $this->belongsToMany(RegistrationCode::class);
    }

    /**
     * A Group belongs to an Organization.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}
