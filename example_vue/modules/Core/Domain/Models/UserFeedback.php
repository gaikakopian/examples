<?php

namespace Modules\Core\Domain\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

class UserFeedback extends Model
{

    /**
     * All possible types that this model can have.
     *
     * @var array
     */
    const CODES = [
        'CONNECTION_QUALITY' => 'CONNECTION_QUALITY',
        'GENERAL_COMMENT' => 'GENERAL_COMMENT',
        'LIKE_SESSION' => 'LIKE_SESSION',
        'LIKE_MENTORSHIP' => 'LIKE_MENTORSHIP', //@depracated (?)
        'LIKE_PROGRAM' => 'LIKE_PROGRAM',
        'MENTORSHIP_HAPPINESS' => 'MENTORSHIP_HAPPINESS',
        'OPEN_FEEDBACK' => 'OPEN_FEEDBACK'
    ];


    const SCALAR_CODES = [
        'LIKE_MENTORSHIP' => 'LIKE_MENTORSHIP',
        'CONNECTION_QUALITY' => 'CONNECTION_QUALITY'
    ];
    /**
     * All possible types that this model can have.
     *
     * @var array
     */
    const STATES = [
        'NEW' => 'NEW',
        'IN_PROGRESS' => 'IN_PROGRESS',
        'DONE' => 'DONE',
        'ESCALATION' => 'ESCALATION'
    ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question_code',
        'response_scalar',
        'response_text',
        'user_id',
        'is_highlighted',
        'processing_state',
        'appointment_id',
        'match_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    /**
     *  A UserFeedback belongs to exactly one User.
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     *  A UserFeedback belongs to exactly one Appointment.
     *
     * @return BelongsTo
     */
    public function appointment()
    {
        return $this->belongsTo(Appointment::class);
    }
}
