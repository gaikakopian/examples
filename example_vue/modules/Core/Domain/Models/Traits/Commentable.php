<?php

namespace Modules\Core\Domain\Models\Traits;

use Modules\Core\Domain\Models\Comment;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;

trait Commentable
{
    /**
     * Get all comments.
     */
    public function comments()
    {
        return $this->morphMany('Modules\Core\Domain\Models\Comment', 'commentable')
            ->orderBy('pin_to_top', 'DESC');
    }

    public function addComment($userid, $body, $pin_to_top = false, $type = Comment::TYPES['TECHNICAL'])
    {

        if ($userid === null) {
            $userid = User::query()->where('primary_role', Role::ROLES['admin'])->inRandomOrder()->firstOrFail()->id;
        }
        $data = ['body' => $body, 'pin_to_top' => $pin_to_top, 'type' => $type];

        $comment = new Comment($data);
        $comment->commentable_type = self::class;
        $comment->commentable_id = $this->id;
        $comment->author_id = $userid;
        $comment->save();

        $comment->refresh(); // Retreive default value for pin_to_top instead of null

    }
}
