<?php

namespace Modules\Core\Domain\Models;

use Eloquent as Model;

class TranslationLog extends Model
{
    protected $fillable = ['tenant_id', 'locale', 'scope', 'json_added','json_removed','user_id'];


}
