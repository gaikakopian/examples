<?php

namespace Modules\Core\Domain\Models;

use App\Services\MultiTenant\BelongsToTenant;
use Illuminate\Database\Eloquent\Model;

class CronjobLog extends Model
{
    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'result', 'duration', 'success'];

    /**
     * The attributes that should be hidden.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];
}
