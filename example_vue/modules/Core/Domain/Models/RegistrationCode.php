<?php

namespace Modules\Core\Domain\Models;

use App\Services\MultiTenant\BelongsToTenant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Matching\Domain\Interfaces\HasAvailabilityInterface;
use Modules\Matching\Domain\Models\Traits\HasAvailability;

class RegistrationCode extends Model implements HasAvailabilityInterface
{
    use SoftDeletes;
    use HasAvailability;
    use BelongsToTenant;


    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'primary_role',
        'code',
        'internal_comment',
        'external_comment',
        'users_count_limit',
        'users_count_planned',
        'name',
        'settings',
        'auto_enroll_in',
        'brand_id',
        'group_id',
        'tenant_id'
    ];

    /**
     * used by the eloquent service to fuzzy search entities
     * @var array
     */
    public $searchable = [
        'name'
    ];


    protected $casts = [
        'valid_until' => 'datetime'
    ];


    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    /**
     * A RegistrationCode belongs to one Brand.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * A RegistrationCode belongs to one Organization.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(Group::class);
    }


    /**
     * A RegistrationCode can have many Users.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     * A RegistrationCode can auto-enroll into a Program.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function autoEnrollIn()
    {
        return $this->belongsTo(Program::class);
    }

    public function getRegistrationLink(Brand $brand)
    {
        return $brand->frontend_url .'/register/code/' . $this->code;

    }
    public function getIsValidAttribute()
    {
        if (!$this->valid_until){
            return true;
        }
        return !$this->valid_until->isPast();

    }


}
