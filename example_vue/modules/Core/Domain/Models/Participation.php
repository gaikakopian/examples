<?php

namespace Modules\Core\Domain\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Matching\Domain\Models\Match;

class Participation extends Model
{
    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['enrollment_id', 'start_matching_after', 'match_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'deleted_at', 'start_matching_after', 'match_confirmed_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['deleted_at'];

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    /**
     * An Participation belongs to an Enrollment.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function enrollment()
    {
        return $this->belongsTo(Enrollment::class);
    }

    /**
     * A Participation can be matched.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function match()
    {
        return $this->belongsTo(Match::class);
    }

    /*
    |--------------------------------------------------------------------------
    | Helper Functions
    |--------------------------------------------------------------------------
    */

    /**
     * Confirm the Match.
     *
     * @return void
     */
    public function confirmMatch()
    {
        $this->match_confirmed_at = Carbon::now();
        $this->save();

        //statemachine
        $this->match->transition('confirm');
    }

    /**
     * Cancel the Match.
     * @deprecated
     * @return void
     */
    public function cancelMatch()
    {
        //statemachine
        $this->match->transition('cancel');
    }  /**
     * Cancel the Match.
     *
     * @return void
     */
    public function rejectMatch()
    {
        //statemachine
        $this->match->transition('reject');
    }


}
