<?php

namespace Modules\Core\Domain\Models;

use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{
    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];
}
