<?php

namespace Modules\Core\Domain\Services;

use App\Infrastructure\Domain\EloquentBreadService;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;

class ProgramService extends EloquentBreadService
{
    /**
     * Return all programs accessible to a User.
     *
     * @param int $userId
     * @return Collection
     */
    public function listForUser(int $userId) : Collection
    {
        $user = User::find($userId);

        return $user->organization->programs;
    }

    /**
     * Return a program if it's accessible to the User.
     *
     * @param int $userId
     * @param int $programId
     * @return Arrayable
     */
    public function getForUser(int $userId, int $programId) : Arrayable
    {
        $user = User::findOrFail($userId);

        return Program::where('id', $programId)
            ->whereHas('organizations', function ($query) use ($user) {
                $query->where('id', $user->organization_id);
            })
            ->firstOrFail();
    }
}
