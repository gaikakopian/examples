<?php

namespace Modules\Core\Domain\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Collection;
use App\Infrastructure\Domain\EloquentBreadService;

class ProfileFieldService extends EloquentBreadService
{
    /**
     * Return all profile fields for a user.
     *
     * @param int $userId
     * @return Collection
     */
    public function listForUser(int $userId) : Collection
    {
        return $this->model->newQuery()
            ->where('user_id', $userId)
            ->get();
    }

    /**
     * Return a specific profile field for a user.
     *
     * @param int $userId
     * @param string $code The field code
     * @return Arrayable
     */
    public function getForUser(int $userId, string $code) : Arrayable
    {
        return $this->model->newQuery()
            ->where('user_id', $userId)
            ->where('code', $code)
            ->firstOrFail();
    }

    /**
     * Create or update a specific profile field for a user.
     *
     * @param int $userId
     * @param string $code
     * @param mixed $value
     * @return Arrayable
     */
    public function     createOrUpdateForUser(int $userId, string $code, $value) : Arrayable
    {
        return $this->model->updateOrCreate([
            'user_id' => $userId,
            'code' => $code,
        ], [
            'value' => $value
        ]);
    }

    /**
     * Create or update an array of `code => value` ProfileFields.
     *
     * @param integer $userId
     * @param array $fields
     * @return Arrayable
     */
    public function createOrUpdateManyForUser(int $userId, array $fields) : Arrayable
    {
        DB::transaction(function () use ($fields, $userId) {
            foreach ($fields as $code => $value) {
                $this->createOrUpdateForUser($userId, $code, $value);
            }
        });

        return $this->model->where('user_id', $userId)->get();
    }
}
