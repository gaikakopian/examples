<?php

namespace Modules\Core\Domain\Services;

use App\Infrastructure\Domain\EloquentBreadService;
use Illuminate\Database\Eloquent\Model;

class SecurityIncidentService extends EloquentBreadService
{


    public function __construct(
        Model $model
    )
    {
        parent::__construct($model);
    }

    public function saveSecurityIncident($type, $meta = [], $user_id = false)
    {
        return $this->create(['type' => $type, 'meta' => $meta, 'affected_user_id' => $user_id]);
    }
}
