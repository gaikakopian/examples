<?php

namespace Modules\Core\Domain\Policies;

use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Comment;
use Modules\Core\Domain\Services\GroupService;
use Modules\Matching\Domain\Services\MatchService;

class UserPolicy
{
    protected $groupService;
    protected $matchService;

    public function __construct(GroupService $groupService, MatchService $matchService)
    {
        $this->groupService = $groupService;
        $this->matchService = $matchService;
    }

    public function showComment(User $authUser, Comment $comment, User $user): bool
    {
        return $authUser->isCoordinator() || $this->isSupervisor($authUser, $user) || $authUser->isAdmin();
    }

    public function addComment(User $authUser, User $user): bool
    {
        return $authUser->isCoordinator() || $this->isSupervisor($authUser, $user) || $authUser->isAdmin();
    }

    public function listComments(User $authUser, User $user): bool
    {
        return $authUser->isCoordinator() || $this->isSupervisor($authUser, $user) || $authUser->isAdmin();
    }

    public function listSupervisorComments(User $authUser, User $user): bool
    {
        return $this->isSupervisor($authUser, $user);
    }

    public function listSupervisorAppointments(User $authUser, User $user): bool
    {
        return $this->isSupervisor($authUser, $user);
    }

    public function resetPassword(User $authUser, User $user): bool
    {
        return $authUser->isCoordinator() || $this->isSupervisor($authUser, $user) || $authUser->isAdmin();
    }

    public function viewCommunityProfile(User $authUser, User $user)
    {
        return $user->community_enabled || $authUser->id == $user->id || $this->matchService->existsMatchOfUsers($authUser, $user) !== null;
    }

    protected function isSupervisor(User $authUser, User $user)
    {
        return $this->groupService->isSupervisorOf($authUser->id, $user->id);
    }
}
