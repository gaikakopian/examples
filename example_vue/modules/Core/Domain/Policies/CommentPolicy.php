<?php

namespace Modules\Core\Domain\Policies;

use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Comment;

class CommentPolicy
{
    public function updateComment(User $user, Comment $comment): bool
    {
        return $user->isSupport() || $comment->author_id === $user->id;
    }

    public function deleteComment(User $user, Comment $comment): bool
    {
        return $user->isSupport() || $comment->author_id === $user->id;
    }
}
