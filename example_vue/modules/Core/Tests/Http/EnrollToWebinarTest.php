<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Modules\Core\Domain\Models\Enrollment;

/**
 * Test the functionality of the `/api/v2/webinars/{id}/enroll` endpoint.
 */
class EnrollToWebinarTest extends EndpointTest
{
    public function test_it_enrolls_in_a_webinar()
    {
        Notification::fake();

        $enrollment = Enrollment::where('user_id', '=', $this->user->id)->firstOrFail();

        $response = $this->actingAs($this->user)->json(
            'POST',
            '/api/v2/enrollments/' . $enrollment->id . '/webinars/1/enroll'
        );

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $this->assertDatabaseHas('enrollment_webinar', [
            'enrollment_id' => $enrollment->id,
            'webinar_id' => 1,
        ]);
    }

    public function test_it_doesnt_enroll_in_a_webinar_for_wrong_enrollment()
    {

        // delete seeder values;

        /** @var Enrollment $otherEnrollment */
        $otherEnrollment = Enrollment::where('user_id', '<>', $this->user->id)->firstOrFail();
        $otherEnrollment->webinars()->sync([]);

        $response = $this->actingAs($this->user)->json(
            'POST',
            '/api/v2/enrollments/' . $otherEnrollment->id . '/webinars/1/enroll'
        );

        $this->assertEquals(403, $response->getStatusCode(), $response->getContent());
        $this->assertDatabaseMissing('enrollment_webinar', [
            'enrollment_id' => $otherEnrollment->id,
            'webinar_id' => 2,
        ]);
    }
}
