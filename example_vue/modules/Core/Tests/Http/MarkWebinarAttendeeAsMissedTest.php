<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Webinar;

/**
 * Test the functionality of the `/api/v2/webinars/{webinarId}/attendees/{userId}/missed` endpoint.
 */
class MarkWebinarAttendeeAsMissedTest extends EndpointTest
{
    public function test_it_throws_unauthorized_for_normal_users()
    {
        $webinar = factory(Webinar::class)->create([
            'program_id' => 1,
        ]);
        $webinar->enrollments()->attach(1);
        $response = $this->actingAs($this->user)
            ->post("/api/v2/webinars/$webinar->id/attendees/1/missed");

        $response->assertStatus(403);
        $this->assertDatabaseHas('enrollment_webinar', [
            'enrollment_id' => 1,
            'webinar_id' => $webinar->id,
            'attended' => null,
        ]);
    }

    public function test_it_marks_as_missed()
    {
        $webinar = factory(Webinar::class)->create([
            'program_id' => 1,
        ]);
        $webinar->enrollments()->attach(1);

        $user = User::where('email', 'admin@volunteer-vision.com')->first();
        $response = $this->actingAs($user)
            ->post("/api/v2/webinars/$webinar->id/attendees/1/missed");

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $this->assertDatabaseHas('enrollment_webinar', [
            'enrollment_id' => 1,
            'webinar_id' => $webinar->id,
            'attended' => false,
        ]);
    }
}
