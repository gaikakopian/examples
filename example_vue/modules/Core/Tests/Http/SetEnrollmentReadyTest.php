<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Support\Facades\Notification;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v1/enrollments/{id}/set-ready` endpoint.
 */
class SetEnrollmentReadyTest extends EndpointTest
{
    /** @var Enrollment $supervisor
     * the state of the enrollment must be 'TRAINING' in order to set it as ready.
     */
    protected $enrollment;

    private $demoUser;


    public function test_it_lets_a_supervisor_set_an_enrollment_to_ready()
    {
        Notification::fake();

        $supervisor = factory(User::class)->create();
        $supervisor->groups()->attach(1, [
            'role' => 'supervisor'
        ]);
        $this->user->groups()->attach(1);

        $response = $this->actingAs($supervisor)->post('/api/v2/enrollments/' . $this->enrollment->id . '/set-ready');

        // ToDo - Test actual state machine change (@Somaya)

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
    }

    public function test_it_prevents_normal_group_member_from_settting_an_enrollment_ready()
    {
        $response = $this->actingAs($this->demoUser)->postJson('/api/v2/enrollments/' . $this->enrollment->id . '/set-ready');

        $this->assertEquals($response->getStatusCode(),403, $response->getContent());

    }

    public function test_it_prevents_a_different_groups_supervisor_from_settting_an_enrollment_ready()
    {
        $otherSupervisor = factory(User::class)->create();
        $otherGroup = factory(Group::class)->create();
        $otherSupervisor->groups()->attach($otherGroup->id, [
            'role' => 'supervisor'
        ]);
        $this->user->groups()->attach(1);

        $response = $this->actingAs($otherSupervisor)->postJson('/api/v2/enrollments/' . $this->enrollment->id . '/set-ready');

        $this->assertEquals($response->getStatusCode(),403, $response->getContent());
    }

    public function setUp()
    {
        parent::setUp();

        /*
         * $normalMember is Member of Group Id 1
         * $normalMember is owner of $this->enrollment
         */
        $normalMember = factory(User::class)->create();
        $normalMember->groups()->attach(1, [
            'role' => 'member'
        ]);

        $this->demoUser = $normalMember;

        $this->enrollment = factory(Enrollment::class)->create(
            [
                'state' => Enrollment::STATES['TRAINING'],
                'user_id' => $this->demoUser->id
            ]
        ); //only enrollments at state TRAINING can be set as ready - new state = 'AVAILABLE'
    }
}
