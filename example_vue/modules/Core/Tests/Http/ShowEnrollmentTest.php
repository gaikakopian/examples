<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;

/**
 * Test the functionality of the `/api/v1/enrollment/{id}` endpoint.
 */
class ShowEnrollmentTest extends EndpointTest
{
    /** @test */
    public function test_it_shows_one_of_my_enrollments()
    {
        $response = $this->actingAs($this->user)->get('/api/v2/enrollments/1');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJsonStructure([
            'data' => [
                'id',
                'program',
                'participations',
                'webinars',
            ]
        ]);
    }
}
