<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserLead;

/**
 * Test the functionality of the `/api/v1/users/me` endpoint.
 */
class UserLeadEmailResponseTest extends EndpointTest
{
    /** @test */
    public function test_it_activates_user_lead()
    {


        /** @var UserLead $userLead */
        $userLead = factory(UserLead::class)->create();

        $action = 'activate';

        $secret = $userLead->emailSecret($action);

        $route = route('email.response.userlead', [
            'action' => $action,
            'leadid' => $userLead->id,
            'secret' => $secret
        ]);

        // public url
        $response = $this->getJson($route);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $lead = UserLead::find($userLead->id);

        $this->assertEquals(UserLead::STATES['ACTIVATED'], $lead->state);
    } /** @test */
    public function test_it_cancels_user_lead()
    {


        /** @var UserLead $userLead */
        $userLead = factory(UserLead::class)->create();

        $action = 'disable';

        $secret = $userLead->emailSecret($action);

        $route = route('email.response.userlead', [
            'action' => $action,
            'leadid' => $userLead->id,
            'secret' => $secret
        ]);

        // public url
        $response = $this->getJson($route);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $lead = UserLead::find($userLead->id);

        $this->assertEquals(UserLead::STATES['DISABLED'], $lead->state);
    }
}
