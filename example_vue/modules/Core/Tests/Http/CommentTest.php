<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Comment;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

class CommentTest extends EndpointTest
{
    public function test_normal_user_cannot_add_comment_for_a_match()
    {

        $actor = factory(User::class)->create();

        $match = Match::find(1);

        $response = $this->actingAs($actor)->json('POST', "/api/v2/matches/{$match->id}/comments", [
            'body' => 'Comment',
            'type' => 'sample'

        ]);
        $this->assertEquals($response->getStatusCode(), 403, $response->getContent());

    }

    public function test_normal_user_cannot_list_any_comments_for_a_match()
    {
        $actor = factory(User::class)->create();

        $match = Match::find(1);

        $author = factory(User::class)->create();
        $comment1 = new Comment();
        $comment1->body = 'Comment 1';
        $comment1->author_id = $author->id;
        $match->comments()->save($comment1);

        // Admin is not connected to the match at all ...
        $response = $this->actingAs($actor)->getJson("/api/v2/matches/{$match->id}/comments");
        $response->assertStatus(403);
    }

    public function test_normal_user_cannot_add_comment_for_a_user()
    {
        $actor = factory(User::class)->create();

        $user = User::find(1);

        $response = $this->actingAs($actor)->json('POST', "/api/v2/users/{$user->id}/comments", [
            'body' => 'Comment',
            'type' => 'sample'

        ]);
        $this->assertEquals($response->getStatusCode(), 403, $response->getContent());

    }

    public function test_normal_user_cannot_list_any_comments_for_a_user()
    {
        $actor = factory(User::class)->create();

        $user = User::find(1);

        $author = factory(User::class)->create();
        $comment1 = new Comment();
        $comment1->body = 'Comment 1';
        $comment1->author_id = $author->id;
        $user->comments()->save($comment1);

        // Admin is not connected to the match at all ...
        $response = $this->actingAs($actor)->getJson("/api/v2/users/{$user->id}/comments");
        $this->assertEquals($response->getStatusCode(), 403, $response->getContent());
    }

    public function test_match_connected_user_can_add_comment_to_a_match()
    {
        $actor = factory(User::class)->create();


        /** @var Match $match */
        $match = Match::find(1);
        $user = $match->enrollments->first()->user;


        $response = $this->actingAs($user)->json('POST', "/api/v2/matches/{$match->id}/comments", [
            'body' => 'Comment',
            'type' => 'sample'
        ]);
        $this->assertEquals($response->getStatusCode(), 201, $response->getContent());


        $response->assertJsonStructure([
            'data' => [
                'id',
                'body',
                'type',
                'pinToTop'
            ]
        ]);
    }

    // Just included a test for pinned top comments here ...
    public function test_match_connected_user_can_list_only_untyped_comments_for_a_match_with_pinned_top()
    {
        $this->markTestSkipped('Comment endpint changed');

        $actor = factory(User::class)->create();

        /** @var Match $match */
        $match = Match::find(1);
        /** @var User $user */
        $user = $match->enrollments->first()->user;


        $author = factory(User::class)->create();
        $comment1 = new Comment();
        $comment1->body = 'Comment 1';
        $comment1->author_id = $author->id;
        $match->comments()->save($comment1);

        $author = factory(User::class)->create();
        $comment2 = new Comment();
        $comment2->body = 'Should not show up';
        $comment2->author_id = $author->id;
        $comment2->type = 'SUPERVISOR_COMMENT';
        $match->comments()->save($comment2);

        $author = factory(User::class)->create();
        $comment3 = new Comment();
        $comment3->body = 'Comment 3';
        $comment3->author_id = $author->id;
        $comment3->pin_to_top = true;
        $match->comments()->save($comment3);

        $author = factory(User::class)->create();
        $comment4 = new Comment();
        $comment4->body = 'Comment 4';
        $comment4->author_id = $author->id;
        $match->comments()->save($comment4);

        $response = $this->actingAs($user)->get("/api/v2/matches/{$match->id}/comments");
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJson([
            'data' => [
                [
                    'id' => $comment3->id,
                    'body' => 'Comment 3',
                    'type' => null,
                    'pinToTop' => true
                ],
                [
                    'id' => $comment1->id,
                    'body' => 'Comment 1',
                    'type' => null,
                    'pinToTop' => false
                ],
                [
                    'id' => $comment4->id,
                    'body' => 'Comment 4',
                    'type' => null,
                    'pinToTop' => false
                ],
            ]
        ]);

        $response->assertJsonMissingExact([
            'id' => $comment2->id,
            'body' => 'Comment 2',
            'type' => 'SUPERVISOR_COMMENT',
            'pinToTop' => false
        ]);
    }

    public function test_supervisor_can_add_supervised_comment_to_a_match()
    {
        $actor = factory(User::class)->create();

        $group = Group::find(1);
        $user = $group->users()->first();
        $supervisor = factory(User::class)->create(['primary_role' => 'mentee']);
        $group->users()->attach($supervisor->id, ['role' => 'supervisor']);
        $match = Match::find(1);

        $response = $this->actingAs($supervisor)->json('POST', "/api/v2/matches/{$match->id}/comments", [
            'body' => 'Comment',
            'type' => 'SUPERVISOR_COMMENT',
        ]);
        $response->assertStatus(201);

        $response->assertJsonStructure([
            'data' => [
                'id',
                'body',
                'type',
                'pinToTop'
            ]
        ]);

        $response->assertJson([
            'data' => [
                'type' => 'SUPERVISOR_COMMENT',
            ]
        ]);
    }

    public function test_supervisor_can_list_untyped_and_supervised_comments_for_a_match()
    {
        $this->markTestSkipped('Comment endpint changed');
        $actor = factory(User::class)->create();

        $group = Group::find(1);
        $user = $group->users()->first();
        $supervisor = factory(User::class)->create(['primary_role' => 'mentee']);
        $group->users()->attach($supervisor->id, ['role' => 'supervisor']);
        $match = Match::find(1);

        $author = factory(User::class)->create();
        $comment1 = new Comment();
        $comment1->body = 'Comment 1';
        $comment1->author_id = $author->id;
        $match->comments()->save($comment1);

        $author = factory(User::class)->create();
        $comment2 = new Comment();
        $comment2->body = 'Comment 2';
        $comment2->author_id = $author->id;
        $comment2->type = 'SUPERVISOR_COMMENT';
        $match->comments()->save($comment2);

        $response = $this->actingAs($supervisor)->get("/api/v2/matches/{$match->id}/comments");
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJson([
            'data' => [
                [
                    'id' => $comment1->id,
                    'body' => 'Comment 1',
                    'type' => null
                ],
                [
                    'id' => $comment2->id,
                    'body' => 'Comment 2',
                    'type' => 'SUPERVISOR_COMMENT'
                ]
            ]
        ]);
    }

    public function test_supervisor_can_add_supervised_comment_to_a_user()
    {
        $actor = factory(User::class)->create();

        $group = Group::find(1);
        $user = $group->users()->first();
        $supervisor = factory(User::class)->create(['primary_role' => 'mentee']);
        $group->users()->attach($supervisor->id, ['role' => 'supervisor']);

        $response = $this->actingAs($supervisor)->json('POST', "/api/v2/users/{$user->id}/comments", [
            'body' => 'Comment',
            'type' => 'SUPERVISOR_COMMENT',
        ]);
        $response->assertStatus(201);

        $response->assertJsonStructure([
            'data' => [
                'id',
                'body',
                'type',
                'pinToTop'
            ]
        ]);

        $response->assertJson([
            'data' => [
                'type' => 'SUPERVISOR_COMMENT',
            ]
        ]);
    }

    public function test_supervisor_can_list_untyped_and_supervised_comments_for_a_user()
    {
        $this->markTestSkipped('Comment endpint changed');

        $actor = factory(User::class)->create();

        $group = Group::find(1);
        $user = $group->users()->first();
        $supervisor = factory(User::class)->create(['primary_role' => 'mentee']);
        $group->users()->attach($supervisor->id, ['role' => 'supervisor']);

        $author = factory(User::class)->create();
        $comment1 = new Comment();
        $comment1->body = 'Comment 1';
        $comment1->author_id = $author->id;
        $user->comments()->save($comment1);

        $author = factory(User::class)->create();
        $comment2 = new Comment();
        $comment2->body = 'Comment 2';
        $comment2->author_id = $author->id;
        $comment2->type = 'SUPERVISOR_COMMENT';
        $user->comments()->save($comment2);

        $response = $this->actingAs($supervisor)->get("/api/v2/users/{$user->id}/comments");
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJson([
            'data' => [
                [
                    'id' => $comment1->id,
                    'body' => 'Comment 1',
                    'type' => null
                ],
                [
                    'id' => $comment2->id,
                    'body' => 'Comment 2',
                    'type' => 'SUPERVISOR_COMMENT'
                ]
            ]
        ]);
    }
}
