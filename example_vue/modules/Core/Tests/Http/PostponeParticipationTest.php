<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v1/participations/{id}/postpone` endpoint.
 */
class PostponeParticipationTest extends EndpointTest
{
    public function test_it_postpones_my_participation()
    {
        $response = $this
            ->actingAs($this->user)
            ->json('POST', '/api/v2/participations/1/postpone', [
                'until' => '2019-01-01 01:00:00',
            ]);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJsonStructure([
            'data' => [
                'id',
            ]
        ]);
        // 2019-01-01T01:00:00+00:00
        $response->assertJsonFragment([
            'startMatchingAfter' => (new Carbon('2019-01-01 01:00:00'))->toIso8601String() ,
        ]);
        $this->assertDatabaseHas('participations', [
            'id' => 1,
            'start_matching_after' => '2019-01-01 01:00:00',
        ]);
    }

    public function test_it_postpones_a_participation_as_admin()
    {
        $user = User::where('email', 'admin@volunteer-vision.com')->first();
        $response = $this
            ->actingAs($user)
            ->json('POST', '/api/v2/participations/1/postpone', [
                'until' => '2019-01-01 01:00:00',
            ]);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJsonStructure([
            'data' => [
                'id',
            ]
        ]);

        $response->assertJsonFragment([
                'startMatchingAfter' => (new Carbon('2019-01-01 01:00:00'))->toIso8601String(),

        ]);
        $this->assertDatabaseHas('participations', [
            'id' => 1,
            'start_matching_after' => '2019-01-01 01:00:00',
        ]);
    }
}
