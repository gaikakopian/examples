<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Services\ConferenceService;

/**
 * Test the functionality of the `/api/v1/conference/auth/user` endpoint.
 */
class ConferenceAuthorizeUserTest extends EndpointTest
{
    /**
     * @var ConferenceService
     */
    protected $conferenceService;

    public function setUp()
    {
        parent::setUp();

        $this->conferenceService = app(ConferenceService::class);
    }

    public function test_it_responds_with_200_when_a_user_may_log_in()
    {
        $validToken = $this->conferenceService->getConferenceTokenForUser($this->user->id);
        $response = $this->json('POST', '/api/v2/conference/authorize/user', ['token' => $validToken]);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
    }

    public function test_it_responds_with_403_when_a_user_may_not_log_in()
    {
        $invalidToken = $this->conferenceService->getConferenceTokenForUser($this->user->id) . 'invalidate';
        $response = $this->json('POST', '/api/v2/conference/authorize/user', ['token' => $invalidToken]);

        $response->assertStatus(403);
    }
}
