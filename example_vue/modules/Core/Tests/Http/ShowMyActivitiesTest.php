<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Activity;

/**
 * Test the functionality of the `/api/v1/users/me/activities` endpoint.
 */
class ShowMyActivitiesTest extends EndpointTest
{
    /** @test */
    public function test_it_shows_my_activities()
    {
        factory(Activity::class, 10)->create(['user_id' => $this->user->id]);


        $this->assertNotNull($this->user->tenant_id);
        $response = $this->actingAs($this->user)->getJson('/api/v2/users/me/activities');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJsonStructure([
            'data' => [
                [
                    'name',
                    'meta',
                    'variables',
                    'createdAt'
                ]
            ]
        ]);
    }
}
