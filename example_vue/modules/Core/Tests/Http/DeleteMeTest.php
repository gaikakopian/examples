<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v1/users/me` endpoint.
 */
class DeleteMeTest extends EndpointTest
{


    /** @test */
    public function it_throws_an_error_for_wrong_password_when_updating_current_user()
    {
        $response = $this->actingAs($this->user)->deleteJson('/api/v2/users/me');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

    }
}
