<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;

/**
 * Test the functionality of the `/api/v2/translations/{scope}/{locale}` endpoint.
 */
class TranslationTest extends EndpointTest
{
    /** @test */
    public function test_translation_api_works()
    {
        $response = $this->get('/api/v2/translations/frontend/en');
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJson(['HELLO' => 'hello']);
    }
}
