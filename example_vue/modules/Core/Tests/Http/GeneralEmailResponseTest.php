<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v1/users/me` endpoint.
 */
class GeneralEmailResponseTest extends EndpointTest
{
    /** @test */
    public function test_it_confirms_my_mail()
    {

        /** @var User $user */
        $user = $this->user;

        $user->email_optin_at = null;
        $user->save();

        $secret = $user->emailSecret('confirmemail');

        $route = route('email.response.general', [
            'action' => 'confirmemail',
            'userid' => $user->id,
            'secret' => $secret
        ]);

        // public url
        $response = $this->get($route);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $user = User::find($user->id);


        $this->assertNotNull($user->email_optin_at);

    }
}
