<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Scheduling\Domain\Models\Appointment;

/**
 * @todo: add real events (e.g. appointment online, appointment end)
 *
 * Test the functionality of the `/api/v1/conference/callback` endpoint.
 *
 * method: 'POST',
 * path: `/appointments/for-room/${this.room.id}`,
 * payload: {
 * date: date
 * }
 * })
 */
class VideoconnectedCallbackTest extends EndpointTest
{

    public function test_it_creates_a_new_appointment()
    {

        $algo = Config::get('conference.signature_algo');
        $secret = Config::get('conference.shared_secret');

        $user = $this->user;

        $match = $user->enrollments->first()->participations->first()->match;
        $match->external_room_id = 12345;
        $match->save();
        $pastAppoinment = factory(Appointment::class)->create(['plannedStart' => Carbon::now()->subMinute(60), 'state' => Appointment::STATES['PLANNED'], 'match_id'=> $match->id]);;


        $date = Carbon::now()->addDays(7);

        $payload = [];
        //@todo: this
        $signature = hash_hmac($algo, json_encode($payload), $secret);

        $response = $this->json(
            'POST',
            '/api/v2/conference/messages/rooms/' . $match->external_room_id . '/videoconnected',
            $payload,
            ['X-VV-Signature' => $signature, "X-VV-User-Id" => $user->id]
        );
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'appointment' => $pastAppoinment->id
        ], $response->getContent());
    }


}
