<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Program;

/**
 * These tests are just to verify http uploads are working. There is a more precise unit test on attachments too.
 */
class UploadAttachmentsTest extends EndpointTest
{
    public function test_it_uploads_and_removes_an_avatar_for_a_user()
    {
        $this->it_uploads_and_removes_an_image_attachment('/api/v2/users/me/avatar', $this->user, 'avatar', 'user/avatar');
    }

    public function test_it_uploads_and_removes_a_cover_for_a_program()
    {
        $program = Program::find(1);
        $this->it_uploads_and_removes_an_image_attachment("/api/v2/programs/{$program->id}/cover", $program, 'cover', 'program/cover');
    }

    public function test_it_uploads_and_removes_a_detail_photo_for_a_program()
    {
        $program = Program::find(1);
        $this->it_uploads_and_removes_an_image_attachment("/api/v2/programs/{$program->id}/detail-photo", $program, 'detail_photo', 'program/detail_photo');
    }

    public function test_it_uploads_and_removes_a_logo_for_a_program()
    {
        $program = Program::find(1);
        $this->it_uploads_and_removes_an_image_attachment("/api/v2/programs/{$program->id}/logo", $program, 'logo', 'program/logo', 'medium');
    }
}
