<?php

namespace Modules\Core\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Comment;

class CommentableTest extends LaravelTest
{
    public function test_it_saves_a_comment_for_a_commentable_model()
    {
        $user = factory(User::class)->create();
        $comment = new Comment();
        $comment->body = 'This user is tested!';
        $comment->author_id = $user->id;
        $user->comments()->save($comment);

        $this->assertDatabaseHas('comments', [
            'commentable_id' => $user->id,
            'commentable_type' => 'Modules\Core\Domain\Models\User',
            'body' => 'This user is tested!'
        ]);
        $this->assertEquals('This user is tested!', $user->comments()->first()->body);
        $this->assertNotNull($user->comments()->first()->created_at);
        $this->assertNotNull($user->comments()->first()->updated_at);
    }
}
