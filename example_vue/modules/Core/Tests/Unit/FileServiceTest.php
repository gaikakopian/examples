<?php

namespace Modules\Core\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;
use Modules\Core\Domain\Models\File;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Comment;
use Modules\Core\Domain\Services\FileService;

class FileServiceTest extends LaravelTest
{

    /**
     * @var FileService
     */
    private $fileService;

    protected function setUp()
    {
        parent::setUp();
        $this->fileService = app(FileService::class);
    }

//
    public function test_it_lists_files_for_a_progam_and_audience()
    {
        $amount = 5;
        $placement = File::PLACEMENTS['preparation'];
        $audience = User::ROLES['mentee'];


        $program = factory(Program::class)->create();

        $filesMentee = factory(File::class, $amount)->create([
            'program_id' => $program->id,
            'placement' => $placement,
            'target_audience' => $audience
        ]);
        $filesMentor = factory(File::class)->create([
            'program_id' => $program->id,
            'placement' => $placement,
            'target_audience' => 'mentor'
        ], 2);

        $result = $this->fileService->listForProgramAndPlacement($placement, $program->id, $audience);

        $this->assertEquals($result->count(), $filesMentee->count());
    }
}
