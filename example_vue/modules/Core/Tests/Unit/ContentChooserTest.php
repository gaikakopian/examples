<?php

namespace Modules\Core\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Services\ContentChooser\SearchObject;
use Modules\Core\Domain\Services\ContentChooser\ContentChooser;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\BrandedDocument;
use Illuminate\Support\Facades\Log;

/**
 * Test the functionality of the ContentChooser Service.
 */
class ContentChooserTest extends LaravelTest
{
    protected $brand1;
    protected $brand2;
    protected $programs;

    /** {@inheritdoc} */
    protected function setUp()
    {
        parent::setUp();

        $this->brand1 = factory(Brand::class)->create();
        $this->brand2 = factory(Brand::class)->create();
        $this->programs = Program::all();
    }

    public function test_it_throws_an_exception_for_an_unexisting_key_and_writes_to_logfile()
    {
        $this->markTestSkipped('actually we should send an email to an admin in this case;');

        config(['database.log_queries' => false]);

        Log::shouldReceive('warning')
            ->once()
            ->with("[BrandedDocuments] Can't find any Branded Documents for the key 'inform_before_start' in format 'sms'");

        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'email',
            'subject' => 'Wrong document',
            'content' => 'I am the wrong document for you.',
            'language' => 'en'
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'email',
            'subject' => 'Wrong document',
            'content' => 'I am the wrong document for you.',
            'language' => 'de',
            'brand_id' => $this->brand1->id
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'email',
            'subject' => 'Wrong document',
            'content' => 'I am the wrong document for you.',
            'language' => 'en',
            'program_id' => $this->programs[0]->id,
            'brand_id' => $this->brand1->id
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'sms',
            'subject' => 'Wrong document',
            'content' => 'I am the wrong document for you.',
            'language' => 'en',
            'program_id' => $this->programs[0]->id,
            'brand_id' => $this->brand2->id
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'email',
            'subject' => 'Wrong document',
            'content' => 'I am the wrong document for you.',
            'language' => 'en',
            'program_id' => $this->programs[1]->id,
            'brand_id' => $this->brand2->id
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'inform_after_first_course',
            'type' => 'email',
            'subject' => 'Wrong document',
            'content' => 'I am the wrong document for you.',
            'language' => 'en',
            'program_id' => $this->programs[0]->id,
            'brand_id' => $this->brand2->id
        ]);

        $this->expectException(\Exception::class);

        $search_params = new SearchObject('inform_before_start', 'sms', 'en', $this->brand2, $this->programs[1]);
        $document = ContentChooser::findDocument($search_params);
    }

    public function test_it_fallsback_to_english_test()
    {

        $key = 'test_entity_avaialble_in_de_and_en_test';

        factory(BrandedDocument::class)->create([
            'key' => $key,
            'type' => 'whatsapp',
            'subject' => null,
            'content' => 'I am the wrong document for you.',
            'language' => 'de',
            'brand_id' => null,
            'audience' => null,
        ]);


        factory(BrandedDocument::class)->create([
            'key' => $key,
            'type' => 'whatsapp',
            'subject' => null,
            'content' => 'I am the right document for you.',
            'language' => 'en',
            'brand_id' => null,
            'audience' => null,
        ]);


        $search_params = new SearchObject($key, 'whatsapp', 'es', $this->brand2, $this->programs[1]);
        $document = ContentChooser::findDocument($search_params);
        $this->assertEquals('en', $document->language);
        $this->assertEquals('I am the right document for you.', $document->content);

    }


    public function test_it_finds_a_branded_document_for_a_program_in_the_right_language_for_key_and_type()
    {
        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'email',
            'subject' => 'Wrong document',
            'content' => 'I am the wrong document for you.',
            'language' => 'en'
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'sms',
            'subject' => 'Wrong document',
            'content' => 'I am the wrong document for you.',
            'language' => 'de',
            'brand_id' => $this->brand1->id
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'email',
            'subject' => 'Wrong document',
            'content' => 'I am the wrong document for you.',
            'language' => 'en',
            'program_id' => $this->programs[0]->id,
            'brand_id' => $this->brand1->id
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'sms',
            'subject' => 'RIGHT document',
            'content' => 'I am the RIGHT document for you.',
            'language' => 'en',
            'program_id' => $this->programs[1]->id,
            'brand_id' => $this->brand2->id
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'sms',
            'subject' => 'Wrong document',
            'content' => 'I am the wrong document for you.',
            'language' => 'en',
            'program_id' => $this->programs[1]->id,
            'brand_id' => $this->brand1->id
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'inform_after_first_course',
            'type' => 'sms',
            'subject' => 'Wrong document',
            'content' => 'I am the wrong document for you.',
            'language' => 'en',
            'program_id' => $this->programs[0]->id,
            'brand_id' => $this->brand2->id
        ]);

        $search_params = new SearchObject('inform_before_start', 'sms', 'en', $this->brand2, $this->programs[1]);
        $document = ContentChooser::findDocument($search_params);
        $this->assertEquals('RIGHT document', $document->subject);
        $this->assertEquals('I am the RIGHT document for you.', $document->content);
    }


    /**
     * @throws \App\Exceptions\Client\NotFoundException
     */
    public function test_it_finds_the_document_with_correct_audience()
    {
        factory(BrandedDocument::class)->create([
            'key' => 'audience_test',
            'type' => 'email',
            'subject' => 'Wrong document',
            'content' => 'I am the wrong document for you.',
            'language' => 'en',
            'audience' => Role::ROLES['mentor']
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'audience_test',
            'type' => 'email',
            'subject' => 'RIGHT document',
            'content' => 'I am the RIGHT document for you.',
            'language' => 'en',
            'audience' => Role::ROLES['mentee']
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'audience_test',
            'type' => 'email',
            'subject' => 'RIGHT document',
            'content' => 'I am the RIGHT document for you.',
            'language' => 'en',
            'audience' => null
        ]);
        $search_params = new SearchObject('audience_test', 'email', 'en', $this->brand2, $this->programs[1], Role::ROLES['mentee']);
        $document = ContentChooser::findDocument($search_params);
        $this->assertEquals('RIGHT document', $document->subject);
    }


    public function test_it_finds_a_branded_document_in_the_right_language_for_key_and_type()
    {
        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'sms',
            'subject' => 'Wrong document1',
            'content' => 'I am the wrong document for you.',
            'language' => 'en'
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'sms',
            'subject' => 'Wrong document2',
            'content' => 'I am the wrong document for you.',
            'language' => 'de',
            'brand_id' => $this->brand1->id
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'sms',
            'subject' => 'Wrong document3',
            'content' => 'I am the wrong document for you.',
            'language' => 'en',
            'program_id' => $this->programs[0]->id,
            'brand_id' => $this->brand1->id
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'sms',
            'subject' => 'Wrong document4',
            'content' => 'I am the wrong document for you.',
            'language' => 'en',
            'program_id' => $this->programs[1]->id,
            'brand_id' => $this->brand2->id
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'sms',
            'subject' => 'Wrong document5',
            'content' => 'I am the wrong document for you.',
            'language' => 'en',
            'program_id' => null,
            'brand_id' => $this->brand2->id
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'inform_after_start',
            'type' => 'sms',
            'subject' => 'Wrong document6',
            'content' => 'I am the wrong document for you.',
            'language' => 'en',
            'program_id' => null,
            'brand_id' => $this->brand1->id
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'sms',
            'subject' => 'RIGHT document',
            'content' => 'I am the RIGHT document for you.',
            'language' => 'en',
            'program_id' => null,
            'brand_id' => $this->brand1->id
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'inform_after_first_course',
            'type' => 'sms',
            'subject' => 'Wrong document7',
            'content' => 'I am the wrong document for you.',
            'language' => 'en',
            'program_id' => $this->programs[0]->id,
            'brand_id' => $this->brand2->id
        ]);

        $search_params = new SearchObject('inform_before_start', 'sms', 'en', $this->brand1);
        $document = ContentChooser::findDocument($search_params);
        $this->assertEquals('RIGHT document', $document->subject);
        $this->assertEquals('I am the RIGHT document for you.', $document->content);
    }

    public function test_it_finds_a_document_for_a_program_in_the_right_language_for_key_and_type()
    {
        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'sms',
            'program_id' => $this->programs[1]->id,
            'subject' => 'Wrong document',
            'content' => 'I am the RIGHT document for you.',
            'language' => 'en'
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'sms',
            'program_id' => $this->programs[1]->id,
            'subject' => 'Wrong document',
            'brand_id' => $this->brand1->id,
            'content' => 'Ich bin das falsche Dokument für dich.',
            'language' => 'de'
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'sms',
            'program_id' => $this->programs[1]->id,
            'subject' => 'RICHTIGES Dokument',
            'content' => 'Ich bin das RICHTIGE Dokument für dich.',
            'language' => 'de'
        ]);

        $search_params = new SearchObject('inform_before_start', 'sms', 'de', null, $this->programs[1]);
        $document = ContentChooser::findDocument($search_params);
        $this->assertEquals('RICHTIGES Dokument', $document->subject);
        $this->assertEquals('Ich bin das RICHTIGE Dokument für dich.', $document->content);
    }

    public function test_it_finds_a_document_in_the_right_language_for_key_and_type()
    {
        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'sms',
            'subject' => 'Wrong document',
            'content' => 'I am the RIGHT document for you.',
            'language' => 'en'
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'sms',
            'subject' => 'Wrong document',
            'brand_id' => $this->brand1->id,
            'content' => 'Ich bin das falsche Dokument für dich.',
            'language' => 'de'
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'sms',
            'subject' => 'RICHTIGES Dokument',
            'content' => 'Ich bin das RICHTIGE Dokument für dich.',
            'language' => 'de'
        ]);

        $search_params = new SearchObject('inform_before_start', 'sms', 'de');
        $document = ContentChooser::findDocument($search_params);
        $this->assertEquals('RICHTIGES Dokument', $document->subject);
        $this->assertEquals('Ich bin das RICHTIGE Dokument für dich.', $document->content);
    }

    public function test_it_falls_back_to_any_document_for_key_and_type()
    {
        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'sms',
            'subject' => 'RIGHT document',
            'content' => 'I am the RIGHT document for you.',
            'language' => 'en'
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'sms',
            'subject' => 'Wrong document',
            'brand_id' => $this->brand1->id,
            'content' => 'Ich bin das falsche Dokument für dich.',
            'language' => 'de'
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'inform_before_start',
            'type' => 'email',
            'subject' => 'Wrong document',
            'content' => 'Ich bin das falsche Dokument für dich.',
            'language' => 'de'
        ]);

        $search_params = new SearchObject('inform_before_start', 'sms', 'de');
        $document = ContentChooser::findDocument($search_params);
        $this->assertEquals('RIGHT document', $document->subject);
        $this->assertEquals('I am the RIGHT document for you.', $document->content);
    }


    public function test_it_finds_a_goodbye_notification()
    {


        $search_params = new SearchObject('user_goodbye_notification', 'email', 'en');
        $document = ContentChooser::findDocument($search_params);
        $this->assertEquals('user_goodbye_notification', $document->key);

    }

}
