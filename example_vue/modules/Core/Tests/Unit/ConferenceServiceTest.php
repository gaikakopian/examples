<?php

namespace Modules\Core\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;
use App\Infrastructure\AbstractTests\MockResponse;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Config;
use Mockery;
use Modules\Core\Domain\Services\ConferenceService;

/*
 * @Todo: add mock for createRoomForMatch
 */
class ConferenceServiceTest extends LaravelTest
{
    protected $httpClient;
    protected $service;

    public function setUp()
    {
        parent::setUp();

        $this->httpClient = Mockery::mock(Client::class);
        $this->service = new ConferenceService($this->httpClient);
    }

    public function test_it_creates_a_conference_room()
    {
//        $this->markTestIncomplete('Seppi knows best?');

        $fakeBackendUrl = 'https://fakeBackend.test/conference';
        // Set a known (fake) base URL for the conference backend
        Config::set('conference.backend_url', $fakeBackendUrl);

        // Create a mock response.
        $response = new MockResponse('{"id": "ab01ca31-6b3b-4809-a518-f7bb2a04f48d"}');

        // Setup mock expectations on the httpClient
        $expectedPayload = [
            'content' => [
                'root' => 'someContentKey',
                'entry' => 'someContentKey',
            ],
            'webhooks' => [
                [
                    'url' => route('conference.callback'),
                    'events' => ['join-room', 'leave-room', 'start-session', 'end-session'],
                ]
            ]
        ];
//        , [
//                'json' => $expectedPayload,
//                'headers' => '*'
//    ]
        $this->httpClient->shouldReceive('request')
            ->once()
            //@todo: this was not accepted since we have no a header.
            ->with('POST', 'https://fakeBackend.test/conference/rooms', \Mockery::any())
            ->andReturn($response);

        $roomId = $this->service->createRoom('someContentKey');
        $this->assertEquals('ab01ca31-6b3b-4809-a518-f7bb2a04f48d', $roomId);
    }

    public function test_it_generates_a_different_token_for_each_user()
    {
        $this->assertNotEquals(
            $this->service->getConferenceTokenForUser(1),
            $this->service->getConferenceTokenForUser(2)
        );

        $this->assertNotEquals(
            $this->service->getConferenceTokenForUser(1),
            $this->service->getConferenceTokenForUser(100)
        );

        $this->assertNotEquals(
            $this->service->getConferenceTokenForUser(100),
            $this->service->getConferenceTokenForUser(1000000)
        );
    }

    public function test_it_generates_the_same_token_for_the_same_user()
    {
        $this->assertEquals(
            $this->service->getConferenceTokenForUser(1),
            $this->service->getConferenceTokenForUser(1)
        );

        $this->assertEquals(
            $this->service->getConferenceTokenForUser(100),
            $this->service->getConferenceTokenForUser(100)
        );

        $this->assertEquals(
            $this->service->getConferenceTokenForUser(1000000),
            $this->service->getConferenceTokenForUser(1000000)
        );
    }

    public function test_it_generates_a_non_trivial_token()
    {
        $this->assertGreaterThanOrEqual(32, strlen($this->service->getConferenceTokenForUser(1)));

        $this->assertNotEquals(
            100,
            $this->service->getConferenceTokenForUser(100)
        );
    }

    public function test_it_prepends_the_user_id_to_the_token()
    {
        $this->assertStringStartsWith('100-', $this->service->getConferenceTokenForUser(100));
    }

    public function test_it_validates_a_token()
    {
        $validToken = $this->service->getConferenceTokenForUser(12);
        $this->assertTrue($this->service->isValidConferenceToken($validToken));

        $this->assertFalse($this->service->isValidConferenceToken('fooBar'));
        $this->assertFalse($this->service->isValidConferenceToken($validToken . 'suffix'));
        $this->assertFalse($this->service->isValidConferenceToken('prefix' . $validToken));
        $this->assertFalse($this->service->isValidConferenceToken('12-' . $validToken));
    }
}
