<?php

namespace Modules\Core\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Modules\Core\Domain\Models\User;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;

/**
 * Test the functionality of the ContentChooser Service.
 */
class AttachmentTest extends LaravelTest
{
    public function test_it_saves_a_posted_file_in_different_styles()
    {
        Storage::fake('public');

        $user = factory(User::class)->create();
        $img = UploadedFile::fake()->image('avatar.jpg');
        $user->updateImageAttachment('avatar', $img, 'image/jpeg');

        $this->assertEquals('.jpeg', substr($user->getAttributes()['avatar'], -5));
        Storage::disk('public')->assertExists("uploads/user/avatar/{$user->partitionId($user->id)}/small/{$user->getAttributes()['avatar']}");
        Storage::disk('public')->assertExists("uploads/user/avatar/{$user->partitionId($user->id)}/medium/{$user->getAttributes()['avatar']}");
        Storage::disk('public')->assertExists("uploads/user/avatar/{$user->partitionId($user->id)}/large/{$user->getAttributes()['avatar']}");
    }

    public function test_it_saves_correct_image_width()
    {
        Storage::fake('public');

        $user = factory(User::class)->create();
        $img_upload = Image::canvas(1200, 600, '#ccc');
        $user->updateImageAttachment('avatar', $img_upload->stream(), 'image/jpeg');
        $saved_image_small = Image::make(Storage::disk('public')->get("uploads/user/avatar/{$user->partitionId($user->id)}/small/{$user->getAttributes()['avatar']}"));
        $saved_image_medium = Image::make(Storage::disk('public')->get("uploads/user/avatar/{$user->partitionId($user->id)}/medium/{$user->getAttributes()['avatar']}"));
        $saved_image_large = Image::make(Storage::disk('public')->get("uploads/user/avatar/{$user->partitionId($user->id)}/large/{$user->getAttributes()['avatar']}"));
        $saved_image_original = Image::make(Storage::disk('public')->get("uploads/user/avatar/{$user->partitionId($user->id)}/original/{$user->getAttributes()['avatar']}"));
        $this->assertEquals(100, $saved_image_small->width());
        $this->assertEquals(500, $saved_image_medium->width());
        $this->assertEquals(1000, $saved_image_large->width());
        $this->assertEquals(1200, $saved_image_original->width());
    }

    public function test_it_doesnt_upscale_image_width()
    {
        Storage::fake('public');

        $user = factory(User::class)->create();
        $img_upload = Image::canvas(800, 600, '#ccc');
        $user->updateImageAttachment('avatar', $img_upload->stream(), 'image/jpeg');
        $saved_image_medium = Image::make(Storage::disk('public')->get("uploads/user/avatar/{$user->partitionId($user->id)}/medium/{$user->getAttributes()['avatar']}"));
        $saved_image_large = Image::make(Storage::disk('public')->get("uploads/user/avatar/{$user->partitionId($user->id)}/large/{$user->getAttributes()['avatar']}"));
        $this->assertEquals(500, $saved_image_medium->width());
        $this->assertEquals(800, $saved_image_large->width());
    }

    public function test_it_returns_the_correct_paths_for_all_styles()
    {
        Storage::fake('public');

        $user = factory(User::class)->create();
        $img = UploadedFile::fake()->image('avatar.jpg');
        $user->updateImageAttachment('avatar', $img, 'image/jpeg');
        $app_url = env('APP_URL');
        $this->assertEquals("/storage/uploads/user/avatar/{$user->partitionId($user->id)}/small/{$user->getAttributes()['avatar']}", $user->avatar['small']);
        $this->assertEquals("/storage/uploads/user/avatar/{$user->partitionId($user->id)}/medium/{$user->getAttributes()['avatar']}", $user->avatar['medium']);
        $this->assertEquals("/storage/uploads/user/avatar/{$user->partitionId($user->id)}/large/{$user->getAttributes()['avatar']}", $user->avatar['large']);
    }

    public function test_it_deletes_an_attachment()
    {
        Storage::fake('public');

        $user = factory(User::class)->create();
        $img = UploadedFile::fake()->image('avatar.jpg');
        $user->updateImageAttachment('avatar', $img, 'image/jpeg');
        $app_url = env('APP_URL');
        $this->assertEquals("/storage/uploads/user/avatar/{$user->partitionId($user->id)}/small/{$user->getAttributes()['avatar']}", $user->avatar['small']);
        $this->assertEquals("/storage/uploads/user/avatar/{$user->partitionId($user->id)}/medium/{$user->getAttributes()['avatar']}", $user->avatar['medium']);
        $this->assertEquals("/storage/uploads/user/avatar/{$user->partitionId($user->id)}/large/{$user->getAttributes()['avatar']}", $user->avatar['large']);

        $user->removeImageAttachment('avatar');

        $user->refresh();

        $this->assertEquals(null, $user->getAttributes()['avatar']);

        Storage::disk('public')->assertMissing("uploads/user/avatar/{$user->partitionId($user->id)}/small/{$user->getAttributes()['avatar']}");
        Storage::disk('public')->assertMissing("uploads/user/avatar/{$user->partitionId($user->id)}/medium/{$user->getAttributes()['avatar']}");
        Storage::disk('public')->assertMissing("uploads/user/avatar/{$user->partitionId($user->id)}/large/{$user->getAttributes()['avatar']}");
    }
}
