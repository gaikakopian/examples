<?php

namespace Modules\Core\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\Core\Domain\Models\Translation;
use Modules\Core\Domain\Services\TranslationService;

class TranslationTest extends LaravelTest
{
    use DatabaseTransactions;

    public function test_all_english()
    {
        $result = TranslationService::retrieveTranslationForLocaleAndScope('EN', 'frontend');

        $english = Translation::select('json_value')->where('locale', '=', 'en')->where('scope', '=',
            'frontend')->first()->json_value;
        $this->assertNotNull($result, 'language service response with empty data');

        $this->assertEquals($result, $english);
    }


    //testing another language that is fully translated
    public function test_all_not_english()
    {
        $result = TranslationService::retrieveTranslationForLocaleAndScope('DE', 'frontend');
        $deutsch = Translation::select('json_value')->where('locale', '=', 'de')->where('scope', '=',
            'frontend')->first()->json_value;
//        $deutsch = json_decode($deutsch, true, 512, JSON_UNESCAPED_UNICODE);
//        $deutsch = json_encode($deutsch, JSON_UNESCAPED_UNICODE);

        $result = json_decode($deutsch);

        $this->assertNotNull($result, 'language service response with empty data');

        $this->assertEquals($result->BYE, 'Tschüss');
        $this->assertEquals($result->HELLO, 'hallo');
    }

//    testing a language that is partially translated
    public function test_combination()
    {
        $json = json_encode(['HELLO' => 'Ohayo']);
        TranslationService::addTranslation(null, 'jr', 'frontend', $json);

        $result = json_decode(TranslationService::retrieveTranslationForLocaleAndScope('jr', 'frontend'), true);
        $english = json_decode(TranslationService::retrieveTranslationForLocaleAndScope('en', 'frontend'), true);

        $this->assertEquals((array_keys($result)), array_keys($english));
        $this->assertEquals($result['BYE'], 'byw'); //en part
        $this->assertEquals($result['HELLO'], 'Ohayo'); // jr part
    }

    public function test_deeper_level()
    {
        $json = json_encode(['HELLO' => 'Ohayo', 'deep' => ['example' => 'overwrite']]);
        TranslationService::addTranslation(null, 'tt', 'frontend', $json);
//        Translation::query()->where('locale','en')->
        $result = json_decode(TranslationService::retrieveTranslationForLocaleAndScope('tt', 'frontend'), true);
        $english = json_decode(TranslationService::retrieveTranslationForLocaleAndScope('en', 'frontend'), true);

        $expected_results = ['BYE' => 'byw', 'HELLO' => 'Ohayo',
            'deep' => ['example' => 'overwrite','example2'=>'deepvalue2']];

        $this->assertEquals((array_keys($result)), array_keys($english));
        $this->assertEquals($expected_results, $result);
    }


    public function test_language_not_in_DB()
    {
        $result = TranslationService::retrieveTranslationForLocaleAndScope('ar', 'frontend');
        $english = TranslationService::retrieveTranslationForLocaleAndScope('en', 'frontend');

        $this->assertEquals($result, $english);
    }
}
