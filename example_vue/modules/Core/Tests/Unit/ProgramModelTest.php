<?php

namespace Modules\Core\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Services\ContentChooser\SearchObject;
use Modules\Core\Domain\Services\ContentChooser\ContentChooser;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\BrandedDocument;
use Illuminate\Support\Facades\Log;

/**
 * Test the functionality of the ContentChooser Service.
 */
class ProgramModelTest extends LaravelTest
{

    public function test_it_gets_custom_links_correctly()
    {

        $program = Program::query()->inRandomOrder()->first();
        $program->custom_links = [
            [
                "id" => 1,
                "role" => "mentor",
                "type" => "EVALUATION_LINK",
                "url" => "http://www.mentor.en",
                "language" => "en"
            ],
            [
                "id" => 2,
                "role" => "mentee",
                "type" => "EVALUATION_LINK",
                "url" => "http://www.mentee.en",
                "language" => "en"
            ],
            [
                "id" => 3,
                "role" => "mentor",
                "type" => "EVALUATION_LINK",
                "url" => "http://www.mentor.es",
                "language" => "es"
            ]
        ];

        $program->save();


        /** @var Program $program */
        $program = Program::query()->where('id', $program->id)->firstOrFail();


        $link = $program->getCustomLink('EVALUATION_LINK', 'mentor', 'en');
        $this->assertNotNull($link);
        $this->assertEquals($link, "http://www.mentor.en");

        $link = $program->getCustomLink('EVALUATION_LINK', 'mentee', 'en');
        $this->assertNotNull($link);
        $this->assertEquals($link, "http://www.mentee.en");

        $link = $program->getCustomLink('EVALUATION_LINK', 'mentor', 'es');
        $this->assertNotNull($link);
        $this->assertEquals($link, "http://www.mentor.es");

        // fallback to mentor en;
        $link = $program->getCustomLink('EVALUATION_LINK', 'mentor', 'de');
        $this->assertNotNull($link);
        $this->assertEquals($link, "http://www.mentor.en");


    }

}
