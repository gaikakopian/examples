<?php

namespace Modules\Core\Listeners\Webinar;

use App\Infrastructure\Domain\EventSubscriberProxy;
use Symfony\Component\EventDispatcher\Event;

class WebinarStateSubscriber extends EventSubscriberProxy
{
    /** {@inheritDoc} */
    protected function mapEvents(): array
    {
        return [
            'Webinar.plan' => 'plan',
            'Webinar.reschedule' => 'reschedule',
            'Webinar.cancel' => 'cancel',
            'Webinar.finish' => 'finish',
        ];
    }

    /** {@inheritDoc} */
    protected function chooseHandler(Event $event)
    {
        return new DefaultWebinarStateHandler();
    }
}
