<?php

namespace Modules\Core\Listeners\Organization;

use App\Infrastructure\Domain\EventSubscriberProxy;
use Symfony\Component\EventDispatcher\Event;

class OrganizationStateSubscriber extends EventSubscriberProxy
{
    /** {@inheritDoc} */
    protected function mapEvents(): array
    {
        return [
            'Organization.perform_it_check' => 'performItCheck',
            'Organization.activate' => 'activate',
            'Organization.endContract' => 'endContract',
        ];
    }

    /** {@inheritDoc} */
    protected function chooseHandler(Event $event)
    {
        return new DefaultOrganizationStateHandler();
    }
}
