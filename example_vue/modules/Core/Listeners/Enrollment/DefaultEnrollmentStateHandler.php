<?php

namespace Modules\Core\Listeners\Enrollment;

use App\Notifications\Enrollment\EnrollmentExcludedNotification;

use App\Notifications\Enrollment\EnrollmentEnrolledNotification;
use App\Notifications\Enrollment\EnrollmentNoreplyNotification;
use App\Notifications\Enrollment\EnrollmentQuitNotification;
use App\Notifications\Enrollment\EnrollmentTrainingCompletedNotification;
use App\Notifications\Enrollment\EnrollmentWebinarMissedYouNotification;
use DateTime;
use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Models\Activity;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ActivityService;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Core\Domain\Services\WebinarService;
use SM\Event\TransitionEvent;

/**
 *
 * Class DefaultEnrollmentStateHandler
 * @package Modules\Core\Listeners\Enrollment
 */
class DefaultEnrollmentStateHandler
{

    /**
     * Handle the `start_training` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function startTraining(TransitionEvent $event): void
    {


        //$this->organizationService = app(OrganizationService::class);
        /** @var Enrollment $enrollment */
        $enrollment = $event->getStateMachine()->getObject();
        /** @var User $user */
        $user = $enrollment->user;

        /** @var User $keyaccount */
        $keyaccount = $user->organization->keyaccount;


        /** @var ActivityService $activityService */
        $activityService = app(ActivityService::class);
        $activityService->logActivityForUser($user, Activity::ACTIVITIES['ENROLLMENT_DONE']);


        if (!$this->isTrainingRequired($enrollment)) {
            $enrollment->transition('complete_training');
            return;
        }

        if ($enrollment->webinars()->count() > 0) {
            $enrollment->transition('register_training');
        }

        $user->notify(new EnrollmentEnrolledNotification($user->brand, $enrollment));


        Log::debug('Handling "start_training" transition', ['event' => $event]);
    }

    /**
     * Handle the `start_training` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function noreply(TransitionEvent $event): void
    {
        /** @var Enrollment $enrollment */
        $enrollment = $event->getStateMachine()->getObject();
        /** @var User $user */
        $user = $enrollment->user;

        $user->notify(new EnrollmentNoreplyNotification($user->brand, $enrollment));
    }

    /**
     * Handle the `start_training` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function quit(TransitionEvent $event): void
    {
        /** @var Enrollment $enrollment */
        $enrollment = $event->getStateMachine()->getObject();
        /** @var User $user */
        $user = $enrollment->user;

        $user->notify(new EnrollmentQuitNotification($user->brand, $enrollment));
    }

    private function isTrainingRequired(Enrollment $enrollment)
    {
        if ($enrollment->role == User::ROLES['mentee'] && !$enrollment->program->has_mentee_webinar) {
            return false;
        }

        if ($enrollment->role == User::ROLES['mentor'] && !$enrollment->program->has_mentor_webinar) {
            return false;
        }

        return true;
    }

    public function completeVideoTraining(TransitionEvent $event): void
    {
        $this->completeTraining($event);
    }

    /**
     * Handle the `complete_training` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function missedTraining(TransitionEvent $event): void
    {
        /** @var Enrollment $enrollment */
        $enrollment = $event->getStateMachine()->getObject();
        /** @var User $user */
        $user = $enrollment->user;

        /** @var WebinarService $webinarService */
        $webinarService = app(WebinarService::class);
        $webinars = $webinarService->listForEnrollment($enrollment);

        //@todo: enable enrollment missed you as soon as the text is here.
//        $user->notify(new EnrollmentWebinarMissedYouNotification($user->brand, $user, $enrollment, $webinars));
    }


    public function completeTraining(TransitionEvent $event): void
    {
        /** @var Enrollment $enrollment */
        $enrollment = $event->getStateMachine()->getObject();
        /** @var User $user */
        $user = $enrollment->user;


        if ($this->isTrainingRequired($enrollment)) {
            $enrollment->update(['training_completed_at' => new DateTime('NOW')]);
            $user->notify(new EnrollmentTrainingCompletedNotification($user->brand, $enrollment));
            /** @var ActivityService $activityService */
            $activityService = app(ActivityService::class);
            $activityService->logActivityForUser($user, Activity::ACTIVITIES['TRAINING_COMPLETED']);
        }

        // Auto-Participate in the program!
        $enrollment->transition('participate');

        Log::debug('Handling "complete_training" transition', ['event' => $event]);
    }

    /**
     * Transition from active -> active
     * User is looking for a new participation;
     *
     * @param TransitionEvent $event
     */
    public function participate(TransitionEvent $event): void
    {

        /** @var Enrollment $enrollment */
        $enrollment = $event->getStateMachine()->getObject();
        /** @var User $user */
        $user = $enrollment->user;

        // Now we create a new participation!
        /** @var ParticipationService $participationService */
        $participationService = app(ParticipationService::class);

        $participationService->createFromEnrollment($enrollment);


        Log::debug('Handling "participate" transition', ['event' => $event]);
    }

    public function exclude(TransitionEvent $event): void
    {

        /** @var Enrollment $enrollment */
        $enrollment = $event->getStateMachine()->getObject();

        /** @var User $user */
        $user = $enrollment->user;
        $user->notify(new EnrollmentExcludedNotification($user->brand, $enrollment));


        Log::debug('Handling "participate" transition', ['event' => $event]);
    }

    //

    /**
     * Handle the `finish` transition event.
     *
     * Actually not yet existing transition
     * @param TransitionEvent $event
     * @return void
     */
    public function finish(TransitionEvent $event): void
    {

        Log::debug('Handling "resume" transition', ['event' => $event]);
    }


    /**
     * Handle the `return_to_available` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function returnToAvailable(TransitionEvent $event): void
    {
        Log::debug('Handling "return_to_available" transition', ['event' => $event]);
    }

    /**
     * Handle the `return_to_training` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function returnToTraining(TransitionEvent $event): void
    {
        Log::debug('Handling "return_to_training" transition', ['event' => $event]);
    }
}
