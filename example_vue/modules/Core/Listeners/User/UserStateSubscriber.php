<?php

namespace Modules\Core\Listeners\User;

use App\Infrastructure\Domain\EventSubscriberProxy;
use Symfony\Component\EventDispatcher\Event;

class UserStateSubscriber extends EventSubscriberProxy
{

    /** {@inheritDoc} */
    protected function mapEvents() : array
    {
        return [
            'User.complete_registration' =>   'completeRegistration',
            'User.quit' => 'quit',
            'User.return' => 'return',
            'User.ban' => 'ban',
            'User.unban' => 'unban',
        ];
    }

    /** {@inheritDoc} */
    protected function chooseHandler(Event $event)
    {
        // Here you could possibly override the event handler based on various criteria.
        //
        // Example:
        // if ($event->program->code = 'eStart')
        // {
        //      return new EstartUserStateHandler();
        // }

        return new DefaultUserStateHandler();
    }
}
