<?php

namespace Modules\Core\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ActivityService;

class UserPublicHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'displayName'=>$this->first_name . ' ' . $this->last_name,
            'firstName' => $this->first_name,
            'lastName' => $this->last_name,
            'gender' => $this->gender,
            'avatar' => $this->avatar,
            'organization' => $this->organization,
        ];
    }
}
