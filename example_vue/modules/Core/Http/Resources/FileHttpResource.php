<?php

namespace Modules\Core\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class FileHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'source' => $this->source,
            'link' => $this->link,
            'cover'=>$this->cover,
            'targetAudience' => $this->target_audience,
            'programId' => $this->program_id,
            'mime' => $this->mime,
            'category' => $this->category,
            'lectionNumber' => $this->lection_number,
            'order' => $this->order,
            'placement' => $this->placement,
            'language' => $this->language,
            'filesize' => $this->filesize,
        ];
    }
}
