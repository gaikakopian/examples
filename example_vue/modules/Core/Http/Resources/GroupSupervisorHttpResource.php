<?php

namespace Modules\Core\Http\Resources;

use App\Infrastructure\Http\HttpResource;

/**
 * Class GroupSupervisorResource
 * @package Modules\Core\Http\Resources
 * @deprecated  replaced by Supervisor module
 *
 */
class GroupSupervisorHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name, // @todo: is missing in current model, will come soon.
            'users' => GroupedUserHttpResource::collection($this->users),
        ];
    }
}
