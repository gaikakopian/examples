<?php

namespace Modules\Core\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ActivityService;

class OrganisationMineResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type,
            'logo' => $this->logo,
            'code' => $this->code,

            // @todo: maybe better provide here some more concrete advise;
            'technicalWebinarCheck'=> $this->technical_webinar_check,
            'technicalGuestWifiCheck'=> $this->technical_guest_wifi_check,
            'technicalDeviceCheck'=> $this->technical_device_check,
            'technicalCompanyNetworkCheck'=> $this->technical_company_network_check,

            'whatsappEnabled' => $this->whatsapp_enabled,
            'communityEnabled'  => $this->community_enabled,
        ];
    }
}
