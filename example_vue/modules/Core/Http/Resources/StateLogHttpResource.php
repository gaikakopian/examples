<?php

namespace Modules\Core\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Admin\Http\Resources\UserMinimalHttpResource;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ActivityService;

class StateLogHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'actorId' => $this->actor_id,
            'transition' => $this->transition,
            'from' => $this->from,
            'to' => $this->to,
            'actor' => new UserMinimalHttpResource($this->whenLoaded('actor')),
            'user' => new UserMinimalHttpResource($this->whenLoaded('user')),
            'createdAt' => $this->created_at->toIso8601String(),
        ];
    }
}
