<?php

namespace Modules\Core\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class RoleOnlyHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return $this->name;
    }
}
