<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\ValidatesWhenResolved;
use App\Exceptions\Client\ValidationException;

class FileUploadRequest extends FormRequest implements ValidatesWhenResolved
{
    /**
     * {@inheritDoc}
     * @throws ValidationException
     */
    public function validate()
    {
        $errors = [];
        $valid_types = ['image/jpeg', 'image/png','inode/x-empty'];
        $maxFilesize = 20000000;

//        $file = $this->file('file');


        //
        // actually usually files where uploaded by $this->file()
        // @todo: $this->>file should also work

//        if (!$file) {
//            $errors[] = 'validation.no_file';
//        }
//
//        if (!$file->isValid()) {
//            $errors[] = 'validation.file_invalid';
//        }
//
//        if ($file->getSize() > $maxFilesize) {
//            $errors[] = 'validation.filesize';
//
//        }
//
//        if (!in_array($file->getMimeType(), $valid_types)) {
//            $errors[] = 'validation.contentg_type';
//        }
//        $this->fileContent();


        if (strlen($this->fileContent()) == 0) {
            $errors[] = 'validation.no_file';
        }

        // Example: 20 MB = 20000000
        if (strlen($this->fileContent()) > $maxFilesize) {
            $errors[] = 'validation.filesize';
        }


        if (!in_array($this->fileType(), $valid_types)) {
            $errors[] = 'validation.content_type';
        }

        if (count($errors)) {
            throw new ValidationException('File upload failed', null, null, ['file' => $errors]);
        }

    }

    /**
     * Returns the sent binary data
     *
     * @return String
     */
    public function fileContent()
    {
        return $this->getContent();
    }

    /**
     * Returns the sent Content-Type Header
     *
     * @return String
     */
    public function fileType()
    {
        return $this->header('Content-Type');
    }
}
