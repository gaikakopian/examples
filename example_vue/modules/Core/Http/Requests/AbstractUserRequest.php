<?php

namespace Modules\Core\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Modules\Core\Domain\Models\User;
use Illuminate\Http\Request;

/**
 * This abstract class contains common validation rules for creating
 * and updating users and is not intended to be used directly.
 */
abstract class AbstractUserRequest extends DeserializedFormRequest
{
    /**
     * Remove whitespaces from phone number
     * {@inheritDoc}
     */
    public function all($keys = null)
    {
        $attributes = parent::all();
        $numberOnlyFields = ['phone_number', 'whatsapp_number', 'whatsapp_number_prefix', 'phone_number_prefix'];

        foreach ($numberOnlyFields as $field) {
            if (isset($attributes[$field])) {
                $attributes[$field] = preg_replace('/[^0-9]/', '', $attributes[$field]);
            }
        }

        return $attributes;
    }

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'first_name' => 'string|min:2',
            'last_name' => 'string|min:2',
            'gender' => 'string|in:female,male',
            'birthday' => 'date|before:today',
            'phone_number' => [
                $this->userHasPhoneNumber() ? 'sometimes' : 'required_if:accept_sms,true',
                'regex:/^\d{1,14}$/',
            ],
            'city' => 'sometimes|string',
            'postcode' => 'sometimes|string',
            'country' => 'string',
            'country_of_origin' => 'string',

            'accept_sms' => 'boolean',
            'accept_email' => 'boolean',
            'accept_push' => 'boolean',
            'address' => 'string|nullable',
            'about_me' => 'string',
            'position' => 'string',
            'state' => 'string',
            'timezone' => 'string',
            'language' => 'string',

//            'is_anonymized' => 'boolean',
//            'last_reminder' => 'date',
//            'last_login' => 'date',
        ];
    }

    /**
     * Check whether the user has a phone number in the DB.
     *
     * @return boolean
     */
    protected function userHasPhoneNumber(): bool
    {
        return User::where('id', $this->get('id') ?: Auth::id())
                ->whereNotNull('phone_number')->count() > 0;
    }
}
