<?php

namespace Modules\Core\Http\Requests;

class RegisterUserRequest extends AbstractUserRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'code' => 'required|exists:registration_codes,code',
            'email' => 'required|email|unique:users',
            'password' => [
                'required',
                'min:6',
                'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!;@\*\.$#%]).*$/'
            ]
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function authorize()
    {
        return true;
    }
}
