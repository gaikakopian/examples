<?php

namespace Modules\Core\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;
use Illuminate\Support\Facades\Auth;

class AddCallcheckRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'success' => 'required|boolean',
//            'download_speed' => 'numeric',
//            'upload_speed' => 'numeric',
            'video_mos' => 'numeric',
            'audio_mos' => 'numeric',
            'packet_loss_ratio' => 'numeric',

            // 'meta'=> json?
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function authorize()
    {
        return true;
    }
}
