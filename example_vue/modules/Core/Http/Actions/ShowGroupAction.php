<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Http\Resources\GroupHttpResource;

class ShowGroupAction extends Action
{
    protected $groupService;
    protected $responder;

    public function __construct(GroupService $groupService, ResourceResponder $responder)
    {
        $this->groupService = $groupService;
        $this->responder = $responder;
    }

    public function __invoke(int $id, Request $request)
    {
        $enrollment = $this->groupService->getForUser(Auth::id(), $id);

        return $this->responder->send($enrollment, GroupHttpResource::class);
    }
}
