<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Services\ContentChooser\ContentChooser;
use Modules\Core\Domain\Services\ContentChooser\SearchObject;
use Modules\Core\Domain\Services\RegistrationCodeService;
use Modules\Core\Http\Resources\ArticleHttpResource;

class ShowArticleForRegistrationCodeAction extends Action
{
    protected $responder;
    protected $registrationCodeService;


    public function __construct(ResourceResponder $responder, RegistrationCodeService $registrationCodeService)
    {
        $this->responder = $responder;
        $this->registrationCodeService = $registrationCodeService;
    }

    public function __invoke(string $code, Request $request)
    {

        /** @var RegistrationCode $registrationCode */
        $registrationCode = $this->registrationCodeService->getByCode($code);



        $program = $registrationCode->autoEnrollIn;

        $language = $request->input('lang', 'en');
        $brand = $request->input('brand', null);
        $key = $request->input('article', 'registration_welcome');

        $searchBy = new SearchObject(
            $key,
            'article',
            $language,
            $brand,
            $program,
            $registrationCode->primary_role
        );

        $brandedDocument = ContentChooser::findDocument($searchBy);
        if (!$brandedDocument) {
            return abort(404);
        }

        $placeholderData = [];

        if ($program) {
            $placeholderData['program'] = $program;
        }

        $brandedDocument->content = $brandedDocument->parseContent($placeholderData);

        return $this->responder->send($brandedDocument, ArticleHttpResource::class);
    }
}
