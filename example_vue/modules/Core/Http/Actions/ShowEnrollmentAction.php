<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Http\Resources\EnrollmentHttpResource;

class ShowEnrollmentAction extends Action
{
    protected $enrollmentService;
    protected $responder;

    public function __construct(EnrollmentService $enrollmentService, ResourceResponder $responder)
    {
        $this->enrollmentService = $enrollmentService;
        $this->responder = $responder;
    }

    public function __invoke(int $id, Request $request)
    {
        $enrollment = $this->enrollmentService->getForUser(Auth::id(), $id);

        return $this->responder->send($enrollment, EnrollmentHttpResource::class);
    }
}
