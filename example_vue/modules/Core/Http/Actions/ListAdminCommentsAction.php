<?php

namespace Modules\Core\Http\Actions;

use Modules\Core\Http\Resources\CommentHttpResource;
use Illuminate\Http\Request;
use App\Infrastructure\Http\ResourceResponder;
use App\Infrastructure\Http\Action;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\Client\NotAuthorizedException;

class ListAdminCommentsAction extends Action
{
    public function __construct(ResourceResponder $responder)
    {
        $this->responder = $responder;
    }

    public function __invoke(Request $request)
    {

        $commentable_instance = $request->commentable_instance;


        // @todo: neeed to comment the idea of this.
        $query = $commentable_instance->comments()->orderBy('created_at','desc');
        $query->with('author');

        return $this->responder->send($query->get(), CommentHttpResource::class);
    }
}
