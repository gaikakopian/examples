<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Activity;
use Modules\Core\Domain\Services\ActivityService;
use Modules\Core\Domain\Services\UserInvitationService;
use Modules\Core\Http\Requests\InviteUserRequest;

class InviteUserAction extends Action
{
    protected $userInvitationService;
    protected $activityService;

    protected $responder;

    public function __construct(UserInvitationService $userInvitationService, ResourceResponder $responder, ActivityService $activityService)
    {
        $this->userInvitationService = $userInvitationService;
        $this->activityService = $activityService;
        $this->responder = $responder;
    }


    public function __invoke(InviteUserRequest $request)
    {
        $invited_user_email= $request->input('invited_user_email');
        $inviting_user = Auth::user();


        $this->activityService->logActivityForUser($inviting_user, Activity::ACTIVITIES['INVITED_USER']);


        $this->userInvitationService->sendInvitationEmail($inviting_user, $invited_user_email);

        return response('', 200);
    }
}
