<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Services\ConferenceService;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Core\Http\Resources\ParticipationHttpResource;
use Modules\Matching\Domain\Models\Match;

class ListMyParticipationsAction extends Action
{
    protected $participationService;
    protected $responder;

    public function __construct(ParticipationService $participationService, ResourceResponder $responder)
    {
        $this->participationService = $participationService;
        $this->responder = $responder;
    }

    public function __invoke(Request $request)
    {
        $userId = Auth::id();
        $participations = $this->participationService->listActiveForUser($userId);
        $participations->load('enrollment');
        $participations->load('match');

        $participations = $this->participationService->filterInactiveParticipations($participations);

        return $this->responder->send($participations, ParticipationHttpResource::class);
    }
}
