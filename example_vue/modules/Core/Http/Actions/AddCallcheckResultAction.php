<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Models\UserCallcheckResult;
use Modules\Core\Domain\Models\UserLaunch;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Requests\AddCallcheckRequest;
use Modules\Core\Http\Requests\LaunchSessionRequest;

class AddCallcheckResultAction extends Action
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Update the User's timezone and log their Browser Version to the console.
     *
     * @param LaunchSessionRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(AddCallcheckRequest $request)
    {
        $myId = Auth::id();

        $callcheckResult = new UserCallcheckResult();
        $callcheckResult->fill($request->all());
        $callcheckResult->user_id = $myId;

        $callcheckResult->save();

        return response('ok', 200);
    }

}
