<?php

namespace Modules\Core\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use App\Services\MultiTenant\Facades\MultiTenant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\RegistrationCodeService;
use Modules\Core\Domain\Services\UserFeedbackService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Requests\AddFeedbackRequest;
use Modules\Core\Http\Requests\RegisterUserRequest;
use Modules\Core\Http\Resources\UserHttpResource;
use Symfony\Component\Finder\Exception\AccessDeniedException;

class ShowConfirmMigrationAction extends Action
{

    protected $responder;
    protected $userService;


    public function __construct(
        ResourceResponder $responder,
        UserService $userService

    )
    {
        $this->userService = $userService;

    }

    public function __invoke(Request $request)
    {

        $secret = $request->secret;
        $id = $request->id;
        /** @var User $user */
        $user = $this->userService->get($id);

        App::setLocale($user->language);

        if ($user->emailSecret('CONFIRM_MIGRATION') !== $secret) {
            throw new AccessDeniedException("Secret invalid");
        }


        if ($user->transitionAllowed('confirmmigration')) {
            $user->transition('confirmmigration');
        }


        $url = $user->brand->frontend_url;


        return view('responses.migrated', ['link' => $url]);


    }

}
