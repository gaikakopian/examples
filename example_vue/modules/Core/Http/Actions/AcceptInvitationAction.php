<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Modules\Core\Domain\Services\UserInvitationService;

class AcceptInvitationAction extends Action
{
    protected $userInvitationService;

    public function __construct(UserInvitationService $userInvitationService)
    {
        $this->userInvitationService = $userInvitationService;
    }

    public function __invoke($uid, Request $request)
    {
        // @todo: in the future this link should include automatically the registation code, would make more sense.
        $invitation = $this->userInvitationService->acceptInvitation($uid);
        $url = Config::get('services.platform.registration_url');
        if (empty($url)) {
            throw new \Exception("'services.platform.registration_url' not set in configuration");
        }

        return redirect($url);
    }
}
