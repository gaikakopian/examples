<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\WebinarService;
use Modules\Core\Http\Resources\WebinarHttpResource;

class ListWebinarsForEnrollmentAction extends Action
{
    protected $webinarService;
    protected $enrollmentService;
    protected $responder;

    public function __construct(WebinarService $webinarService, EnrollmentService $enrollmentService, ResourceResponder $responder)
    {
        $this->webinarService = $webinarService;
        $this->enrollmentService = $enrollmentService;
        $this->responder = $responder;
    }

    public function __invoke($id, Request $request)
    {
        /**
         * @var $enrollment Enrollment
         */
        $enrollment = $this->enrollmentService->get($id);

        $webinars = $this->webinarService->listForEnrollment($enrollment);

        return $this->responder->send($webinars, WebinarHttpResource::class);
    }
}
