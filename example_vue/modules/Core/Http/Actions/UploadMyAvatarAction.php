<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Resources\UserHttpResource;
use Modules\Core\Http\Requests\FileUploadRequest;

class UploadMyAvatarAction extends Action
{
    protected $userService;
    protected $responder;

    public function __construct(UserService $userService, ResourceResponder $responder)
    {
        $this->userService = $userService;
        $this->responder = $responder;
    }

    public function __invoke(FileUploadRequest $request)
    {
        $me = $this->userService->get(Auth::id());
        $me->updateImageAttachment('avatar', $request->fileContent(), $request->fileType());

        return $this->responder->send($me, UserHttpResource::class);
    }
}
