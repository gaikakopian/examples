<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\Query;
use App\Infrastructure\Http\QueryParams;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Resources\UserHttpResource;
use Modules\External\Domain\Services\FirebaseService;

class ShowMeAction extends Action
{
    protected $userService;
    protected $firebaseService;
    protected $responder;

    public function __construct(UserService $userService, FirebaseService $firebaseService, ResourceResponder $responder)
    {
        $this->userService = $userService;
        $this->responder = $responder;
        $this->firebaseService = $firebaseService;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \App\Exceptions\Client\ClientException
     * @throws \App\Exceptions\Client\NotAuthenticatedException
     */
    public function __invoke(Request $request)
    {
        $myId = Auth::id();
        $query = new QueryParams();
        $query->setIncludes('roles');


        /**
         * @var User $me
         */
        $me = $this->userService->get($myId, $query);
        $me->load('roles');

        if ($me->transitionAllowed('confirmmigration')) {
            $me->transition('confirmmigration');
        }

        $me->isAllowedToLogin();
        $me->firebaseToken = $this->firebaseService->getAuthTokenForUser($me);


        return $this->responder->send($me, UserHttpResource::class);
    }
}
