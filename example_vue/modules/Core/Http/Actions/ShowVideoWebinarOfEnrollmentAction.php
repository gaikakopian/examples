<?php

namespace Modules\Core\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Exceptions\Client\NotFoundException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Webinar;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\WebinarService;
use Modules\Core\Http\Resources\EnrollmentHttpResource;
use Modules\Core\Http\Requests\EnrollToWebinarRequest;

class ShowVideoWebinarOfEnrollmentAction extends Action
{
    protected $enrollmentService;
    protected $responder;

    public function __construct(
        EnrollmentService $enrollmentService,
        ResourceResponder $responder
    )
    {
        $this->enrollmentService = $enrollmentService;
        $this->responder = $responder;
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function __invoke(int $id)
    {
        /** @var Enrollment $enrollment */
        $enrollment = $this->enrollmentService->get($id);

        /** @var Program $program */
        $program = $enrollment->program;


        $user = Auth::user();

        try {
            $videolink = $program->getCustomLink('WEBINAR_VIDEO', $enrollment->role, $user->getPreferredLanguageAttribute());
        } catch (\Exception $e) {
            throw new NotFoundException('This Program has no video webinar set');
        }

        return response()->json(['data' => ['videolink' => $videolink]]);


    }
}
