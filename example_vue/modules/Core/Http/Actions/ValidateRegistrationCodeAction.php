<?php

namespace Modules\Core\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Exceptions\Client\NotFoundException;
use App\Exceptions\Client\ValidationException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Admin\Http\Resources\RegistrationCodePublicHttpResource;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Services\RegistrationCodeService;
use Modules\Core\Http\Requests\ValidateRegistrationCodeRequest;

class ValidateRegistrationCodeAction extends Action
{
    /**
     * @var OrganizationService $organizationService
     */
    protected $registrationCodeService;

    protected $responder;

    public function __construct(RegistrationCodeService $registrationCodeService, ResourceResponder $responder)
    {
        $this->registrationCodeService = $registrationCodeService;
        $this->responder = $responder;

    }


    public function __invoke(ValidateRegistrationCodeRequest $request)
    {


        // We can respond with 200 in any case, as the check whether the code
        // is valid takes place in the ValidateRegistrationCodeRequest,
        // thus: Whenever this action is called, the code is valid.

        
        try {
            $code =  $this->registrationCodeService->getByCode($request->code);
        } catch (ModelNotFoundException $e){
            throw new ValidationException('validation.codenotfound');
        }


        return $this->responder->send($code, RegistrationCodePublicHttpResource::class);

    }
}
