<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Core\Http\Resources\EnrollmentHttpResource;

class ListMyEnrollmentsAction extends Action
{
    protected $enrollmentService;
    protected $participationService;
    protected $responder;

    public function __construct(EnrollmentService $enrollmentService, ResourceResponder $responder, ParticipationService $participationService)
    {
        $this->enrollmentService = $enrollmentService;
        $this->participationService = $participationService;
        $this->responder = $responder;
    }

    public function __invoke(Request $request)
    {
        $enrollments = $this->enrollmentService->listForUser(Auth::id());


        foreach ($enrollments as $enrollment){
            $enrollment->participations = $this->participationService->filterInactiveParticipations($enrollment->participations);
        }

        return $this->responder->send($enrollments, EnrollmentHttpResource::class);
    }
}
