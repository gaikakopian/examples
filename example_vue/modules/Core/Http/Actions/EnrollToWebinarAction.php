<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Webinar;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\WebinarService;
use Modules\Core\Http\Resources\EnrollmentHttpResource;
use Modules\Core\Http\Requests\EnrollToWebinarRequest;

class EnrollToWebinarAction extends Action
{
    protected $webinarService;
    protected $responder;
    protected $enrollmentService;

    public function __construct(
        WebinarService $webinarService,
        EnrollmentService $enrollmentService,
        ResourceResponder $responder
    )
    {
        $this->webinarService = $webinarService;
        $this->enrollmentService = $enrollmentService;
        $this->responder = $responder;
    }

    public function __invoke(int $id, int $webinar_id, EnrollToWebinarRequest $request)
    {
        /** @var Webinar $webinar */
        $webinar = $this->webinarService->get($webinar_id);
        $user = Auth::user();
        $enrollmentId = $id;


        if ($user->cannot('enroll', [$webinar, $enrollmentId])) {
            return response('', 403);
        }

        /** @var Enrollment $enrollment */
        $enrollment = $this->enrollmentService->get($enrollmentId);
        $this->webinarService->enrollUser($enrollmentId, $webinar);

//        this happens at the end,
//        if ($enrollment->transitionAllowed('register_training')) {
//            $enrollment->transition('register_training');
//        }


        return response('', 200);
    }
}
