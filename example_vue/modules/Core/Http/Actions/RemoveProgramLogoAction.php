<?php

namespace Modules\Core\Http\Actions;

use Modules\Core\Http\Actions\RemoveProgramImageAction;

class RemoveProgramLogoAction extends RemoveProgramImageAction
{
    protected $attachmentName = 'logo';
}
