<?php

namespace Modules\External\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;

use App\Notifications\User\UserRegistrationMobileAcceptedNotification;
use App\Services\ChatbotService;
use App\Services\WaboboxService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Log;


use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\SmsLog;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Services\ConferenceService;
use Modules\Core\Domain\Services\UserFeedbackService;
use Modules\Core\Domain\Services\UserService;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Matching\Http\Requests\AddFeedbackRequest;
use Modules\Matching\Http\Resources\MatchHttpResource;
use App\Exceptions\Client\NotAuthorizedException;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Scheduling\Domain\Models\Appointment;

/**
 * Class CallMatchAction
 * @package Modules\Matching\Http\Actions
 */
class WaboboxHookAction extends Action
{

    protected $userService;
    protected $waboxService;
    protected $matchService;
    protected $chatbotService;

    public function __construct(
        MatchService $matchService,
        UserFeedbackService $userFeedbackService,
        UserService $userService,
        WaboboxService $waboboxService,
        ChatbotService $chatbotService
    )
    {
        $this->matchService = $matchService;
        $this->userService = $userService;
        $this->waboxService = $waboboxService;
        $this->chatbotService = $chatbotService;


    }
    /***
     * Incoming message example
     * (message):
     *      {"event":"message","token":"5f8395e79198a5d595ab82debe6540725a66fb9ec0fc9","uid":"4915208656328","contact":{"uid":"491776706937","name":"mentee-demo GS (mentee)","type":"user"},"message":{"dtm":"1542387819","uid":"B06BB29B9836725D3AA33135D82BC752","cuid":null,"dir":"i","type":"chat","body":{"text":"Ok!"},"ack":"3"}}
     * (we send a message): ACK:
     * {"event":"message","token":"5f8395e79198a5d595ab82debe6540725a66fb9ec0fc9","uid":"4915208656328","contact":{"uid":"491776706937","name":"mentee-demo GS (mentee)","type":"user"},"message":{"dtm":"1542387919","uid":null,"cuid":"400c7d8e0adaa6193aa7537054f54284","dir":"o","type":"chat","body":{"text":"Hello Simon! thanks for reaching out"},"ack":"-1"}}
     * user replies:
     * {"event":"message","token":"5f8395e79198a5d595ab82debe6540725a66fb9ec0fc9","uid":"4915208656328","contact":{"uid":"491776706937","name":"mentee-demo GS (mentee)","type":"user"},"message":{"dtm":"1542387939","uid":"6C0BCDA51F366CAFAA426708E5F5C6AF","cuid":null,"dir":"i","type":"chat","body":{"text":"Of course!"},"ack":"3"}}
     */

    /**
     *
     * @param $id
     * @param AddFeedbackRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ClientException
     */
    public function __invoke(Request $request)
    {

        Log::warning('[wabobox] got incoming message' . json_encode($request->all()));


        $params = $request->all();

        $event = $params['event'];


        // @todo: this all shoubd be moved to waboxserivce;

        switch ($event) {
            case 'ack':
                $this->handleAckEvent($request, $params);
                break;
            case 'message':
                $this->handleMessageEvent($request, $params);
                break;
            default:
                throw new \InvalidArgumentException();
        }

        return response()->json(['message' => 'ok']);
    }

    private function handleAckEvent(Request $request, $params)
    {


        $cuid = $params['cuid']; // string = 75217f8bf0d2a904379a94980cdbffa5
        $ack = $params['ack'];

//        $params = [
//            "event" => "ack",
//            "token" => "abcd1234",
//            "uid" => "74397B58E3ELZ",
//            "cuid" => "your",
//            "ack" => 3
//        ];

        $log = SmsLog::query()->where(['unique_message_id' => $cuid])->first();
        if (!$log) {
            return;
        }
        $log->whatsapp_status = $ack;
        $log->save();

    }

    /**
     * @param Request $request
     * @param $params
     */
    private function handleMessageEvent(Request $request, $params)
    {
//  change user's whatsapp_status

        $contactuid = $params['contact']['uid'];
        $body = isset($params['message']['body']) && isset($params['message']['body']['text']) ? $params['message']['body']['text'] : '';
        $direction  = $params['message']['dir']; // i / o == input / output
        $ack = $params['message']['ack'];

        $whatsappcuid = $params['message']['cuid'];

        Log::debug("Wabox Hook: " . json_encode($params));

        $user = $this->userService->findUserByInternationalPhoneNumber($contactuid);


        $smsLog = $this->waboxService->logIncomingWhatsapp($contactuid, $user, $body, $ack, $direction, $whatsappcuid);

        /// don't process outgoing messages or if the user is not known;
        if ($user) {

            if ($direction === 'o'){
                $this->chatbotService->handleOutgoingMessage($user, $body);
            } else {
                $handled = $this->chatbotService->handleIncomingMessage($user, $body);

                if ($handled) {
                    $smsLog->reviewed_at = Carbon::now();
                    $smsLog->save();
                }
            }

        }
    }


}
