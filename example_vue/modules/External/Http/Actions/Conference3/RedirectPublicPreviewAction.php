<?php

namespace Modules\External\Http\Actions\Conference3;

use App\Exceptions\Client\NotAuthorizedException;
use App\Exceptions\Client\NotFoundException;
use App\Infrastructure\Http\Action;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Domain\Services\UserService;
use Modules\External\Domain\Services\Conference3Service;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RedirectPublicPreviewAction extends Action
{

    /**
     * @var UserService
     */
    protected $userService;

    protected $conference3Service;


    public function __construct(UserService $userService, Conference3Service $conference3Service, ProgramService $programService)
    {
        $this->userService = $userService;
        $this->conference3Service = $conference3Service;
        $this->programService = $programService;
    }

    /**
     * Handle the request.
     *
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request, string $hash, int $programid)
    {

        /** @var Program $program */
        $program = $this->programService->get($programid);
        if ($program->secretHash() !== $hash) {
            throw new NotAuthorizedException("Invalid hash");
        }
        $roomId = $this->conference3Service->getDemoRoomId($program);
        $user = $this->userService->findDemoMentor();
        return redirect($this->conference3Service->getExternalLinkForRoomId($roomId, $user,'demo'));

    }

}
