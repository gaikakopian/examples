<?php

namespace Modules\External\Http\Actions\Conference3;

use App\Exceptions\Client\NotAuthorizedException;
use App\Exceptions\Client\NotFoundException;
use App\Infrastructure\Http\Action;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\UserService;
use Modules\External\Domain\Services\Conference3Service;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;

class AuthorizeRoomAction extends Action
{
    /**
     * An instance of the MatchService.
     *
     * @var MatchService
     */
    protected $matchService;

    /**
     * @var UserService
     */
    protected $userService;
    /**
     * @var Collection
     */
    protected $enrollments;

    protected $conference3Service;

    /**
     * Construct an instance of the action.
     *
     * @param MatchService $matchService
     */
    public function __construct(MatchService $matchService, UserService $userService, Conference3Service $conference3Service)
    {
        $this->matchService = $matchService;
        $this->userService = $userService;
        $this->conference3Service = $conference3Service;

    }


    /**
     * Handle the request.
     *
     * @param Request $request
     * @return Response
     * @throws NotAuthorizedException
     */
    public function __invoke(Request $request)
    {
        $userId = $request->input('userId');
        $externalRoomId = $request->input('roomId');

        $user = $this->find_user_or_null($userId);

        if ($user && $user->isAdmin()) {
            return response()
                ->json(['message' => 'The user is admin and so he is allowed.', 'id' => $userId, 'isDriver' => true])
                ->setStatusCode(200);
        }

        if ($user
            && $this->conference3Service->isDemoRoom($externalRoomId)
            && $this->userService->isDemoMentor($user)) {
            return response()
                ->json(['message' => 'The user is allowed to enter demo room.', 'id' => $userId, 'isDriver' => true])
                ->setStatusCode(200);
        }


        /** @var Match $match */
        $match = $this->try_find_match($externalRoomId);

        if (!$match) {
            throw new NotAuthorizedException('RoomId invalid or unknown');
        }

        $allowToAccessWithRole = $this->find_and_try_match($userId, $match);
        // usual user access;
        if ($allowToAccessWithRole) {
            $isDriver = $allowToAccessWithRole === Role::ROLES['mentor'];

            return response()
                ->json(['message' => 'The match was found and the user is allowed.', 'id' => $userId, 'isDriver' => $isDriver])
                ->setStatusCode(200);
        }

        // admin access

        // no access
        throw new NotAuthorizedException('The user is not authorized to join the room.');
    }

    private function try_find_match($externalRoomId)
    {
        try {
            $match = $this->matchService->getByExternalRoomId($externalRoomId);
        } catch (ModelNotFoundException $e) {
            // match does not exist
            return null;
        }
        return $match;

    }
    private function find_user_or_null(int $userId)
    {
        try {
            /** @var User $user */
            $user = $this->userService->get($userId);
        } catch (NotFoundException $e) {
            return null;
        }
        return $user;

    }

    private function find_and_try_admin(int $userId)
    {

//        var_dump($user->hasRole('ADMIN'));
        // @todo: eigentlich ist man nur Admin für 1 tenant - so kann jeder Admin jeden Conferenec Room joien.
        if (!$user->isAdmin()) {
            return false;
        }

        return true;
    }

    /**
     * @param int $userId
     * @param $externalRoomId
     * @return bool
     */
    private function find_and_try_match(int $userId, Match $match)
    {

        /**
         * @var $userIdsInMatch Collection
         */

        /** @var Collection $enrollments */
        $enrollments = $match->enrollments;


        $enrollment = $enrollments->firstWhere('user_id', $userId);
        if (!$enrollment) {
            return false;
        }

        return $enrollment->role;
    }
}
