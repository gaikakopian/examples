<?php

namespace Modules\External\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class LandingRegisterRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'webinar_id' => 'nullable|integer',
            'name' => 'required|string',
            'email' => 'required|string'
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function authorize()
    {
        // Authorization handled via Scope Middleware.
        return true;
    }
}
