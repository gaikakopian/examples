<?php

namespace Modules\External\Tests\Http\Conference3;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v1/conference/auth/room` endpoint.
 */
class ConferenceGetDemoRoomTest extends EndpointTest
{
    public function test_it_allows_a_demo_room()
    {
        $userId = $this->user->id;
        $program = Program::query()->inRandomOrder()->first();

        $externalRoomId = 'DEMO-' . $program->id . '-' . rand(0, 1000);


        $response = $this->json('GET', '/api/v2/conference3/rooms/' . $externalRoomId);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJson(['data' => [
            'content' =>
                [
                    'entry' => $program->conference_entry_url,
                    'root' => $program->conference_entry_url
                ],
            'access' => 'public', // should be public for demo;
            'video' => ['provider' => 'twilio']
        ]]);


    }


}
