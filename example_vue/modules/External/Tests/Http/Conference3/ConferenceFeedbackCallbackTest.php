<?php

namespace Modules\External\Tests\Http\Conference3;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Modules\Core\Domain\Models\UserFeedback;

/**
 * @todo: add real events (e.g. appointment online, appointment end)
 *
 * Test the functionality of the `/api/v1/conference/callback` endpoint.
 *
 * method: 'POST',
 * path: `/appointments/for-room/${this.room.id}`,
 * payload: {
 * date: date
 * }
 * })
 */
class ConferenceFeedbackCallbackTest extends ConferenceEndpointTest
{
    public function test_it_mocks_demo_feedback()
    {
        $user = $this->user;

        $match = $user->enrollments->first()->participations->first()->match;
        $match->external_room_id = 12345;
        $match->save();

        $date = Carbon::now()->addDays(7);

        $payload = [
            'feedbacks' => [
                ['code' => UserFeedback::CODES['CONNECTION_QUALITY'], 'rating' => 3, 'text' => 'Hello'],
                ['code' => UserFeedback::CODES['GENERAL_COMMENT'], 'rating' => null, 'text' => 'Genral Comment'],
            ]
        ];
        $signature = $this->getRequestSignature($payload);

        $response = $this->json(
            'POST',
            '/api/v2/conference3/rooms/DEMO-1-123123/feedbacks',
            $payload,
            ['X-VV-Signature' => $signature, "X-VV-User-Id" => $user->id]
        );
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'added' => 3
        ], $response->getContent());

    }
    public function test_it_creates_a_new_feedback()
    {


        $user = $this->user;

        $match = $user->enrollments->first()->participations->first()->match;
        $match->external_room_id = 12345;
        $match->save();

        $date = Carbon::now()->addDays(7);

        $payload = [
            'feedbacks' => [
                ['code' => UserFeedback::CODES['CONNECTION_QUALITY'], 'rating' => 3, 'text' => 'Hello'],
                ['code' => UserFeedback::CODES['GENERAL_COMMENT'], 'rating' => null, 'text' => 'Genral Comment'],
            ]
        ];
        $signature = $this->getRequestSignature($payload);

        $response = $this->json(
            'POST',
            '/api/v2/conference3/rooms/' . $match->external_room_id . '/feedbacks',
            $payload,
            ['X-VV-Signature' => $signature, "X-VV-User-Id" => $user->id]
        );
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'added' => 2
        ], $response->getContent());
    }
}
