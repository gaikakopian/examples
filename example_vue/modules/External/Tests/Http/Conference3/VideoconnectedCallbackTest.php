<?php

namespace Modules\External\Tests\Http\Conference3;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\External\Tests\Http\Conference3\ConferenceEndpointTest;
use Modules\Scheduling\Domain\Models\Appointment;

/**
 * @todo: add real events (e.g. appointment online, appointment end)
 *
 * Test the functionality of the `/api/v1/conference/callback` endpoint.
 *
 * method: 'POST',
 * path: `/appointments/for-room/${this.room.id}`,
 * payload: {
 * date: date
 * }
 * })
 */
class VideoconnectedCallbackTest extends ConferenceEndpointTest
{

    public function test_it_accepts_video_callback()
    {


        $user = $this->user;

        $match = $user->enrollments->first()->participations->first()->match;
        $match->external_room_id = 12345;
        $match->save();
        $pastAppoinment = factory(Appointment::class)->create(['plannedStart' => Carbon::now()->subMinute(60), 'state' => Appointment::STATES['PLANNED'], 'match_id'=> $match->id]);;

        $date = Carbon::now()->addDays(7);

        $payload = [];

        $signature = $this->getRequestSignature($payload);

        $response = $this->json(
            'POST',
            '/api/v2/conference3/rooms/' . $match->external_room_id . '/videoconnected',
            $payload,
            ['X-VV-Signature' => $signature, "X-VV-User-Id" => $user->id]
        );
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'appointment' => $pastAppoinment->id
        ], $response->getContent());
    }
    public function test_it_accepts_demo_callback()
    {


        $payload = [];

        $signature = $this->getRequestSignature($payload);
        $response = $this->json(
            'POST',
            '/api/v2/conference3/rooms/DEMO-1231-22/videoconnected',
            $payload,
            ['X-VV-Signature' => $signature, "X-VV-User-Id" => 1]
        );
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJson([], $response->getContent());
    }
}
