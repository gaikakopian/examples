<?php

namespace Modules\External\Tests\Http\Conference3;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v1/conference/auth/room` endpoint.
 */
class ConferenceGetRoomTest extends EndpointTest
{
    public function test_it_responds_with_200_when_a_user_may_join_a_room()
    {
        $userId = $this->user->id;
        $externalRoomId = '8579d842-309a-4947-a992-fce85db4ce8b';

        $match = $this->user->enrollments->first()->participations->first()->match;
        $match->update([
            'external_room_id' => $externalRoomId
        ]);


        $response = $this->json('GET', '/api/v2/conference3/rooms/'.$externalRoomId);
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


    }


}
