<?php

namespace Modules\External\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Matching\Domain\Models\Match;

/**
 * Test the functionality of the `/api/v1/matches/{id}/confirm` endpoint.
 */
class TwilioSmsHookTest extends EndpointTest
{
    public function test_it_receives_a_short_message()
    {
        // Disable model events, as we don't want to call the ConferenceService during this test
        Match::flushEventListeners();


        $response = $this->postJson("/api/v2/external/twilio/smshook", [
            'from'=> '491776706937',
            'to'=> env('TWILIO_FROM','00000'),
            'body' => 'Hello Simon'
        ]);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $this->assertDatabaseHas('sms_logs', [
            'type'=> 'sms',
            'direction'=> 'incoming',
            'from'=> '491776706937',
            'to'=> env('TWILIO_FROM','00000'),
            'body' => 'Hello Simon'
        ]);
    }
}
