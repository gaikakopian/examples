<?php

namespace Modules\External\Domain\Services;

use GuzzleHttp\Client;
use http\Exception\InvalidArgumentException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\SmsLog;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\UserService;
use Modules\Matching\Domain\Models\Match;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;


/**
 * Class WaboboxService
 * @docs: https://www.waboxapp.com/assets/doc/waboxapp-API-v2.pdf
 * @package App\Services
 */
class Conference3Service
{


    /**
     * An HTTP client instance for making requests to the conference service.
     *
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;
    protected $firebaseService;

    /**
     * Construct an instance of the Service.
     *
     * @param \GuzzleHttp\Client $httpClient
     */
    public function __construct(Client $httpClient, FirebaseService $firebaseService)
    {
        $this->httpClient = $httpClient;
        $this->firebaseService = $firebaseService;
        $this->classroomsUrl = Config::get('conference.frontend3_url');


    }

    public function getExternalRoomLink(Match $match, User $user)
    {

        if (empty($match->external_room_id)) {
            $match->external_room_id = $this->gen_uid();
            $match->save();
        }
        $roomId = $match->external_room_id;
        // room/:id?viewMode=default&language=en&accestoken=abced

        return $this->getExternalLinkForRoomId($roomId, $user);
    }

    public function getExternalLinkForRoomId($roomId, User $user, $mode = 'default')
    {

        $lang = $user->getPreferredLanguageAttribute();
//        $token = $this->firebaseService->getAuthTokenForUser($user);
        $token = $user->createToken('Room Link');
        $stringToken = $token->accessToken;

        return $this->classroomsUrl . '/room/' . $roomId . '?lang=' . $lang . '&token=' . $stringToken . '&mode=' . $mode;
    }

    public function isDemoRoom($roomId)
    {
        return (substr($roomId, 0, 4) === 'DEMO');
    }

    public function getDemoRoomId(Program $program)
    {
        return "DEMO-" . $program->id . "-" . rand(0, 100);
    }

    public function getPublicPreviewUrl(Program $program)
    {
        return route('conference3.auth.getpublicroom', [
            'hash' => $program->secretHash(),
            'id' => $program->id
        ]);

    }

    /**
     * @param $roomName DEMO-{programId}-{randomNumber}
     */
    public function findSettingsForDemoRoom($roomName)
    {
        $parts = explode('-', $roomName);
        if (count($parts) !== 3) {
            throw new InvalidArgumentException("Demo Room Name must follow format DEMO-{programId}-{randomNumber}");
        }

        $program_id = (int)$parts[1];
        /** @var Program $program */
        $program = Program::find($program_id);
        if (!$program) {
            throw new ModelNotFoundException("Invalid Program Id: " . $program_id);
        }
        return ['content' =>
            [
                'entry' => $program->conference_entry_url,
                'root' => $program->conference_entry_url
            ]
        ];
    }

    private function gen_uid($l = 10): string
    {
        $str = "";
        for ($x = 0; $x < $l; $x++) {
            $str .= substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"), 0, 1);
        }
        return $str;
    }
}

