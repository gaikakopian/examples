<?php

namespace Modules\Distribution\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Role;

class UpdateDistributableContentRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    public function authorize()
    {
        return Auth::user()->hasRole(Role::ROLES['distributionservice']);
    }
}
