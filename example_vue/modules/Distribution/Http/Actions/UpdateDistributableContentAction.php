<?php

namespace Modules\Distribution\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Exceptions\Client\NotFoundException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Arr;
use Modules\Distribution\Domain\Services\DistributionService;
use Modules\Distribution\Http\Requests\UpdateDistributableContentRequest;
use Modules\Distribution\Http\Resources\DistributionHttpResource;

class UpdateDistributableContentAction extends Action
{
    protected $distService;
    protected $responder;

    public function __construct(DistributionService $distService, ResourceResponder $responder)
    {
        $this->distService = $distService;
        $this->responder = $responder;
    }

    /**
     * @param UpdateDistributableContentRequest $request
     * @param string $type
     * @return mixed
     * @throws ClientException
     * @throws NotFoundException
     */
    public function __invoke(UpdateDistributableContentRequest $request, string $type)
    {
        $model = $this->distService->resolveModel($type);
        if (is_null($model)) {
            throw new NotFoundException("Bad type for distributable content: $type");
        }

        $sourceId = $request->input('data.id');
        if (is_null($sourceId)) {
            throw new ClientException('Payload must contain an ID in order to identify a model');
        }

        $attributes = Arr::except($request->input('data'), ['id', 'distributor_source_id']);

        $instance = $model->updateOrCreate(['distributor_source_id' => $sourceId], $attributes);


        return response('ok', 200);
//        return $this->responder->send($instance, DistributionResource::class);
    }
}
