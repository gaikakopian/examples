<?php

namespace Modules\Distribution\Domain\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

trait HasDistributionLogic
{
    /**
     * Limit a query to those records that should be distributed.
     *
     * @param  Builder $builder
     * @return void
     */
    public function scopeDistributable(Builder $builder)
    {
        $builder->where('distribute_to_tenants', true);
    }

    /** @inheritDoc */
    public function allAttributesToArray(): array
    {
        return $this->attributes;
    }

    /** @inheritDoc */
    public static function getDistributableRecords(): Collection
    {
        return static::withTrashed()->distributable()->get();
    }
}
