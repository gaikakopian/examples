<?php

namespace Modules\Distribution\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Support\Facades\App;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;
use Modules\Distribution\Domain\Services\DistributionService;

class GetDistributableContentTest extends EndpointTest
{
    protected $url = '/api/v2/distribution/distributable-content';

    public function test_it_denies_access_to_unauthenticated_users()
    {
        $response = $this->json('GET', $this->url . '/branded-documents');
        $response->assertStatus(401);
    }

    public function test_it_denies_access_to_unauthorized_users()
    {
        $response = $this->actingAs($this->user)->json('GET', $this->url . '/branded-documents');
        $response->assertStatus(403);
    }

    public function test_it_fails_with_a_bad_type()
    {
        $distributor = factory(User::class)->create([
            'primary_role' => Role::ROLES['distributionservice']
        ]);
        $response = $this->actingAs($distributor)->json('GET', $this->url . '/bad-type');
        $response->assertStatus(404);
    }

    public function test_it_lists_all_branded_documents()
    {
        $documents = factory(BrandedDocument::class)->times(4)->create([
            'distribute_to_tenants' => true,
        ]);
        $distributor = factory(User::class)->create([
            'primary_role' => Role::ROLES['distributionservice']
        ]);

        $response = $this->actingAs($distributor)->json('GET', $this->url . '/branded-documents');
        $response->assertStatus(200);
        $this->assertCount(4, $response->json()['data']);
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                    'key',
                    'type',
                    'subject',
                    'content',
                    'brand_id',
                    'program_id',
                    'language',
                    'markdown',
                    'internal_title',
                    'audience',
                    'distribute_to_tenants',
                    'distributor_source_id',
                ]
            ]
        ]);
    }
}
