<?php

namespace Modules\Partnerapi\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

/**
 * Test the functionality of the `/api/v1/matches/{id}/confirm` endpoint.
 */
class ShowPartnerUsersTest extends EndpointTest
{

    public function test_it_authenticates_by_token()
    {
//        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQ4M2ZmZmJiMjkxZTEyZDhjMGY0MzIxYjQ1ZmMyNmIxMzc0NTQ1YjQ3OGViNTRjOWE2NjdjMDY3MGE0ZmNjZGYxZDEyYWJmMGFkYmI0NThhIn0.eyJhdWQiOiIyIiwianRpIjoiNDgzZmZmYmIyOTFlMTJkOGMwZjQzMjFiNDVmYzI2YjEzNzQ1NDViNDc4ZWI1NGM5YTY2N2MwNjcwYTRmY2NkZjFkMTJhYmYwYWRiYjQ1OGEiLCJpYXQiOjE1MzgzODQ2ODMsIm5iZiI6MTUzODM4NDY4MywiZXhwIjoxNTY5OTIwNjgzLCJzdWIiOiIyODc4Iiwic2NvcGVzIjpbXX0.JgI5lLwk8HIfutbRa5Uw_H3Hdo1rBTqigv58o9c24TGjsv0KgvDQ64SxSBRWYUdMWkcPX-gBzyGWP4CNv93dKv6gGEQ78hXiLhBr2LR0VGiXkc7jTJw2fALBfC9V073KKlxmAw8ZX4kfRjBNh4NHZ4YV7IbWs4A54PuGDDUjZmlwmPRSF3_E893jH0ZhNfa4WHOpx1FVLUI6VPz-YIrlgEJGqpTcWZVI84HLkABSLJbz13EFXx_Pt-hCCo_dCN_je90htB_UCyF7svEclIRWpDIJOlWUELcAgiRu026ikp3ePQASSlqXHsdfwYPra3iwJJkxLzmKiyK9g3c2prFKuuMTiDSePsCQhB0SNnZNXBihJdrB8GVQV2Y3Z6A2ycoa8y0Ndse2UtM3K3MBzrd6A48wtwh9szrRy-FMpCEU_53tfN6Tvzfa9PdYWMreLzebuLX6vsH3hv8raiDHxrCQw49IjboKdaXQ6n6uCONwXYPt-6AOj0sg7HiRi5Au2lPFKvuElwgwFdpAzu7FlPMu14wqcFuygj6q_rzVpX3Sz01wFfJm837C627cRKC7m1sy1sDXNgAOcdhr22A-LxQBV59wHTUGSBS3s3rSoAKwbwwzAcDGq_FRQPdRDluQN1ZVppn8JPMS1Axgy3v_TaVRNq_NduzIbpCDWV3g8CVbMsY';
        $user = User::query()->where('email', 'kironapi@volunteer-vision.com')->firstOrFail();

        /** @var \Laravel\Passport\PersonalAccessTokenResult $token */
        $token = $user->createToken('Personal Access Token');
        $headers = ['authorization' => 'Bearer ' . $token->accessToken];

        $route = route('partnerapi.users');
        $response = $this->getJson($route, $headers);
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

    }

    public function test_it_gets_users()
    {


        // CustardappleService

        $user = User::query()->where('email', 'kironapi@volunteer-vision.com')->firstOrFail();

        $route = route('partnerapi.users');

        $response = $this->actingAs($user)->getJson($route);
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJsonStructure([
            'data' =>
                [[
                    'id',
                    'displayName',
                    'firstName'
                ]]

        ]);

    }

}
