<?php

namespace Modules\Partnerapi\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use GuzzleHttp\Psr7\Request;
use Modules\Admin\Http\Resources\OrganizationInteractionHttpResource;
use Modules\Partnerapi\Domain\Models\OrganizationSaml;
use Modules\Partnerapi\Domain\Models\PartnerAccess;

/**
 *
 * @docs https://www.samltool.com/generic_sso_res.php
 */
class SamlAfterLoginTest extends EndpointTest
{

    public function test_it_processes_an_acs_rquest()
    {
        $this->markTestSkipped("the request probably is outdated");

        /** @var OrganizationSaml $organiationSaml */
        $organiationSaml = factory(OrganizationSaml::class)->create([
            'slug' => 'FUJ',
            'organization_id' => 1,
            'certificate' => 'MIICajCCAdOgAwIBAgIBADANBgkqhkiG9w0BAQ0FADBSMQswCQYDVQQGEwJ1czETMBEGA1UECAwKQ2FsaWZvcm5pYTEVMBMGA1UECgwMT25lbG9naW4gSW5jMRcwFQYDVQQDDA5zcC5leGFtcGxlLmNvbTAeFw0xNDA3MTcxNDEyNTZaFw0xNTA3MTcxNDEyNTZaMFIxCzAJBgNVBAYTAnVzMRMwEQYDVQQIDApDYWxpZm9ybmlhMRUwEwYDVQQKDAxPbmVsb2dpbiBJbmMxFzAVBgNVBAMMDnNwLmV4YW1wbGUuY29tMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDZx+ON4IUoIWxgukTb1tOiX3bMYzYQiwWPUNMp+Fq82xoNogso2bykZG0yiJm5o8zv/sd6pGouayMgkx/2FSOdc36T0jGbCHuRSbtia0PEzNIRtmViMrt3AeoWBidRXmZsxCNLwgIV6dn2WpuE5Az0bHgpZnQxTKFek0BMKU/d8wIDAQABo1AwTjAdBgNVHQ4EFgQUGHxYqZYyX7cTxKVODVgZwSTdCnwwHwYDVR0jBBgwFoAUGHxYqZYyX7cTxKVODVgZwSTdCnwwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQ0FAAOBgQByFOl+hMFICbd3DJfnp2Rgd/dqttsZG/tyhILWvErbio/DEe98mXpowhTkC04ENprOyXi7ZbUqiicF89uAGyt1oqgTUCD1VsLahqIcmrzgumNyTwLGWo17WDAa1/usDhetWAMhgzF/Cnf5ek0nK00m0YZGyc4LzgD0CROMASTWNg=='
        ]);


        $xmlData = file_get_contents(__DIR__ . '/mock/onelogin.response.xml');
        $base64 = base64_encode($xmlData);

        $slug = $organiationSaml->slug;
        $url = route('saml.acs', ['slug' => $slug]);

        $headers = [
            'Accept' => 'application/json'
        ];

        $response = $this->json('POST', $url, [
            'SAMLResponse' => $base64
        ],
            $headers);

        $this->assertEquals(302, $response->getStatusCode(), $response->getContent());
    }

    public function test_it_processes_an_get_request()
    {
        $this->markTestSkipped("the request probably is outdated");
        /** @var OrganizationSaml $organiationSaml */
        $organiationSaml = factory(OrganizationSaml::class)->create([
            'slug' => 'FUJ',
            'organization_id' => 1,
            'certificate' => 'MIICajCCAdOgAwIBAgIBADANBgkqhkiG9w0BAQ0FADBSMQswCQYDVQQGEwJ1czETMBEGA1UECAwKQ2FsaWZvcm5pYTEVMBMGA1UECgwMT25lbG9naW4gSW5jMRcwFQYDVQQDDA5zcC5leGFtcGxlLmNvbTAeFw0xNDA3MTcxNDEyNTZaFw0xNTA3MTcxNDEyNTZaMFIxCzAJBgNVBAYTAnVzMRMwEQYDVQQIDApDYWxpZm9ybmlhMRUwEwYDVQQKDAxPbmVsb2dpbiBJbmMxFzAVBgNVBAMMDnNwLmV4YW1wbGUuY29tMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDZx+ON4IUoIWxgukTb1tOiX3bMYzYQiwWPUNMp+Fq82xoNogso2bykZG0yiJm5o8zv/sd6pGouayMgkx/2FSOdc36T0jGbCHuRSbtia0PEzNIRtmViMrt3AeoWBidRXmZsxCNLwgIV6dn2WpuE5Az0bHgpZnQxTKFek0BMKU/d8wIDAQABo1AwTjAdBgNVHQ4EFgQUGHxYqZYyX7cTxKVODVgZwSTdCnwwHwYDVR0jBBgwFoAUGHxYqZYyX7cTxKVODVgZwSTdCnwwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQ0FAAOBgQByFOl+hMFICbd3DJfnp2Rgd/dqttsZG/tyhILWvErbio/DEe98mXpowhTkC04ENprOyXi7ZbUqiicF89uAGyt1oqgTUCD1VsLahqIcmrzgumNyTwLGWo17WDAa1/usDhetWAMhgzF/Cnf5ek0nK00m0YZGyc4LzgD0CROMASTWNg=='
        ]);

        $xmlData = file_get_contents(__DIR__ . '/mock/onelogin.response.xml');
        $base64 = base64_encode($xmlData);

        $slug = $organiationSaml->slug;
        $url = route('saml.acs', ['slug' => $slug]);

        $headers = [
            'Accept' => 'application/json'
        ];

        $response = $this->json('GET', $url, [
            'SAMLResponse' => $base64
        ],
            $headers);

        $this->assertEquals(302, $response->getStatusCode(), $response->getContent());
    }
//
//    public function test_it_an_get_rquest()
//    {
//
//        $xmlData = file_get_contents(__DIR__ . '/mock/saml.response.basic.xml');
//
//        $base64 = base64_encode($xmlData);
//
//
//        /** @var OrganizationSaml $organiationSaml */
//        $organiationSaml = OrganizationSaml::query()->firstOrFail();
//        $url = route('saml.acs', ['slug' => $organiationSaml->slug]);
//
//        $headers = [
//            'Accept' => 'application/json'
//        ];
//
//        $response = $this->json('GET', $url, [
//            'SAMLResponse' => $base64,
//            'body' => ['SAMLResponse' => $base64],
//            'headers' => $headers],
//            $headers);
//
//        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
//    }

}
