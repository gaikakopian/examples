<?php

namespace Modules\Partnerapi\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Modules\Admin\Domain\Models\CoordinatorTodo;
use Modules\Admin\Domain\Services\CoordinatorTodoService;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\GroupService;
use Modules\Partnerapi\Domain\Models\OrganizationSaml;
use Modules\Partnerapi\Domain\Services\PartnerOrganizationAccessService;
use Modules\Partnerapi\Domain\Services\SamlConfigProvider;
use Modules\Partnerapi\Domain\Services\SamlService;


class SamlServiceTest extends LaravelTest
{

    /**
     * @var SamlService
     */
    protected $samlService;

    public function setUp()
    {
        parent::setUp();

        $this->samlService = app(SamlService::class);
    }

    /**
     * @throws \OneLogin\Saml2\Error
     * @throws \App\Exceptions\Client\NotAuthorizedException
     */
    public function test_it_finds_email_regex()
    {

        $org = factory(OrganizationSaml::class)->create(['email_pattern' => '/(uniquecompany\.com)/', 'organization_id' => 1]);

        $samlOrg = $this->samlService->findSamlRedirectByEmail("test@uniquecompany.com");

        $this->assertEquals($org->id, $samlOrg->id);

    }
    /**
     * @throws \OneLogin\Saml2\Error
     * @throws \App\Exceptions\Client\NotAuthorizedException
     */
    public function test_it_does_not_match_wrong_sso()
    {

        $org = factory(OrganizationSaml::class)->create(['email_pattern' => '/(uniquecompany\.com)/', 'organization_id' => 1]);

        $samlOrg = $this->samlService->findSamlRedirectByEmail("test@otherCompany.com");

        $this->assertNull($samlOrg);

    }
    /**
     * @throws \OneLogin\Saml2\Error
     * @throws \App\Exceptions\Client\NotAuthorizedException
     */
    public function test_it_parses_base64_request_correctly()
    {

        $this->markTestSkipped('no idea why this does not work');

        $certificate = file_get_contents(__DIR__ . '/../Http/mock/onelogin.cert');


        /** @var OrganizationSaml $organizationSaml */
        $organizationSaml = $saml = factory(OrganizationSaml::class)->create([
            'organization_id' => 1,
            'certificate' => $certificate,
            'slug' => 'INHOUSE_SAMPLE'
        ]);
        $configProvider = app(SamlConfigProvider::class);
        $saml2Auth = $configProvider->getSaml2AuthBySlug($organizationSaml->slug);


        $xml = file_get_contents(__DIR__ . '/../Http/mock/onelogin.response.xml');

        $_POST['SAMLResponse'] = base64_encode($xml);


        $errors = $saml2Auth->acs();

        $this->assertEmpty($errors, 'ACS Parser has errors');


        $samlUser = $saml2Auth->getSaml2User();

        $user = $this->samlService->findOrCreate($organizationSaml, $samlUser);

        $this->assertNotNull($user);


    }




}