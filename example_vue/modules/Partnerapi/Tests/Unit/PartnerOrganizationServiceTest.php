<?php

namespace Modules\Partnerapi\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Modules\Admin\Domain\Models\CoordinatorTodo;
use Modules\Admin\Domain\Services\CoordinatorTodoService;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\GroupService;
use Modules\Partnerapi\Domain\Services\PartnerOrganizationAccessService;


class PartnerOrganizationServiceTest extends LaravelTest
{

    /**
     * @var PartnerOrganizationAccessService
     */
    protected $partnerOrganizationService;

    public function setUp()
    {
        parent::setUp();

        $this->partnerOrganizationService = app(PartnerOrganizationAccessService::class);
    }

    public function test_it_reads_country_correctly()
    {
        $kironSample = <<<EOT
{
  "data": {
    "status": "active",
    "first_name": "abc",
    "last_name": "def",
    "email": "mohibullah.wardak@kiron.ngo",
    "phone": "+123456789",
    "country": {
      "id": 1,
      "code2": "AF",
      "code3": "AFG",
      "denomination": "Afghanistan",
      "remark": "",
      "created_at": "2015-11-15 14:22:30",
      "updated_at": "2015-11-15 14:22:30"
    }
  }
}
EOT;
        $data = json_decode($kironSample, true);
        $userArray = $this->partnerOrganizationService->mapPartnerInformation($data['data']);

        $this->assertArrayHasKey('country', $userArray);
        $this->assertEquals($userArray['country'], 'AF');

    }

    public function test_it_converst_user_correctly()
    {

        $kironSample = <<<EOT
{
  "data": {
    "status": "active",
    "first_name": "abc",
    "last_name": "def",
    "email": "mohibullah.wardak@kiron.ngo",
    "phone": "+491776706937",
    "country": {
      "id": 1,
      "code2": "AF",
      "code3": "AFG",
      "denomination": "Afghanistan",
      "remark": "",
      "created_at": "2015-11-15 14:22:30",
      "updated_at": "2015-11-15 14:22:30"
    }
  }
}
EOT;

//        $data = file_get_contents(__DIR__.'/kiron.sample1.json');
        $data = json_decode($kironSample, true);
        $userArray = $this->partnerOrganizationService->mapPartnerInformation($data['data']);


        $this->assertArrayHasKey('state', $userArray);
        $this->assertArrayHasKey('first_name', $userArray);
        $this->assertArrayHasKey('last_name', $userArray);
        $this->assertArrayHasKey('email', $userArray);
        $this->assertArrayHasKey('phone_number', $userArray);


        $this->assertEquals($userArray['state'], User::STATES['REGISTERED']);
        $this->assertEquals($userArray['first_name'], 'abc');
        $this->assertEquals($userArray['last_name'], 'def');
        $this->assertEquals($userArray['email'], 'mohibullah.wardak@kiron.ngo');

    }

     public function test_it_converts_phonenumber_correctly()
    {

        $kironSample = <<<EOT
{
  "data": {
    "status": "active",
    "first_name": "abc",
    "last_name": "def",
    "email": "mohibullah.wardak@kiron.ngo",
    "phone": "+491776706937"
  }
}
EOT;

//        $data = file_get_contents(__DIR__.'/kiron.sample1.json');
        $data = json_decode($kironSample, true);
        $userArray = $this->partnerOrganizationService->mapPartnerInformation($data['data']);

        $this->assertArrayHasKey('phone_number', $userArray);
        $this->assertArrayHasKey('phone_number_prefix', $userArray);

         $this->assertEquals($userArray['phone_number'], '1776706937');
         $this->assertEquals($userArray['phone_number_prefix'], '49');

    }


}