<?php

namespace Modules\Partnerapi\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Illuminate\Support\Collection;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Http\Resources\CommentHttpResource;
use Modules\Core\Http\Resources\EnrollmentHttpResource;
use Modules\Core\Http\Resources\StateLogHttpResource;

class PartnerEnrollmentHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {

        // do some when loaded magic;
        if ($this->whenLoaded('participations')) {
            /** @var Collection $participations */
            $participations = $this->participations;
            $matches = $participations->map(function ($p) {
                return $p->match;
            });
            $matches = $matches->filter(function ($m) {
                return $m != null;
            });
        }

        return [
            'id' => $this->id,
            'program' => new PartnerProgramHttpResource($this->program),
            'role' => $this->role,
            'state' => $this->state,
            'lastStateChangeAt' => $this->last_state_change ? $this->last_state_change->toIso8601String() : $this->last_state_change,
            'lastActivityAt' => $this->last_activity_at ? $this->last_activity_at->toIso8601String() : $this->last_activity_at,
            'matches' => $matches ? PartnerMatchHttpResource::collection($matches) : [],
//            'participations' => ParticipationResource::collection($this->participations),
        ];
    }
}
