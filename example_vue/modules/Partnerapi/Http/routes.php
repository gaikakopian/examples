<?php



Route::group(['prefix' => '/partnerapi', 'middleware' => ['throttle:60,1']], function () {
    Route::get('/{id}/oauth', 'Actions\HandleOauthUserAction')->name('partnerapi.oauth')->where('id', '[0-9]+');
    Route::get('/kiron/oauth', 'Actions\HandleKironOauthUserAction')->name('partnerapi.kironoauth');
});



Route::group(['prefix' => '/', 'middleware' => ['throttle:60,1','slug2saml']], function () {

    Route::get('/saml/{slug}/login', 'Actions\SamlLoginAction')->name('saml.login');


    Route::get('/saml/{slug}/acs', 'Actions\SamlAfterLoginAction')->name('saml.acspost');
    Route::post('/saml/{slug}/acs', 'Actions\SamlAfterLoginAction')->name('saml.acs'); // regular reciver;
    Route::post('/saml/{slug}/login', 'Actions\SamlAfterLoginAction')->name('saml.acslogin'); //should not be needed;



    Route::get('/saml/{slug}/sls', 'Actions\SamlLogoutAction')->name('saml.sls');
    Route::get('/saml/{slug}/metadata', 'Actions\SamlMetadataAction')->name('saml.metadata');
    Route::get('/saml/{slug}/idp', 'Actions\SamlIdpInfoAction')->name('saml.idp');
//    Route::get('/saml/:slug/singleLogoutService', 'Actions\SamlAfterLoginAction')->name('me.firebasetoken');
    Route::get('/saml/error', 'Actions\SamlErrorAction')->name('saml.error');


});

