<?php

Route::group(['prefix' => '/partnerapi', 'middleware' => ['auth']], function () {
    Route::get('/meta', 'Actions\ShowPartnerMetaAction')->name('partnerapi.meta');
    Route::get('/users', 'Actions\ShowPartnerUsersAction')->name('partnerapi.users');
    Route::get('/users/{id}', 'Actions\ShowPartnerUserAction')->name('partnerapi.user');
});