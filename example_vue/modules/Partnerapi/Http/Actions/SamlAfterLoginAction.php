<?php

namespace Modules\Partnerapi\Http\Actions;

use Aacotroneo\Saml2\Events\Saml2LoginEvent;
use Aacotroneo\Saml2\Saml2Auth;
use App\Exceptions\Client\ClientException;
use App\Exceptions\Client\NotFoundException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;


use http\Exception\InvalidArgumentException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Services\UserService;
use Modules\Matching\Http\Requests\AddFeedbackRequest;
use Modules\Partnerapi\Domain\Services\SamlService;


/**
 * Class CallMatchAction
 * @package Modules\Matching\Http\Actions
 */
class SamlAfterLoginAction extends Action
{

    private $samlService;
    private $saml2Auth;

    private $userService;


    public function __construct(
        SamlService $samlService,
        Saml2Auth $saml2Auth,
        UserService $userService,
        ResourceResponder $responder
    )
    {
        $this->samlService = $samlService;
        $this->userService = $userService;

        $this->responder = $responder;
        $this->saml2Auth = $saml2Auth;
    }

    /**
     *
     * @param $id
     * @param AddFeedbackRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ClientException
     * @throws \Exception
     */
    public function __invoke(Request $request, $slug)
    {


        try {
            $organizationSaml = $this->samlService->findBySlug($slug);
        } catch (ModelNotFoundException $exception) {
            throw new NotFoundException("Saml Provider not find by  " . $slug);
        }

        /** @var Saml2Auth $saml2Auth */
        $saml2Auth = $request->saml2Auth;
        $_POST['SAMLResponse'] = $request->input('SAMLResponse');
        $xml = base64_decode($_POST['SAMLResponse']);

        Log::info("[saml2 login request]" . $xml);

        if (empty($xml)) {
            return response()->view('errors.samlerror', [
                'message' => "SAMLResponse is missing (did you configure HTTP POST Bindings?)",
                'errors' => "SAMLResponse is missing (did you configure HTTP POST Bindings?)"
            ], 422);
        }


        $errors = $saml2Auth->acs();


        if (!empty($errors)) {
            logger()->error('Saml2 error_detail', ['error' => $saml2Auth->getLastErrorReason()]);
            logger()->error('Saml2 error', $errors);

            return response()->view('errors.samlerror', [
                'message' => $saml2Auth->getLastErrorReason(),
                'errors' => join(',', $errors)
            ], 500);
        }


        $samlUser = $saml2Auth->getSaml2User();
//        event(new Saml2LoginEvent($samlUser, $saml2Auth));

        $user = $this->samlService->findOrCreate($organizationSaml, $samlUser);
        $link = $this->userService->getLoginLink($user);

        return redirect($link);
    }
}
