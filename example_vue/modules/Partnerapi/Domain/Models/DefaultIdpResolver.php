<?php


namespace Modules\Partnerapi\Domain\Models;

use App\Exceptions\Server\ServerException;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Partnerapi\Domain\Services\SamlService;

/**
 * Has to return IDP Settings based on the given slug
 * Class DefaultIdpResolver
 * @package Modules\Partnerapi\Domain\Models
 */
// should extend idp resolver?
class DefaultIdpResolver extends Model
{
    public static function idpSettings($slug)
    {
        // get from service or database;
        throw  new ServerException("NOt implemented");


    }
}