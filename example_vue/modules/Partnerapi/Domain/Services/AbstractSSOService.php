<?php

namespace Modules\Partnerapi\Domain\Services;

use App\Exceptions\Client\NotAuthorizedException;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\RegistrationCodeService;
use Modules\Core\Domain\Services\UserService;

abstract class AbstractSSOService
{

    protected $userService;

    /**
     * @param array $userdata
     * @param Organization $organization
     * @return User
     * @throws NotAuthorizedException
     * @throws \Exception
     */
    public function findOrCreateUser(array $userdata, Organization $organization, RegistrationCode $registrationCode)
    {

        // @todo: move to user Service?

        /** @var UserService $userService */
        $this->userService = app(UserService::class);

        /** @var User $user */
        $user = $this->userService->findUserByEmail($userdata['email']);

        if (!$user) {
            /** @var RegistrationCodeService $registrationCodeService */

            $userdataAll = $userdata;

            $userdataAll['organization_id'] = $organization->id;
            // set to false, since we want Kiron users to register regularly in the futre.
            $user = $this->userService->createAndInviteUser($userdataAll, $registrationCode, false);

        }

        if ($user->organization_id != $organization->id) {
            throw new NotAuthorizedException('User is not related to the correct organization');
        }

        return $user;
    }


}