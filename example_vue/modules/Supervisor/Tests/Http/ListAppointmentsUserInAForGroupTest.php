<?php

namespace Modules\Supervisor\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

/**
 * Test the functionality of the `/api/v1/groups/{id}/appointments` endpoint.
 */
class ListAppointmentsUserInAForGroupTest extends EndpointTest
{
    public function test_it_lists_all_appointments_for_supervisor()
    {
        $numAppointments = 5;

        $supervisor = factory(User::class)->create();
        $supervisor->groups()->attach(1, [
            'role' => 'supervisor',
        ]);

        $user = $this->seedUserWithAppointments($numAppointments);



        $response = $this->actingAs($supervisor)->getJson('/api/v2/supervisor/users/' . $user->id . '/appointments');


        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'plannedStart',
                    'state'
                ]
            ]
        ]);
    }

    public function test_it_doesnot_allow_acces_to_none_supervisor()
    {
        $numAppointments = 5;

        $supervisor = factory(User::class)->create();
        $supervisor->groups()->attach(1, [
            'role' => 'supervisor',
        ]);

        // this user user is not member of group 1
        $user = $this->seedUserWithAppointments($numAppointments);

        $response = $this->actingAs($user)->getJson('/api/v2/supervisor/users/' . $user->id . '/appointments');

        $response->assertStatus(403);
    }

    /**
     * @param $numAppointments int
     * @return User
     */
    private function seedUserWithAppointments(int $numAppointments): User
    {
        Match::flushEventListeners();  // not needed for testing;
        $user = factory(User::class)->create();
        $user->groups()->attach(1, [
            'role' => 'member',
        ]);

        $program = factory(Program::class)->create(['title' => 'Nice Program']);
        $enrollment = factory(Enrollment::class)->create(['user_id' => $user->id, 'program_id' => $program->id]);
        $match = factory(Match::class)->create();

        factory(Participation::class)->create([
            'enrollment_id' => $enrollment->id,
            'match_id' => $match->id
        ]);
        $appointments = factory(Appointment::class, $numAppointments)->create(['match_id' => $match->id]);

        return $user;
    }
}
