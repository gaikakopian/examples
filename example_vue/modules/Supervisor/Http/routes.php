<?php

/*
|--------------------------------------------------------------------------
| Module routes
|--------------------------------------------------------------------------
|
| Here's the place to register API routes for the current module. These routes
| are loaded by the RouteServiceProvider within a group which is assigned
| the "api" middleware group and a version prefix, eg. `/api/v1`.
|
 */

/**
 * Private Scope
 */
Route::group(['middleware' => ['auth']], function () {
    // depreactade;
    Route::get('/users/me/supervisor/groups', 'Actions\ListMySupervisingGroupsAction');

    Route::group(['prefix' => '/supervisor'], function () {
        Route::get('/groups', 'Actions\ListMySupervisingGroupsAction');
        Route::get('/groups/{id}/participations', 'Actions\ListSupervisingParticipationsForGroupAction')->where('id', '[0-9]+');;
        Route::get('/groups/{id}/users', 'Actions\ListSupervisingUsersForGroupAction')->where('id', '[0-9]+');;
        Route::get('/groups/{id}/appointments', 'Actions\ListAppointmentsForGroupAction')->where('id', '[0-9]+');;

        Route::get('/users/{id}', 'Actions\ShowUserToSupervisorAction')->where('id', '[0-9]+');;
        Route::get('/users/{id}/appointments', 'Actions\ListAppointmentsForAUserInAGroupAction')->where('id', '[0-9]+');;
        Route::get('/users/{id}/availability', 'Actions\ShowUserAvailabilityToSupervisorAction')->where('id', '[0-9]+');;
        Route::post('/users/{id}/comments', 'Actions\AddCommentToUserAction')->where('id', '[0-9]+');;
        Route::get('/users/{id}/comments', 'Actions\ListCommentsForSupervisorAction')->where('id', '[0-9]+');;
        Route::post('/users/{id}/resetpassword', 'Actions\SupervisorResetPasswordOfUserAction')->where('id', '[0-9]+');;
    });
});
