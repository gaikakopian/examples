<?php

namespace Modules\Supervisor\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class GroupSupervisorHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }
}
