<?php

namespace Modules\Supervisor\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class ParticipationSupervisorHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'startMatchingAfter' => ($this->start_matching_after ? $this->start_matching_after->toIso8601String() : null),
            'matchConfirmedAt' => ($this->match_confirmed_at ? $this->match_confirmed_at->toIso8601String() : null),
            'match_id' => $this->match_id,
            'match' => new MatchSupervisorHttpResource($this->whenLoaded('match')),
            'enrollment' => new EnrollmentSupervisorHttpResource($this->whenLoaded('enrollment')),

        ];
    }
}
