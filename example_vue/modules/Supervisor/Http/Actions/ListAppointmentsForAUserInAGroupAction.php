<?php

namespace Modules\Supervisor\Http\Actions;

use App\Exceptions\Client\NotAuthorizedException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Domain\Services\UserService;
use Modules\Scheduling\Domain\Services\AppointmentService;
use Modules\Scheduling\Http\Resources\AppointmentHttpResource;

class ListAppointmentsForAUserInAGroupAction extends Action
{
    /**
     * @var AppointmentService
     */
    protected $appointmentService;
    protected $groupService;
    protected $userService;
    protected $responder;

    public function __construct(
        AppointmentService $appointmentService,
        GroupService $groupService,
        UserService $userService,
        ResourceResponder $responder
    ) {
        $this->appointmentService = $appointmentService;
        $this->groupService = $groupService;
        $this->userService = $userService;
        $this->responder = $responder;
    }

    /**
     *
     * @param int $user_id
     * @param Request $request
     * @return mixed
     * @throws NotAuthorizedException
     */
    public function __invoke(int $user_id, Request $request)
    {
        /**
         * @var $currentUser User
         * @var $appointmentUser User
         * @var $group Group
         */
        $currentUser = Auth::user();
        $appointmentUser = $this->userService->get($user_id);


        if ($currentUser->cannot('listSupervisorAppointments', $appointmentUser)) {
            throw new NotAuthorizedException();
        }


        $appointments = $this->appointmentService->listAppointmentsForUser($appointmentUser);


        return $this->responder->send($appointments, AppointmentHttpResource::class);
    }
}
