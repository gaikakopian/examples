<?php

namespace Modules\Supervisor\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Resources\GroupHttpResource;
use Modules\Supervisor\Http\Resources\ParticipationSupervisorHttpResource;
use Modules\Supervisor\Http\Resources\UserSupervisorHttpResource;

/**
 * Class ListSupervisingParticipationsForGroupAction
 * @package Modules\Supervisor\Http\Actions
 * @depracted
 */
class ListSupervisingUsersForGroupAction extends Action
{
    protected $groupService;
    protected $participationService;
    protected $userService;
    protected $responder;

    public function __construct(
        ParticipationService $participationService,
        UserService $userService,
        GroupService $groupService,
        ResourceResponder $responder
    ) {
        $this->groupService = $groupService;
        $this->participationService = $participationService;
        $this->userService = $userService;
        $this->responder = $responder;
    }


    public function __invoke($id, Request $request)
    {
        $users = $this->userService->listUsersForOverviewOfGroup((int)$id);
        return $this->responder->send($users, UserSupervisorHttpResource::class);
    }
}
