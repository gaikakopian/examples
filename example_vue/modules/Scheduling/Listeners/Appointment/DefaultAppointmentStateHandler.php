<?php

namespace Modules\Scheduling\Listeners\Appointment;

use App\Notifications\Appointment\AppointmentCanceledNotification;
use App\Notifications\Appointment\AppointmentNoattendanceNotification;
use App\Notifications\Appointment\AppointmentPlannedFirstNotification;
use App\Notifications\Appointment\AppointmentPlannedNotification;
use App\Notifications\Appointment\AppointmentRescheduledNotification;

use App\Services\ChatbotService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Modules\Admin\Domain\Models\CoordinatorTodo;
use Modules\Admin\Domain\Services\CoordinatorTodoService;
use Modules\Core\Domain\Models\Activity;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ActivityService;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;
use SM\Event\TransitionEvent;

class DefaultAppointmentStateHandler
{
    /**
     * Handle the `plan` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     * @throws \App\Exceptions\Client\ClientException
     */
    public function plan(TransitionEvent $event)
    {

        /** @var Appointment $appointment */
        $appointment = $event->getStateMachine()->getObject();

        /** @var Match $match */
        $match = $appointment->match;
        $enrollments = $appointment->enrollments;
        $activityService = app(ActivityService::class);
        $isFirstAppointment = $match->isFirstAppointment();

        if ($match->state === Match::STATES['PAUSED'] &&
            $match->transitionAllowed('resume')) {
            $match->transition('resume');
        }


        /** @var Enrollment $enrollment */
        foreach ($enrollments as $enrollment) {

            // only send to mentees
            if (!$enrollment->isMentee()) {
                continue;
            }

            /** @var User $mentee */
            $mentee = $enrollment->user;
            if (!$mentee) {
                return;
            }

            $link = route('email.appointment.response', [
                'id' => $appointment->id,
                'userid' => $mentee->id,
                'secret' => $mentee->emailSecret('RESPONSE_APPOINTMENT'),
                'action' => 'confirm'
            ]);
            $chatbotService = app(ChatbotService::class);
            $chatbotService->setUserState($mentee, ChatbotService::STATES['appointment_confirmation'], ['id' => $appointment->id]);

            if ($isFirstAppointment) {
                $mentee->notify(new AppointmentPlannedFirstNotification($mentee->brand, $appointment, $enrollment, $link));
            } else {
                $mentee->notify(new AppointmentPlannedNotification($mentee->brand, $appointment, $enrollment, $link));
            }

        }

        Log::debug('Handling "plan" transition', ['event' => $event]);
    }

    /**
     * Handle the `reschedule` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function reschedule(TransitionEvent $event): void
    {
        // @todo: find sender.

        /** @var Appointment $appointment */
        $appointment = $event->getStateMachine()->getObject();
        $enrollments = $appointment->enrollments;


        foreach ($enrollments as $e) {
            /** @var User $user */
            $user = $e->user;
            $user->notify(new AppointmentRescheduledNotification($user->brand, $appointment, $e));
        }

        Log::debug('Handling "reschedule" transition', ['event' => $event]);
    }

    /**
     * Handle the `cancel` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function cancel(TransitionEvent $event): void
    {
        /** @var Appointment $appointment */
        $appointment = $event->getStateMachine()->getObject();
        $enrollments = $appointment->enrollments;

        $activityService = app(ActivityService::class);

        foreach ($enrollments as $e) {
            /** @var User $user */
            $user = $e->user;
            $activityService->logActivityForUser($user, Activity::ACTIVITIES['APPOINTMENT_CANCELED']);
            $user->notify(new AppointmentCanceledNotification($user->brand, $appointment, $e));
        }
        Log::debug('Handling "cancel" transition', ['event' => $event]);
    }

    /**
     * Handle the `start` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function start(TransitionEvent $event): void
    {
        /** @var Appointment $appointment */
        $appointment = $event->getStateMachine()->getObject();
        if (!$appointment->actual_start) {
            $appointment->actual_start = Carbon::now();
        }
        $appointment->save();


        Log::debug('Handling "start" transition', ['event' => $event]);
    }

    /**
     * Handle the `report_noattendance` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function reportNoattendance(TransitionEvent $event)
    {

        /** @var Appointment $appointment */

        $appointment = $event->getStateMachine()->getObject();

        /** @var Match $match */
        $match = $appointment->match;

        $mentorParticipation = $match->getMentorParticipation();
        $mentorEnrollment = $mentorParticipation->enrollment;
        $user = $mentorEnrollment->user;

        $user->notify(new AppointmentNoattendanceNotification($user->brand, $appointment, $mentorEnrollment));
        Log::debug('Handling "report_noattendance" transition', ['event' => $event]);
    }

    public function reportTechnicalissues(TransitionEvent $event): void
    {
        /** @var CoordinatorTodoService $coordinatorTddoService */
        $coordinatorTddoService = app(CoordinatorTodoService::class);

        $appointment = $event->getStateMachine()->getObject();
        $user = $appointment->enrollments->first()->user;

        $todo = new CoordinatorTodo(
            [
                'customer_id' => $user->id,
                'description' => '',
                'meta' => ['match_id' => $appointment->match->id],
                'type' => CoordinatorTodo::TYPES['APPOINTMENT_TECHNICAL_ISSUES'],
                'duedate' => Carbon::now()
            ]);

        $coordinatorTddoService->createTodoIfNotExistsWithinDays($todo, 7);

    }

    /**
     * Handle the `report_noshow` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function reportNoshow(TransitionEvent $event): void
    {


        /** @var CoordinatorTodoService $coordinatorTddoService */
        $coordinatorTddoService = app(CoordinatorTodoService::class);

        $appointment = $event->getStateMachine()->getObject();
        $user = $appointment->enrollments->first()->user;

        $todo = new CoordinatorTodo(
            [
                'customer_id' => $user->id,
                'description' => '',
                'meta' => ['match_id' => $appointment->match->id],
                'type' => CoordinatorTodo::TYPES['NO_SHOW'],
                'duedate' => Carbon::now()
            ]);
        $coordinatorTddoService->createTodoIfNotExistsWithinDays($todo, 7);

        Log::debug('Handling "report_noshow" transition', ['event' => $event]);
    }

    // not sure which one get's called
    public function email_noshow(TransitionEvent $event): void
    {
        Log::info('Handling "email_noshow" transition (correct one)', ['event' => $event]);

        $this->reportNoshow($event);
    }

    public function emailNoshow(TransitionEvent $event): void
    {
        Log::info('Handling "emailNoshow" transition (incorrect one)', ['event' => $event]);
        $this->reportNoshow($event);
    }

    /**
     * Handle the `undo_noshow` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function undoNoshow(TransitionEvent $event): void
    {
        Log::debug('Handling "undo_noshow" transition', ['event' => $event]);
    }

    /**
     * Handle the `finish` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function finish(TransitionEvent $event)
    {
        /**
         * @var Appointment $appointment
         * @var Match $match
         * @var Program $program
         * @var User $ser
         * @var Enrollment $enrollment
         */

        $appointment = $event->getStateMachine()->getObject();
        $match = $appointment->match;
        $program = $match->program;
        $enrollments = $appointment->enrollments;
        $activityService = app(ActivityService::class);

        if ($appointment->current_lecture && $appointment->current_lecture <= $program->lections_total) {
            $match->lections_completed = $appointment->current_lecture;
        } else {
            $match->lections_completed = $match->appointments()->where('state', '=', Appointment::STATES['SUCCESS'])->count();
            $match->lections_completed++;
        }
        $match->save();

        if ($match->transitionAllowed('activate')) {
            $match->transition('activate');
        }

        $isFirstAppointment = $match->lections_completed == 1;
        $isLastAppointment = $program->sessions_total - $match->lections_completed === 0;
        $isPreLastAppointment = $program->sessions_total - $match->lections_completed === 1;

//        if ($isLastAppointment) { //after finishing last appointment
//            $match->transition('finish');
//        }


        foreach ($enrollments as $enrollment) {
            $user = $enrollment->user;

            if ($isPreLastAppointment) {
                // @todo: certificate info for mentor notification
            }

            $activityService->logActivityForUser($user, Activity::ACTIVITIES['APPOINTMENT_COMPLETED']);
        }
        $match->save();

        Log::debug('Handling "finish" transition', ['event' => $event]);
    }

    /**
     * Handle the `finish_offline` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function finishOffline(TransitionEvent $event): void
    {
        $appointment = $event->getStateMachine()->getObject();

        Log::debug('Handling "finish_offline" transition', ['event' => $event]);
    }

    /**
     * Handle the `hangup` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function hangup(TransitionEvent $event): void
    {

        // no notficiation on hangup;

        /** @var Appointment $appointment */
//        $appointment = $event->getStateMachine()->getObject();
//        $enrollments = $appointment->enrollments;

//        foreach ($enrollments AS $e) {
//            /** @var User $user */
//            $user = $e->user;
//            $user->notify(new AppointmentHangedUpNotification($user->brand, $appointment, $e));
//        }

        Log::debug('Handling "hangup" transition', ['event' => $event]);
    }
}
