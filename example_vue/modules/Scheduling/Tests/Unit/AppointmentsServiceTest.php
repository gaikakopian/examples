<?php

namespace Modules\Scheduling\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;
use Carbon\Carbon;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ContentChooser\ContentChooser;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;
use Modules\Scheduling\Domain\Services\AppointmentService;

/**
 * Test the functionality of the ContentChooser Service.
 */
class AppointmentsServiceTest extends LaravelTest
{
    /**
     * @var AppointmentService
     */
    protected $service;

    public function setUp()
    {
        parent::setUp();

        $this->service = app(AppointmentService::class);
    }

    public function test_listForUserInGroup_selects_correctly()
    {
        Match::flushEventListeners(); // not needed for testing;

        $numAppointments = 5;
        /**
         * @var $user User
         * @var $enrollment Enrollment
         * @var $participation Participation
         * @var $match Match
         * @var $group Group
         */
        $user = factory(User::class)->create();
        $program = factory(Program::class)->create(['title' => 'Nice Program']);
        $enrollment = factory(Enrollment::class)->create(['user_id' => $user->id, 'program_id' => $program->id]);
        $match = factory(Match::class)->create();

        $participation = factory(Participation::class)->create([
            'enrollment_id' => $enrollment->id,
            'match_id' => $match->id
        ]);
        $date = Carbon::now()->addDays(20);

        $appointments = factory(Appointment::class, $numAppointments)->create([
            'match_id' => $match->id,
            'planned_start' => $date,
            'state' => Appointment::STATES['PLANNED']
        ]);

        $group = factory(Group::class)->create();
        $group->users()->attach($user, ['role' => 'member']);

        $serviceAppointments = $this->service->listWithUserForSupervisorOfGroup($group, 21);


        $this->assertEquals($appointments->count(), $serviceAppointments->count());
        $this->assertEquals($numAppointments, $serviceAppointments->count());
    }
}
