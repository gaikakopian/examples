<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Modules\Core\Domain\Models\User;
use Modules\Scheduling\Domain\Models\Appointment;

/**
 * Test the functionality of the `/api/v1/groups/{id}/appointments` endpoint.
 */
class ListAppointmentsForMatchTest extends EndpointTest
{
    public function test_it_lists_all_appointments_for_supervisor()
    {
        $supervisor = factory(User::class)->create();
        $supervisor->groups()->attach(1, [
            'role' => 'supervisor',
        ]);

        $pastAppointment = factory(Appointment::class)->create([
            'match_id' => 1,
            'planned_start' => Carbon::yesterday(),
        ]);
        $upcomingAppointment = factory(Appointment::class)->create([
            'match_id' => 1,
            'planned_start' => Carbon::tomorrow(),
        ]);

        $response = $this->actingAs($supervisor)->getJson('/api/v2/matches/1/appointments');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJsonStructure([
            'data' => [['id','plannedStart']]
        ]);
    }
}
