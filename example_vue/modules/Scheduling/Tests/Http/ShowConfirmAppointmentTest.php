<?php

namespace Modules\Scheduling\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;
use Modules\Scheduling\Domain\Models\Appointment;
use Illuminate\Support\Facades\Notification;
use App\Notifications\Appointment\AppointmentNoShowMissedYouNotification;
use App\Notifications\Appointment\AppointmentNoShowSenderNotification;

/**
 * Test the functionality of the `/api/v1/matches/{id}/noshow` endpoint.
 */
class ShowConfirmAppointmentTest extends EndpointTest
{
    public function test_it_confirms_an_appointment()
    {
        Notification::fake();

        $user = User::query()->where('email',
            'mentor@volunteer-vision.com')->firstOrFail();
        $match = $user->enrollments->first()->participations->first()->match;

        $appointment = factory(Appointment::class)->create([
            'match_id' => 1,
            'state' => 'PLANNED',
        ]);


        $secret = $user->emailSecret('RESPONSE_APPOINTMENT');

        $route = route('email.appointment.response', [
            'id' => $appointment->id,
            'action' => 'confirm',
            'userid' => $user->id,
            'secret' => $secret
        ]);

        $response = $this->actingAs($this->user)->get($route);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $this->assertDatabaseHas('appointments', [
            'id' => $appointment->id,
            'state' => Appointment::STATES['CONFIRMED']
        ]);
    }
}
