<?php

namespace Modules\Scheduling\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Tenant;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

/**
 * Test the functionality of the `/api/v1/matches/{id}/cancel` endpoint.
 */
class CancelAppointmentTest extends EndpointTest
{
    public function test_it_cancels_an_appointment()
    {
        $appointment = factory(Appointment::class)->create([
            'match_id' => 1,
            'state' => 'PLANNED',
        ]);

        $this->doRequest($this->user, $appointment);
    }

    public function test_that_supervisors_can_also_cancel_appointments()
    {
        $supervisor = factory(User::class)->create();
        $supervisor->groups()->attach(1, [
            'role' => 'supervisor',
        ]);
        $appointment = factory(Appointment::class)->create([
            'match_id' => 1,
            'planned_start' => Carbon::tomorrow(),
            'state' => 'PLANNED',
        ]);

        $this->doRequest($supervisor, $appointment);
    }

    public function test_that_supervisors_of_other_group_cant_cancel_appointments()
    {
        // Disable model events, as we don't want to call the ConferenceService during this test
        Match::flushEventListeners();

        $supervisor = factory(User::class)->create();
        $group = factory(Group::class)->create();
        $supervisor->groups()->attach($group->id, [
            'role' => 'supervisor',
        ]);
        $match = factory(Match::class)->create();
        $appointment = factory(Appointment::class)->create([
            'match_id' => $match->id,
            'planned_start' => Carbon::tomorrow(),
            'state' => 'PLANNED',
        ]);

        $response = $this->actingAs($supervisor)
            ->json('POST', "/api/v2/appointments/$appointment->id/cancel");

        $response->assertStatus(403);
        $this->assertDatabaseHas('appointments', [
            'id' => $appointment->id,
            'deleted_at' => null,
        ]);
    }

//    public function test_an_appointment_with_tenant_can_not_be_deleted_by_users_of_another_tenant()
//    {
//        Match::flushEventListeners();
//
//        $newTenant = factory(Tenant::class)->create();
//
//        $otherUser = factory(User::class)->create([
//            'tenant_id' => $newTenant->id
//        ]);
//        $appointment = factory(Appointment::class)->create([
//            'match_id' => 1
//        ]);
//
//        $response = $this->actingAs($otherUser)
//            ->json('POST', "/api/v2/appointments/$appointment->id/cancel", [
//                'comment' => 'Everyone is sick'
//            ]);
//
//        $response->assertStatus(404);
//    }
//
//    public function test_an_appointment_with_tenant_can_be_deleted_by_users_of_same_tenant()
//    {
//        Match::flushEventListeners();
//
//
//        $appointment = factory(Appointment::class)->create([
//            'match_id' => 1,
//            'tenant_id' => $this->user->tenant_id,
//            'state' => Appointment::STATES['PLANNED']
//        ]);
//
//
//        $response = $this->actingAs($this->user)
//            ->json('POST', "/api/v2/appointments/$appointment->id/cancel", [
//                'comment' => 'Everyone is sick'
//            ]);
//
//        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
//
//        $this->assertDatabaseHas('appointments', [
//            'id' => $appointment->id,
//            'state' => Appointment::STATES['CANCELED'],
//            'comment' => 'Everyone is sick'
//        ]);
//
//
////        $this->assertSoftDeleted('appointments', ['id' => $appointment->id]);
//    }

    protected function doRequest($user, $appointment)
    {
        //using a mock for the notifications
        Notification::fake();

        $response = $this->actingAs($user)
            ->json('POST', "/api/v2/appointments/$appointment->id/cancel", [
                'comment' => 'Everyone is sick'
            ]);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $this->assertDatabaseHas('appointments', [
            'id' => $appointment->id,
            'comment' => 'Everyone is sick'
        ]);
    }
}
