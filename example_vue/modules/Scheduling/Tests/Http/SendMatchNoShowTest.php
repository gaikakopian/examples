<?php

namespace Modules\Scheduling\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Support\Carbon;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;
use Modules\Scheduling\Domain\Models\Appointment;
use Illuminate\Support\Facades\Notification;
use App\Notifications\Appointment\AppointmentNoShowMissedYouNotification;
use App\Notifications\Appointment\AppointmentNoShowSenderNotification;

/**
 * Test the functionality of the `/api/v1/matches/{id}/noshow` endpoint.
 */
class SendMatchNoShowTest extends EndpointTest
{
    public function test_it_sends_notifications_about_missing_users()
    {
        Notification::fake();

        $otherUser = factory(User::class)->create();
        $match_id = 1;

        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $otherUser->id,
        ]);
        factory(Participation::class)->create([
            'enrollment_id' => $enrollment->id,
            'match_id' => $match_id,
        ]);
        $appointment = factory(Appointment::class)->create([
            'match_id' => 1,
            'plannedStart' => Carbon::now(),
            'state' => 'PLANNED',
        ]);

        $response = $this->actingAs($this->user)->post("/api/v2/matches/$match_id/noshow");

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        Notification::assertSentTo(
            $this->user,
            AppointmentNoShowSenderNotification::class
        );

        Notification::assertSentTo(
            $otherUser,
            AppointmentNoShowMissedYouNotification::class
        );

        $this->assertDatabaseHas('appointments', [
            'id' => $appointment->id,
            'noshow_reportee_id' => $this->user->id,
        ]);
    }
}
