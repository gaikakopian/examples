<?php

namespace Modules\Scheduling\Domain\Models;

use App\Infrastructure\Traits\Statable;
use Carbon\Carbon;
use App\Services\MultiTenant\BelongsToTenant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\Traits\Commentable;
use Modules\Matching\Domain\Models\Match;

class Appointment extends Model
{
    use BelongsToTenant;
    use Commentable; // Appointments don't have an extra endpoint for creating comments; happens in cancel method

    use SoftDeletes;
    use Statable;

    /*
    * State Machine Configuration
    */
    const HISTORY_MODEL = AppointmentStateLog::class;
    const SM_CONFIG = 'appointment'; // the SM graph to use

    /*
    |--------------------------------------------------------------------------
    | Constants
    |--------------------------------------------------------------------------
    */

    /**
     * All possible states that this model can have.
     *
     * @var array
     */
    const STATES = [
        'NEW' => 'NEW',
        'PLANNED' => 'PLANNED',
        'CONFIRMED' => 'CONFIRMED',
        'CANCELED' => 'CANCELED',
        'NOSHOW' => 'NOSHOW',
        'ONLINE' => 'ONLINE',
        'NOATTENDANCE' => 'NOATTENDANCE',
        'TECHNICAL_ISSUES' => 'TECHNICAL_ISSUES',
        'SUCCESS' => 'SUCCESS',
        'ITCHECK' => 'ITCHECK',
        'HANGUP' => 'HANGUP',
    ];

    /**
     * All possible state transitions that can be performed on the model.
     *
     * @var array
     */
    const TRANSITIONS = [
        'plan' => [
            'from' => ['NEW'],
            'to' => 'PLANNED'
        ],
        'reschedule' => [
            'from' => ['PLANNED', 'CONFIRMED'],
            'to' => 'PLANNED'
        ],
        'confirm' => [
            'from' => ['PLANNED'],
            'to' => 'CONFIRMED'
        ],
        'cancel' => [
            'from' => ['PLANNED', 'NOATTENDANCE', 'CONFIRMED'],
            'to' => 'CANCELED'
        ],
        'cancel_after' => [
            'from' => ['PLANNED', 'NOATTENDANCE', 'CONFIRMED'],
            'to' => 'CANCELED'
        ],
        'start' => [
            'from' => ['PLANNED', 'CONFIRMED'],
            'to' => 'ONLINE'
        ],
        'report_noattendance' => [
            'from' => ['PLANNED', 'CONFIRMED'],
            'to' => 'NOATTENDANCE'
        ],
        'report_noshow' => [
            'from' => ['PLANNED', 'CONFIRMED'],
            'to' => 'NOSHOW'
        ],
        'email_noshow' => [
            'from' => ['PLANNED', 'CONFIRMED', 'NOATTENDANCE'],
            'to' => 'NOSHOW'
        ],
        'undo_noshow' => [
            'from' => ['NOSHOW'],
            'to' => 'PLANNED'
        ],
        'finish' => [
            'from' => ['PLANNED', 'ONLINE', 'CONFIRMED'],
            'to' => 'SUCCESS'
        ],
        'finish_after' => [
            'from' => ['PLANNED', 'ONLINE', 'CONFIRMED'],
            'to' => 'SUCCESS'
        ],
        'finish_offline' => [
            'from' => ['PLANNED', 'NOATTENDANCE', 'CONFIRMED'],
            'to' => 'SUCCESS'
        ],
        'report_technicalissues' => [
            'from' => ['PLANNED', 'NOATTENDANCE', 'CONFIRMED'],
            'to' => 'TECHNICAL_ISSUES'
        ],
        'hangup' => [
            'from' => ['ONLINE'],
            'to' => 'HANGUP'
        ],
    ];

    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'match_id',
        'planned_start',
        'planned_duration_minutes',
        'state',
        'noshow_reportee_id',
        'last_state_change_at',
        'comment'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * The fields that should be transformed to date objects.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
//        'planned_start',
        'actual_start',
        'last_nofication_at',
    ];


    protected $casts = [
        'planned_start' => 'datetime'
    ];
    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    /*** An Appointment belongs to a Match.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function match()
    {
        return $this->belongsTo(Match::class);
    }

    public function setPlannedStartAttribute($value)
    {
        if (is_string($value)) {
            $value = Carbon::parse($value);
        }
        $this->attributes['planned_start'] = $value;
    }


    /**
     * An Appointment can have many Enrollments.
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function enrollments()
    {
        return $this->hasManyThrough(
            Enrollment::class,
            Participation::class,
            'match_id', // Participations table
            'id', // Enrollments table
            'match_id', // Appointments table
            'enrollment_id' // Participations table
        );
    }
}
