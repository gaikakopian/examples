<?php

namespace Modules\Scheduling\Http\Actions;

use App\Exceptions\Client\NotAuthorizedException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\GroupService;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Scheduling\Domain\Services\AppointmentService;
use Modules\Scheduling\Http\Resources\AppointmentHttpResource;

class ListAppointmentsForMatch extends Action
{
    protected $appointmentService;
    protected $matchService;
    protected $responder;

    public function __construct(
        AppointmentService $appointmentService,
        MatchService $matchService,
        ResourceResponder $responder
    ) {
        $this->appointmentService = $appointmentService;
        $this->matchService = $matchService;

        $this->responder = $responder;
    }

    public function __invoke(int $id, Request $request)
    {
        /**
         * @var $match Match
         * @var $user User
         */

        $user = Auth::user();
        $match = $this->matchService->get($id);

        // @todo :auth;
//        if ($user->cannot('viewAppointments', $group)) {
//            throw new NotAuthorizedException();
//        }
        $match = $this->matchService->get($id);
        $appointments = $match->appointments;

        return $this->responder->send($appointments, AppointmentHttpResource::class);
    }
}
