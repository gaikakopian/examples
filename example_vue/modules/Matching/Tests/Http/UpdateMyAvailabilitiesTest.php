<?php

namespace Modules\Matching\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Faker\Factory;
use Modules\Matching\Domain\Models\Availability;

/**
 * Test the functionality of the `/api/v1/users/me/availabilities` endpoint.
 */
class UpdateMyAvailabilitiesTest extends EndpointTest
{
    private static $defaultTimezone = 'Europe/Berlin';

    public function test_it_updates_my_availabilities()
    {
        factory(Availability::class)->create([
            'day_of_week' => 1,
            'start_minute_of_day' => 12 * 60,
            'duration_minutes' => 60,
        ]);


        $response = $this->actingAs($this->user)->json('PUT', '/api/v2/users/me/availabilities', [
            'timezone' => self::$defaultTimezone,
            'data' => [
                [
                    'dayOfWeek' => 3,
                    'startMinuteOfDay' => 12 * 60,
                    'durationMinutes' => 30,
                ],
                [
                    'dayOfWeek' => 4,
                    'startMinuteOfDay' => 12 * 60,
                    'durationMinutes' => 60,
                ],
                [
                    'dayOfWeek' => 5,
                    'startMinuteOfDay' => 12 * 60,
                    'durationMinutes' => 90,
                ],
            ]
        ]);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'data' => [
                [
                    'dayOfWeek' => 3,
                    'durationMinutes' => 30,
                    'startMinuteOfDay' => 12 * 60,

                    'timezone' => self::$defaultTimezone,
                ],
                [
                    'dayOfWeek' => 4,
                    'durationMinutes' => 60,
                    'startMinuteOfDay' => 12 * 60,

                    'timezone' => self::$defaultTimezone,
                ],
                [
                    'dayOfWeek' => 5,
                    'durationMinutes' => 90,
                    'startMinuteOfDay' => 12 * 60,

                    'timezone' => self::$defaultTimezone,
                ],
            ]
        ]);

        $this->assertDatabaseHas('availabilities', [
            'owner_id' => 1,
            'day_of_week' => 3,
            'start_minute_of_day' => 12 * 60,
            'duration_minutes' => 30,
            'timezone' => self::$defaultTimezone,
        ]);
    }
}
