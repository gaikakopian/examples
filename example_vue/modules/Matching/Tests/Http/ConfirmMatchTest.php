<?php

namespace Modules\Matching\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Matching\Domain\Models\Match;

/**
 * Test the functionality of the `/api/v1/matches/{id}/confirm` endpoint.
 */
class ConfirmMatchTest extends EndpointTest
{
    public function test_it_confirms_a_match()
    {
        Notification::fake();

        // Disable model events, as we don't want to call the ConferenceService during this test
        Match::flushEventListeners();

        $match = factory(Match::class)->create(
            ['state' => Match::STATES['UNCONFIRMED']]
        );
        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'role' => 'mentor',
        ]);
        $participation = factory(Participation::class)->create([
            'enrollment_id' => $enrollment->id,
            'match_id' => $match->id,
            'match_confirmed_at' => Carbon::now(),
        ]);

        $response = $this->actingAs($this->user)->postJson("/api/v2/matches/$match->id/confirm");

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJsonStructure([
            'data' => [
                'id',
                'completedLection',
                'program',
                'matchedParticipations' => [[]],
            ]
        ]);

        // Make sure the match has been confirmed.
        $participation->refresh();
        $this->assertTrue($participation->match_confirmed_at !== null);
        $this->assertDatabaseHas('participations', [
            'id' => $participation->id,
            'match_confirmed_at' => $participation->match_confirmed_at,
        ]);
    }
}
