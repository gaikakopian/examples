<?php

namespace Modules\Matching\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;
use Modules\Core\Domain\Models\User;


use Modules\Matching\Domain\Models\Availability;
use Modules\Matching\Domain\Services\AvailabilityService\AvailabilityTimeService;

/**
 *
 * Hint:
 * Class AvailabilityServiceTest
 * @package Modules\Core\Tests\Unit
 */
/*
Weekdays:
0 = monday
6 = sunday
 */

class AvailabilityServiceTest extends LaravelTest
{

    /**
     * @var AvailabilityTimeService
     */
    protected $service;
    /**
     * @var User
     */
    protected $user1;
    /**
     * @var User
     */
    protected $user2;
    /**
     * @var User
     */
    protected $user3;
    /**
     * @var User
     */
    protected $user4;

    /** {@inheritdoc} */
    protected function setUp()
    {
        parent::setUp();

        $this->service = new AvailabilityTimeService();

        $this->user1 = factory(User::class)->create(['timezone' => 'Europe/Berlin']);
        $this->user2 = factory(User::class)->create(['timezone' => 'Europe/Dublin']); // 1 hour behind berlin in summer and winter
        $this->user3 = factory(User::class)->create(['timezone' => 'Europe/Berlin']);
        $this->user4 = factory(User::class)->create(['timezone' => 'Europe/Berlin']);


        // tz here are always utc, no matter of the owner;

        /*******************************
         * User 1: Berlin
         ********************************/
        // user 1 = utc +1 timezone
        // UTC: Monday, 15-16
        factory(Availability::class)->create([
            'day_of_week' => 0,
            'start_minute_of_day' => 16 * 60,
            'duration_minutes' => 60,
            'owner_id' => $this->user1->id
        ])->first();
        // UTC: Monday, 9-12
        factory(Availability::class)->create([
            'day_of_week' => 0,
            'start_minute_of_day' => 10 * 60,
            'duration_minutes' => 180,
            'owner_id' => $this->user1->id
        ])->first();

        // UTC: Thursday, 13-15
        factory(Availability::class)->create([
            'day_of_week' => 3,

            'start_minute_of_day' => 14 * 60,
            'duration_minutes' => 120,
            'owner_id' => $this->user1->id
        ])->first();
        // UTC: Friday, 9-19
        factory(Availability::class)->create([
            'day_of_week' => 4,
            'start_minute_of_day' => 10 * 60,
            'duration_minutes' => 600,
            'owner_id' => $this->user1->id
        ])->first();


        /*******************************
         * User 2: Dublin
         ********************************/
        // user 2 = utc timezone
        // UTC: Sunday, 23 - Monday, 19
        factory(Availability::class)->create([
            'day_of_week' => 6,
            'start_minute_of_day' => 23 * 60,
            'duration_minutes' => 1200,
            'owner_id' => $this->user2->id,
            'timezone' => 'Europe/Dublin'
        ])->first();
        // UTC: Thursday, 13-15
        factory(Availability::class)->create([
            'day_of_week' => 3,
            'start_minute_of_day' => 13 * 60,
            'duration_minutes' => 120,
            'owner_id' => $this->user2->id,
            'timezone' => 'Europe/Dublin'
        ])->first();


        /*******************************
         * User 3: Berlin
         ********************************/

        // user 3 = utc +1 timezone
        // UTC: Monday, 9-19
        factory(Availability::class)->create([
            'day_of_week' => 0,
            'start_minute_of_day' => 10 * 60,
            'duration_minutes' => 600,
            'owner_id' => $this->user3->id
        ])->first();
        // UTC: Thursday, 17-18
        factory(Availability::class)->create([
            'day_of_week' => 3,
            'start_minute_of_day' => 18 * 60,
            'duration_minutes' => 60,
            'owner_id' => $this->user3->id
        ])->first();
        // UTC: Friday, 9-19
        $user3_availability_friday = factory(Availability::class)->create([
            'day_of_week' => 4,
            'start_minute_of_day' => 10 * 60,
            'duration_minutes' => 600,
            'owner_id' => $this->user3->id
        ])->first();

        /*******************************
         * User 4: Berlin
         ********************************/

        // user 4 = utc +1 timezone
        // UTC: Wednesday, 14-15
        $user4_availability_wednesday = factory(Availability::class)->create([
            'day_of_week' => 2,
            'start_minute_of_day' => 15 * 60,
            'duration_minutes' => 60,
            'owner_id' => $this->user4->id

        ])->first();
    }

    public function test_it_filters_correctly()
    {

        // utc availabilities:
        // user1
        //     Monday, 15-16
        //     Monday, 9-12
        //     Thursday, 13-15
        //     Friday, 9-19
        // user2
        //     Sunday, 23 - Monday, 19 -> 240 m
        //     Thursday, 13-15 -> 120 m
        // user3
        //     Monday, 9-19 -> 240
        //     Thursday, 17-18
        //     Friday, 9-19 -> 600 m
        // user4
        //     Wednesday, 14-15
        // intersections should be:

        $other_users = collect([$this->user2, $this->user3, $this->user4]);

//        $this->printAvailbilityOfUser($this->user1);

//        foreach ($other_users as $user) {
//            $this->printAvailbilityOfUser($user);
//        }

        $available_users = $this->service->findOverlappingAvailbilityUsers($this->user1, $other_users);

        $this->assertEquals(2, $available_users->count(), 'Overlapping avialbilityies wrong;');


        $this->assertEquals($this->user3->id, $available_users[0]->id);
        $this->assertEquals($this->user2->id, $available_users[1]->id);

        // overlap betweem user 1 with user 2
        $this->assertEquals(840, $available_users[0]->overlapping_minutes);

        // overlap betweem user 1 with user 3
        $this->assertEquals(360, $available_users[1]->overlapping_minutes);
    }

    /**
     * To debug evalaiblityes
     * @param User $user
     */
    protected function printAvailbilityOfUser(User $user)
    {
        echo $user->email . ':' . $user->availabilities->count() . PHP_EOL;

        foreach ($user->availabilities as $availability) {

            /** @var Availability $availability */
            $inTz = $availability->getInTz('Europe/Berlin');
            echo $inTz['dayOfWeek'] . $inTz['minuteOfWeek'] . $inTz['durationMinutes'] . PHP_EOL;
        }
    }
}
