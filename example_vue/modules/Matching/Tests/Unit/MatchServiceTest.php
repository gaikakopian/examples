<?php

namespace Modules\Matching\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;

class MatchServiceTest extends LaravelTest
{
    public function test_it_retrieves_a_match_by_external_room_id()
    {
        $service = app(MatchService::class);

        $match = $service->update(1, ['external_room_id' => '908b0f1d-d1e9-4ca4-a1d9-b8783f9a0ba7']);
        $result = $service->getByExternalRoomId('908b0f1d-d1e9-4ca4-a1d9-b8783f9a0ba7');

        $this->assertEquals($match->id, $result->id);
    }

    public function test_it_determins_a_mutual_match()
    {
        /** @var MatchService $service */
        $service = app(MatchService::class);

        $match = Match::with('enrollments')->find(2);

        $enrollments = $match->enrollments;

        $userA = $enrollments[0]->user;
        $userB = $enrollments[1]->user;


        $result = $service->findMatchOfUsers($userA, $userB);

        $this->assertEquals($match->id, $result->id);
    }

    public function test_it_determins_not_a_mutual_match()
    {
        /** @var MatchService $service */
        $service = app(MatchService::class);


        $user = User::where('email', '=', 'user@volunteer-vision.com')->firstOrFail();
        $userB = Factory(User::class)->create();


        $result = $service->findMatchOfUsers($user, $userB);

        $this->assertNull($result);
    }

    public function test_activation_on_multple_rejections()
    {

        // activateMatchIfHasMultipleRejections
        /** @var MatchService $service */
        $service = app(MatchService::class);

        $userB = Factory(User::class)->create();
        $programId = 3;

        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $userB->id,
            'program_id' => $programId,
            'role' => Role::ROLES['mentor'],
        ]);
        $match = null;

        // generate two rejected matches;
        for ($i = 0; $i < 2; $i++) {
            $match = factory(Match::class)->create([
                'program_id' => $programId,
                'state' => Match::STATES['REJECTED'],
                'licence_activated' => null
            ]);
            factory(Participation::class)->create([
                'enrollment_id' => $enrollment->id,
                'match_id' => $match->id,
            ]);

        }
        $service->activateMatchIfHasMultipleRejections($match);

        $this->assertNull($match->licence_activated);

        $match = factory(Match::class)->create(['program_id' => $programId, 'state' => Match::STATES['REJECTED']]);
        factory(Participation::class)->create([
            'enrollment_id' => $enrollment->id,
            'match_id' => $match->id,
        ]);

        $service->activateMatchIfHasMultipleRejections($match);
        $this->assertNotNull($match->licence_activated);


    }
}
