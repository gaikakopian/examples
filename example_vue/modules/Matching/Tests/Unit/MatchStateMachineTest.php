<?php

namespace Modules\Matching\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;
use App\Notifications\Match\MatchConfirmedNotification;
use App\Notifications\Match\MatchFinishedNotification;
use App\Notifications\Match\MatchRequestNotification;
use App\Notifications\Match\MatchSuggestionNotification;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Notification;
use Mockery;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ConferenceService;
use Modules\Matching\Domain\Models\Match;

class MatchStateMachineTest extends LaravelTest
{
    use DatabaseTransactions;

    /**
     * @var Match
     */
    protected $model;
    protected $mentor_user;
    protected $mentee_user;


    public function test_it_does_not_send_a_request_to_the_mentee_when_accepted_already()
    {
        Notification::fake();
        $this->assertEquals('NEW', $this->model->currentState());

        $en = $this->model->getMenteeParticipation()->enrollment;
        $en->last_request_accepted_at = Carbon::now();
        $en->save();

        //------------------------- test transition 'suggest'-------------------
        $result = $this->model->transition('request');

        $this->assertTrue($result);
        $this->assertEquals(Match::STATES['UNCONFIRMED'], $this->model->currentState());

    }
    public function test_it_sends_a_request_to_the_mentee()
    {
        Notification::fake();
        $this->assertEquals('NEW', $this->model->currentState());

        $en = $this->model->getMenteeParticipation()->enrollment;
        $en->last_request_accepted_at = null;
        $en->save();

        //------------------------- test transition 'suggest'-------------------
        $result = $this->model->transition('request');

        $this->assertTrue($result);
        $this->assertEquals(Match::STATES['REQUESTED'], $this->model->currentState());

        //asserting that the transition has been logged in the history
        $this->assertEquals(1, $this->model->history()->count());
        $this->assertNotNull($this->model->history()->first()->actor_id);

        //asserting that the 'last_state_change_at' column is updated
        $this->assertNotNull($this->model->last_state_change_at);

        //asserting that a notification has been sent
        Notification::assertSentTo(
            $this->mentee_user,
            MatchRequestNotification::class
        );

    }  public function test_it_does_not_send_a_request_if_mentee_already_confirmed()
    {
        $this->markTestSkipped('Not implmented');

        Notification::fake();
        $this->assertEquals('NEW', $this->model->currentState());

        //------------------------- test transition 'suggest'-------------------
        $result = $this->model->transition('request');

        $this->assertTrue($result);
        $this->assertEquals(Match::STATES['UNCONFIRMED'], $this->model->currentState());

        //asserting that the 'last_state_change_at' column is updated
        $this->assertNotNull($this->model->last_state_change_at);

        //asserting that a notification has been sent
        Notification::assertSentTo(
            $this->mentor_user,
            MatchSuggestionNotification::class
        );

    }
    public function test_it_applies_a_transition()
    {
        Notification::fake();
        $this->model->state = Match::STATES['APPROVED'];
        $this->model->save();

        $this->assertEquals('APPROVED', $this->model->currentState());

        //------------------------- test transition 'suggest'-------------------
        $result = $this->model->transition('suggest');

        $this->assertTrue($result);
        $this->assertEquals(Match::STATES['UNCONFIRMED'], $this->model->currentState());

        //asserting that the transition has been logged in the history
        $this->assertEquals(1, $this->model->history()->count());
        $this->assertNotNull($this->model->history()->first()->actor_id);

        //asserting that the 'last_state_change_at' column is updated
        $this->assertNotNull($this->model->last_state_change_at);

        //asserting that a notification has been sent
        Notification::assertSentTo(
            $this->mentor_user,
            MatchSuggestionNotification::class
        );

        //--------------------------------------------------------------------


        //----------------------------- test transition 'confirm' --------------
        $result = $this->model->transition('confirm');

        $this->assertTrue($result);
        $this->assertEquals(Match::STATES['CONFIRMED'], $this->model->currentState());

        //asserting that a cancel notification has been sent
//        Notification::assertSentTo(
//            $this->mentee_user,
//            MatchConfirmedNotification::class
//        );
        //asserting that the transition has been logged in the history
        $this->assertEquals(2, $this->model->history()->count());

        //--------------------------------------------------------------------

        //----------------------------- test transition 'activate' --------------
        $result = $this->model->transition('activate');

        $this->assertTrue($result);
        $this->assertEquals(Match::STATES['ACTIVE'], $this->model->currentState());

        //asserting that the transition has been logged in the history
        $this->assertEquals(3, $this->model->history()->count());

        //--------------------------------------------------------------------

        //----------------------------- test transition 'finish' --------------
        $result = $this->model->transition('finish');

        $this->assertTrue($result);
        $this->assertEquals(Match::STATES['DONE'], $this->model->currentState());

        //asserting that a cancel notification has been sent
        Notification::assertSentTo(
            $this->mentee_user,
            MatchFinishedNotification::class
        );
        Notification::assertSentTo(
            $this->mentor_user,
            MatchFinishedNotification::class
        );
        //asserting that the transition has been logged in the history
        $this->assertEquals(4, $this->model->history()->count());

        //--------------------------------------------------------------------
    }

    protected function setUp()
    {
        parent::setUp();
        Notification::fake();


        $this->mentor_user = factory(User::class)->create(['accept_email' => true, 'accept_sms' => true, 'preferred_channel' => null]);
        $this->mentee_user = factory(User::class)->create(['accept_email' => true, 'accept_sms' => true, 'preferred_channel' => null]);

        // Mock the ConferenceService because we don't want to test that
        $conferenceServiceMock = Mockery::mock(ConferenceService::class);
//        $conferenceServiceMock->shouldReceive('createRoomForMatch')
//            ->once()
//            ->andReturn('someRoomId');
        App::instance(ConferenceService::class, $conferenceServiceMock);

        //tested Model
        $this->model = factory(Match::class)->create();

        $mentor_enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->mentor_user->id,
            'role' => 'mentor'
        ]);
        $mentee_enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->mentee_user->id,
            'role' => 'mentee'
        ]);
        factory(Participation::class)->create([
            'enrollment_id' => $mentor_enrollment->id,
            'match_id' => $this->model->id,
        ]);
        factory(Participation::class)->create([
            'enrollment_id' => $mentee_enrollment->id,
            'match_id' => $this->model->id,
        ]);

        //setting a fake_actor that represents the authenticated user who is applying changes
        $fake_actor = factory(User::class)->create();
        $this->be($fake_actor);
    }
}
