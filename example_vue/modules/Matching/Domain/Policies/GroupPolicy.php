<?php

namespace Modules\Matching\Domain\Policies;

use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\User;

class GroupPolicy
{
    /**
     * A User can view all appointments for Users of a Group if the User
     * is a `supervisor` for the Group.
     *
     * @param User $user
     * @param Group $group
     * @return boolean
     */
    public function viewAppointments(User $user, Group $group) : bool
    {
        return $this->supervise($user, $group);
    }

    /**
     * Determine whether a user has `supervisor` role in certain group.
     *
     * @param User $user
     * @param Group $group
     * @return boolean
     */
    public function supervise(User $user, Group $group) : bool
    {
        return $user->groups()
            ->where('group_id', $group->id)
            ->where('role', 'supervisor')
            ->count() >= 1;
    }
}
