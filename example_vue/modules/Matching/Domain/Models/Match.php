<?php

namespace Modules\Matching\Domain\Models;

use App\Domain\Models\BaseModel;
use App\Infrastructure\Traits\Statable;

use App\Reminders\Traits\Remindable;
use App\Services\MultiTenant\BelongsToTenant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\Traits\Commentable;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Events\MatchCreated;
use Modules\Scheduling\Domain\Models\Appointment;

class Match extends BaseModel
{
    use BelongsToTenant;
    use Commentable;
    use Statable;
    use Remindable;

    /*
    * State Machine Configuration
    */
    const HISTORY_MODEL = MatchStateLog::class;
    const SM_CONFIG = 'match'; // the SM graph to use

    /*
    |--------------------------------------------------------------------------
    | Constants
    |--------------------------------------------------------------------------
    */

    /**
     * All possible states that this model can have.
     *
     * @var array
     */
    const STATES = [
        'NEW' => 'NEW',
        'REQUESTED' => 'REQUESTED',
        'APPROVED' => 'APPROVED',
        'UNCONFIRMED' => 'UNCONFIRMED',
        'CONFIRMED' => 'CONFIRMED',
        'ACTIVE' => 'ACTIVE',
        'PAUSED' => 'PAUSED',
        'REJECTED' => 'REJECTED',
        'DONE' => 'DONE',
        'CANCELED' => 'CANCELED',
    ];

    const ACTIVESTATES = [self::STATES['ACTIVE'], self::STATES['CONFIRMED']];
    const NEWSTATES = [
        self::STATES['NEW'],
        self::STATES['REQUESTED'],
        self::STATES['APPROVED'],
        self::STATES['UNCONFIRMED'],
        self::STATES['CONFIRMED'],
    ];


    /**
     * All possible state transitions that can be performed on the model.
     *
     * @var array
     */
    const TRANSITIONS = [

// mentee
        'request' => [
            'from' => ['NEW'],
            'to' => 'REQUESTED'
        ],
        'mentee_approve' => [
            'from' => ['NEW', 'REQUESTED'],
            'to' => 'APPROVED'
        ],
        'mentee_reject_auto' => [
            'from' => ['NEW', 'REQUESTED'],
            'to' => 'REJECTED'
        ],
        'mentee_reject' => [
            'from' => ['NEW', 'REQUESTED'],
            'to' => 'REJECTED'
        ],

// mentor
        'suggest' => [
            'from' => ['REQUESTED', 'APPROVED'],
            'to' => 'UNCONFIRMED'
        ],
        'confirm' => [
            'from' => ['UNCONFIRMED'],
            'to' => 'CONFIRMED'
        ],

        'reject' => [
            'from' => ['UNCONFIRMED', 'REQUEST'],
            'to' => 'REJECTED'
        ],
        'autoreject' => [
            'from' => ['UNCONFIRMED'],
            'to' => 'REJECTED'
        ],
// after accepting both;
        'activate' => [
            'from' => ['CONFIRMED'],
            'to' => 'ACTIVE'
        ],
        'pause' => [
            'from' => ['ACTIVE'],
            'to' => 'PAUSED'
        ],
        'pause_again' => [
            'from' => ['ACTIVE', 'PAUSED'],
            'to' => 'PAUSED'
        ],
        'resume' => [
            'from' => ['PAUSED'],
            'to' => 'ACTIVE'
        ],
        'auto_resume' => [
            'from' => ['PAUSED'],
            'to' => 'ACTIVE'
        ],
        'finish' => [
            'from' => ['ACTIVE'],
            'to' => 'DONE'
        ],
        'cancel' => [
            'from' => ['CONFIRMED', 'PAUSED', 'ACTIVE'],
            'to' => 'CANCELED'
        ],
    ];

    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'program_id',
        'external_room_id',
        'state',
        'rejector_id',
        'lections_completed',
        'licence_activated',
        'licence_reason',
        'last_state_change_at',
        'tokbox_session_id',
        'match_done_at',
        'user_comment', // comment for rejection, etc.
        'mentor_comment', // comment for certificate
        'last_reminder',
        'last_reminder_type',
        'licence_free',
        'classroom_version',
        'licence_rescinded_at',
        'old_id'

    ];

    /**
     * The attributes that should be interpreted as dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The attributes that should be hidden on arrays.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    //

//    protected $dateFormat = 'Y-m-d\TH:i:s.v\Z';
//    protected $dateFormat = 'Iso8601Zulu';
//    protected $dateFormat = 'Y-m-d\TH:i:s\Z';

    protected $casts = [
        'last_state_change_at' => 'datetime',
        'licence_rescinded_at' => 'datetime',
        'last_reminder' => 'datetime',
        'match_done_at' => 'datetime',
        'match_confirmed_at' => 'datetime',
        'licence_activated' => 'datetime',
    ];


    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => MatchCreated::class,
    ];

    public $conferenceRoomUri;
    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */


    /**
     * A Match belongs to a Program.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function program()
    {
        return $this->belongsTo(Program::class);
    }

    /**
     * Many Participations can be matched together.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function participations()
    {
        return $this->hasMany(Participation::class);
    }


    /**
     *
     * @param User $me
     * @return Collection <Participation>
     */
    public function getOtherParticipations(User $me)
    {

        /** @var Collection $participations */
        $participations = $this->participations()
            ->leftJoin('enrollments', 'enrollments.id', '=', 'participations.enrollment_id')
            ->where('enrollments.user_id', '<>', $me->id)
            ->get();

        return $participations;
    }


    /**
     * Many Enrollments can be matched together.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function enrollments()
    {
        return $this->hasManyThrough(
            Enrollment::class,
            Participation::class,
            'match_id', // Participation table
            'id', // Enrollment table
            'id', // Match table
            'enrollment_id' // Participation table
        );
    }

    /**
     * A Match can have many associated Appointments.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    public function rejector()
    {
        return $this->belongsTo(User::class);
    }


    public function getLatestAppointment()
    {
        $appointments = $this->appointments->filter(function ($appointment) {
            return $appointment->state == Appointment::STATES['PLANNED'];
        });
        $appointments->sortBy(function ($appointment) {
            return $appointment->planned_start;
        });
        return $appointments->first();
    }

    /**
     * @return null | Appointment
     */
    public function getCurrentAppointment()
    {
        $latest = null;
        $appointments = $this->appointments->filter(function ($appointment) {
            return $appointment->state == Appointment::STATES['PLANNED'];
        });
        if ($appointments->count() === 0) {
            $this->appointments->first();
        }
        $appointments->sortBy(function ($appointment) {
            return $appointment->planned_start;
        });

        return $appointments->first();
    }

    public function getMentorParticipation(): Participation
    {
        /** @var Collection $participations */
        $participations = $this->participations;
        return $participations->first(function ($participation) {
            return $participation->enrollment->role == Role::ROLES['mentor'];
        });


    }

    public function getMenteeParticipation(): Participation
    {
        /** @var Collection $participations */
        $participations = $this->participations;
        return $participations->first(function ($participation) {
            return $participation->enrollment->role == Role::ROLES['mentee'];
        });
    }

    public function isFirstAppointment()
    {
        // could also use this state
        return $this->lections_completed == 0;
    }

    public function getParticipationOfUser(User $user): Participation
    {
        /** @var Collection $participations */
        $participations = $this->participations;
        return $participations->first(function ($participation) use ($user) {
            return $participation->enrollment->user_id == $user->id;
        });

    }


    public function activateLicence(string $reason)
    {
        $this->licence_reason = $reason;
        $this->licence_activated = Carbon::now();

    }

    public function getHash()
    {
        return hash('sha1', $this->id);
    }

    public function getHashAttribute()
    {
        return $this->getHash();

    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return in_array($this->state, self::ACTIVESTATES);
    }

    /**
     * @return bool
     */
    public function isNew(): bool
    {
        return in_array($this->state, self::NEWSTATES);
    }

    /**
     * @return bool
     */
    public function certificateAllowed(): bool
    {
        return in_array($this->state, [self::STATES['ACTIVE'], self::STATES['DONE']]);
    }
}
