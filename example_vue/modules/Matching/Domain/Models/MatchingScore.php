<?php

namespace Modules\Matching\Domain\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\Program;

class MatchingScore extends Model
{

    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mentor_participation_id',
        'mentee_participation_id',
        'program_id',

        'algorithm_value',
        'final_value',
        'time_overlap',
        'time_difference',
        'distance',
        'notes',
        'algorithm'
    ];

    /**
     * The attributes that should be interpreted as dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The attributes that should be hidden on arrays.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    //

    protected $casts = [

    ];

    public function mentorParticipation()
    {
        return $this->belongsTo(Participation::class);
    }

    public function menteeParticipation()
    {
        return $this->belongsTo(Participation::class);
    }
    public function Program()
    {
        return $this->belongsTo( Program::class);
    }


}
