<?php

namespace Modules\Matching\Domain\Models;

use App\Infrastructure\Traits\LogsState;
use Illuminate\Database\Eloquent\Model;

class MatchStateLog extends Model
{
    use LogsState;

    protected $fillable = ['match_id', 'actor_id', 'transition', 'from', 'to'];

    public function match()
    {
        return $this->belongsTo(Match::class);
    }

    public function actor()
    {
        return $this->belongsTo(Match::class);
    }
}
