<?php

namespace Modules\Matching\Domain\Interfaces;

interface HasAvailabilityInterface
{
    public function availabilities();
}
