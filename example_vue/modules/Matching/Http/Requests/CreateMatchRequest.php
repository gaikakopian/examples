<?php

namespace Modules\Matching\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class CreateMatchRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules() : array
    {
        return [
            'participation_ids' => 'required|array',
            'participation_ids.*' => 'required|integer|exists:participations,id',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function authorize() : bool
    {
        // Authentication is done via the Scope Middleware
        return true;
    }
}
