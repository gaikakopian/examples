<?php

namespace Modules\Matching\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Services\AvailabilityService;
use Modules\Matching\Http\Requests\ShowAvailabilitiesRequest;
use Modules\Matching\Http\Resources\AvailabilityHttpResource;

/**
 * @tdb: this is used be admin route;
 *
 * Class ShowAvailabilitiesOfUserAction
 * @package Modules\Matching\Http\Actions
 */
class ShowAvailabilitiesOfUserAction extends Action
{
    protected $availabilityService;
    protected $responder;

    public function __construct(AvailabilityService $availabilityService, ResourceResponder $responder)
    {
        $this->availabilityService = $availabilityService;
        $this->responder = $responder;
    }

    public function __invoke($id, Request $request)
    {

        /** @var User $user * */
        $user = User::findOrFail($id);

        $availabilities = $this->availabilityService->listFor($user);

        return $this->responder->send($availabilities, AvailabilityHttpResource::class);
    }
}
