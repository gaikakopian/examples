<?php

namespace Modules\Matching\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchingService;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Matching\Http\Requests\CreateMatchRequest;
use Modules\Matching\Http\Resources\MatchHttpResource;

class CreateMatchScoresAction extends Action
{
    protected $matchingService;
    protected $responder;

    public function __construct(MatchingService $matchingService, ResourceResponder $responder)
    {
        $this->matchingService = $matchingService;
        $this->responder = $responder;
    }

    public function __invoke()
    {

        $result = $this->matchingService->generateMissingCombinations();

        return response()->json(['combinations' => $result]);
    }
}
