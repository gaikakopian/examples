<?php

namespace Modules\Matching\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\AvailabilityService;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Matching\Http\Requests\ShowAvailabilitiesRequest;
use Modules\Matching\Http\Resources\AvailabilityHttpResource;

class ShowMatchAvailabilitiesAction extends Action
{
    protected $availabilityService;
    protected $matchService;
    protected $responder;

    public function __construct(
        AvailabilityService $availabilityService,
        MatchService $matchService,
        ResourceResponder $responder
    )
    {
        $this->availabilityService = $availabilityService;
        $this->responder = $responder;
        $this->matchService = $matchService;
    }

    public function __invoke($id, Request $request)
    {

        /** @var User $user * */
        $user = Auth::user();

        /** @var Match $match */
        $match = $this->matchService->get($id);

        /** @var Enrollment $otherUsersEnrollment */
        $otherUsersParticipation = $match->getOtherParticipations($user)->first();

        if (!$otherUsersParticipation) {
            return [];
        }

        /** @var Participation $opponent */
        $opponent = $otherUsersParticipation->enrollment->user;

        $availabilities = $this->availabilityService->listForUser($opponent); // @todo: only mock;

        // 1. find participant who is not mysefl on match.
        // 2. find availaiblities of other person,
        // 3. send to client.


        return $this->responder->send($availabilities, AvailabilityHttpResource::class);
    }
}
