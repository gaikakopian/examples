<?php

namespace Modules\Matching\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Participation;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Matching\Http\Resources\MatchHttpResource;
use App\Exceptions\Client\NotAuthorizedException;
use Modules\Core\Domain\Services\ParticipationService;

class ConfirmMatchAction extends Action
{
    protected $participationService;
    protected $matchService;
    protected $responder;

    public function __construct(
        ParticipationService $participationService,
        MatchService $matchService,
        ResourceResponder $responder
    ) {
        $this->participationService = $participationService;
        $this->matchService = $matchService;
        $this->responder = $responder;
    }

    public function __invoke($id, Request $request)
    {
        $user = Auth::user();
        /** @var Match $match */
        $match = $this->matchService->get($id);
        /** @var Participation $participation */
        $participation = $this->participationService->getForUserAndMatch($user->id, $match->id);

        if ($user->cannot('confirm', [$match, $participation])) {
            throw new NotAuthorizedException;
        }

        $participation->confirmMatch();
        $match->refresh();

        return $this->responder->send($match, MatchHttpResource::class);
    }
}
