<?php

namespace Modules\Matching\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use App\Notifications\Match\MatchUserLeftAMessageNotification;
use App\Services\WaboboxService;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Services\UserFeedbackService;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Matching\Http\Requests\AddFeedbackRequest;

use Modules\Matching\Http\Requests\AddLeaveAMessageRequest;
use Modules\Scheduling\Domain\Models\Appointment;

/**
 * Class CallMatchAction
 * @package Modules\Matching\Http\Actions
 */
class AddLeaveAMessageAction extends Action
{
    protected $matchService;
    protected $userFeedbackService;
    protected $waboboxService;


    public function __construct(
        MatchService $matchService,
        UserFeedbackService $userFeedbackService,
        WaboboxService $waboboxService
    )
    {
        $this->matchService = $matchService;
        $this->userFeedbackService = $userFeedbackService;
        $this->waboboxService = $waboboxService;


    }

    /**
     * @todo: check if user has right to post feedback.
     *
     * @param $id
     * @param AddFeedbackRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ClientException
     */
    public function __invoke($id, AddLeaveAMessageRequest $request)
    {
        $sender = Auth::user();

        /** @var Match $match */
        $match = $this->matchService->get($id);

        // get user of this match which are not me...

        $enrollments = $match->enrollments;


        foreach ($enrollments as $enrollment) {
            /** @var User $user */
            $user = $enrollment->user;
            if ($user->id === $sender->id) {
                continue;
            }
            // @todo -- this is a whatsapp test
//            $this->waboboxService->queueMessage($user, 'Hello '. $user->first_name . '! Your Partner left a message for you on the Volunteer Vision platform : '. $request->message );
            $notification = new MatchUserLeftAMessageNotification($enrollment->user->brand, $sender, $request->attach_contact_information, $request->message);
            $user->notify($notification);
        }


        return response()->json(['message' => 'ok']);
    }
}
