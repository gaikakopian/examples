<?php

namespace Modules\Admin\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;
use App\Reminder\Handler\MatchReminderHandler;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Modules\Admin\Domain\Models\CoordinatorTodo;
use Modules\Admin\Domain\Services\CoordinatorTodoService;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\GroupService;

class CoordinatorTodoServiceTest extends LaravelTest
{

    /**
     * @var GroupService
     */
    protected $coordinatorTodoService;

    public function setUp()
    {
        parent::setUp();

        $this->coordinatorTodoService = app(CoordinatorTodoService::class);
    }


    public function test_it_allows_object_as_description()
    {


        $user = User::query()->inRandomOrder()->first();

        $todo = new CoordinatorTodo(
            [
                'customer_id' => $user->id,
                'description' => '',
                'meta' => ['match_id' => 1],
                'type' => CoordinatorTodo::TYPES['NO_SHOW'],
                'duedate' => Carbon::now()
            ]);
        $added = $this->coordinatorTodoService->createTodoIfNotExistsWithinDays($todo, 7);

        $this->assertEquals($added, true);


    }

}
