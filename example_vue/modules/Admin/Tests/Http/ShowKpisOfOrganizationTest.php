<?php

namespace Modules\Admin\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Support\Carbon;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\User;

class ShowKpisOfOrganizationTest extends EndpointTest
{
    public function test_it_shows_the_kpis_with_different_dates()
    {


        $org = Organization::query()->inRandomOrder()->first();
        $response = $this->actingAs($this->admin)
            ->json('GET', "/api/v2/admin/organizations/" . $org->id . "/kpis");

        $this->assertEquals($response->getStatusCode(), 200, $response->getContent());

        $response->assertJsonStructure([
            'userCount',
            'matchablePaticipations',
            'activeMentorships'
        ]);
    }
}
