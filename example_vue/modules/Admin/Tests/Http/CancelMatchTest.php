<?php

namespace modules\Admin\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

class CancelMatchTest extends EndpointTest
{
    public function test_i_can_cancel_a_match_in_admin()
    {
        // Disable model events, as we don't want to call the ConferenceService during this test
        Match::flushEventListeners();
        $match = factory(Match::class)->create(
            ['state' => Match::STATES['ACTIVE']]
        );

        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'role' => 'mentee',
        ]);
        $participation = factory(Participation::class)->create([
            'enrollment_id' => $enrollment->id,
            'match_id' => $match->id,
            'match_confirmed_at' => Carbon::now(),
        ]);

        $response = $this->actingAs($this->admin)->postJson("/api/v2/admin/matches/$match->id/cancel");

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $this->assertDatabaseHas('matches', [
            'id' => $match->id,
            'state' => Match::STATES['CANCELED'],
        ]);
    }

    public function test_i_can_cancel_a_match_in_admin_with_comment()
    {
        // Disable model events, as we don't want to call the ConferenceService during this test
        Match::flushEventListeners();
        $match = factory(Match::class)->create(
            ['state' => Match::STATES['ACTIVE']]
        );

        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'role' => 'mentee',
        ]);
        $participation = factory(Participation::class)->create([
            'enrollment_id' => $enrollment->id,
            'match_id' => $match->id,
            'match_confirmed_at' => Carbon::now(),
        ]);

        $message = 'hello';

        $response = $this->actingAs($this->admin)->postJson("/api/v2/admin/matches/$match->id/cancel",
            ['comment' => $message]);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $this->assertDatabaseHas('matches', [
            'id' => $match->id,
            'state' => Match::STATES['CANCELED'],
        ]);
        $this->assertDatabaseHas('comments', [
            'commentable_id' => $this->user->id,
            'author_id' => $this->admin->id
//            'body' => $message,
        ]);
    }
}
