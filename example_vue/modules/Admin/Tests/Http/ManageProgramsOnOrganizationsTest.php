<?php

namespace modules\Admin\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Support\Facades\DB;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;

class ManageProgramsOnOrganizationsTest extends EndpointTest
{
    protected $progam;
    protected $organization;

    protected function setUp()
    {
        parent::setUp();
        $this->organization = Organization::inRandomOrder()->firstOrFail();
        $this->program = Program::inRandomOrder()->firstOrFail();
    }


    public function test_addProgramToAnOrganization()
    {
        $organization = $this->organization;
        $program =  $this->program;

        // make sure it does not exist already;

//        DB::raw("DELETE FROM organization_program WHERE organization_id = ? AND program_id = ? ",
//            [$organization->id, $program->id])->get();
        $organization->programs()->detach();

        $url = '/api/v2/admin/organizations/' . $organization->id . '/programs/' . $program->id;

        // Retrieve a single Translation via API
        $response = $this->actingAs($this->admin)
            ->postJson($url);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


        $this->assertDatabaseHas('organization_program', [
            'organization_id' => $organization->id,
            'program_id' => $program->id
        ]);


        // try to add the same program gain
        $response = $this->actingAs($this->admin)
            ->postJson($url);
        $this->assertEquals(422, $response->getStatusCode(), $response->getContent());
    }

    public function test_removeProgramToAnOrganization()
    {
        $organization = $this->organization;
        $program =  $this->program;

        DB::delete(
            'DELETE FROM organization_program WHERE organization_id = ? AND program_id = ? ',
            [$organization->id, $program->id]
        );

        $organization->programs()->attach($program);

        $url = '/api/v2/admin/organizations/' . $organization->id . '/programs/' . $program->id;

        // Retrieve a single Translation via API
        $response = $this->actingAs($this->admin)
            ->delete($url)
            ->assertStatus(200);

        $this->assertDatabaseMissing('organization_program', [
            'organization_id' => $organization->id,
            'program_id' => $program->id
        ]);
    }
}
