<?php

namespace Modules\Admin\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\User;

class sendWhatsappActionTest extends EndpointTest
{
    public function test_it_receives_an_access_token_for_another_user()
    {
        $this->markTestSkipped('does not work if whatsapp is offline');

        $subject = factory(User::class)->create();

        $response = $this->actingAs($this->admin)
            ->json('POST', "/api/v2/admin/users/$subject->id/sendwhatsapp", ['message' => 'hello world']);


        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

    }

}
