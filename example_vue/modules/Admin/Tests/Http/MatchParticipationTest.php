<?php

namespace modules\Admin\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

class MatchParticipationTest extends EndpointTest
{
    public function test_it_matches_to_participations()
    {
        // Disable model events, as we don't want to call the ConferenceService during this test
        Match::flushEventListeners();
        \Notification::fake();

        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'role' => 'mentee',
        ]);
        $participation = factory(Participation::class)->create([
            'enrollment_id' => $enrollment->id
        ]);
        $enrollment2 = factory(Enrollment::class)->create([
            'user_id' => $this->admin->id,
            'role' => 'mentor',
        ]);
        $participation2 = factory(Participation::class)->create([
            'enrollment_id' => $enrollment->id
        ]);

        $route = route('admin.match.participation', ['id' => $participation->id]);

        $response = $this->actingAs($this->admin)->postJson($route, ['participationId'=>$participation2->id]);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $participation2->refresh();
        $participation->refresh();
        $this->assertEquals($participation->match_id, $participation2->match_id);
    }
    public function test_it_does_not_allow_to_match_if_user_has_already_new_match()
    {
        // Disable model events, as we don't want to call the ConferenceService during this test
        Match::flushEventListeners();
        \Notification::fake();

        $match = factory(Match::class)->create(
            ['state' => Match::STATES['UNCONFIRMED']]
        );


        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'role' => 'mentee',
        ]);

        factory(Participation::class)->create([
            'enrollment_id' => $enrollment->id,
            'match_id' => $match->id
        ]);
        $participation = factory(Participation::class)->create([
            'enrollment_id' => $enrollment->id
        ]);
        $enrollment2 = factory(Enrollment::class)->create([
            'user_id' => $this->admin->id,
            'role' => 'mentor',
        ]);

        factory(Participation::class)->create([
            'enrollment_id' => $enrollment2->id,
            'match_id' => $match->id
        ]);
        $participation2 = factory(Participation::class)->create([
            'enrollment_id' => $enrollment2->id
        ]);

        $route = route('admin.match.participation', ['id' => $participation->id]);

        $response = $this->actingAs($this->admin)->postJson($route, ['participationId'=>$participation2->id]);

        $this->assertEquals(400, $response->getStatusCode(), $response->getContent());

        $participation2->refresh();
        $participation->refresh();
        $this->assertNull($participation->match_id);
        $this->assertNull($participation2->match_id);
    }

//    public function test_i_can_cancel_a_match_in_admin_with_comment()
//    {
//        // Disable model events, as we don't want to call the ConferenceService during this test
//        Match::flushEventListeners();
//        $match = factory(Match::class)->create(
//            ['state' => Match::STATES['ACTIVE']]
//        );
//
//        $enrollment = factory(Enrollment::class)->create([
//            'user_id' => $this->user->id,
//            'role' => 'mentee',
//        ]);
//        $participation = factory(Participation::class)->create([
//            'enrollment_id' => $enrollment->id,
//            'match_id' => $match->id,
//            'match_confirmed_at' => Carbon::now(),
//        ]);
//
//        $message = 'hello';
//
//        $response = $this->actingAs($this->admin)->postJson("/api/v2/admin/matches/$match->id/cancel",
//            ['comment' => $message]);
//
//        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
//
//        $this->assertDatabaseHas('matches', [
//            'id' => $match->id,
//            'state' => Match::STATES['CANCELED'],
//        ]);
//        $this->assertDatabaseHas('comments', [
//            'commentable_id' => $this->user->id,
//            'author_id' => $this->admin->id
////            'body' => $message,
//        ]);
//    }
}
