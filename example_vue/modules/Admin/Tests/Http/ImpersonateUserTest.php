<?php

namespace Modules\Admin\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\User;

class ImpersonateUserTest extends EndpointTest
{
    public function test_it_receives_an_access_token_for_another_user()
    {
        $subject = factory(User::class)->create();

        $response = $this->actingAs($this->admin)
            ->json('POST', "/api/v2/admin/users/$subject->id/impersonate")
            ->assertStatus(200)
            ->assertJsonStructure([
                'token_type',
                'access_token',
            ]);
    }

    public function test_it_denies_impersonation_to_non_admins()
    {
        $subject = factory(User::class)->create();

        $response = $this->actingAs($this->user)
            ->json('POST', "/api/v2/admin/users/$subject->id/impersonate")
            ->assertStatus(403);

        $response = $this
            ->json('POST', "/api/v2/admin/users/$subject->id/impersonate")
            ->assertStatus(403);
    }
}
