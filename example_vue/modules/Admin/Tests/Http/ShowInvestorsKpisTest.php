<?php

namespace Modules\Admin\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Support\Carbon;
use Modules\Core\Domain\Models\User;

class ShowInvestorsKpisTest extends EndpointTest
{
    public function test_it_shows_the_kpis_with_different_dates()
    {
        $subject = factory(User::class)->create();

        $startDate = Carbon::now()->subMonth(3);
        $endDate = Carbon::now();

        $options = [
            'body' => [
                'startDate' => $startDate->toIso8601String(),
                'endDate' => $endDate->toIso8601String()
            ]
        ];

        $response = $this->actingAs($this->admin)
            ->json('POST', "/api/v2/admin/reporting/investorkpis",$options);

        $this->assertEquals($response->getStatusCode(), 200, $response->getContent());

        $response->assertJsonStructure([
            'reachedPeople'
        ]);
    }
}
