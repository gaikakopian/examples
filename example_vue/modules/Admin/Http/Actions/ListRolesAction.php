<?php

namespace Modules\Admin\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Http\Resources\AdminRoleHttpResource;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Http\Resources\ProgramHttpResource;
use Modules\Core\Http\Resources\RoleHttpResource;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;

class ListRolesAction extends Action
{
    protected $responder;

    public function __construct(ResourceResponder $responder)
    {
        $this->responder = $responder;
    }

    public function __invoke()
    {
        $roles = Role::all();
        return $this->responder->send($roles, AdminRoleHttpResource::class);
    }
}
