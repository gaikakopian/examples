<?php

namespace Modules\Admin\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Exceptions\Server\ServerException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use App\Services\WaboboxService;
use http\Exception\InvalidArgumentException;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Http\Requests\SendWhatsappMessageRequest;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ConferenceService;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Resources\ProgramHttpResource;

class SendWhatsappMessageAction extends Action
{
    protected  $userService;

    protected  $waboxService;

    public function __construct(WaboboxService $waboboxService, UserService $userService )
    {
        $this->waboxService = $waboboxService;
        $this->userService = $userService;
    }

    /**
     * @param int $id
     * @param SendWhatsappMessageRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws ClientException
     */
    public function __invoke(int $id, SendWhatsappMessageRequest $request)
    {
        /** @var User $user */
        $user = $this->userService->get($id);

        if (!$user->qualifiedWhatsappNumber){
            throw new ClientException("User has no whatsapp number configured");
        }

        $this->waboxService->queueMessage($user, $request->message, 'manual_admin');

        return response('ok', 200);
    }
}
