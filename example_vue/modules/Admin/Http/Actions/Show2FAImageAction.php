<?php

namespace Modules\Admin\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Domain\Services\TwoFactorAuthService;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Resources\ProgramHttpResource;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Reporting\Services\ReportingService;
use Illuminate\Http\Request;

class Show2FAImageAction extends Action
{
    protected $organizationService;
    protected $reportingService;

    protected $twoFactorAuthService;

    public function __construct(TwoFactorAuthService $twoFactorAuthService)
    {
        $this->twoFactorAuthService = $twoFactorAuthService;

    }

    public
    function __invoke(Request $request)
    {

        /** @var User $user */
        $user = Auth::user();

        $data = [
            'qrcode' => $this->twoFactorAuthService->getQRCode($user)
        ];

        return response()->json($data);
    }

}
