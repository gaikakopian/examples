<?php

namespace Modules\Admin\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Models\File;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Services\ConferenceService;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Http\Resources\ProgramHttpResource;
use Modules\Matching\Domain\Services\MatchService;

/**
 * Class
 * @package Modules\Admin\Http\Actions
 * @todo add Test for Endpoint
 */
class ShowOptionsAction extends Action
{
    public function __construct()
    {
    }

    public function __invoke()
    {


        $data = [
            'Roles' => array_keys(Role::ROLES),
            'UserFeedbackStates' => array_keys(UserFeedback::STATES),
            'UserFeedbackCodes' => array_keys(UserFeedback::CODES),
            'BrandedDocumentTypes' => array_keys(BrandedDocument::TYPES),
            'BrandedDocumentKeys' => BrandedDocument::getAllKnownKeys(),
            'BrandedDocumentDescriptions' => BrandedDocument::getAllKnownKeysWithComments(),
            'Placements' => array_keys(File::PLACEMENTS),
        ];

        return response()->json($data);
    }
}
