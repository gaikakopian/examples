<?php

namespace Modules\Admin\Http\Controllers;

use App\Infrastructure\Http\BreadServiceController;
use Modules\Admin\Http\Requests\BrandRequest;
use Modules\Admin\Http\Requests\FileRequest;
use Modules\Admin\Http\Resources\BrandHttpResource;
use Modules\Admin\Http\Resources\FileAdminResource;
use Modules\Core\Http\Resources\FileHttpResource;
use Modules\Core\Http\Resources\UserHttpResource;

class FileController extends BreadServiceController
{
    /** {@inheritdoc} */
    protected function getResourceClass(string $view): string
    {
        return FileAdminResource::class;
    }

    /** {@inheritdoc} */
    protected function getStoreRequestClass() : string
    {
        return FileRequest::class;
    }

    /** {@inheritdoc} */
    protected function getUpdateRequestClass() : string
    {
        return FileRequest::class;
    }
}
