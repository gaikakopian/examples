<?php

namespace Modules\Admin\Http\Controllers;

use App\Infrastructure\Http\BreadServiceController;
use Modules\Admin\Http\Requests\EnrollmentRequest;
use Modules\Admin\Http\Resources\EnrollmentAdminHttpResource;
use Modules\Admin\Http\Resources\EnrollmentMinimalHttpResource;

class EnrollmentController extends BreadServiceController
{
    /** {@inheritdoc} */
    protected function getResourceClass(string $view): string
    {

        if ($view === 'list') {
            return EnrollmentMinimalHttpResource::class;
        }
        return EnrollmentAdminHttpResource::class;

    }

    /** {@inheritdoc} */
    protected function getStoreRequestClass(): string
    {
        return EnrollmentRequest::class;
    }

    /** {@inheritdoc} */
    protected function getUpdateRequestClass(): string
    {
        return EnrollmentRequest::class;
    }
}
