<?php

namespace Modules\Admin\Http\Controllers;

use App\Infrastructure\Http\BreadServiceController;
use Modules\Admin\Http\Requests\BrandedDocumentRequest;
use Modules\Admin\Http\Requests\BrandRequest;
use Modules\Admin\Http\Resources\BrandedDocumentListHttpResource;
use Modules\Admin\Http\Resources\BrandedDocumentHttpResource;
use Modules\Admin\Http\Resources\BrandHttpResource;

class BrandedDocumentController extends BreadServiceController
{
    /** {@inheritdoc} */
    protected function getResourceClass(string $view): string
    {
        if ($view == 'list') {
            return BrandedDocumentListHttpResource::class;
        }
        return BrandedDocumentHttpResource::class;
    }

    /** {@inheritdoc} */
    protected function getStoreRequestClass(): string
    {
        return BrandedDocumentRequest::class;
    }

    /** {@inheritdoc} */
    protected function getUpdateRequestClass(): string
    {
        return BrandedDocumentRequest::class;
    }

    public function update($id): \ArrayAccess
    {

        request()->merge(['reviewed_at' => null,'reviewer_id' => null]);
        return parent::update($id);

    }


}
