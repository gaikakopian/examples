<?php

namespace Modules\Admin\Http\Controllers;

use App\Infrastructure\Http\BreadServiceController;
use Modules\Admin\Http\Requests\ImpersonateUserRequest;
use Modules\Admin\Http\Requests\UserRequest;
use Modules\Admin\Http\Resources\SmslogAdminResource;
use Modules\Admin\Http\Resources\SmsLogResource;
use Modules\Admin\Http\Resources\UserAdminHttpResource;
use Modules\Admin\Http\Resources\UserMapHttpResource;
use Modules\Admin\Http\Resources\UserMinimalHttpResource;
use Modules\Admin\Http\Resources\UserResource;
use Modules\Core\Domain\Models\User;
use Modules\Core\Http\Requests\FileUploadRequest;

class SmslogController extends BreadServiceController
{
    /** {@inheritdoc} */
    protected function getResourceClass(string $view): string
    {
        if ($view === 'list') {
            return SmsLogResource::class;
        }
        return SmsLogResource::class;
    }

    /** {@inheritdoc} */
    protected function getStoreRequestClass() : string
    {
        return null;
    }

    /** {@inheritdoc} */
    protected function getUpdateRequestClass() : string
    {
        return null;
    }

}
