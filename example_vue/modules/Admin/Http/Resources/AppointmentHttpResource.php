<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Http\Resources\StateLogHttpResource;

class AppointmentHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'actualDurationMinutes' => $this->actual_duration_minutes,

            'state' => $this->state,
            'lastStateChangeAt' => $this->last_state_change_at,

            'match' => new MatchDetailHttpResource($this->whenLoaded('match')),
            'actualStart' => ($this->actual_start ? $this->actual_start->toIso8601String() : null),
            'history' => StateLogHttpResource::Collection($this->whenLoaded('history')),
//            'match' => new MatchDetailResource($this->whenLoaded('match')),
            'currentLecture' => $this->current_lecture,
            'createdAt' => $this->created_at->toIso8601String(),
            'plannedStart' => ($this->planned_start ? $this->planned_start->toIso8601String() : null)
        ];
    }
}
