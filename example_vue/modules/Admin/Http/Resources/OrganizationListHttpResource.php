<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class OrganizationListHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'state' => $this->state,
            'type' => $this->type,
            'logo' => $this->logo,
            'usersCount' => isset($this->users_count) ? $this->users_count : null,
            'warnings' => $this->isIncomplete,
            'lastCustomerInteraction' => $this->last_customer_interaction,
            'classification' => $this->classification,
            'dpaStatus' => $this->dpa_status,
            'contractStatus' => $this->contract_status,
            'licenceLogic' => $this->licence_logic,
            'contractExpiration' => $this->contract_expiration,
            'satisfactionLevel' => $this->satisfaction_level,
            'jfInterval' => $this->jf_interval,
            'jfLastAt' => $this->jf_last_at ? $this->jf_last_at->toIso8601String() : $this->jf_last_at,
            'jfPlannedAt' => $this->planned_jf_at ? $this->planned_jf_at->toIso8601String() : null,
            'keyaccount' => new UserMinimalHttpResource($this->whenLoaded('keyaccount')),

        ];
    }
}
