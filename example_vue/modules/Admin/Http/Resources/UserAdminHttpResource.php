<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Http\Resources\CommentHttpResource;
use Modules\Core\Http\Resources\EnrollmentHttpResource;
use Modules\Core\Http\Resources\StateLogHttpResource;

class UserAdminHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'displayName' => $this->first_name . ' ' . $this->last_name,
            'firstName' => $this->first_name,
            'lastName' => $this->last_name,
            'state' => $this->state,
            'lastStateChangeAt' => $this->last_state_change_at ? $this->last_state_change_at->toIso8601String() : $this->last_state_change_at,
            'lastStateChangeAt' => $this->email_optin_at ? $this->email_optin_at->toIso8601String() : $this->email_optin_at,
            'email' => $this->email,
            'gender' => $this->gender,
            'primaryRole' => $this->primary_role,
            'servicelevel' => $this->servicelevel,
            'smsDisabled' => $this->sms_disabled,

            'supervisorWeeklyEnabled' => $this->supervisor_weekly_enabled,
            'supervisorDailyEnabled' => $this->supervisor_daily_enabled,

            'phoneNumber' => $this->phone_number,
            'phoneNumberPrefix' => $this->phone_number_prefix,
            'qualifiedPhoneNumber' => $this->qualifiedPhoneNumber,
            'qualifiedWhatsappNumber' => $this->QualifiedWhatsappNumber,
            'whatsappNumber' => $this->whatsapp_number,
            'whatsappNumberPrefix' => $this->whatsapp_number_prefix,
            'whatsappSetup' => $this->whatsapp_setup,
            'whatsappStatus' => $this->whatsapp_status,

            'lastManualInteractionAt' => $this->last_manual_interaction_at ? $this->last_manual_interaction_at->toIso8601String() : $this->last_manual_interaction_at,
            'remindAgainAt' => $this->remind_again_at ? $this->remind_again_at->toIso8601String() : $this->remind_again_at,
            'remindComment' => $this->remind_comment,

            'zendesklink' => $this->zendeskLink,

            'address' => $this->address,
            'colorCode' => $this->color_code,
            'city' => $this->city,
            'postcode' => $this->postcode,
            'country' => strtolower($this->country),
            'countryOfOrigin' => strtolower($this->country_of_origin),
            'birthday' => $this->birthday,
            'language' => $this->language,
            'acceptSms' => $this->accept_sms,
            'acceptEmail' => $this->accept_email,
            'acceptPush' => $this->accept_push,
            'timezone' => $this->timezone,
            'avatar' => $this->avatar,
            'position' => $this->position,
            'aboutMe' => $this->about_me,
            'brand' => new BrandHttpResource($this->whenLoaded('brand')),
            'enrollments' => EnrollmentHttpResource::collection($this->whenLoaded('enrollments')),
            'groups' => GroupHttpResource::collection($this->whenLoaded('groups')),
            'roles' => ($this->whenLoaded('roles')),
            'registrationCode' => new RegistrationCodeHttpResource($this->whenLoaded('registrationCode')),
            'feedbacks' => UserFeedbackHttpResource::collection($this->whenLoaded('feedbacks')),
            'organization_id' => $this->organization_id,
            'organization' => new OrganizationHttpResource($this->whenLoaded('organization')),
            'profileFields' => new ProfileFieldResourceCollection($this->whenLoaded('profileFields')),
            'notifications' => NotificationHttpResource::collection($this->whenLoaded('notifications')),
            'comments' => CommentHttpResource::collection($this->whenLoaded('comments')),
            'callcheckResults' => UserCallcheckResultHttpResource::collection($this->whenLoaded('callcheckResults')),
            'relatedTodos' => CoordinatorTodoHttpResource::collection($this->whenLoaded('relatedTodos')),
            'launches' => UserLaunchHttpResource::collection($this->whenLoaded('launches')),
            'createdAt' => $this->created_at->toIso8601String(),
            'smslogs' => SmsLogResource::collection($this->whenLoaded('smslogs')),
            'history' => StateLogHttpResource::Collection($this->whenLoaded('history')),
            'countForReporting' => $this->count_for_reporting,
            'anonymizedReporting' =>  $this->anonymized_reporting,
            'generalComment' =>  $this->general_comment,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'oldPlattformId' => $this->old_id,
            'validUntil' => $this->valid_until ? $this->valid_until->toIso8601String() : $this->valid_until,

            'inviteSent' => $this->invite_sent,
            'zendeskUserId' => $this->zendesk_user_id
        ];
    }
}
