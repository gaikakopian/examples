<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class AppointmentListHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'actualDurationMinutes' => $this->actual_duration_minutes,

            'state' => $this->state,
            'lastStateChangeAt' => $this->last_state_change_at,

            'match' => new MatchAdminListHttpResource($this->whenLoaded('match')),
            'createdAt' => $this->created_at->toIso8601String(),
            'plannedStart' => ($this->planned_start ? $this->planned_start->toIso8601String() : null)
        ];
    }
}
