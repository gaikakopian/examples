<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class MatchMinimalHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'lectionsCompleted' => $this->lectionsCompleted,
            'state' => $this->state

//            'brand' => new BrandResource($this->whenLoaded('brand')),
//            'organization' => new OrganizationResource($this->whenLoaded('organization')),
//            'profileFields' => new ProfileFieldResourceCollection($this->whenLoaded('profileFields')),
        ];
    }
}
