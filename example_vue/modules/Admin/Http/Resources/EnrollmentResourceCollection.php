<?php

namespace Modules\Admin\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class EnrollmentResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function ($enrollment) {
            return [
                'id' => $enrollment->id,
                'title' => $enrollment->role
            ];
        });
    }
}
