<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Http\Resources\EnrollmentHttpResource;

class UserCallcheckResultHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'success' => $this->success,
            'mosVideo' => $this->mos_video,
            'mosAudio' => $this->mos_audio,
            'packetLossRatio' => $this->packet_loss_ratio,
            'downloadSpeed' => $this->download_speed,
            'uploadSpeed' => $this->upload_speed,
            'createdAt' => $this->created_at->toIso8601String(),
        ];
    }
}
