<?php

namespace Modules\Admin\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;
use Illuminate\Support\Facades\Auth;

class ImpersonateUserRequest extends DeserializedFormRequest
{
    /** {@inheritDoc} */
    public function rules()
    {
        return [];
    }

    /** {@inheritDoc} */
    public function authorize()
    {
        $user = Auth::user();
        return $user->isAdmin() || $user->isCoordinator();

    }
}
