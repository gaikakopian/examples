<?php


namespace Modules\Admin\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class EnrollmentRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
        ];
    }

    /** {@inheritDoc} */
    public function authorize()
    {
        return true;
    }
}
