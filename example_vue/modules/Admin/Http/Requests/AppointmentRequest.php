<?php

namespace Modules\Admin\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class AppointmentRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'match_id' => 'integer',
            'planned_start' => 'date',
            'planned_duration_minutes' => 'integer|min:30'
        ];
    }

    /** {@inheritDoc} */
    public function authorize()
    {
        return true;
    }
}
