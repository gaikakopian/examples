<?php

namespace Modules\Admin\Domain\Models;

use App\Infrastructure\Traits\LogsState;
use Illuminate\Database\Eloquent\Model;

class CoordinatorTodoStateLog extends Model
{
    use LogsState;

    protected $fillable = ['coordinator_todo_id', 'actor_id', 'transition', 'from', 'to'];

    public function coordinatorTodo()
    {
        return $this->belongsTo(CoordinatorTodo::class);
    }
}
