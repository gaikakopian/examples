<?php

namespace Modules\Admin\Domain\Models;

use App\Domain\Models\BaseModel;

class TranslationCache extends BaseModel
{

    /*
     * State Machine Configuration
     */


    /*
    |--------------------------------------------------------------------------
    | Constants
    |--------------------------------------------------------------------------
    */


    protected $fillable = ['input_hash', 'input_text', 'output_text', 'reviewed_at', 'target_language'];

    protected $casts = [
        'reviewed_at' => 'datetime',
    ];




}