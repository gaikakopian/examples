<?php

namespace Modules\Admin\Domain\Models;

use App\Infrastructure\Traits\Statable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Modules\Core\Domain\Models\User;

class CoordinatorTodo extends Model
{

    use Statable;
    /*
     * State Machine Configuration
     */

    const HISTORY_MODEL = CoordinatorTodoStateLog::class;
    const SM_CONFIG = 'coordinatorTodo'; // the SM graph to use

    /*
    |--------------------------------------------------------------------------
    | Constants
    |--------------------------------------------------------------------------
    */


    protected $fillable = ['meta', 'type', 'customer_id', 'creator_id', 'duedate', 'description', 'state', 'last_state_change_at', 'assignee_id'];

    protected $casts = [
        'duedate' => 'datetime',
        'last_state_change_at' => 'datetime',


    ];

    /**
     * All possible states that this model can have.
     *
     * @var array
     */
    const TYPES = [

        'NO_APPOINTMENT_PLANNED' => 'NO_APPOINTMENT_PLANNED',

        'WITHOUT_MATCH' => 'WITHOUT_MATCH',
        'NO_SHOW' => 'NO_SHOW',

        'NO_FIRST_APPOINTMENT' => 'NO_FIRST_APPOINTMENT',
        'NO_FUTURE_APPOINTMENT' => 'NO_FUTURE_APPOINTMENT',

        'REMINDER' => 'REMINDER',
        'USER_BAD_FEEDBACK' => 'USER_BAD_FEEDBACK',

        'ENROLLMENT_INCOMPLETE' => 'ENROLLMENT_INCOMPLETE',
        'WEBINAR_REMINDER' => 'WEBINAR_REMINDER',

        'WAITING_FOR_MATCH' => 'WAITING_FOR_MATCH',
        'MATCH_REJECTED' => 'MATCH_REJECTED',
        'MATCH_AUTO_REJECTED' => 'MATCH_AUTO_REJECTED', // unsued
        'EMAIL_bounce' => 'EMAIL_bounce',
        'EMAIL_complain' => 'EMAIL_complain',
        'MATCH_CANCELED' => 'MATCH_CANCELED',
        'LEAD_NORESPONSE' => 'LEAD_NORESPONSE',
        'APPOINTMENT_TECHNICAL_ISSUES' => 'APPOINTMENT_TECHNICAL_ISSUES'
    ];


    /**
     * All possible states that this model can have.
     *
     * @var array
     */
    const STATES = [
        'OPEN' => 'OPEN',
        'WAITING' => 'WAITING',
        'DONE' => 'DONE',
        'REJECTED' => 'REJECTED',
    ];


    /**
     * All possible state transitions that can be performed on the model.
     *
     * @var array
     */
    const TRANSITIONS = [
        'contacted' => [
            'from' => ['OPEN', 'DONE'],
            'to' => 'WAITING'
        ],
        'solve' => [
            'from' => ['OPEN', 'WAITING', 'WAITING'],
            'to' => 'DONE'
        ],
        'reactivate' => [
            'from' => ['DONE'],
            'to' => 'OPEN'
        ],
        'reject' => [
            'from' => ['OPEN', 'IN_PROGRESS', 'WAITING'],
            'to' => 'REJECTED'
        ],
    ];


    public function setDescriptionAttribute($value)
    {
        if (!is_string($value)) {
            $value = json_encode($value);
        }
        $this->attributes['description'] = $value;
    }


    public function setLastStateChangeAtAttribute($value)
    {
        if (is_string($value)) {
            $value = Carbon::parse($value);
        }
        $this->attributes['last_state_change_at'] = $value;
    }

    public function setDuedateAttribute($value)
    {
        if (is_string($value)) {
            $value = Carbon::parse($value);
        }
        $this->attributes['duedate'] = $value;
    }


    public function setMetaAttribute($value)
    {
        if (!is_string($value)) {
            $value = json_encode($value);
        }
        $this->attributes['meta'] = $value;
    }


    /**
     *
     *
     * @return BelongsTo
     */
    public function assignee()
    {
        return $this->belongsTo(User::class);
    }

    /***
     * @return mixed
     */
    public function customer()
    {
        return $this->belongsTo(User::class);
    }

    /***
     * @return mixed
     */
    public function creator()
    {
        return $this->belongsTo(User::class);
    }


    /**
     * @param $type
     * @return int hours
     */
    public static function getReactionTimeByType($type): int
    {
        return 36;
    }
}