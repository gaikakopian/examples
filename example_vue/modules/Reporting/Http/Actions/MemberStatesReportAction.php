<?php

namespace Modules\Reporting\Http\Actions;

use App\Infrastructure\Http\ResourceResponder;
use App\Infrastructure\Http\PlainHttpResource;
use Illuminate\Support\Facades\DB;

class MemberStatesReportAction extends BaseReportAction
{
    protected $organizationId;

    /** {@inheritDoc} */
    protected function mainQuery()
    {
        //
    }

    /** {@inheritDoc} */
    protected function report()
    {
        $registered = new RegisteredMembersReportAction();
        $trained = new TrainedMembersReportAction();
        $matching = new MatchingMembersReportAction();
        $active = new WorkingMembersReportAction();
        $done = new DoneMembersReportAction();

        $data = [
            'registered' => $registered(),
            'trained' => $trained(),
            'matching' => $matching(),
            'active' => $active(),
            'completed' => $done(),
        ];

        return $this->responder->send($data, PlainHttpResource::class);
    }
}
