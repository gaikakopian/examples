<?php

namespace Modules\Reporting\Http\Actions;

use Illuminate\Support\Facades\DB;
use Modules\Core\Domain\Models\Webinar;

class TrainedMembersReportAction extends BaseReportAction
{
    /** {@inheritDoc} */
    protected function mainQuery()
    {
        return DB::table('users')
            ->join('webinar_state_logs', 'users.id', '=', 'webinar_state_logs.actor_id')
            ->where('webinar_state_logs.to', '=', Webinar::STATES['SUCCESS'])
            ->selectRaw('COUNT(DISTINCT users.id) AS data');
    }

    /** {@inheritDoc} */
    protected function scopeOrganization(int $organizationId)
    {
        $this->query->where('users.organization_id', '=', $organizationId);
    }

    /** {@inheritDoc} */
    protected function scopeStartDate(string $startDate)
    {
        $this->query->where('users.created_at', '>=', $startDate);
    }

    /** {@inheritDoc} */
    protected function scopeEndDate(string $endDate)
    {
        $this->query->where('users.created_at', '<=', $endDate);
    }

    protected function report()
    {
        $data = $this->query->get();

        return $data[0]->data;
    }
}
