<?php

// show Anzahl aktivierter Lizenzen (mit/ohne Freilizenzen)

namespace Modules\Reporting\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Exceptions\Server\ServerException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use App\Services\MultiTenant\Facades\MultiTenant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\RegistrationCodeService;
use Modules\Core\Domain\Services\UserFeedbackService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Requests\AddFeedbackRequest;
use Modules\Core\Http\Requests\RegisterUserRequest;
use Modules\Core\Http\Resources\UserHttpResource;
use Modules\Reporting\Services\ReportingService;
use Modules\Scheduling\Domain\Services\AppointmentService;
use Symfony\Component\Finder\Exception\AccessDeniedException;

class BvReportingAction extends Action
{

    protected $responder;
    protected $userService;
    protected $reportingService;


    public function __construct(
        ResourceResponder $responder,
        ReportingService $reportingService,
        UserService $userService

    )
    {
        $this->userService = $userService;
        $this->reportingService = $reportingService;

    }

    public function __invoke(Request $request)
    {

        // @todo move password to ENV
        $passwordInvalid = $request->password && $request->password != 'bv4impact!';
        if(!$request->password || $passwordInvalid ){
            return view('bv.bvLogin', ['warning'=> $passwordInvalid]);
        }
        $startDate = null;
        $endDate = null;


        $data = [];

        $data = [
            'activatedLicences'=>  $this->reportingService->getLicencesActivated($startDate, $endDate, true),
            'billableLicences'=>  $this->reportingService->getLicencesActivated($startDate, $endDate, false),
            'reachedPeople' => $this->reportingService->getReachedPeople($startDate, $endDate),

        ];

        return view('bv.bvReporting', $data);


    }

}
