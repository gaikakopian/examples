<?php

namespace Modules\Reporting\Http\Actions;

use Illuminate\Database\Query\Builder;

class SessionHoursReportAction extends SessionReportAction
{
    /** {@inheritDoc} */
    protected function sessionSelect(Builder $query)
    {
        $query->selectRaw(
            'COALESCE(
                ROUND(SUM(actual_duration_minutes) / 60.0, 2),
                0.00
            ) AS data'
        );
    }

    /** {@inheritDoc} */
    protected function transformData($data)
    {
        return $data[0];
    }
}
