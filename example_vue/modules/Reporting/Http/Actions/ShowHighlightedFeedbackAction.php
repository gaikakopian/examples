<?php

namespace Modules\Reporting\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Http\Resources\UserFeedbackHttpResource;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\UserFeedbackService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Requests\AcceptTermsRequest;
use Modules\Reporting\Services\ReportingService;

class ShowHighlightedFeedbackAction extends Action
{
    protected $reportingService;
    protected $responder;
    protected $organizationService;

    public function __construct(
        ReportingService $reportingService,
        OrganizationService $organizationService,
        ResourceResponder $responder
    ) {
        $this->reportingService = $reportingService;
        $this->organizationService = $organizationService;
        $this->responder = $responder;
    }

    public function __invoke()
    {

        $organization = Auth::user()->organization;

        $feedback = $this->reportingService->getHighlightedFeedbackOfOrganization($organization);

        return $this->responder->send($feedback, UserFeedbackHttpResource::class);
    }
}
