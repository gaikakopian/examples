<?php

namespace Modules\Reporting\Http\Actions;

use Illuminate\Database\Query\Builder;

class SessionMinutesReportAction extends SessionReportAction
{
    /** {@inheritDoc} */
    protected function sessionSelect(Builder $query)
    {
        $query->selectRaw('COALESCE(SUM(actual_duration_minutes), 0) AS data');
    }

    /** {@inheritDoc} */
    protected function transformData($data)
    {
        return $data[0];
    }
}
