<?php

use Modules\Admin\Domain\Models\CoordinatorTodo;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserLead;
use Modules\Core\Domain\Models\Webinar;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

return [
    'user' => [
        // Class of the domain object
        'class' => User::class,

        // Name of this graph
        'graph' => 'default',

        // Name of the property holding the current state
        'property_path' => 'state',

        // List of all possible states
        'states' => User::STATES,

        // List of all possible transitions
        'transitions' => User::TRANSITIONS,
    ],

    'appointment' => [
        // Class of the domain object
        'class' => Appointment::class,

        // Name of this graph
        'graph' => 'default',

        // Name of the property holding the current state
        'property_path' => 'state',

        // List of all possible states
        'states' => Appointment::STATES,

        // List of all possible transitions
        'transitions' => Appointment::TRANSITIONS,
    ],

    'enrollment' => [
        // Class of the domain object
        'class' => Enrollment::class,

        // Name of this graph
        'graph' => 'default',

        // Name of the property holding the current state
        'property_path' => 'state',

        // List of all possible states
        'states' => Enrollment::STATES,

        // List of all possible transitions
        'transitions' => Enrollment::TRANSITIONS,
    ],

    'match' => [
        // Class of the domain object
        'class' => Match::class,

        // Name of this graph
        'graph' => 'default',

        // Name of the property holding the current state
        'property_path' => 'state',

        // List of all possible states
        'states' => Match::STATES,

        // List of all possible transitions
        'transitions' => Match::TRANSITIONS,
    ],

    'webinar' => [
        // Class of the domain object
        'class' => Webinar::class,

        // Name of this graph
        'graph' => 'default',

        // Name of the property holding the current state
        'property_path' => 'state',

        // List of all possible states
        'states' => Webinar::STATES,

        // List of all possible transitions
        'transitions' => Webinar::TRANSITIONS,
    ],
    'userlead' => [
        // Class of the domain object
        'class' => UserLead::class,

        // Name of this graph
        'graph' => 'default',

        // Name of the property holding the current state
        'property_path' => 'state',

        // List of all possible states
        'states' => UserLead::STATES,

        // List of all possible transitions
        'transitions' => UserLead::TRANSITIONS,
    ],

    'organization' => [
        // Class of the domain object
        'class' => Organization::class,

        // Name of this graph
        'graph' => 'default',

        // Name of the property holding the current state
        'property_path' => 'state',

        // List of all possible states
        'states' => Organization::STATES,

        // List of all possible transitions
        'transitions' => Organization::TRANSITIONS,
    ],
    'coordinatortodo' => [
        // Class of the domain object
        'class' => CoordinatorTodo::class,

        // Name of this graph
        'graph' => 'default',

        // Name of the property holding the current state
        'property_path' => 'state',

        // List of all possible states
        'states' => CoordinatorTodo::STATES,

        // List of all possible transitions
        'transitions' => CoordinatorTodo::TRANSITIONS,
    ],
];
