<?php
return [
    'uri' => env('FIREBASE_DATABASE_URI'),
    'env' => env('FIREBASE_ENV')
];
