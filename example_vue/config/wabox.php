<?php
return [
    'enabled' => env('WABOX_ENABLED'),
    'token' => env('WABOX_TOKEN'),
    'uid' => env('WABOX_PHONE'),
];
