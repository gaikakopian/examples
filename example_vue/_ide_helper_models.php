<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Infrastructure\Tests\Mocks{
/**
 * Helper class for testing the `Statable` traits' history feature.
 *
 * @property-read \Modules\Core\Domain\Models\User $actor
 * @mixin \Eloquent
 */
	class StatableModelHistory extends \Eloquent {}
}

namespace App\Infrastructure\Tests\Mocks{
/**
 * Helper class for testing the `Statable` trait.
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Infrastructure\Tests\Mocks\StatableModelHistory[] $history
 * @mixin \Eloquent
 */
	class StatableModel extends \Eloquent {}
}

