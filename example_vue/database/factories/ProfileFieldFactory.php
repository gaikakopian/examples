<?php

use Faker\Generator as Faker;
use Modules\Core\Domain\Models\ProfileField;

$factory->define(ProfileField::class, function (Faker $faker) {
    return [
        'code' => $faker->randomElement(ProfileField::$KNOWN_CODES),
        'value' => $faker->word,
    ];
});
