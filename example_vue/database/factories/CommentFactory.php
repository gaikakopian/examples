<?php

use Faker\Generator as Faker;
use Carbon\Carbon;
use Modules\Core\Domain\Models\Comment;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

$factory->define(Comment::class, function (Faker $faker) {

    return [
        'body' => $faker->paragraph,
        'pin_to_top' => $faker->numberBetween(0,10) % 10 == 0,
        'type' =>  $faker->randomElement(array_keys(Comment::TYPES)),
        'commentable_type' => 'users',
        'commentable_id' => 1,
        'author_id' => 1

    ];
});
