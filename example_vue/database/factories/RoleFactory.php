<?php

use Faker\Generator as Faker;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;

$factory->define(\Modules\Core\Domain\Models\Role::class, function (Faker $faker) {

    $roles = array_keys(Role::ROLES);
    $role = $faker->randomElement($roles);

    return [
        'name' => $role,
        'display_name' =>  ucfirst(strtolower($role)),
        'description' => $faker->text()
    ];
});
