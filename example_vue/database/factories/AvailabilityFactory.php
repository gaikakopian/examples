<?php

use Faker\Generator as Faker;
use Carbon\Carbon;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Availability;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

$factory->define(Availability::class, function (Faker $faker) {

//    $startTime = $faker->numberBetween(0, 23) . ":" . $faker->randomElement(['00', '30']) . ":00";

    $startTime = $faker->numberBetween(0, 45) * 30;
    return [
        'duration_minutes' => 30,
        'day_of_week' => $faker->numberBetween(0, 5),
        'start_minute_of_day' => $startTime,
        'owner_id' => 1,
        'timezone' => 'Europe/Berlin',
        'owner_type' => User::class
    ];
});
