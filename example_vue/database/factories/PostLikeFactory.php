<?php

use Faker\Generator as Faker;
use Modules\Community\Domain\Models\PostLike;
use Modules\Community\Domain\Models\Post;
use Modules\Core\Domain\Models\User;

$factory->define(PostLike::class, function (Faker $faker, $overrides) {
    $postId = isset($overrides['post_id']) ? $overrides['post_id'] : Post::inRandomOrder()->first()->id;
    $userId = isset($overrides['user_id']) ? $overrides['user_id'] : User::inRandomOrder()->first()->id;

    return [
        'user_id' => $userId,
        'post_id' => $postId,
        'created_at' => new \DateTime(),
    ];
});