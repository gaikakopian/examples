<?php

use Faker\Generator as Faker;
use Modules\Core\Domain\Models\Translation;


$factory->define(Translation::class, function (Faker $faker) {
    static $password;

    $locale = $faker->randomElement(['en', 'de', 'es']);
    $scope = $faker->randomElement(['frontend', 'admin', 'conference', 'callcheck']);


    $json = json_encode(['hello' => $faker->word, 'list' => $faker->words]);


    return [
        'locale' => $locale,
        'scope' => $scope,
        'json_value' => $json
    ];
});
