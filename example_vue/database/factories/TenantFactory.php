<?php

use Faker\Generator as Faker;
use Modules\Core\Domain\Models\Tenant;

$factory->define(Tenant::class, function (Faker $faker) {
    return [
        'name' => $faker->company(),
    ];
});
