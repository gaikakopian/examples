<?php

use Faker\Generator as Faker;
use Modules\Core\Domain\Models\Program;

$factory->define(Program::class, function (Faker $faker, $overrides) {
    return [
        'code' => $faker->domainWord,
        'language' => $faker->languageCode,
        'title' => $faker->catchPhrase,
        'color_code' => $faker->safeHexColor,
        'description' => $faker->sentence,
        'matching_logic' => 'default',
    ];
});
