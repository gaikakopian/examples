<?php

use Faker\Generator as Faker;
use Carbon\Carbon;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Availability;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

$factory->define(\Modules\Admin\Domain\Models\Supportanswer::class, function (Faker $faker) {

//    $startTime = $faker->numberBetween(0, 23) . ":" . $faker->randomElement(['00', '30']) . ":00";

    $startTime = $faker->numberBetween(0, 45) * 30;
    return [
        'title' => $faker->text(20),
        'description' => $faker->text(200),
        'description_markdown' => $faker->text(200),
        'views' => 0,
        'likes' => 0,
        'dislikes' => 0,
    ];
});
