<?php

use Faker\Generator as Faker;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Organization;

$factory->define(Group::class, function (Faker $faker, $overrides) {

    $organization_id = isset($overrides['organization_id']) ?
        $overrides['organization_id'] :
        Organization::inRandomOrder()->first()->id;

    return [
        'name' => 'Group #' . $faker->numberBetween(1,20),
        'organization_id' => $organization_id
    ];
});
