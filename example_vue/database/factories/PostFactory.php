<?php

use Faker\Generator as Faker;
use Modules\Community\Domain\Models\Post;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\User;

$factory->define(Post::class, function (Faker $faker, $overrides) {
    $groupId = isset($overrides['group_id']) ? $overrides['group_id'] : Group::inRandomOrder()->first()->id;
    $userId = isset($overrides['user_id']) ? $overrides['user_id'] : User::inRandomOrder()->first()->id;

    return [
        'user_id' => $userId,
        'group_id' => $groupId,
        'body' => 'Some content',
    ];
});