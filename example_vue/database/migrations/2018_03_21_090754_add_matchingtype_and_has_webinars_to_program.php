<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMatchingtypeAndHasWebinarsToProgram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('programs', function (Blueprint $table) {
            $table->string('matching_algorithm')->default('default');
            $table->boolean('has_mentor_webinar')->default(true);
            $table->boolean('has_mentee_webinar')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('programs', function (Blueprint $table) {
            $table->dropColumn('matching_algorithm');
            $table->dropColumn('has_mentor_webinar');
            $table->dropColumn('has_mentee_webinar');
        });
    }
}
