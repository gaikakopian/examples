<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImportFieldsToUserAndOrganization extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('organizations', function (Blueprint $table) {
            $table->string('code')->nullable();
            $table->string('old_plattform_id')->nullable();
        });
        Schema::table('brands', function (Blueprint $table) {
            $table->string('code')->nullable();
        });
        Schema::table('matches', function (Blueprint $table) {
            $table->string('old_id')->nullable();
        });
        Schema::table('programs', function (Blueprint $table) {
            $table->string('old_id')->nullable();
        });
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('count_for_reporting')->default(true);
            $table->dateTime('invite_sent')->nullable();
            $table->text('about_me')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('organizations', function (Blueprint $table) {
            $table->dropColumn('code');
            $table->dropColumn('old_plattform_id');
        });
        Schema::table('brands', function (Blueprint $table) {
            $table->dropColumn('code');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('count_for_reporting');
            $table->dropColumn('invite_sent');
            $table->string('about_me')->change();
        });
        Schema::table('matches', function (Blueprint $table) {
            $table->dropColumn('old_id');
        });
        Schema::table('programs', function (Blueprint $table) {
            $table->dropColumn('old_id');
        });
    }
}
