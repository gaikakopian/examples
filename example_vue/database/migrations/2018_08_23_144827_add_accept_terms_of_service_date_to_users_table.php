<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAcceptTermsOfServiceDateToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::dropIfExists('terms_user');
        Schema::dropIfExists('terms');
        Schema::table('users', function (Blueprint $table) {
            $table->dateTime('tos_accepted_at')->nullable();
            $table->dateTime('pp_accepted_at')->nullable();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('tos_accepted_at');
            $table->dropColumn('pp_accepted_at');
        });

    }
}
