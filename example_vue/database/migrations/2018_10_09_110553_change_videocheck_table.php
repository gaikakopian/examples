<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeVideocheckTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_callcheck_results', function (Blueprint $table) {
            $table->dropColumn('mos');
            $table->float('mos_video')->nullable();
            $table->float('mos_audio')->nullable();
            $table->float('packet_loss_ratio')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_callcheck_results', function (Blueprint $table) {
            $table->float('mos')->nullable();
            $table->dropColumn('mos_video');
            $table->dropColumn('mos_audio');
            $table->dropColumn('packet_loss_ratio');
        });
    }
}
