<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTranslationsCacheTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translation_caches', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('input_hash');
            $table->text('input_text');
            $table->text('output_text');
            $table->string('target_language');
            $table->dateTime('reviewed_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public
    function down()
    {
        Schema::dropIfExists('translation_caches');
    }
}
