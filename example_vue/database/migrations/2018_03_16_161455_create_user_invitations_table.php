<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Core\Domain\Models\UserInvitation;

class CreateUserInvitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_invitations', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uid');
            $table->integer('inviting_user_id')->unsigned();
            $table->foreign('inviting_user_id')->references('id')->on('users');
            $table->string('invited_user_email');
            $table->dateTime('invitation_sent_at');
            $table->integer('invited_user_id')->unsigned()->nullable();
            $table->foreign('invited_user_id')->references('id')->on('users');
            $table->string('state')->default(UserInvitation::STATES['PENDING']);
            $table->dateTime('last_clicked_at')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_invitations');
    }
}
