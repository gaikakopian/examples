<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePrimaryRoleFieldOfUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('primary_role');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->enum('primary_role', [
                'mentee', 'mentor', 'manager', 'admin','supervisor','coordinator'
            ])->default('mentee');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('primary_role');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->enum('primary_role', [
                'mentee', 'mentor', 'manager', 'admin'
            ])->default('mentee');
        });
    }
}
