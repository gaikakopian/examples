<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddZendeskValuesToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dateTime('zendesk_last_sync')->nullable();
            $table->unsignedBigInteger('zendesk_user_id')->nullable();
        });
        Schema::table('organizations', function (Blueprint $table) {
            $table->dateTime('zendesk_last_sync')->nullable();
            $table->unsignedBigInteger('zendesk_id')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('zendesk_last_sync');
            $table->dropColumn('zendesk_user_id');
        });

        Schema::table('organizations', function (Blueprint $table) {
            $table->dropColumn('zendesk_last_sync')->nullable();
            $table->dropColumn('zendesk_id')->nullable();
        });
    }
}
