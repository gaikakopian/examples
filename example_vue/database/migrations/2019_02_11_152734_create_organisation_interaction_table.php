<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganisationInteractionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization_interactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('organization_id');
            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('cascade');

            $table->integer('coordinator_id');
            $table->foreign('coordinator_id')->references('id')->on('users')->onDelete('cascade');

            $table->text('preparation')->nullable();
            $table->text('documentation')->nullable();

            $table->string('type')->default('jf');
            $table->dateTime('interaction_at')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organization_interactions');
    }
}
