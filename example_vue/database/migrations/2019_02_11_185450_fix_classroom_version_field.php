<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixClassroomVersionField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('matches', function (Blueprint $table) {
            $table->integer("classroom_version")->default(2);

        });
        Schema::table('organizations', function (Blueprint $table) {
            $table->dropColumn("classroom_version");
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('matches', function (Blueprint $table) {
            $table->dropColumn("classroom_version");

        });
        Schema::table('organizations', function (Blueprint $table) {
            $table->integer("classroom_version")->default(2);

        });

    }
}
