<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLanguageJsonFunctionToRespectDeletedAtColumn extends Migration
{
    protected $oldFunction = "
CREATE OR REPLACE FUNCTION language_json(searchscope text, searchlang text) returns jsonb
language plpgsql
as $$
DECLARE
  fallback jsonb;
  locallang jsonb;

BEGIN
  SELECT json_value INTO fallback FROM translations WHERE scope = searchscope AND lower(locale) = 'en';

  SELECT json_value INTO locallang FROM translations WHERE scope = searchscope AND lower(locale) =  lower(searchlang);
  IF NOT FOUND THEN
      RETURN fallback;
  END IF;


  RETURN fallback || locallang ;

END;
$$;";

    protected $newFunction = "
CREATE OR REPLACE FUNCTION language_json(searchscope text, searchlang text) returns jsonb
language plpgsql
as $$
DECLARE
  fallback jsonb;
  locallang jsonb;

BEGIN
  SELECT json_value INTO fallback FROM translations WHERE deleted_at is null AND scope = searchscope AND lower(locale) = 'en';

  SELECT json_value INTO locallang FROM translations WHERE deleted_at is null AND scope = searchscope AND lower(locale) =  lower(searchlang);
  IF NOT FOUND THEN
      RETURN fallback;
  END IF;


  RETURN fallback || locallang ;

END;
$$;";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared($this->newFunction);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared($this->oldFunction);
    }
}
