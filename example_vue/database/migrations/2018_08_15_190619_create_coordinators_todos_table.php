<?php

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoordinatorsTodosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coordinator_todos', function (Blueprint $table) {

            $table->increments('id');
            $table->json('meta')->nullable();
            $table->string('type')->nullable();
            $table->datetime('duedate')->nullable();
            $table->string('description')->nullable();
            $table->string('state')->default('OPEN');
            $table->dateTime('last_state_change_at')->default(Carbon::now());

            $table->integer('assignee_id')->nullable();
            $table->foreign('assignee_id')->references('id')->on('users');

            $table->integer('customer_id')->nullable();
            $table->foreign('customer_id')->references('id')->on('users');

            $table->timestamps();
        });


        Schema::create('coordinator_todo_state_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('coordinator_todo_id');
            $table->foreign('coordinator_todo_id')->references('id')->on('coordinator_todos');

            $table->integer('actor_id')->nullable();
            $table->foreign('actor_id')->references('id')->on('users');

            $table->string('transition');
            $table->string('from');
            $table->string('to');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coordinator_todo_state_logs');
        Schema::dropIfExists('coordinator_todos');



    }
}
