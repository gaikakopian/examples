<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuestionColumnToSupportAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('supportanswers', function (Blueprint $table) {
            $table->string('question')->default('hello!')->nullable();
            $table->string('type')->default('answer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('supportanswers', function (Blueprint $table) {
            $table->dropColumn('question');
            $table->dropColumn('type');
        });
    }
}
