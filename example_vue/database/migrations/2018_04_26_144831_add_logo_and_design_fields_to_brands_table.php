<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLogoAndDesignFieldsToBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('brands', function (Blueprint $table) {
            $table->string('primary_colour')->nullable();
            $table->string('background_colour')->nullable();
            $table->string('email_signature')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('brands', function (Blueprint $table) {
            $table->dropColumn('primary_colour');
            $table->dropColumn('background_colour');
            $table->dropColumn('email_signature');
        });


    }
}
