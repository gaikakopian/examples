<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsSamlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization_saml', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('organization_id');
            $table->foreign('organization_id')
                ->references('id')
                ->on('organizations')
                ->onDelete('cascade');
            $table->integer('registration_code_id');
            $table->foreign('registration_code_id')
                ->references('id')
                ->on('registration_codes');

            $table->string('login_redirect_uri');
            $table->string('entity_uri'); // url to metadata
            $table->string('logout_redirect_uri'); // url to metadata
            $table->string('slug');
            $table->string('email_pattern');
            $table->text('certificate');
            $table->text('certificate_fingerprint');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organization_saml');
    }
}
