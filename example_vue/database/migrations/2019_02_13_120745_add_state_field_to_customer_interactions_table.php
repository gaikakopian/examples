<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStateFieldToCustomerInteractionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('organization_interactions', function (Blueprint $table) {
            $table->string("state")->nullable()->default('PLANNED');
            $table->integer("satisfactioon_estimate")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('organization_interactions', function (Blueprint $table) {
            $table->dropColumn("state");
            $table->dropColumn("satisfactioon_estimate");
        });
    }
}
