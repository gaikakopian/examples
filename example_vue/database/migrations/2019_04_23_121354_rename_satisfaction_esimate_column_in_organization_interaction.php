<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameSatisfactionEsimateColumnInOrganizationInteraction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('organization_interactions', function(Blueprint $table) {
            $table->renameColumn('satisfactioon_estimate', 'satisfaction_estimate');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('organization_interactions', function(Blueprint $table) {
            $table->renameColumn('satisfaction_estimate', 'satisfactioon_estimate');
        });

    }
}
