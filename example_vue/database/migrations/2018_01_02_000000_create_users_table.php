<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Core\Domain\Models\User;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->enum('gender', ['female', 'male', 'other'])->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();

            $table->enum('primary_role', [
                'mentee', 'mentor', 'manager', 'admin'
            ])->default('mentee');

            $table->string('phone_number')->nullable();
            $table->string('whatsapp_number')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('postcode')->nullable();
            $table->string('country')->nullable();
            $table->string('country_of_origin')->nullable();
            $table->date('birthday')->nullable();

            $table->integer('organization_id')->unsigned()->nullable();
            $table->foreign('organization_id')->references('id')->on('organizations');
            $table->integer('brand_id')->unsigned()->nullable();
            $table->foreign('brand_id')->references('id')->on('brands');

            $table->boolean('accept_sms')->default(true);
            $table->boolean('accept_email')->default(true);
            $table->boolean('accept_push')->default(true);
            $table->boolean('is_anonymized')->default(false);

            $table->dateTime('last_reminder')->nullable();
            $table->dateTime('last_login')->nullable();

            $table->string('language', 2)->default('en'); // Application UI language
            $table->string('timezone')->default('Europe/Berlin');

            $table->string('avatar')->nullable();
            $table->string('state')->default(User::STATES['NEW']);

            $table->dateTime('last_state_change_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
