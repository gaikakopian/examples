<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnerAccessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_accesses', function (Blueprint $table) {
            $table->increments('id');

            $table->string('api_base');
            $table->string('secret');

            $table->integer('organization_id')->unsigned()->nullable();
            $table->foreign('organization_id')
                ->references('id')->on('organizations');
            $table->integer('registration_code_id')->unsigned()->nullable();
            $table->foreign('registration_code_id')
                ->references('id')->on('registration_codes');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partner_accesses');
    }
}
