<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrandedDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branded_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->string('key');
            $table->enum('type', ['email', 'article', 'sms']);
            $table->string('subject')->nullable();
            $table->text('content');
            $table->integer('brand_id')->unsigned()->nullable();
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->integer('program_id')->unsigned()->nullable();
            $table->foreign('program_id')->references('id')->on('programs');
            $table->string('language', 2);
        });

        // PARTIAL INDEXES: We need partial indexes because of nullable brand_id and nullable program_id
        // key, type, language, brand_id, program_id
        $first_patial_index = 'CREATE UNIQUE INDEX branded_documents_heavy_unique ON branded_documents
            (key, type, language, brand_id, program_id)
            WHERE brand_id IS NOT NULL AND program_id IS NOT NULL;';

        // key, type, language, brand_id
        $second_patial_index = 'CREATE UNIQUE INDEX branded_documents_key_type_language_brand_id_unique ON branded_documents
            (key, type, language, brand_id)
            WHERE brand_id IS NOT NULL AND program_id IS NULL;';

        // key, type, language, program_id
        $third_patial_index = 'CREATE UNIQUE INDEX branded_documents_key_type_language_program_id_unique ON branded_documents
            (key, type, language, program_id)
            WHERE program_id IS NOT NULL AND brand_id IS NULL;';

        // key, type, language
        $fourth_patial_index = 'CREATE UNIQUE INDEX branded_documents_key_type_language_unique ON branded_documents
            (key, type, language)
            WHERE program_id IS NULL AND brand_id IS NULL;';

        DB::statement($first_patial_index);
        DB::statement($second_patial_index);
        DB::statement($third_patial_index);
        DB::statement($fourth_patial_index);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branded_documents');
    }
}
