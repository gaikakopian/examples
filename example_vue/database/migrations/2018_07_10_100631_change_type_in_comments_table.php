<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Core\Domain\Models\Comment;

class ChangeTypeInCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->dropColumn('type');
        });
        Schema::table('comments', function (Blueprint $table) {
            $table->string('type')->default(Comment::TYPES['ORGANISATIONAL'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->dropColumn('type');
        });
        Schema::table('comments', function (Blueprint $table) {
            $table->enum('type', [
                'SUPERVISOR_COMMENT'
            ])->nullable();
        });
    }
}
