<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomerSuccessFieldsToOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('organizations', function (Blueprint $table) {
            $table->dateTime('last_customer_interaction')->nullable();
            $table->string('classification')->nullable()->default('C');
            $table->tinyInteger('dpa_status')->nullable()->default(0);
            $table->tinyInteger('contract_status')->nullable()->default(0);
            $table->string('licence_logic')->nullable()->default('classic');
            $table->dateTime('contract_expiration')->nullable();
            $table->integer('satisfaction_level')->nullable()->default(5);

            $table->integer('manager_id')->nullable();
            $table->foreign('manager_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('organizations', function (Blueprint $table) {
            $table->dropColumn(['last_customer_interaction','classification','manager_id','dpa_status','contract_status','licence_logic','contract_expiration','satisfaction_level']);
        });
    }
}
