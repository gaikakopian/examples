<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebinarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webinars', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->string('title');
            $table->text('description');
            $table->dateTime('starts_at');
            $table->integer('duration_minutes');

            $table->integer('program_id')->unsigned();
            $table->foreign('program_id')->references('id')->on('programs');
            // Language is inferred via the program.

            $table->string('state')->default('NEW');
            $table->dateTime('last_state_change_at')->default(Carbon::now());

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webinars');
    }
}
