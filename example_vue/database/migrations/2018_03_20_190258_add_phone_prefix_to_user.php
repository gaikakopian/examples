<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhonePrefixToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::table('users', function (Blueprint $table) {
                $table->smallInteger('phone_number_prefix', false, true)->default('49');
                $table->smallInteger('whatsapp_number_prefix', false, true)->nullable()->default('49');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('phone_number_prefix');
            $table->dropColumn('whatsapp_number_prefix');
        });
    }
}
