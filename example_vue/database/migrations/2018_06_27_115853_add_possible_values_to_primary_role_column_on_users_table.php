<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPossibleValuesToPrimaryRoleColumnOnUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->alterEnum('users', 'primary_role', [
            'user',
            'mentor',
            'mentee',
            'supervisor',
            'manager',
            'coordinator',
            'ambassador',
            'editor',
            'admin',
            'superadmin',
            'distributionservice',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->alterEnum('users', 'primary_role', [
            'user',
            'mentor',
            'mentee',
            'supervisor',
            'manager',
            'coordinator',
            'ambassador',
            'editor',
            'admin',
            'superadmin',
        ]);
    }

    /**
     * Alter an enum field constraints
     * @param $table
     * @param $field
     * @param array $options
     */
    protected function alterEnum($table, $field, array $options)
    {
        $check = "${table}_${field}_check";

        $enumList = [];

        foreach ($options as $option) {
            $enumList[] = sprintf("'%s'::CHARACTER VARYING", $option);
        }

        $enumString = implode(", ", $enumList);

        DB::transaction(function () use ($table, $field, $check, $options, $enumString) {
            DB::statement(sprintf('ALTER TABLE %s DROP CONSTRAINT %s;', $table, $check));
            DB::statement(sprintf('ALTER TABLE %s ADD CONSTRAINT %s CHECK (%s::TEXT = ANY (ARRAY[%s]::TEXT[]))', $table, $check, $field, $enumString));
        });
    }
}
