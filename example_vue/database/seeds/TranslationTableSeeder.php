<?php

use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\Translation;

class TranslationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(Translation::class)->create([
            'locale' => 'en',
            'scope' => 'frontend',
            'json_value' => json_encode(['HELLO' => 'hello', 'BYE' => 'byw', 'deep' => ['example' => 'deepvalue', 'example2' => 'deepvalue2']])

        ]);

        factory(Translation::class)->create([
            'locale' => 'de',
            'scope' => 'frontend',
            'json_value' => json_encode(['HELLO' => 'hallo', 'BYE' => 'Tschüss'])
        ]);
        factory(Translation::class)->create([
            'locale' => 'es',
            'scope' => 'frontend',
            'json_value' => json_encode(['HELLO' => 'hola']) // BYE is missing on purpose;
        ]);


        factory(Translation::class)->create([
            'locale' => 'en',
            'scope' => 'backend',
            'json_value' => json_encode(['USERS' => 'Users']) // BYE is missing on purpose;
        ]);

    }
}
