<?php

use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\BrandedDocument;
use Faker\Generator as Faker;


class BrandedArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {



        $Parsedown = new Parsedown();
        $documents = ['registration_welcome', 'next_steps', 'user_help', 'supervisor_help','statementofgoodconduct','terms','privacypolicy'];

        foreach ($documents as $key) {
            $key = strtolower($key);
            $markdownFile = __DIR__ . '/articles/' . $key . '.md';
            $subjectFile = __DIR__ . '/articles/' . $key . '.subject.txt';
            $content = '-- Missing Article --';

            $markdown = ' -- Markdown missing --';

            if (file_exists($markdownFile)) {
                $markdown = file_get_contents($markdownFile);
                $content = $Parsedown->text($markdown);
            } else {
                echo 'Missing file ' . $markdownFile . PHP_EOL;
            }

            $subject = file_exists($subjectFile) ? file_get_contents($subjectFile) : ' -- Subject Missing --';


            factory(BrandedDocument::class)->create([
                'key' => $key,
                'subject' => $subject,
                'markdown' => $markdown,
                'content' => $content,
                'type' => BrandedDocument::TYPES['article'],
            ]);

        }

    }
}
