<?php

use App\Services\MultiTenant\Facades\MultiTenant;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        MultiTenant::setTenantId(1);

        $this->call(OauthClientsTableSeeder::class);
        $this->call(TenantsTableSeeder::class);
        $this->call(RolesSeeder::class);

        $this->call(CoreOrganizationsSeeder::class);

        if (Config::get('app.env', false) !== 'production') {

            $this->call(BrandsTableSeeder::class);

            $this->call(OrganizationsTableSeeder::class);

            $this->call(CoreUserSeeder::class);

            $this->call(ProgramsTableSeeder::class);
            $this->call(FilesTableSeeder::class);

            $this->call(UsersTableSeeder::class); // releis on roles
//            $this->call(MatchTableSeeder::class); //relies on Programs
            $this->call(UserAvatarsSeeder::class);
            $this->call(BrandedArticlesTableSeeder::class);

            $this->call(RegistrationCodesTableSeeder::class); // requires org + brands
            $this->call(WebinarsTableSeeder::class); // reuqires programs to be before
            //
            $this->call(TestingUserSeeder::class);
            $this->call(UserInvitationsTableSeeder::class);
            $this->call(DefaultUserSeeder::class);

            $this->call(UsersWithMatchesSeeder::class); //relies on Programs
            $this->call(UsersWithoutMatchesSeeder::class); //relies on Programs

            $this->call(ActivitiesTableSeeder::class);

            $this->call(SupervisorSeeder::class);
            $this->call(ProgramPhotosSeeder::class);

            $this->call(UserAvatarsSeeder::class);
            $this->call(UserRolesSeeder::class); // relies on role & user;

            $this->call(BrandedDocumentsTableSeeder::class);
            $this->call(TranslationTableSeeder::class);
            $this->call(OrganizationBookingsSeeder::class);
            $this->call(SupportAnswerSeeder::class);

            $this->call(OrganizationsSamlSeeder::class);



        } else {
            $this->call(ProgramsTableSeeder::class);
            $this->call(CoreUserSeeder::class);
            $this->call(TranslationTableSeeder::class);
            $this->call(UsersWithMatchesSeeder::class); //relies on Programs


        }
    }

}
