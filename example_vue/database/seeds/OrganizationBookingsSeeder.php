<?php

use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\OrganizationBooking;

class OrganizationBookingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\Faker\Generator $generator)
    {
        $organizations = Organization::where('type', '=', Organization::TYPES['company']);

        $organizations->each(function ($organization) {
            factory(OrganizationBooking::class)->create(['organization_id' => $organization->id], 3);
        });
    }
}

