<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;

class CoreUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Create a known Organization
        $org = Organization::find(1);




        factory(Brand::class)->create([
            'name' => 'Default Brand',
        ]);
        // Create a known user
        $user = factory(User::class)->create([
            'first_name' => 'Default',
            'last_name' => 'User',
            'email' => 'user@volunteer-vision.com',
            'password' => bcrypt('start123'),
            'organization_id' => $org->id,
            'brand_id' => 1,
        ]);


        /** @var User $kironApi */
        $kironApi = factory(User::class)->create([
            'first_name' => 'Kiron',
            'last_name' => 'Plattform',
            'email' => 'kironapi@volunteer-vision.com',
            'password' => bcrypt('start123'),
            'organization_id' => $org->id,
            'brand_id' => 1,
            'primary_role' => Role::ROLES['admin']
        ]);

        $role = Role::where('name', '=', Role::ROLES['partnerapi'])->firstOrFail();
        $kironApi->roles()->attach($role->id);

        // Create a known admin user
        $user = factory(User::class)->create([
            'first_name' => 'Admin',
            'last_name' => 'User',
            'email' => 'admin@volunteer-vision.com',
            'password' => bcrypt('start123'),
            'organization_id' => $org->id,
            'brand_id' => 1,
            'primary_role' => 'admin',
        ]);


        // Create a known admin user
        $fr = factory(User::class)->create([
            'first_name' => 'Franziska',
            'last_name' => 'Böhm',
            'email' => 'franziska.boehm@volunteer-vision.com',
            'password' => bcrypt('start123'),
            'organization_id' => $org->id,
            'brand_id' => 1,
            'primary_role' => 'admin',
            'avatar' => 'sample.png'
        ]);
        $sd = factory(User::class)->create([
            'first_name' => 'Suska',
            'last_name' => 'Dreesbach',
            'email' => 'suska.dreesbach@volunteer-vision.com',
            'password' => bcrypt('start123'),
            'organization_id' => $org->id,
            'brand_id' => 1,
            'primary_role' => 'admin',
            'avatar' => 'sample.png'
        ]);
        $sd = factory(User::class)->create([
            'first_name' => 'Simon',
            'last_name' => 'Fakir',
            'email' => 'simon.fakir@volunteer-vision.com',
            'password' => bcrypt('start123'),
            'organization_id' => $org->id,
            'brand_id' => 1,
            'primary_role' => 'admin',
            'avatar' => 'sample.png'
        ]);



    }
}
