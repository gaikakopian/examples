<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Webinar;

class WebinarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $future = Carbon::now()->addWeek(1);

        /** @var \Doctrine\Common\Collections\Collection $programs */
        $programs = Program::all();


        // add 5 webinars for each rpogram.

        foreach ($programs as $program) {
            for ($i = 0; $i < 2; $i++) {
                $future = Carbon::now()->addHours(rand(24, 800));
                factory(Webinar::class, 1)->create([
                    'program_id' => $program->id,
                    'starts_at' => $future,
                    'state' => Webinar::STATES['PLANNED']
                ]);
            }
        }

        factory(Webinar::class, 2)->create([
            'program_id' => 1,
            'starts_at' => $future
        ]);

        // past webinars
        factory(Webinar::class, 10)->create();
    }
}
