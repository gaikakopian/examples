# <span class="accented">WELCOME</span> TO YOUR 1:1 DIGITAL MENTORING PROGRAM


We are pleased to welcome you to your new home for your 1:1 digital mentorship. Through Volunteer Vision’s unique elearning programs, you will be building new and interesting relationships to empower others and help them achieve their goals. Let’s get started!