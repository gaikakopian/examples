#Preparation

##How much time do I need to prepare for a session? 

You can decide for yourself how much time you would like to use for preparation. Our recommendation is a period of about 30 minutes per week. The better you prepare for your time together, the more relaxed and unconstrained you can make the sessions. 

##How do I prepare myself properly for a session with my mentee?  

In your profile, in the “Preparation” section, you will find everything you need for your sessions. Here you can, for example, view the presentation on the preparation training or special videos about platform usage and designing the session. In order to prepare for a specific date, you can first view the document “Mentor Preparation” to get an overview of the content of the session. To familiarize yourself with the guide, you can go to the conference room under “Peek at the slides,” without your mentee being present. Alternatively, you can download the guide in the “Preparation” section of the lesson as a PDF.  

##Do I need anything besides a computer?  

Essentially, you only require your computer to do the sessions with your mentee. You can also keep a notepad and pen ready if you want to take notes. Also, we recommend using a headset or headphones to ensure good sound quality. For some sessions, you can use a few tools to make the lesson livelier. You may find a reference to this in the document “Mentor Preparation.” 

##How can I set up the equipment correctly? 

To do this, you can easily perform our system check under “Check this device” in your profile. If something does not work, you can find tips about what you can do to fix it. It is important that you always open the platform via the Chrome or Firefox browser. 

##Can I also participate by iPad or Smartphone? 

Unfortunately, only usage of a laptop or PC is currently possible. We are working on a solution for other devices. 

#My mentee & I

##How can I keep in touch with my mentee? 

Depending on what you want to talk about with your mentee, you can use different communication methods. To plan the next session, you can send a text message to your mentee in the platform under “Scheduling” to set the next date. In addition, the chat in the lower right corner of the platform allows you to send messages. Keep in mind that your mentee has limited access to computers, and he/she can probably not read your message right away. To make your communication even easier, we recommend you exchange phone numbers and stay in contact with things like WhatsApp. This, however, is completely voluntary.     

##What do I do if my mentee is late? 

Speak to the mentee about it. Explain why punctuality is important to you. There may also be a specific reason for your mentee’s delay. Does he/she require help logging in? 

##What if we don’t cover all the material in one appointment?

Our sessions are designed to take approximately one hour per guide. If you wish to take additional time for a guide, you can also extend an appointment if this is compatible with your availability and that of your mentee. You should not exceed the fixed number of sessions, as the mentee may only have access to the computer for the agreed period, and we can only provide you with proper support during this time.  
    
##What if I have to cancel an appointment?  

It can always happen that you get ill or have to cancel a session for another reason. The important thing is to keep your mentee informed. First of all, you should always schedule the appointment in the platform and send an automated SMS to your mentee in which he/she will be informed about the new date. If you are also in direct contact, it’s best to give notice directly. Please try to move as few dates as possible. Reliability and trust are important to your mentoring relationship!  

##What can I talk about with my mentee apart from the information given in the guide?

In the webinar, you already heard how important sensitivity is when dealing with refugees. First, develop a feel for who your mentee is and what he/she may not be comfortable talking about (for example, escape reasons and experiences). Apart from that, you can talk about everything you enjoy and enrich the exchange. 

## What do I do if my mentee is too advanced for the exercises?

There is a chance that the exercises might be easy for your mentee at first if their German is already a little advanced. The level of the sessions, however, will increase a lot over time, so the sessions will probably match your mentees’ language level soon. There are a couple of things you can do if the exercises are too easy:

-	Switch to the “advanced” mode of the exercise.
-	Talk to your mentee to practice conversation. For example, you can talk about his/her everyday learning environment, living in Germany or your immediate surroundings.
-	If you feel like the exercises do not challenge your mentee enough, you can go through several lessons per session, so that they advance to a higher level more quickly.  
-	Ask your mentee if (s)he has any specific questions about the things they have learned in the German courses and include them in your session.

##What do I do if my mentee doesn’t show up?  

There can be many reasons why your mentee may be absent unexcused. Please record this via the button “My mentee did not show up today” and reschedule the appointment in the platform. Should this happen more frequently, we will take a closer look at the situation. If you are uncertain, approach us directly.  

##What do I do if there is no connection between us?

Think about what exactly is bothering you. Maybe you and your mentee only need a little time to get used to each other. If, after a few appointments, nothing changes, please contact us. We will find a fitting solution for you.  

##Can I meet my mentee in person? 

The beauty of meeting digitally is that you and your mentee can connect from anywhere. Since our program is understood to be a digital employee engagement, a personal meeting is not planned, and we cannot accept any liability in this scenario. If you both wish, you can possibly arrange a personal meeting after the program.

##What happens after the regular appointments?  

Both you and your mentee will receive a certificate for your participation from us. After that, the support of Volunteer Vision ends, but many of our mentoring pairs remain in contact with each other. For example, you can use Skype or simply make a phone call.  

##What do I do if my mentee drops the program?  

Even if a mentee volunteers to participate in the program and looks forward to the exchange with you, it can always happen that he/she will have to end the program early. If you are still during the period of the first three sessions, we’ll match you directly with a new mentee. If you are further into it, we will discuss other possibilities with both of you.  

##Whom do I turn to when I get the impression that my mentee requires psychological support?

Many people with a history of escape have had an emotionally distressing experience in their home countries or on the voyage that sometimes affects them even after their arrival in Germany. Should you identify that your mentee is in need of professional support, please contact us. 


##Can I provide Volunteer Vision with feedback about content? 

As a mentor, you know exactly what works best for your mentee. Therefore, we are always happy hearing about your experience in general as well as individual sessions. Please feel free to send us an e-mail (info@volunteer-vision.com) containing the program or possibly the  session number. Also our short evaluations at the end of sessions gives you the opportunity for brief feedback. 

##Can I exchange ideas with other mentors? 

We are currently working to facilitate exchange between mentors. Until this is possible via the platform, you can contact us if you would like to be put into contact with other mentors.  

##Is it possible to participate in the program multiple times?  

If your employer agrees to your repeated participation, you are welcome to participate as an online mentor again. Just approach us directly with the request.  

##Will I receive a certificate for my mentorship participation?

Yes. As soon as you and your mentee complete the final session, you will receive a personal certificate of your participation by e-mail. 

##Can I follow Volunteer Vision on social media?  

Of course! We would be happy if you followed us on Facebook. You may also find our profile on LinkedIn.  

##Can acquaintances outside of my company also become mentors? 

For the moment, all of our mentors are exclusively employees of participating partner companies. You can find a list of our partners on our website. 

#Technical

##What do I do if there are problems with the sound? 

As with any technical connection, it can always happen that something is not working properly. This may be due to various reasons. In general, you can protect against this with a regular system check, found under “Check this device.” To solve immediate sound problems, try the following:  

-	Reload the page and call the mentee back. 
-	Exit the conference room and reenter.
-	Reset the call. You can find this option next to the video in the conference room, by clicking the “?”.
-	Close all other video applications on your computer, such as Skype. 
-	Use a headset and make sure that the sound is adjusted on both your computer and your headset.
-	Your browser may possibly be blocking the platform’s access to your microphone. To remedy this, click on the small camera icon in the browser bar and change the settings accordingly.
-	If none of the above help, simply call your mentee directly and work through the material together while on the phone. 

If you need support, our system administrator (support@volunteer-vision.com) is at your disposal. 

##What do I do if there are problems with the video connection?

As with any video chat application, it can happen that something is not working quite right. There may be various reasons for this. In general, you can protect yourself with a regular system check, found under “Check this device.” To solve immediate video problems, try the following:  

-	Reload the page and call the mentee back.  
-	Exit the conference room and reenter.
-	Reset the call. You can find this option next to the video in the conference room, by clicking the “?”.
-	Close all other video applications on your computer, such as Skype  
-	Use a headset and make sure that the sound is adjusted on both your computer and your headset.
-	Your browser may possibly be blocking the platform’s access to your webcam. To remedy this, click on the small camera icon in the browser bar and change the settings accordingly.
-	If none of the above should help, simply call your mentee directly and work through the material together while on the phone. 

If you need support, our system administrator (support@volunteer-vision.com) is at your disposal. 

##What can I do to help the system administrator assist me with problems during a session? 

You can allow the system administrator to view your screen so that he/she can provide you with direct remote support. You can download “Quick support” for:

•	Windows

•	Apple Mac

•	Chrome OS


##Whom can I contact if I need support?

If you have any technical questions or problems, please do not hesitate to contact our system administrator (support@volunteer-vision.com). If you need immediate support during a session, you can also contact us directly by phone: Simon Fakir +49 177 670 69 37. 

Do you have questions that were not answered? Please turn to our support at: info@volunteer-vision.com