# <span class="accented">JOIN</span> the eSister Program


eSister is a 1:1 leadership mentoring program from employees for female students and young professionals.
 
 
This program consists of six, one-hour sessions during which mentees learn to get to know their strengths, negotiation skills, communication skills, how to develop their leadership style and more. Mentors get the opportunity to connect with the generation of leadership while gaining appreciation and having the chance to reflect on their own leadership and career skills. Mentees gain business insights and job connections in their industry of choice and are empowered to kick-start their careers. eSister is for mentors who are highly motivated to empower, are fluent in English or German, have 5+ years of work experience, have 2+ years of leadership experience with a team of 5 or more, are male or female.  
Supporting documents:
