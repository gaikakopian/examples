# <span class="accented">JOIN</span> the eCareer Program


eCareer is a 1:1 mentoring program by employees for young adults under the care of SOS Children’s Villages.
 
 The program consists of five, one-hour sessions in English or Spanish. The program supports young adults between the ages of 16-25 who are looking to enter the job market for the first time and are looking for a corporate mentor. Mentees are located all around the world in one of the 135 countries of SOS Children’s Villages. Mentees gain practical job advise from analysing their job preferences to setting goals and writing CVs. eCareer is perfect for mentors who are highly motivated to empower, are understanding and empathetic, are available for all 5 sessions, are fluent in English or Spanish, and are interested in engaging in online learning formats. 