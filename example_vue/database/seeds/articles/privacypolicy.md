Effective: December ‎2016

Volunteer-Vision GmbH, Amalienstraße 87, 80799 München ("Volunteer Vision") takes the protection of its users' personal data seriously. Data protection and the protection of privacy during the processing of personal data are of high importance to Volunteer Vision.
##1.	General Information
Volunteer Vision processes and uses personal data of the users of their online services, as the responsible entity, as long as the users have given their consent or a legal provision allows the process or the use of personal data.
"Personal data" are individual pieces of personal or factual information about a specific or specifiable natural person. This includes e.g. a user's name, e-mail address and leisure interests.

##2.	Data processing for contractual performance

####2.1	Use of the online services

Volunteer Vision shall collect, process and use the user's personal data, which the user discloses to Volunteer Vision or which are collected through the use of the online services, as far as this is necessary for the reason and execution of the contract.
####2.2	Registration of the user account
Volunteer Vision requires certain compulsory details, marked as mandatory in the registration process, for the registration and creation of a user account. Volunteer Vision uses this data to identify the users and prevent multiple registrations.
####2.3	Matching-relevant data
Volunteer Vision uses a specially developed matching-algorithm to ensure that users are brought together by their personality and their goals. Therefore, certain compulsory details, marked as mandatory in the registration process, (e.g. leisure interests, motivation and details about study programs or career goals) are needed for the registration. In order to refine the matching-algorithm users may add additional personal data, such as a free text about their personality, to their user account. The matching-relevant data is used solely to connect the users based on the matching-algorithm.
##3.	Use of personal data for other purposes
####3.1.	Contact and support form
When users send a request by contact form to Volunteer Vision, the data found in the contact form (including contact data stated in the form) shall be used solely for processing the request and, in case of follow-up requests, shall be stored and processed.
####3.2.	Improvement of the online services
Volunteer Vision uses certain user data, such as the average stay per user/session or the amount of completed sessions, as well as for the purpose of evaluating collected user feedback, in order to improve the online services of Volunteer Vision, with special regards to their organization, as well as their form, type and scope.
####3.3.	Use for marketing purposes
If a user explicitly gives his/her consent to Volunteer Vision, Volunteer Vision shall use personal data for marketing purposes, e.g. for sending e-mails containing general information about products and services of Volunteer Vision (newsletter).
####3.4.	Use for research purposes
Volunteer Vision shall analyze collected data for the purpose of scientific research, especially for the evaluation of key indicators (so called "Key Performance Indicators - KPIs") and social impacts. The use for research purposes shall only be conducted within the scope of legal requirements, if possible by means of anonymized or pseudonymized data, or with the user's consent.
##4.	Web analysis (Google Analytics)
Volunteer Vision uses Google Analytics, a web analysis service of Google Inc. ("Google"). Google Analytics uses so called "Cookies", text files that are saved on the users' computer and that enable the analysis of the user's use of the website. The information generated by the cookie concerning the usage of this website is usually sent to a server of Google in the US and saved there.
Volunteer Vision has activated the IP-anonymisation. Therefore, Google beforehand abbreviates the user's IP address within the member states of the European Union or other countries which are contracting parties to the Agreement on the European Economic Area. Only in exceptional cases the full IP address is sent to a server of Google in the United States and abbreviated there.
Google shall use this information under the authority of the provider of this website to evaluate the use of the website by the user, to compile reports on the website activity, and for other website and Internet usage related services for the provider of the website. The IP address provided by the user's browser within the framework of Google Analytics shall not be brought together with other data of Google.
Users can prevent the storage of the Cookies with corresponding settings of their browser software. In that case, however, users may not be able to use all the functions of this website in their entirety. Furthermore, users can prevent the collection of the data generated by the cookie and based on the use of the website (including the user's IP address), as well as the processing of this data by Google, by downloading and installing the browser plug-in provided under the following link: http://tools.google.com/dlpage/gaoptout?hl=de 
##5.	Plug-ins and third party services
The online services of Volunteer Vision include plug-ins and services of the third parties listed below:
####5.1	Facebook
Volunteer Vision uses plug-ins of the provider Facebook Inc., 1601 S. California Ave, Palo Alto, CA 94304, USA. This is recognizable by Facebook's own logo. More information about the social plug-ins of Facebook user's receive under the following web address:
http://developers.facebook.com/plugins
When a visitor opens a site of Volunteer Vision's Internet domain containing such plug-in, a connection between the user's browser und the servers of Facebook is established if necessary. Facebook receives and stores data that enable Facebook to recognize what site of the web presence was visited by the user. This applies regardless of whether a Facebook plug-in is interacted with or not. When the visitor interacts with a Facebook plug-in, an exchange of the corresponding information is sent to Facebook in the United States and saved there. If the user is logged in with his/her Facebook account at that time, the forwarded data can be assigned to the corresponding account.
The type, extent and purpose of data collection as well as how the data is processed further and used by Facebook, together with the user's rights and optional settings to protect his/her privacy, can be found and, if necessary, altered in the Facebook data protection notes (https://www.facebook.com/about/privacy).
####5.2	LinkedIn
Volunteer Vision uses plug-ins from the provider LinkedIn Inc., Sunnyvale, California, USA. This is recognizable by LinkedIn's own logo.
When a visitor opens a site of Volunteer Vision's Internet domain containing such plug-in, a connection between the user's browser und the servers of LinkedIn is established if necessary. LinkedIn receives and stores data that enable LinkedIn to recognize what site of the web presence was visited by the user. This applies regardless of whether a LinkedIn plug-in is interacted with or not. When the visitor interacts with a LinkedIn plug-in an exchange of the corresponding information is sent to LinkedIn in the United States and saved there. If the user is logged in with his/her LinkedIn account at that time, the forwarded data can be assigned to the corresponding account.
The type, extent and purpose of data collection as well as how the data is processed further and used by LinkedIn, together with the user's rights and optional settings to protect his/her privacy, can be found and, if necessary, altered in the LinkedIn data protection notes (https://www.linkedin.com/legal/privacy-policy?trk=uno-reg-guest-home-privacy-policy).
####5.3	Additional services
The following services are included in the online services of Volunteer Vision:

* Google-Fonts and YouTube-videos
(http://www.google.de/intl/de/policies/privacy)
Google Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA
* Vimeo-videos
(https://vimeo.com/privacy)
* Video conferences and text messages from Twilio
(https://www.twilio.com/legal/privacy?lang=de_DE)
* Audio files from Amazon Web Services (S3)
(https://aws.amazon.com/compliance/data-privacy-faq/?nc1=h_ls)

##6.	Cookies
####6.1	Use of cookies
Volunteer Vision uses cookies in order to recognize users during their visit and to store technical data for the proper functioning of the online services on the computer of the user. A "cookie" is a text file which is stored on the user's computer and enables the analysis of the use.
####6.2	Disabling the use of cookies
If the user does not want the use of cookies, he/she can disable the use of cookies in his/her browser. However, it cannot be excluded that certain sectors of the online services cannot or not properly be used.
##7.	Disclosure of personal data to third parties
Volunteer Vision's online services essentially serve the establishment of contact between the users. Therefore, in order to provide their services, Volunteer Vision shall share contact details and any further profile data with other users for the purpose of networking and interactive exchange within the course of communicative functionalities.
Moreover, Volunteer Vision transmits the following data for the purpose of a) the comprehensive support of Mentees to cooperating social organizations and b) the comprehensive support of Mentors to their employers: name, e-mail address, phone number, address, educational background, professional development and goals, country of origin, language skills, hobbies, personal details, values and motivation as well as user data (such as the average stay per user/session or the amount of completed sessions) as well as for the purpose of evaluating collected user feedback. Apart from that Volunteer Vision shall not disclose personal data to third parties, unless the user has given his/her consent, the disclosure is prescribed by law or the disclosure is legally admissible without consent. The disclosure of data to governmental institutions and authorities shall only be provided within the framework of binding national law. As far as third parties have been commissioned by Volunteer Vision to process or use personal data, this happens on the sole basis of written data processing agreements.
Volunteer Vision checks the names of the Mentees against official sanctions lists within the limits of applicable provisions. For further information refer to the following link: http://www.finanz-sanktionsliste.de/fisalis/jsp/index.jsf
##8.	Storage and deletion of data
Personal data is stored and processed only as long as necessary for the purposes mentioned herein, especially for the use of the online services of Volunteer Vision or as long as Volunteer Vision is obligated by law to store the data.
##9.	System check data
Volunteer Vision shall automatically collect and store data the user's browser transfers when using the online services of Volunteer Vision via the system used by the user. These include:

*	User's IP address 
*	Operating system
*	Browser type/browser version
*	URL from which the user has been redirected
*	Time of use

When the user has given his/her consent, Volunteer Vision uses this data to verify whether all the functionalities can be executed by the user's system in order to guarantee the proper use of the online services of Volunteer Vision. There shall be no link between this data and data from other sources.

##10.	Consent, Revocation, Information and Rectification
####10.1	Consent and Revocation
Previously given consents may be revoked with future effect at any time.
####10.2	Information
Users have the right to receive information about their personal data stored with Volunteer Vision at any time without additional costs.
####10.3	Rectification
In the event that information has been stored incorrectly, Volunteer Vision shall immediately rectify them upon the request of the user concerned.
##11.	Modifications to the privacy policy
Volunteer Vision reserves the right to modify the privacy policiy from time to time. Therefore, Volunteer Vision recommends users to regularly check whether there is a new version available.
##12.	Data protection officer
Users shall direct any requests for information, questions, complaints or suggestions to the following address:

Volunteer Vision GmbH |
Amalienstraße 87 |
80799 München

info@volunteer-vision.com