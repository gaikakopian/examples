{{$salutation}}

We are happy to have you as a participant in our Online Mentoring Program. Great that you scheduled your first mentoring session with your mentor. 

**You will meet your mentor on {{data.date}} in the virtual conference room of our Online Mentoring Platform.**

**How do I get to the platform to meet my mentor online?**

1.	Please follow this link to get to the Online Mentoring Platform: https://www.volunteer-vision.com
2.	Log in with your email address and the password you set during the initial registration. If you forgot your password, you can easily reset it or use our birth year (YYYY) as back-up. 
3.	Please click on the button "conference room" and wait here for your mentor 5 minutes prior to the agreed time. 
4. As soon as your mentor has joined the conference room, you can start the session. Your mentor will call you through the platform for this.

All you need for the mentoring program is a laptop/computer with a functioning camera and microphone and a stable internet connection. Smart phones are not suitable for this program.

Please don't hesitate to get in touch with us in case you have further questions.

Have a great first session with your mentor!

Warm wishes,
Your Volunteer Vision Team