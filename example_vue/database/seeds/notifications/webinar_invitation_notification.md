{{$salutation}}


It’s time: On **{{ $webinar::starts_at }} (CEST)**, we will go through the mentor preparation training with you. The webinar takes place online via “GoToMeeting” and will take about 90 minutes. 

**Logging into the webinar with GoToMeeting**

- **Please note:** data.companyWebinarInfo 

- **Infrastructure:** Please make sure you have working speakers and a microphone. We recommend using a headset or headphones.

- **Login:** Open this **[link]({{ $webinar::login_link }})** a few minutes before the start.

- **Problems:** If problems occur, connect via smartphone. data.webinarDialInInfo. To do this, please download the [GoToMeeting App](https://support.citrixonline.com/de/Meeting/all_files/G2M010002)

- **First GoToMeeting?** Try a [Test session](https://support.logmeininc.com/test/g2mgetready).

In the webinar, the trainer will introduce you to all topics related to your commitment and, of course, answer your questions. We look forward to beginning the program with you!


Best regards,

Your Volunteer Vision team