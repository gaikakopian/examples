{{$salutation}}

It looks as if you started registering on the Online Mentoring Platform some time ago. You are only a few steps away from being able to participate!

Please complete the registration in order to participate in a webinar training and start your participation. You can log onto the platform using your selected data to complete the process:  

**[Log onto the platform ](https://portal.volunteer-vision.com/)**

Should you experience any difficulties or have any questions, please contact Volunteer Vision:  support@volunteer-vision.com

We look forward to welcoming you to the program as an online mentor!

Kind regards,

Your Volunteer Vision Team