{{$salutation}}

Your mentoring appointment was canceled.

Please log onto the platform in order to specify a new date.

Message from your mentee:


Kind regards,

Your Volunteer Vision Team