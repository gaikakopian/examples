{{$salutation}}

Congrats for successfully enrolling in this program! Your mentoring is almost ready to take-off!

**Wenn Webinar ausgewählt:**
Next up, you will join one of our webinars to get you ready for your engagement. 
A few days before your selected webinar date, we will email you all the necessary information to dial into the webinar as well as information regarding next steps.

**Wenn kein Webinar ausgwählt**
All you have to to now is choose a date for your preparatory webinar. There, we will talk about everything to get you ready for your engagement. To choose a date, just click on this link:

**Choose a webinar date**

If you have any questions about this, please don't hesitate to contact us! 

Warm wishes, 
Your Volunteer Vision Team