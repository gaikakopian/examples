{{$salutation}}

Great news: we have found a mentee for you!


**[Confirm your match here]({{$brand->frontendurl}})** 

You can see when your mentee has time to meet here and arrange a first meeting. 

**Please reply within the next 48 hours.**

If you have any questions about the process, please feel free to contact us.

Warm wishes, 

Your Volunteer Vision Team