<?php

use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\Tenant;

class TenantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Tenant::class)->create(['name' => 'Default Tenant']);
        factory(Tenant::class)->create(['name' => 'VolunteeringMatters']);
    }
}
