<?php

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;
use Modules\Community\Domain\Models\Chatroom;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\ProfileField;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Models\Webinar;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

class ChatroomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\Faker\Generator $generator)
    {
        /**
         * @var $randomUsers Collection
         */
        $randomUsers = User::whereRaw('MOD(id, 3) = 0')->get();




        foreach ($randomUsers as $user) {

            $randomOtherUser = $randomUsers->random();

            if ($randomOtherUser->id != $user->id) {
                $room = new Chatroom();
                $room->external_room_id = rand(0,1000);
                $room->save();
                $room->users()->attach($randomOtherUser->id);
                $room->users()->attach($user->id);
            }
        }
    }

}