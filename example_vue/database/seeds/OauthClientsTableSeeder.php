<?php

use Illuminate\Database\Seeder;
use Laravel\Passport\Client;
use Laravel\Passport\PersonalAccessClient;

class OauthClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if (Config::get('app.env', false) === 'local') {
            // Only in Development
            Client::create([
                'password_client' => 1,
                'secret' => 'non-secret',
                'name' => 'Development Password Client',
                'redirect' => Config::get('app.url'),
                'personal_access_client' => 0,
                'revoked' => 0,
            ]);

            $personal = Client::create([
                'password_client' => 0,
                'secret' => 'non-secret',
                'name' => 'Development Personal Access Client',
                'redirect' => Config::get('app.url'),
                'personal_access_client' => 1,
                'revoked' => 0,
            ]);

            PersonalAccessClient::create([
                'client_id' => $personal->id,
            ]);
        }
        // since we need to reset and seed also in staging
        // we need also to create a client here.
        if (Config::get('app.env', false) === 'staging') {
            Client::create([
                'password_client' => 1,
                'secret' => "non-secret", // getenv('CLIENT_SECRET'),
                'name' => 'Public Password Client',
                'redirect' => Config::get('app.url'),
                'personal_access_client' => 0,
                'revoked' => 0,
            ]);
        }

    }
}
