<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\ProfileField;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Models\Webinar;

use Modules\Matching\Domain\Models\Availability;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

class ActivitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $users = User::all();

        $users->each(function($user) {
            $amount = rand(0,10);
            if ($user->email == 'client@volunteer-vision.com') {
                $amount = 10;
            }
            factory(\Modules\Core\Domain\Models\Activity::class, $amount)->create(['user_id' => $user->id]);
        });


    }
}
