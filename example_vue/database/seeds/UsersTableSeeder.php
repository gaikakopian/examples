<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\ProfileField;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\Tenant;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Models\Webinar;

use Modules\Matching\Domain\Models\Availability;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tenant = Tenant::first();

        // Create a known user
        $user = User::where('email', 'user@volunteer-vision.com')->firstOrFail();
        UserAvatarsSeeder::seedProfilePicture($user);


        // Attach User to previously created Group
        $user->groups()->attach(1);

        // Create a few different ProfileFields for the user
        factory(ProfileField::class)->create([
            'user_id' => $user->id,
            'code' => 'languages',
            'value' => [
                'en' => 'advanced',
                'de' => 'basic',
                'ar' => 'fluent',
            ],
        ]);
        factory(ProfileField::class)->create([
            'user_id' => $user->id,
            'code' => 'interests',
            'value' => ['Climbing', 'Reading', 'Coding'],
        ]);
        factory(ProfileField::class)->create([
            'user_id' => $user->id,
            'code' => 'personality',
            'value' => 'outgoing',
        ]);

        // Disable Model lifecycle events during seeding
        Match::flushEventListeners();

        // Create an enrollment, a participation and a match
        $match = factory(Match::class)->create(['program_id' => 1]);
        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $user->id,
            'program_id' => 1,
            'role' => 'mentor',
        ]);

        factory(Participation::class)->create([
            'enrollment_id' => $enrollment->id,
            'match_id' => $match->id,
            'match_confirmed_at' => Carbon::now(),
        ]);


        // Create a known user
        $keyaccount = factory(User::class)->create([
            'first_name' => 'Alexandra',
            'last_name' => 'Graf',
            'email' => 'alexandra.graf@volunteer-vision.com',
            'password' => bcrypt('start123'),
            'organization_id' => 1,
            'brand_id' => 1,
            'tenant_id' => $tenant->id
        ]);
        //@todo: seed with photo of Alex

        UserAvatarsSeeder::seedProfilePicture($keyaccount);

        Organization::query()->update(['keyaccount_id' => $keyaccount->id]);
        $programmanager = factory(User::class)->create([
            'first_name' => 'Max',
            'last_name' => 'Programmanager',
            'email' => 'programmanager@volunteer-vision.com',
            'password' => bcrypt('start123'),
            'organization_id' => 1,
            'brand_id' => 1,
            'tenant_id' => $tenant->id,
        ]);

        Program::query()->update(['manager_id' => $programmanager->id]);
        Webinar::query()->update(['referent_id' => $programmanager->id]);

//        // Create some more random users
//
//        factory(User::class, 30)//@todo: ['registration_code_id' => 1]
////        ->create(['registration_code_id' => 1])
//        ->create(['avatar' => 'sample.png'])
//            ->each(function (User $user) {
//
//                $group = Group::inRandomOrder()->get()->first();
//                $user->groups()->attach($group->id);
//
//                factory(ProfileField::class)->create(['user_id' => $user->id]);
//                $enrollment = factory(Enrollment::class)->create(['user_id' => $user->id]);
//                $this->seedEnrollment($enrollment, $user);
//
//                factory(Availability::class, 10)->create(['owner_id' => $user->id]);
//                self::copySampleAvatar($user->id);
//
//            });
    }

    private function seedEnrollment(Enrollment $enrollment, User $user)
    {

        if ($enrollment->state == Enrollment::STATES['NEW']) {
            return;
        }
        if ($enrollment->state == Enrollment::STATES['TRAINING']) {
            $webinar = Webinar::where('starts_at', '>=', new \DateTime())->get()->first();

            if ($webinar) {
                $enrollment->webinars()->attach($webinar->id);
            }

            return;
        }


        $webinar = Webinar::where('starts_at', '>', new \DateTime())->inRandomOrder()->get()->first();
        $enrollment->webinars()->attach($webinar->id,['attended' => true]);

        if ($enrollment->state == Enrollment::STATES['AVAILABLE']) {
            return;
        }

        /*
         * auto match some enrollments to have a match
         */
        if (rand(0, 1) === 0 && $enrollment->state == Enrollment::STATES['ACTIVE']) {
            // @?? find second person ??

            $otherRole = ($enrollment->role == 'mentee' ? 'mentor' : 'mentee');
            $otherEnrollment = Enrollment::where([
                'role' => $otherRole,
                'state' => Enrollment::STATES['AVAILABLE']
            ])->inRandomOrder()->first();

            if ($otherEnrollment) {
                // we found a match... let's bring them together!
                $match = factory(Match::class)->create(['program_id' => $enrollment->program_id]);
                factory(Participation::class)->create(['enrollment_id' => $enrollment->id, 'match_id' => $match]);
                factory(Participation::class)->create(['enrollment_id' => $otherEnrollment->id, 'match_id' => $match]);
                $otherEnrollment->state = Enrollment::STATES['ACTIVE'];
                $otherEnrollment->save();

                factory(Appointment::class)->create(['match_id' => $match->id], 5);
            }


        }

    }


    public static function copySampleAvatar($userid)
    {

        $src = __DIR__ . '/sampledata/sample.png';
        $root = __DIR__ . '/../../storage/app/public/uploads/user/avatar/';

        $paddedId = implode('/', str_split(sprintf('%09d', $userid), 3));
        $root .= $paddedId;


        foreach (['large', 'medium', 'small', 'orignal'] as $subfolder) {
            self::createFolderAndCopy($root . '/' . $subfolder, $src);
        }
    }

    private static function createFolderAndCopy($folder, $src)
    {

        if (!File::exists($folder)) {
            File::makeDirectory($folder, 0755, true);
        }
        $target = $folder . '/' . basename($src);
        if (!file_exists($target)) {
            copy($src, $target);
        }

        return $target;
    }


}
