<?php

use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\Brand;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create a known Brand

        factory(Brand::class)->create([
            'name' => 'Default Brand',
        ]);
        factory(Brand::class)->create([
            'name' => 'Volunteer Vision',
            'code' => 'VV'
        ]);
        factory(Brand::class)->create([
            'name' => 'Volunteering Matters',
            'code' => 'VM'
        ]);
        factory(Brand::class)->create([
            'name' => 'Global Match',
            'code' => 'GM'
        ]);
        factory(Brand::class)->create([
            'name' => 'Kiron',
            'code' => 'KIRON'
        ]);


        // Create some more random Brands
//        factory(Brand::class, 3)->create();
    }
}
