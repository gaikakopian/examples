<?php

use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\BrandedDocument;

class BrandedDocumentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $documents = [];

        /*
         * Appointment Branded Documents
         */
        factory(BrandedDocument::class)->create([
            'key' => 'appointment_no_show_sender_notification',
            'subject' => 'Sorry for the appointment failure',
            'content' => 'Hello {{ $user->first_name }}! We\'re sorry you were alone at the appointment which was planned at {{ $appointment->planned_start }} ...',
            'markdown' => 'Hello {{ $user->first_name }}! We\'re sorry you were alone at the appointment which was planned at {{ $appointment->planned_start }} ...',
            'type' => 'email',
        ]);

        factory(BrandedDocument::class)->create([
            'key' => 'appointment_no_show_sender_notification',
            'subject' => 'Sorry for the appointment failure',
            'content' => 'Hello {{ $user->first_name }}! We\'re sorry you were alone at the appointment which was planned at {{ $appointment->planned_start }} ...',
            'markdown' => 'Hello {{ $user->first_name }}! We\'re sorry you were alone at the appointment which was planned at {{ $appointment->planned_start }} ...',
            'type' => 'sms',
        ]);

        factory(BrandedDocument::class)->create([
            'key' => 'appointment_no_show_missed_you_notification',
            'subject' => 'Where have you been at the appointment?',
            'content' => 'Hello {{ $user->first_name }}! We missed you at the appointment which was planned at {{ $appointment->planned_start }}. Where have you been?',
            'markdown' => 'Hello {{ $user->first_name }}! We missed you at the appointment which was planned at {{ $appointment->planned_start }}. Where have you been?',
            'type' => 'email',
        ]);

        factory(BrandedDocument::class)->create([
            'key' => 'appointment_no_show_missed_you_notification',
            'subject' => 'Where have you been at the appointment?',
            'content' => 'Hello {{ $user->first_name }}! We missed you at the appointment which was planned at {{ $appointment->planned_start }}. Where have you been?',
            'type' => 'sms',
        ]);

        factory(BrandedDocument::class)->create([
            'key' => 'user_invitation_notification',
            'language' => 'en',
            'subject' => 'Invitation To Join Volunteer Vision',
            'content' => 'Hello! You are invited to Volunteer Vision Platform, register from here {{ $userInvitation->uid }}',
            'type' => 'email',
        ]);

        factory(BrandedDocument::class)->create([
            'key' => 'appointment_rescheduled_notification',
            'subject' => '',
            'content' => '',
            'type' => 'email',
        ]);

        factory(BrandedDocument::class)->create([
            'key' => 'appointment_rescheduled_notification',
            'subject' => '',
            'content' => '',
            'type' => 'sms',
        ]);

        factory(BrandedDocument::class)->create([
            'key' => 'appointment_canceled_notification',
            'subject' => '',
            'content' => '',
            'type' => 'email',
        ]);

        factory(BrandedDocument::class)->create([
            'key' => 'appointment_canceled_notification',
            'subject' => '',
            'content' => '',
            'type' => 'sms',
        ]);

        factory(BrandedDocument::class)->create([
            'key' => 'appointment_noattendance_notification',
            'subject' => '',
            'content' => '',
            'type' => 'email',
        ]);

        factory(BrandedDocument::class)->create([
            'key' => 'appointment_noattendance_notification',
            'subject' => '',
            'content' => '',
            'type' => 'sms',
        ]);

        factory(BrandedDocument::class)->create([
            'key' => 'user_registration_mobile_confirmation_notification',
            'subject' => '',
            'content' => '',
            'type' => 'whatsapp',
            'language' => 'en'
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'user_registration_mobile_accepted_confirmation_notification',
            'subject' => '',
            'content' => '',
            'language' => 'en',
            'type' => 'whatsapp',
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'appointment_planned_notification',
            'subject' => '',
            'content' => '',
            'type' => 'email',
        ]);

        factory(BrandedDocument::class)->create([
            'key' => 'appointment_planned_notification',
            'subject' => '',
            'content' => '',
            'type' => 'sms',
        ]);


        factory(BrandedDocument::class)->create([
            'key' => 'landingpage_registration_notification',
            'subject' => 'Some subject',
            'content' => 'Test',
            'type' => 'email',
        ]);

        factory(BrandedDocument::class)->create([
            'key' => 'appointment_planned_first_notification_mentee',
            'subject' => '',
            'content' => '',
            'type' => 'email',
        ]);

        factory(BrandedDocument::class)->create([
            'key' => 'appointment_planned_first_notification_mentee',
            'subject' => '',
            'content' => '',
            'type' => 'sms',
        ]);

        factory(BrandedDocument::class)->create([
            'key' => 'appointment_planned_last_notification_mentor',
            'subject' => '',
            'content' => '',
            'type' => 'email',
        ]);

        factory(BrandedDocument::class)->create([
            'key' => 'appointment_planned_last_notification_mentor',
            'subject' => '',
            'content' => '',
            'type' => 'sms',
        ]);
        factory(BrandedDocument::class)->create([
            'key' => 'appointment_reminder_notification',
            'subject' => '',
            'content' => '',
            'type' => 'email',
        ]);

        factory(BrandedDocument::class)->create([
            'key' => 'appointment_reminder_notification',
            'subject' => '',
            'content' => '',
            'type' => 'sms',
        ]);

        /*
         * User Branded Documents
         */
        factory(BrandedDocument::class)->create([
            'key' => 'user_completing_registration_notification',
            'subject' => 'Welcome to VV {{ $user->first_name }}',
            'content' => 'Dear {{ $user->first_name }}, Thank you for registering in VV',
            'type' => 'email',
        ]);

        factory(BrandedDocument::class)->create([
            'key' => 'user_goodbye_notification',
            'subject' => 'We will miss you {{ $user->first_name }}',
            'content' => 'Dear {{ $user->first_name }}, Thank you for being a part of VV all this time. All the best.',
            'type' => 'email',
        ]);

        factory(BrandedDocument::class)->create([
            'key' => 'user_welcome_back_notification',
            'subject' => 'Welcome Back {{ $user->first_name }}',
            'content' => 'Dear {{ $user->first_name }}, We are happy to have you back',
            'type' => 'email',
        ]);

        factory(BrandedDocument::class)->create([
            'key' => 'user_complete_registration_reminder',
            'subject' => '',
            'content' => '',
            'type' => 'email',
        ]);

        factory(BrandedDocument::class)->create([
            'key' => 'user_password_reset_notification',
            'subject' => 'Reset Password',
            'markdown' => "Hi {{ \$user->first_name }},\n\n please click the following link to reset your login password: [{{ \$reset_link }}]({{ \$reset_link }})",
            'type' => 'email',
        ]);

        // factory(BrandedDocument::class)->create([
        //     'key' => 'user_password_reset_notification',
        //     'subject' => '',
        //     'content' => '',
        //     'type' => 'sms',
        // ]);

        /*
         * Webinar Branded Documents
         */

        array_push($documents, [
            'key' => 'welcomeexample',
            'types' => ['article'],
        ]);
        array_push($documents, [
            'key' => 'webinar_invitation_notification',
            'types' => ['email', 'sms'],
        ]);

//        array_push($documents, [
//            'key' => 'appointment_first_completed_notification',
//            'types' => ['email'],
//        ]);
        array_push($documents, [
            'key' => 'user_password_has_been_changed_notification',
            'types' => ['email'],
        ]);

        array_push($documents, [
            'key' => 'user_lead_reminder_notification',
            'types' => ['email'],
        ]);
        array_push($documents, [
            'key' => 'user_lead_invitation_notification',
            'types' => ['email'],
        ]);
        array_push($documents, [
            'key' => 'user_lead_reminder_2_notification',
            'types' => ['email'],
        ]);


        array_push($documents, [
            'key' => 'user_optin_again_notification',
            'types' => ['email'],
        ]);
        array_push($documents, [
            'key' => 'enrollment_enrolled_notification',
            'types' => ['email'],
        ]);

        array_push($documents, [
            'key' => 'webinar_finished_notification',
            'types' => ['email', 'sms'],
        ]);

        array_push($documents, [
            'key' => 'webinar_rescheduled_notification',
            'types' => ['email'],
        ]);
        array_push($documents, [
            'key' => 'webinar_canceled_notification',
            'types' => ['email'],
        ]);
        array_push($documents, [
            'key' => 'webinar_reminder',
            'types' => ['email'],
        ]);

//        array_push($documents, [
//            'key' => 'enrollment_program_completed_notification',
//            'types' => ['email','sms'],
//        ]);
        array_push($documents, [
            'key' => 'enrollment_training_completed_notification',
            'types' => ['email'],
        ]);
        array_push($documents, [
            'key' => 'user_registration_complete_notification',
            'types' => ['email'],
        ]);

        array_push($documents, [
            'key' => 'enrollment_without_webinar_notification',
            'types' => ['email'],
        ]);

        array_push($documents, [
            'key' => 'enrollment_user_quit_notification',
            'types' => ['email'],
        ]);

        array_push($documents, [
            'key' => 'enrollment_postponed_notification',
            'types' => ['email'],
        ]);
        array_push($documents, [
            'key' => 'enrollment_program_enrolled_notification',
            'types' => ['email', 'sms'],
        ]);
        array_push($documents, [
            'key' => 'enrollment_resume_reminder',
            'types' => ['email', 'sms'],
        ]);

        array_push($documents, [
            'key' => 'match_suggestion_notification', // to mentor
            'types' => ['email', 'sms'],
        ]);

        array_push($documents, [
            'key' => 'match_confirmed_notification', // to mentee
            'types' => ['email', 'sms'],
        ]);

        array_push($documents, [
            'key' => 'match_finished_notification',
            'types' => ['email', 'sms'],
        ]);

        array_push($documents, [
            'key' => 'match_confirm_reminder', // after xx days in unconfirmed;
            'types' => ['email', 'sms'],
        ]);

        /*
         * Supervisor  Branded Documents
         */

        array_push($documents, [
            'key' => 'supervisor_weekly_report_notification', // after xx days in unconfirmed;
            'types' => ['email', 'sms'],
        ]);

        $Parsedown = new Parsedown();

        foreach ($documents as $document) {
            $document['key'] = strtolower($document['key']);

            $markdownFile = __DIR__ . '/notifications/' . $document['key'] . '.md';

            $subjectFile = __DIR__ . '/notifications/' . $document['key'] . '.subject.txt';
            $smsfile = __DIR__ . '/notifications/' . $document['key'] . '.sms.txt';
            $content = '';

            $markdown = ' -- Markdown missing --';
            if (file_exists($markdownFile)) {
                $markdown = file_get_contents($markdownFile);
                $content = $Parsedown->text($markdown);
            } else {
                echo 'Missing file ' . $markdownFile . PHP_EOL;
            }

            $sms = file_exists($smsfile) ? file_get_contents($smsfile) : '-- Content Missing --';
            $subject = file_exists($subjectFile) ? file_get_contents($subjectFile) : ' -- Subject Missing --';

            foreach ($document['types'] as $type) {
                factory(BrandedDocument::class)->create([
                    'key' => $document['key'],
                    'subject' => $subject,
                    'language' => 'en',
                    'markdown' => ($type === 'sms' ? $sms : $markdown),
                    'content' => ($type === 'sms' ? $sms : $content),
                    'type' => $type,
                ]);
            }
        }
        $keys = BrandedDocument::getAllKnownKeys();

        foreach ($keys as $key) {

            // make sure all emails exist;
            if (!BrandedDocument::where('key', '=', $key)->exists()) {
                Factory(BrandedDocument::class)->create(['key' => $key, 'type' => 'email']);
            }
        }
    }
}
