<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\ProfileField;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Models\Webinar;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

/**
 * Class MatchTableSeeder
 *
 * Here we generate users + matches randomly but realistic;
 * This affects only users who have already an match.
 * For Users
 *
 */
class UsersWithoutMatchesSeeder extends Seeder
{

    /**
     * @var \Faker\Generator
     */
    private $generator;


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\Faker\Generator $generator)
    {
        $amount = 40; // for staging
        if (in_array(App::Environment(), ['local'])) {
            $amount = 5;
        }


        echo sprintf("[UsersWithoutMatchSeeder] seeding %s users" , $amount) . PHP_EOL;

        $this->generator = $generator;
        $this->preLoadData();

        // Disable Model lifecycle events during seeding
        Match::flushEventListeners();

        $primaryUser = User::where('email', 'client@volunteer-vision.com')->firstOrFail();
        $this->seedEnrollment($primaryUser, Enrollment::STATES['ACTIVE'], 1);
        $this->seedEnrollment($primaryUser, Enrollment::STATES['TRAINING'], 2);
        $this->seedEnrollment($primaryUser, Enrollment::STATES['DONE'], 3);


        for ($i = 0; $i < $amount; $i++) {
            echo '.';
            $state = $this->generator->randomElement([
                Enrollment::STATES['NEW'],
                Enrollment::STATES['TRAINING'],
                Enrollment::STATES['TRAINING'],
                Enrollment::STATES['ACTIVE'],
                Enrollment::STATES['ACTIVE'],
                Enrollment::STATES['ACTIVE']
            ]);

            $mentor = $this->generateUser(User::ROLES['mentor']);
            $mentee = $this->generateUser(User::ROLES['mentee']);
            $this->seedEnrollment($mentor, $state);
            $this->seedEnrollment($mentee, $state);

        }
        echo PHP_EOL;

    }


    /**
     * @param $email
     * @return array
     */
    private function seedEnrollment(User $user, $state, $programId = null): Enrollment
    {

        if ($programId == null) {
            $programId = $this->generator->randomElement($this->programs)['id'];
        }

        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $user->id,
            'program_id' => $programId,
            'role' => $user->primary_role,
            'state' => $state
        ]);


        if ($state === Enrollment::STATES['TRAINING']) {

            $webinar = $this->generator->randomElement($this->futureWebinars)['id'];
            $enrollment->webinars()->attach($webinar, ['attended' => null]);

        }
        if ($state === Enrollment::STATES['ACTIVE']) {

            $webinar = $this->generator->randomElement($this->pastWebinars)['id'];
            $enrollment->webinars()->attach($webinar, ['attended' => true]);

            factory(Participation::class)->create([
                'enrollment_id' => $enrollment->id,
                'match_id' => null
            ]);
        }

        return $enrollment;

    }


    private $socialServices;
    private $companies;
    private $programs;
    private $pastWebinars;
    private $futureWebinars;

    private function preLoadData()
    {
        $this->socialServices = Organization::where('type', '=',
            Organization::TYPES['socialservice'])->get()->toArray();
        $this->companies = Organization::where('type', '=', Organization::TYPES['company'])->get()->toArray();
        $this->programs = Program::all()->toArray();
        $this->pastWebinars = Webinar::where('starts_at', '<', Carbon::now())->get()->toArray();
        $this->futureWebinars = Webinar::where('starts_at', '>', Carbon::now())->get()->toArray();

    }

    public function generateUser($role): User
    {
        $clist = $role === User::ROLES['mentor'] ? $this->companies : $this->socialServices;
        $organizaton = $this->generator->randomElement($clist);

        $user = factory(User::class)->create([
            'email' => $this->generator->companyEmail,
            'password' => bcrypt('start123'),
            'organization_id' => $organizaton['id'],
            'primary_role' => $role,
            'brand_id' => 1,
        ]);

        UsersTableSeeder::copySampleAvatar($user->id);
        return $user;


    }
}
