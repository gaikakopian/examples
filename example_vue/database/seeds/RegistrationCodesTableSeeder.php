<?php

use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Tenant;

class RegistrationCodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tenant = Tenant::first();

        // Create a known RegistrationCode
        factory(RegistrationCode::class)->create([
            'code' => 'default',
            'name' => 'Default Code',
            'brand_id' => 1,
            'group_id' => 1,
            'tenant_id' => $tenant->id,
        ]);

        // Create a known RegistrationCode
        factory(RegistrationCode::class)->create([
            'code' => 'mentor1',
            'primary_role' => User::ROLES['mentor'],
            'name' => 'Mentor Demo Code',
            'brand_id' => 1,
            'auto_enroll_in' => 1,
            'tenant_id' => $tenant->id,
        ]);

        // Create a known RegistrationCode
        factory(RegistrationCode::class)->create([
            'code' => 'mentee1',
            'primary_role' => User::ROLES['mentee'],
            'name' => 'Mentor Demo Code',
            'brand_id' => 1,
            'group_id' => 1,
            'auto_enroll_in' => 1,
            'tenant_id' => $tenant->id,
        ]);

        // Another known Code with auto-enrollment
        factory(RegistrationCode::class)->create([
            'code' => 'auto-enroll',
            'name' => 'Auto Enroll Code',
            'brand_id' => 1,
            'group_id' => 1,
            'auto_enroll_in' => 1,
            'tenant_id' => $tenant->id,
        ]);

        // Create some more random RegistrationCodes
        factory(RegistrationCode::class, 20)->create([
            'tenant_id' => $tenant->id,
        ]);
    }
}
