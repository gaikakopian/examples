<?php

use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\File;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Role;

class FilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Generate some random files
        factory(File::class, 100)->create();

        $program = Program::where('code', '=', 'eAbility')->firstOrFail();
        $this->addWebinarToProgram($program);

        $program = Program::where('code', '=', 'eSister')->firstOrFail();
        $this->addWebinarToProgram($program);
        $this->addLectionMaterialToProgram($program);



    }

    private function addWebinarToProgram(Program $program)
    {


        $baseurl = 'http://share.volunteer-vision.com/demodata/webinar/%s_Webinar_%s_%s.pdf';

        DB::select("DELETE FROM files WHERE lection_number = 0 AND program_id = $program->id");

        $langs = ['de', 'en'];
        $roles = [Role::ROLES['mentor'], Role::ROLES['mentee']];

        foreach ($roles as $role) {

            foreach ($langs as $l) {
                $data = [
                    'source' => sprintf($baseurl, $program->code, $role, $l),
                    'target_audience' => $role,
                    'mime' => 'application/pdf',
                    'language' => 'de',
                    'title' => 'Webinar - ' . ($l == 'de' ? ' Deutsch ' : ' English '),
                    'program_id' => $program->id, //esister
                    'lection_number' => 0
                ];
                factory(File::class)->create($data);
            }

        }


    }

    private function addLectionMaterialToProgram(Program $program)
    {

        $baseurl = 'http://share.volunteer-vision.com/demodata/lections/%s_Lection%s_%s.pdf';
        $langs = ['de', 'en'];
        $roles = [Role::ROLES['mentor'], Role::ROLES['mentee']];

        for ($i = 1; $i < $program->lections_total; $i++) {

            foreach ($roles as $role) {

                foreach ($langs as $l) {
                    $data = [
                        'source' => sprintf($baseurl, $program->code, $i, $l),
                        'target_audience' => $role,
                        'mime' => 'application/pdf',
                        'language' => 'de',
                        'title' => 'Lection ' . $i . ' - ' . ($l == 'de' ? ' Deutsch ' : ' English '),
                        'program_id' => $program->id, //esister
                        'lection_number' => $i
                    ];
                    factory(File::class)->create($data);
                }

            }

        }
    }


}
