@extends('layouts.singlepage')

@section('title', 'Appointment confirmed')

@section('content')

    <section class="section">


        <div class="box">
            <h1 class="title">Investoren Dashboard</h1>
            <h2 class="subtitle">Echtzeit KPIs für Investoren</h2>

            <table class="table" >
                <tr>
                    <th>Lizenzen</th>
                    <th>Abrechenbar</th>
                    <th>Aktiviert</th>
                </tr>
                <tr class="has-text-grey">
                    <td width="300">Lizenzen (erfasst)</td>
                    <td class="has-text-right">{{$billableLicences}}</td>
                    <td class="has-text-right">{{$activatedLicences}}</td>
                    <td></td>

                </tr>
                <tr class="has-text-grey">
                    <td width="300">Nicht erfasste Lizenzen*</td>
                    <td class="has-text-right">80</td>
                    <td class="has-text-right">80</td>
                </tr>
                <tr>
                    <td width="300">Abrechenbare Lizenzen</td>
                    <td class="has-text-right">{{$billableLicences + 80}}</td>
                    <td class="has-text-right">{{$activatedLicences + 80}}</td>

                </tr>

                <tr>
                    <th colspan="3">Weitere Kennzahlen</th>
                </tr>
                <tr>
                    <td width="300">Erreichte Personen (Neue Plattform)</td>
                    <td class="has-text-right">{{$reachedPeople}}</td>

                    <td></td>
                </tr>
            </table>

            <p class="has-text-grey" style="font-size:80%"> * Aus Skypepilot oder alter Plattform, die nicht übertragen wurden </p>


        </div>
    </section>
@endsection