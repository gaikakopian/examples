<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>{{ $senderName or '' }}</title>
    <style>

        a,p,span, body h1 {
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;

        }

        body {
            background: #fafafa;
        }
        a.btn {
            border-radius: 15px;
            background:rgb(255, 202, 62);
            color:#fff;
            padding:8px 10px;
            cursor:pointer;
            font-weight:bold;
            display:inline-block;
            font-size:14px;


        }
        a {
            text-decoration:none;

        }
        .box {
            background:#fff;
            padding:10px 20px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.07);
        }


        {{ $css or '' }}
    </style>
</head>
<body>

<div class="container">
    @yield('content')
</div>

</body>
</html>