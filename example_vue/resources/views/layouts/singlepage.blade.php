<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>{{ $senderName or '' }}</title>

    <link rel="stylesheet" type="text/css" href="{{ asset('css/vendor.css') }}" >
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}" >


    <style>

        h1,h2,h3 {
            padding-bottom:10px;
        }
        p {
            padding-bottom:10px;
        }
        h1, h2, h3 {
            font-weight:600;

        }
        h1 {
            font-size:120%;
        }
        h2 {
            padding-top:30px;
            font-size:120%;
        }
        h3 {
            font-size:110%;
        }
        body {
            background: #fafafa;
        }
        ul {
            list-style: square;
            margin-left:30px;
            padding-bottom:30px;
        }
        li {
            padding-left:0px;
        }
        /*a.btn {*/
            /*border-radius: 15px;*/
            /*background:rgb(255, 202, 62);*/
            /*color:#fff;*/
            /*padding:8px 10px;*/
            /*cursor:pointer;*/
            /*font-weight:bold;*/
            /*display:inline-block;*/
            /*font-size:14px;*/


        /*}*/
        /*a {*/
            /*text-decoration:none;*/

        /*}*/
        /*.box {*/
            /*background:#fff;*/
            /*padding:10px 20px;*/
            /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.07);*/
        /*}*/
        .container {
            max-width:800px;
        }

        {{ $css or '' }}
    </style>
</head>
<body>


<div id="content" class="container">
    @yield('content')
</div>

</body>
</html>