@extends('layouts.singlepage')

@section('title', 'Appointment confirmed')

@section('content')

    <section class="section">

        <div class="box">
            <h1>
                {{ __('responses.appointment.confirmed.title') }}
            </h1>

            <p>
                {{ __('responses.appointment.confirmed.text') }}

                <br/><br/><br/>
                <a class="btn btn-primary" href="{{$link}}">{{ __('responses.next') }} </a>

            </p>
        </div>
    </section>
@endsection