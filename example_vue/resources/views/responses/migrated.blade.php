@extends('layouts.singlepage')

@section('title', 'Page Title')

@section('content')


    <div class="box" style="margin-top:100px">
        <h1>
            {{ __('migration.headline') }}
        </h1>

        <p>
            {{ __('migration.text') }}

            <br/><br/><br/>
            <a class="btn btn-primary" href="{{$link}}">{{ __('migration.next') }} </a>

        </p>
    </div>
@endsection