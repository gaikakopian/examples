@extends('layouts.errorlayout')

@section('title', 'Page Title')


@section('content')
    <h1>Ohh no!</h1>

    <p> Something went wrong...</p>

    <p><b>{{$message}}</b></p>

    <p class="muted">Technical Message: {{$technicalMessage}}</p>
@endsection


@section('nextsteps')
    <br/><br/><br/>

    <p> Next steps:</p>
    <a href="javascript:history.back()"> &raquo; Back to last page </a><br/>
    <a href="mailto:info@volunteer-vision.com">&raquo; Contact us</a><br/>
    <a href="mailto:simon.fakir@volunteer-vision.com">&raquo; Fire the IT guy</a><br/>

@endsection
