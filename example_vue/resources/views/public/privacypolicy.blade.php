@extends('layouts.singlepage')

@section('title', 'Privacy Policy')

@section('content')

    <section class="section">

        <div class="box">

            {!!  $document->content !!}


            <br/><br/><br/>
            <a class="btn btn-primary" href="{{$frontendUrl}}">{{ __('responses.next') }} </a>


        </div>
    </section>
@endsection