<?php

return [
    'nameOfMentor' => 'Name des Mentors',
    'nameOfMentee' => 'Name des Mentees',
    'munich' => 'München',
    'inCooperationWith' => 'In Kooperation mit'
];