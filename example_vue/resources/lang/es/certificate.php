<?php

return [
    'nameOfMentor' => 'Nombre de Mentor',
    'nameOfMentee' => 'Nombre de Mentee',
    'munich' => 'München',
    'inCooperationWith' => 'In Kooperation mit'
];