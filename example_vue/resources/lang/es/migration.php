<?php

return [
    'headline' => 'Gracias por confirmar tu perfil ',
    'text' => '¡Bienvenido a tu nueva plataforma!',
    'next' => 'Login'
];