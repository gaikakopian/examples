<?php

return [
    'nameOfMentor' => 'Name of the mentor',
    'nameOfMentee' => 'Name of the mentee',
    'munich' => 'Munich',
    'inCooperationWith' => 'In cooperation with'
];