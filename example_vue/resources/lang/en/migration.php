<?php

return [
    'headline' => 'Thank you for confirming your profile',
    'text' => 'Welcome to your new online mentoring platform!',
    'next' => 'Continue to Login'
];