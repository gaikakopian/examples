
Make an entity commentable:


1) Open MapCommentable -> add mapping
2) Add Commentable trait to Entity
3) Add rights to policy (see userpolicy for example)
4) If policy does not exist, creat one (Core/domain/policies) and map it in AuthServiceProvider


that's it.
