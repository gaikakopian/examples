
php code sniffer (phpcs) checks the code for quality issues like todos etc.

please install it using the following command

    composer global require "squizlabs/php_codesniffer=*"

You can run it either with

    ~/.composer/vendor/bin/phpcs

or
    
    phpcs

