# Folder Structure

## Modules

We use a slightly altered folder structure, compared to the laravel default structure. Code is split up into *modules*, which group related functionality of the app, that is independent from functionality in other modules. Modules contain the main logic of the app. Most day-to-day development should take place inside modules.

Another example implementation of modules in Laravel, that we're **not using** but which inspired us: https://nwidart.com/laravel-modules/v2/introduction

### Rules:
1. A module brings everything it needs, including
	- Controllers
	- Services
	- Models
	- Tests
	- Route definitions
	- all other resources spcific to the model
2. A module must not depend on another module except `Core`
3. The only allowed dependencies of a Module are:
	- Infrastructure & external or app-wide services, such as laravel facades
	- Abstract definitions from `app/Infrastructure`
	- Whatever the module itself contains

### Creating a new Module
When creating a new Module, copy the `ModuleBlueprint`, rename everything as needed, and register its Service Provider in `config/app.php`.

> `modules/{ModuleName}/Http`

contains Presentation Logic for HTTP interface:

- `Actions` / `Controllers`: Orchestrate services
- `Resources`: Response transformation layer
- `Requests`: Input deserialization & validation
- `routes.php`: Map URLs to their Controllers/Actions
- **NO BUSINESS LOGIC**

> `modules/{ModuleName}/Domain`

contains Business Logic

- `Models`: Database structure definition and interface
- `Services`: Business logic goes here

> `modules/{ModuleName}/Tests`

Unit and acceptance tests for the module go here.

## `app/Infrastructure`

Contains mostly abstract parent classes or interfaces that you use inherit from or implement in your modules Http or Domain code. Typically, this folder should not need to be touched that often.
