# GIT Workflow

We strictly stick to gitflow as [documented by atlassian](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)

1. Develop you feature on a branch called `feature/xyz`, which branches off `develop`
2. Make sure all tests pass, before…
3. …creating a merge request into `develop`, and adding your teammates as reviewers
4. At least one peer has to carefully review the pull request and *approve* it
5. After the pull request has been approved, *merge* it, and *close* the source branch

## Branch naming conventions

- feature branches: `feature/{name}`
- release branches: `release/{version}`
- hotfix branches: `hotfix/{name}`

## Releases

1. Branch a release branch off of `develop`
2. Ensure all tests are passing
3. Tag the HEAD of the release branch with a version tag, like `v{semantic_version_number}`
4. Merge the release branch into `master`
5. `git push` the new release branch and master, and also `git push --tags`
