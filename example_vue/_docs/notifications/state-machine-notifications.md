#How to create a new notification

1) create a folder for each Model that will use Notifications inside the Notifications folder

    e.g: App\Notifications\Appointment
    
2) add a class in the notifications class that extends BrandedNotification

    e.g AppointmentNotification in App\Notifications\Appointment

3) for each notification needed, create a class in the same folder that extends the class created in step 2

    e.g class NoShowMissedYouNotification extends AppointmentNotification
    
4) each notification should have $document_key that references the stored notification in the database

    e.g     protected $document_key = 'appointment_no_show_missed_you_notification';


5) All notifications are stored in the database ('branded_documents') and referenced by the '$document_key' in the Notification class

 
6) Entries in the table ('branded_documents') to be updated manually or by using BrandedDocumentsTableSeeder:

    e.g:
    
        factory(BrandedDocument::class)->create([
            'key' => 'appointment_no_show_missed_you_notification',
            'subject' => 'Where have you been at the appointment?',
            'content' => 'Hello {{ $user->first_name }}! We missed you at the appointment which was planned at {{ $appointment->planned_start }}. Where have you been?',
            'type' => 'email',
        ]);
        
        
       ** IMPORTANT: every $document_key in a notification class must exactly match the 'key' value of the referenced row in the table 'branded_documents'

7) individual notifications to be instantiated as below:

        $notification = new NoShowMissedYouNotification($user->brand, $appointment);

        $mail = $notification->toMail($user);
        $sms = $notification->toTwilio($user);
        
        
8) place notifications in the proper transition state machine handler or in the Http Action if it is not possible to handle:


    e.g: in SendAppointmentNoShowAction
    
      public function __invoke(int $id, EmptyAppointmentRequest $request)
        {
            
            .
            .
            .
    
            foreach($otherUsers AS $missingUser) {
                $missingUser->notify(new NoShowMissedYouNotification($missingUser->brand, $appointment));
            }
    
            $sender->notify(new NoShowSenderNotification($sender->brand, $appointment));
            
            .
            .
            .
        }
    
    
    e.g: in class DefaultAppointmentStateHandler:
        public function reportNoshow(Event $event): void
        {
           .
           .
           .
           
            foreach($otherUsers AS $missingUser) {
                $missingUser->notify(new NoShowMissedYouNotification($missingUser->brand, $appointment));
            }            
            
            $sender->notify(new NoShowSenderNotification($sender->brand, $appointment));

                   
           .
           .
           
        }
        
    
      

        