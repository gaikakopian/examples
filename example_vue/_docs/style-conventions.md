# Style & Conventions

## Coding Style

There are no exceptions to Laravel's [Coding Style](https://laravel.com/docs/5.5/contributions#coding-style)


## you can use php-cs-fixer


    php php-cs-fixer.phar fix
    
or if installed with brew
    
    php php-cs-fixer.phar fix 
  