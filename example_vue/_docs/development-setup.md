# Development Setup

## Table of Contents

* [Local Setup](#local-setup)
   * [1. Install composer dependencies](#1-install-composer-dependencies)
   * [2. Setup Homestead virtual development server](#2-setup-homestead-virtual-development-server)
   * [3. Create .env file](#3-create-env-file)
   * [4. Migrate &amp; seed the database](#4-migrate--seed-the-database)
   * [5. Create a public-private keypair for Passport](#5-create-a-public-private-keypair-for-passport)
   * [6. (optional) Configure a local database client](#6-optional-configure-a-local-database-client)
* [PHPStorm config](#phpstorm-config)

Please refer to the **[Cheatsheet](cheatsheet.md)** for a quick lookup of commonly used commands.

On the **[Hints & Tricks](hints-tricks.md)** page you might find some useful stuff to ease your daily routine.

## Local Setup

### 1. Install composer dependencies

```bash
composer install
```

### 2. Setup Homestead virtual development server

a) Install [Virtualbox](https://www.virtualbox.org/) and [Vagrant](https://www.vagrantup.com/).

b) Create a `Homestead.yml` configuration:

```bash
php vendor/bin/homestead make
```

Set the url of the development site in `Homestead.yml`, for example:

```yaml
sites:
    -   map: vvapi.dev
        to: /home/vagrant/code/public
```

c) Create and launch the virtual machine:

```bash
vagrant up
```

### 3. Create `.env` file

```bash
cp .env.example .env
php artisan key:generate  # Write a random APP_KEY to .env
```

Adapt variables like database connection etc. as necessary. The `.env` file is gitignored – make sure to update `.env.example` as well, if a change needs to be available to all developers.

### 4. Migrate & seed the database

**This command needs to be run inside Homestead. See section "SSH into Homestead" on how to do this.**

```bash
php artisan migrate --seed
```

### 5. Create a public-private keypair for Passport

```bash
php artisan passport:keys
```

### 6. (optional) Configure a local database client

Use the following settings to connect to the database inside vagrant from the host machine:

```
HOST: 127.0.0.1
PORT: 54320
USER: homestead
PASS: secret
```

If you're using Postico, just copy the following URL before "adding a new favourite": `postgres://homestead:secret@127.0.0.1:54320`
