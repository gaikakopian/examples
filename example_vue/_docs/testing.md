# Testing

There are two different test suites:

- Unit: Contains unit tests for all parts of the application
- Http: Contains acceptance tests for controller functionality performed over http

## Setup

PHPUnit is installed as a dev-dependency under `vendor/bin/phpunit`.

## Running Tests

When running tests locally, a separate database connection 'testing' is used, which can be configured using the `DB_TESTING_xyz` variables in `.env`. The database is migrate:fresh'ed and seeded before running the tests. You can safely test against the seed data.

### Run all tests
```bash
composer test
```

### Run unit tests only
```bash
composer unit-test
```

### Run http tests only
```bash
composer http-test
```

## Writing Tests

Create a separate *test class* for each "unit" or controller you want to test. Write a separate *test method* for each aspect of that unit you're testing. Use the method name to describe what behaviour the method asserts, for example `test_it_updates_contact_details_when_a_user_signs_in_using_google_oauth`. I've deliberately chosen a long example to show you that it's okay to choose a long name, as long as it's accurate. The name is always in snake_case and always starts with `test_it_…`.

### Database Transactions

All tests that inherit from `App\Infrastructure\Testing\LaravelTest` automatically use the `DatabaseTransactions` trait, with the effect that each test method is executed within a database transaction. The transaction is rolled back after each test method. This means that the database is reset to its initial state for each test.


### Viewing raw SQL transactions

    
       \DB::listen(/**
         * @param QueryExecuted $sql
         */
            function($sql) {
            var_dump($sql->sql);
        });