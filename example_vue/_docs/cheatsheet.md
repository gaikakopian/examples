# Cheatsheet

This is just a brief overview over the most commonly used commands. Please refer to the [full documentation](https://laravel.com/docs/5.5) for more details.

## Table of contents

* [Homestead](#homestead)
   * [Start / Stop Homestead](#start--stop-homestead)
   * [SSH into Homestead](#ssh-into-homestead)
* [Migrations](#migrations)
   * [Run mirations](#run-mirations)
   * [Rollback mirations](#rollback-mirations)
   * [Re-run all migrations](#re-run-all-migrations)
* [Seeds](#seeds)

## Homestead

### Start / Stop Homestead

```bash
vagrant up
vagrant halt
```

### SSH into Homestead

```bash
vagrant ssh
cd code
```

## Migrations

**All artisan commands involving the database need to be run inside vagrant. (see "SSH into Homestead")**

### Run mirations

```bash
php artisan migrate
```

### Rollback mirations

```bash
php artisan migrate:rollback
```

This command rolls back only the migrations that were run during the last `php artisan migrate`.

### Re-run all migrations

```bash
php artisan migrate:refresh # or `php artisan migrate:fresh` when rollback fails
```

## Seeds

**All artisan commands involving the database need to be run inside vagrant. (see "SSH into Homestead")**

```
php artisan db:seed
```
