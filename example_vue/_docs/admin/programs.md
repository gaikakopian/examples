# Programs (admin guide)

## Matching Logic

When creating a program, you can specify based on which criteria candidates should be matched, when paricipating in the program. This can be done by entering one of the following values into the `matching_logic` field:

- Empty value (`NULL`): No special matching criteria except `mentor<=>mentee`
- `DefaultLogic`: Same as above
- `SameGenderLogic`: Only match mentors to mentees of the same gender
