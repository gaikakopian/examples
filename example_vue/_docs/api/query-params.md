# Query Parameters

All Admin "list" actions (eg. `GET /api/v1/admin/users`) support a standardized set of query parameters for sorting, filtering, eager loading and pagination of results. This functionality is basically provided by [bruno](https://github.com/esbenp/bruno).

## Syntax

The exact syntax can be looked up at [bruno's github](https://github.com/esbenp/bruno).

## Examples

### Sorting

```
GET /api/v1/admin/users?sort[0][key]=firstName&sort[1][key]=lastName&sort[1][direction]=desc
```

-> `ORDER BY firstName ASC, lastName DESC`

### Filtering

Filtering is a bit more difficult, as bruno expects us to pass an object similar to the following:

```json
{
    "filters": [
        {
            "key": "firstName",
            "value": "Simon",
            "operator": "eq"  // equals
        }
    ]
}
```

… which looks as follows, when expressed as a query string:

```
GET /api/v1/admin/users?filter_groups[0][filters][0][key]=firstName&filter_groups[0][filters][0][value]=Simon&filter_groups[0][filters][0][operator]=eq
```

-> `WHERE firstName STARTSWITH Simon`

### Eager Loading

```
GET /api/v1/admin/users?includes[]=organization&includes[]=profileFields
```

### Pagination

```
GET /api/v1/admin/users?limit=5&page=2
```

### Custom Filter

