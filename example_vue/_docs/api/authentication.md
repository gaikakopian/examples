# API Authentication

Authentication is handled by Passport. Please refer to the [official documentation](https://laravel.com/docs/5.5/passport) for reference. The frontend authenticates using a *password client*. For development, a client is defined in the database seeds, with `id: 1` and `secret: non-secret`.

## Authenticate a request

Many requests to the API require authentication as a registered user. In order to do so, send an `Authorization` header with your request, with a value of `Bearer {access_token}`.

## Obtain a token

Access tokens are obtained using a `POST` request against `/oauth/token`, with a json payload containing `grant_type`, `client_id`, `client_secret` plus the fields for the respective grant_type.

### Using email+password

```json
POST /oauth/token
Content-Type: application/json
Accept: application/json

{
	"grant_type": "password",
	"client_id": "1",
	"client_secret": "non-secret",
	"username": "user@example.com",
	"password": "secret"
}
```

### Using a refresh token

```json
POST /oauth/token
Content-Type: application/json
Accept: application/json

{
	"grant_type": "refresh_token",
	"client_id": "1",
	"client_secret": "non-secret",
	"refresh_token": "refresh-token-goes-here"
}
```
