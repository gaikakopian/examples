<?php

namespace App\Services\Tests\MultiTenant;

use App\Infrastructure\AbstractTests\LaravelTest;
use App\Services\MultiTenant\Facades\MultiTenant;
use App\Services\MultiTenant\TenantScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Mockery;

class TenantScopeTest extends LaravelTest
{
    /**
     * Check that the scope is properly applied to a query builder.
     *
     * In this test we're basically checking, that the following
     * is called on the builder:
     *
     * $builder->where(fuction ($query) {
     *     $query->where('tenant_id', '=', $the_global_tenant_id)
     *         ->orWhereNull('tenant_id')
     * });
     *
     * @return void
     */
    public function test_it_scopes_the_query_to_a_tenant_id()
    {
        $this->markTestSkipped();
        $globalTenantId = 'Some_Tenant_ID';
        MultiTenant::setTenantId($globalTenantId);

        // This function receives the arguments passed to the expected method call.
        // In our case, $argument must be a closure that applies further wheres
        // to a passed-in query builder instance for grouping nested wheres.
        $argValidator = function ($argument) use ($globalTenantId) {
            $nestedBuilder = Mockery::mock(Builder::class);
            $nestedBuilder->shouldReceive('where')
                ->once()
                ->with('table.tenant_id', '=', $globalTenantId)
                ->andReturnSelf();
            $nestedBuilder->shouldReceive('orWhereNull')
                ->once()
                ->with('table.tenant_id');

            $argument($nestedBuilder);

            return true;
        };

        $builder = Mockery::mock(Builder::class);
        $builder->shouldReceive('where')
            ->once()
            ->withArgs($argValidator);

        $model = Mockery::mock(Model::class);
        $model->shouldReceive('getTable')
            ->twice()
            ->andReturn('table');



        $subject = new TenantScope();
        $subject->apply($builder, $model);
    }
}
