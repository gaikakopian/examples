<?php

namespace App\Service\Tests\MultiTenant;

use App\Infrastructure\AbstractTests\LaravelTest;
use App\Services\MultiTenant\BelongsToTenant;
use App\Services\MultiTenant\Facades\MultiTenant;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Mockery;

class Model extends EloquentModel
{
    use BelongsToTenant;

    public static $mock;

    public static function creating($creatingHook)
    {
        $creatingHook(static::$mock);
    }
}

class BelongsToTenantTest extends LaravelTest
{
    public function test_it_sets_the_tenant_id_on_model_creation()
    {
        MultiTenant::setTenantId('Some_Tenant_ID');

        Model::$mock = Mockery::mock(Model::class)->makePartial();
        $model = new Model();

        $this->assertEquals('Some_Tenant_ID', $model::$mock->tenant_id);
    }
}
