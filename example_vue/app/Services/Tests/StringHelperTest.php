<?php

namespace App\Services\Tests;

use App\Infrastructure\AbstractTests\LaravelTest;
use App\Services\StringHelper;
use App\Services\TimeHelper;
use Carbon\Carbon;

class StringHelperTest extends LaravelTest
{
    public function test_it_trims_correctly()
    {
        $this->assertEquals('Yes', StringHelper::trimOnlyLetters('Yes!!!!'));
        $this->assertEquals('Yes', StringHelper::trimOnlyLetters('Yes '));
        $this->assertEquals('Yes', StringHelper::trimOnlyLetters('Y,es'));
    }

    public function test_it_startswith_correctly()
    {
        $this->assertEquals(true, StringHelper::startsWith('Yes!!!!', 'Yes'));
        $this->assertEquals(true, StringHelper::startsWith('Yes, asdsad!', 'Yes'));
        $this->assertEquals(true, StringHelper::startsWith('Yes', 'Yes'));
        $this->assertEquals(true, StringHelper::startsWith('yes', 'yes'));

        $this->assertEquals(false, StringHelper::startsWith('yes', 'Yes'));
        $this->assertEquals(false, StringHelper::startsWith('!yes', 'Yes'));
        $this->assertEquals(false, StringHelper::startsWith('Yes', 'No'));

    }
    public function test_it_trims_introduction() {

        $message = 'Hallo Nein leider kann nicht mehr.Vielen Dank Beste Grüße';

        $this->assertEquals( 'Nein leider kann nicht mehr.Vielen Dank Beste Grüße', StringHelper::strip_introduction($message));


    }

    public function test_it_converts_camel_to_snake_correctly()
    {

       $this->assertEquals(StringHelper::dashesToCamelCase('match_user_left_a_message_notification'), 'MatchUserLeftAMessageNotification');
        $this->assertEquals(StringHelper::camel_to_snake('SimonIstCool'), 'simon_ist_cool');
        $this->assertEquals(StringHelper::camel_to_snake('MatchUserLeftAMessageNotification'), 'match_user_left_a_message_notification');
        $this->assertEquals(StringHelper::camel_to_snake('MatchNotConfirmedReminder3Notification'), 'match_not_confirmed_reminder_3_notification');

    }
}
