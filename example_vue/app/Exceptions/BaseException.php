<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response as ResponseFacade;
use Illuminate\Support\Facades\View;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

/**
 * Base class for all custom Exceptions thrown by this application.
 */
abstract class BaseException extends Exception implements Arrayable
{
    /**
     * HTTP status code with which to return the Exception.
     *
     * @var int
     */
    protected $httpStatus = 500;

    public function __construct(string $message = '', $code = null, Throwable $previous = null)
    {
        parent::__construct($message, null, $previous);

        if (!is_null($code)) {
            $this->code = $code;
        }
    }

    /**
     * Render the Exception into a HTTP response.
     *
     * @param Illuminate\Http\Request $request
     * @return Response
     */
    public function render(Request $request) : Response
    {
        if ($request->wantsJson()) {
            return $this->renderJson($request);
        }

        return $this->renderHtml($request);
    }

    /**
     * Get an array representation of the Exception.
     *
     * @return array
     */
    public function toArray() : array
    {
        return [
            'code' => $this->code,
            'message' => $this->message,
        ];
    }

    /**
     * Get the HTTP status code with which to return the Execption.
     *
     * @return integer
     */
    public function getHttpStatus() : int
    {
        return $this->httpStatus;
    }

    /**
     * Set the HTTP status code with which to return the Execption.
     *
     * @return BaseException $this;
     */
    public function setHttpStatus(int $status) : BaseException
    {
        $this->httpStatus = $status;

        return $this;
    }

    /**
     * Generate a JSON response for the Exception.
     *
     * @param Request $request
     * @return JsonResponse
     */
    protected function renderJson(Request $request) : Response
    {
        return ResponseFacade::json($this->toArray(), $this->httpStatus);
    }

    /**
     * Generate an HTML response for the Exception.
     *
     * @param Request $request
     * @return Response
     */
    protected function renderHtml(Request $request) : Response
    {
        return ResponseFacade::view('error', $this->toArray(), $this->httpStatus);
    }
    
    /**
     * Create an App exception from a generic Exception.
     *
     * @param Exception $exception
     * @return BaseException
     */
    public static function fromBase(Exception $exception) : BaseException
    {
        return new static($exception->getMessage());
    }
}
