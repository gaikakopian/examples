<?php

namespace App\Exceptions\Client;

class NotAuthorizedException extends ClientException
{
    /** {@inheritDoc} */
    protected $code = 'client.not_authorized';

    /** {@inheritDoc} */
    protected $httpStatus = 403;

    /** {@inheritDoc} */
    protected $message = 'You are not authorized to perform this action.';
}
