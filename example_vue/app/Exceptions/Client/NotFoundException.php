<?php

namespace App\Exceptions\Client;

class NotFoundException extends ClientException
{
    /** {@inheritDoc} */
    protected $httpStatus = 404;

    /** {@inheritDoc} */
    protected $code = 'client.not_found';

    /** {@inheritDoc} */
    protected $message = 'The requested resource could not be found.';
}
