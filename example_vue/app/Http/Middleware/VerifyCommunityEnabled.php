<?php

namespace App\Http\Middleware;

use App\Exceptions\Client\NotAuthorizedException;
use Closure;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Check if user has enabled(joint) community and returns error response otherwise
 *
 * Class VerifyCommunityEnabled
 *
 * @package App\Http\Middleware
 */
class VerifyCommunityEnabled
{
    /**
     * Handle an incoming request.
     *
     * @param $request
     * @param Closure $next
     *
     * @return mixed
     *
     * @throws AccessDeniedException
     */
    public function handle($request, Closure $next)
    {
        $authUser = Auth::user();
        if (false === $authUser->community_enabled) {
            throw new NotAuthorizedException(
                'User can not implement this action, until he/she has not joint the community'
            );
        }

        return $next($request);
    }
}
