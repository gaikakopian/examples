<?php

namespace App\Http\Middleware;

use App\Exceptions\Client\NotAuthorizedException;
use Closure;
use Illuminate\Support\Facades\Config;

class VerifyConferenceSignature
{
    /**
     * Name of the request header field that contains the signature.
     *
     * @var string
     */
    protected $signatureHeader = 'X-VV-Signature';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $receivedSignature = $request->header($this->signatureHeader);

        if (!$receivedSignature || $this->computeSignature($request) !== $receivedSignature) {
            throw new NotAuthorizedException("Bad signature in {$this->signatureHeader}: {$receivedSignature}");
        }

        return $next($request);
    }

    /**
     * Compute the signature of the request payload.
     *
     * @param mixed $request
     * @param string $receivedSignature
     * @return string
     */
    private function computeSignature($request) : string
    {
        $payload = $request->getContent();
        $algo = Config::get('conference.signature_algo');
        $sharedSecret = Config::get('conference.shared_secret');

        return hash_hmac($algo, $payload, $sharedSecret);
    }
}
