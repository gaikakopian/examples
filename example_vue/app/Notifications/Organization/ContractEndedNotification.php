<?php

namespace App\Notifications\Organization;

class ContractEndedNotification extends OrganizationNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'organization_contract_ended_notification';
}