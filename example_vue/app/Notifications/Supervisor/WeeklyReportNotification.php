<?php

namespace App\Notifications\Supervisor;

use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\User;

class WeeklyReportNotification extends SupervisorNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'supervisor_weekly_report_notification';

    protected $supervisor_user;
    protected $new_matches;

    public function __construct(Brand $brand, array $newMatches)
    {
        $this->brand = $brand;
        $this->new_matches = $newMatches;
    }

    public function placeholderData(User $user)
    {
        return [
            'user' => $user,
            'brand' => $this->brand,
            'newMatches' => $this->new_matches
        ];
    }


}