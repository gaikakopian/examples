<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\User;

class BrandedMailMessage extends MailMessage
{

    public function __construct(Brand $brand = null)
    {
        if (!$brand) {
            $brand = Brand::where('code','=','VV')->firstOrFail(); // default Brand
        }

        $this->addData('brand', $brand->toArray());
        $this->bcc('outbox2@volunteer-vision.com', 'Archiv');
    }

    public function addData($name, array $value)
    {
        $data = [];
        $data[$name] = $value;
        $this->viewData = array_merge($this->viewData, $data);
    }


}