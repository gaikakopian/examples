<?php

namespace App\Notifications\User;

use App\Notifications\BrandedNotification;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\User;

class UserNotification extends BrandedNotification
{
    protected $user;

    public function __construct(Brand $brand, User $user)
    {
        $this->brand = $brand;
        $this->user = $user;
    }

    /** {@inheritDoc} */
    protected function placeholderData(User $user)
    {
        return [
            'brand' => $this->brand,
            'user' => $this->user,
        ];
    }
    public static function samplePlaceholderData($user)
    {
        return [
            'user' => $user,
            'brand' => $user->brand
        ];

    }
}