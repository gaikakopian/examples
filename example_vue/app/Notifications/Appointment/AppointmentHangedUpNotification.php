<?php

namespace App\Notifications\Appointment;

class AppointmentHangedUpNotification extends AppointmentNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'appointment_hangedup_notification';
}