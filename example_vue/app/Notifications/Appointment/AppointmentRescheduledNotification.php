<?php

namespace App\Notifications\Appointment;

class AppointmentRescheduledNotification extends AppointmentNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'appointment_rescheduled_notification';
}