<?php

namespace App\Notifications\Appointment;

class AppointmentNoShowMissedYouNotification extends AppointmentNotification
{
    public static function getComment()
    {
        return 'the person who did not come to an appointment';
    }
}
