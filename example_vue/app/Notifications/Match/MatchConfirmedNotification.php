<?php

namespace App\Notifications\Match;

class MatchConfirmedNotification extends MatchNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'match_confirmed_notification';

    protected $available_channels = ['email'];

    protected $additional_mobile_channels = ['sms'];


}