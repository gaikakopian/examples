<?php

namespace App\Notifications\Match;

use App\Notifications\BrandedNotification;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

class MatchNoFutureAppointmentNotification extends MatchNotification
{


    /** {@inheritDoc} */
    protected function placeholderData(User $user)
    {


        $secret = $user->emailSecret('PAUSE_MATCH');
        $route = route('match.email.response',
            [
                'userid' => $user->id,
                'id' => $this->match->id,
                'action' => 'pause',
                'secret' => $secret,
            ]
        );

        return [
            'user' => $user,
            'brand' => $this->brand,
            'match' => $this->match,
            'enrollment' => $this->enrollment,
            'pausementorshiplink' => $route,
            'pause_mentorship_link' => $route,
        ];
    }

    public static function samplePlaceholderData($user)
    {
        $data = parent::samplePlaceholderData($user);
        $data['pausementorshiplink'] = 'http://link';
        $data['pause_mentorship_link'] = 'http://link';
        return $data;
    }


}