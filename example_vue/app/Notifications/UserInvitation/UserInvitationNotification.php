<?php

namespace App\Notifications\UserInvitation;

use App\Notifications\AnonymousBrandedNotification;
use App\Notifications\BrandedNotification;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserInvitation;

class UserInvitationNotification extends AnonymousBrandedNotification
{
    /**
     * to notify invited users on the sysem
     */
    protected $userInvitation;

    /**
     * @var string
     */
    protected $document_key = 'user_invitation_notification';

    public function __construct(Brand $brand, UserInvitation $userInvitation)
    {
        $this->brand = $brand;
        $this->userInvitation = $userInvitation;
    }

    /** {@inheritDoc} */
    protected function placeholderData()
    {
        return [
            'brand' => $this->brand,
            'userInvitation' => $this->userInvitation,
        ];
    }

    /** {@inheritDoc} */
    public function via($notifiable)
    {
        return ['mail'];
    }
}