<?php

namespace App\Notifications;

use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use App\Services\BrandedDocumentService;
use Modules\Core\Domain\Services\ContentChooser\SearchObject;
use Modules\Core\Domain\Services\ContentChooser\ContentChooser;

class BrandedNotificationForProgram extends BrandedNotification
{
    /**
     * The program for which a template will be searched
     *
     * @var Modules\Core\Domain\Models\Program
     */
    protected $program;

    public function __construct(Brand $brand, Program $program)
    {
        $this->brand = $brand;
        $this->program = $program;
    }

    /** {@inheritDoc} */
    public function toDatabase(User $user) : array
    {
        return [
            'user' => $user,
            'brand' => $this->brand,
            'program' => $this->program
        ];
    }

    /** {@inheritDoc} */
    protected function getTemplateDocument(User $user, String $type)
    {
        $search_by = new SearchObject($this->document_key, $type, $user->getPreferredLanguageAttribute(), $this->brand, $this->program);
        $document = ContentChooser::findDocument($search_by);
        return $document;
    }

    /** {@inheritDoc} */
    protected function placeholderData(User $user)
    {
        return [
            'user' => $user,
            'brand' => $this->brand,
            'program' => $this->program
        ];
    }
}
