<?php

namespace App\Notifications\Enrollment;

use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\UserService;

/**
 * Class EnrollmentCompleteNotification
 * @package App\Notifications\Enrollment
 */
class EnrollmentNoreplyNotification extends EnrollmentNotification
{
    public static function getComment()
    {
        return 'When the enrollment switches to noreply';
    }


}