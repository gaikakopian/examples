<?php

namespace App\Notifications\Enrollment;

class EnrollmentPostponedNotification extends EnrollmentNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'enrollment_postponed_notification';


}