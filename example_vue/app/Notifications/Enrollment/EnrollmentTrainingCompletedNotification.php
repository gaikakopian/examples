<?php

namespace App\Notifications\Enrollment;

use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;

class EnrollmentTrainingCompletedNotification extends EnrollmentNotification
{
    public static function getComment()
    {
        return 'Training was completed.';
    }

    /** {@inheritDoc} */
    protected function placeholderData(User $user)
    {
        return [
            'user' => $user,
            'brand' => $this->brand,
            'enrollment' => $this->enrollment,
            'technicalinfo' => $user->organization->technical_usage_comment
        ];
    }

    public static function samplePlaceholderData($user)
    {
        return [
            'user' => $user,
            'brand' => $user->brand,
            'enrollment' => Enrollment::query()->where('state', Enrollment::STATES['ACTIVE'])->firstOrFail(),
            'technicalinfo' => $user->organization->technical_usage_comment
        ];

    }

}