<?php

namespace App\Notifications\Enrollment;

use App\Notifications\Webinar\WebinarChoiceReminderNotification;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Webinar;

class EnrollmentWebinarMissedYouNotification extends WebinarChoiceReminderNotification
{

    public static function getComment()
    {
        return 'User did not come to the webinar';
    }


}