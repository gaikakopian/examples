<?php

/*
 *         $value = Carbon::parse($value);
 *
 */

namespace App\Domain\Tests;

use App\Infrastructure\AbstractTests\LaravelTest;
use Carbon\Carbon;

/**
 * Test the functionality of the `/api/v1/users/me` endpoint.
 */
class BaseModelTest extends LaravelTest
{
    /** @test */
    public function test_it_converts_a_zulu_string()
    {

        $m = new SampleModel();
        /** @var Carbon $result */
        $result = $m->testAsDateTime('2018-08-30T22:00:00.000Z');

        $this->assertEquals($result->getTimezone()->getName(), 'Z');

        $this->assertEquals($result->month, 8);

    }
}

