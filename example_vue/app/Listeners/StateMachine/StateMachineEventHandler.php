<?php

namespace App\Listeners\StateMachine;

use App\Infrastructure\Traits\Statable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Event as EventFacade;
use SM\Event\TransitionEvent;
use Symfony\Component\EventDispatcher\Event;

class StateMachineEventHandler
{
    /**
     * Handle the event.
     *
     * @param  Event $event
     * @return void
     */
    public function handle(TransitionEvent $event)
    {
        // The state machine doesn't know anything that our
        // models need to be persisted in the database.
        $event->getStateMachine()->getObject()->save();

        $this->logTransition($event);
        $this->dispatchNamedEvent($event);
    }

    /**
     * Create a new entry in the state history, if the model supports it.
     *
     * @param Event $event
     * @return void
     */
    private function logTransition(TransitionEvent $event) : void
    {
        $sm = $event->getStateMachine();
        /** @var Statable $model */
        $model = $sm->getObject();

        if (!$model->logsTransitions()) {
            return;
        }

        $model->logTransition([
            'transition' => $event->getTransition(),
            'from' => $event->getState(),
            'to' => $sm->getState(),
        ]);
    }

    /**
     * Dispatch another event identifying the model and transition.
     *
     * @param Event $event
     * @return void
     */
    private function dispatchNamedEvent(Event $event)
    {
        $transitionMethod = $event->getTransition();
        $model = (new \ReflectionClass($event->getStateMachine()->getObject()))->getShortName();

        $eventName = "$model.$transitionMethod";

        EventFacade::dispatch($eventName, $event);
    }
}
