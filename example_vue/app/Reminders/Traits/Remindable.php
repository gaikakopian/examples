<?php

namespace App\Reminders\Traits;

trait Remindable
{
    public function hasAttribute($attr)
    {
        return in_array($attr, $this->fillable);
    }

}
