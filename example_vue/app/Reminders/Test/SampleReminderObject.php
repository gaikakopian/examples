<?php

namespace App\Reminder\Test;

use App\Infrastructure\AbstractTests\LaravelTest;
use App\Infrastructure\Http\QueryParams;
use App\Reminder\Handler\DummyReminderHandler;
use App\Reminder\Handler\MatchReminderHandler;
use App\Reminders\Traits\Remindable;
use Carbon\Carbon;
use Modules\Admin\Domain\Services\CoordinatorTodoService;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Domain\Services\UserService;
use Modules\Matching\Domain\Models\Match;

class SampleReminderObject extends LaravelTest
{
    use Remindable;

    protected $fillable = ['last_reminder'];


    /**
     * @var int
     */
    public $id;

    /**
     * @var Carbon
     */
    public $last_reminder = null;


    /**
     * @var Carbon
     */
    public $last_status_changed;

    public function save()
    {

    }
}