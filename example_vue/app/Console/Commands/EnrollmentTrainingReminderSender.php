<?php

namespace App\Console\Commands;

use App\Notifications\Enrollment\EnrollmentIncompleteReminderNotification;
use App\Notifications\User\CompleteRegistrationReminderNotification;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Reminder\EnrollmentIncompleteReminderHandler;
use Modules\Core\Reminder\WebinarReminderHandler;


class EnrollmentTrainingReminderSender extends AbstractCronjobCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:reminder:enrollment:training';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = ' description';


    /**
     * @var UserService
     */
    protected $enrollmentService;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(EnrollmentService $enrollmentService)
    {
        parent::__construct();
        $this->enrollmentService = $enrollmentService;

    }


    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {

        $this->init();
        $enrollments = $this->enrollmentService->listHangingOnState(Enrollment::STATES['TRAINING'], 24);
        // to save performance, we could add last_reminder < now() - 2 days, but once a day it does not matter

        /** @var EnrollmentIncompleteReminderHandler $handler */
        $handler = App(WebinarReminderHandler::class);
        $eventsFired = $handler->applyActions($enrollments);
        $this->end('[EnrollmentTrainingRemender] Events fired: %s of %s checked entities ',  $eventsFired, $enrollments->count());
    }
}
