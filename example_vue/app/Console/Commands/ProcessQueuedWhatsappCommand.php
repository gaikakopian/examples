<?php

namespace App\Console\Commands;

use App\Services\WaboboxService;
use Illuminate\Console\Command;
use Modules\Core\Domain\Models\SmsLog;
use Modules\Core\Domain\Models\User;

class ProcessQueuedWhatsappCommand extends AbstractCronjobCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:whatsapp:queues';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Whatsapp';

    protected $waboboxService;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(WaboboxService $waboboxService)
    {
        parent::__construct();
        $this->waboboxService = $waboboxService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();

        $queuedMessages = SmsLog::query()
            ->whereNull('sent_at')
            ->where('from', 'outgoing')
            ->where('channel', 'whatsapp')
            ->inRandomOrder()
            ->limit(5)
            ->get()
        ;

        $sent = 0;

        foreach ($queuedMessages as $message) {
            $response = $this->waboboxService->sendQueuedMessage($message);
            if ($response) {
                $sent++;
            }
        }

        $this->end(' sent out %s of %s messages', $sent, $queuedMessages->count());
        return;
    }
}
