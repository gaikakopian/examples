<?php

namespace App\Console\Commands;

use App\Notifications\Match\MatchNoFutureAppointmentNotification;
use App\Services\MultiTenant\Facades\MultiTenant;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Scheduling\Reminder\FutureAppointmentsReminderHandler;

class NoFutureAppointmentReminderSenderCommand extends AbstractCronjobCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:reminder:nofutureappointment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    protected $enrollmentService;
    protected $matchService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(EnrollmentService $enrollmentService, MatchService $matchService)
    {
        parent::__construct();
        $this->enrollmentService = $enrollmentService;
        $this->matchService = $matchService;
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
        $now = Carbon::now();



        $query = $this->matchService
            ->findMatchesWithoutPlannedAppointments(Match::STATES['ACTIVE'])
            ->where(function ($query) {
                $query->where('last_reminder', '<', Carbon::now()->subDays(2))
                    ->orWhereNull('last_reminder'); // @todo first
            });


        $matches = $query->get();

        /** @var FutureAppointmentsReminderHandler $handler */
        $handler = App(FutureAppointmentsReminderHandler::class);

        $eventsFired = $handler->applyActions($matches);

        $this->end(' %s events founds ', $eventsFired);

//
//        $query = $this->matchService
//            ->findMatchesWithoutPlannedAppointments(Match::STATES['ACTIVE'])
//            ->where(function ($query) {
//                $query->where('last_reminder', '<', Carbon::now()->subDays(7))
//                    ->orWhereNull('last_reminder');
//            })
//        ;
//
//
//        $matches = $query->get();
//
//
//        $i = 0;
//
//        /**
//         * @var Collection $matches
//         * @var Match $match
//         * @var User $user
//         * @var Collection $enrollments
//         * @var Enrollment $enrollment
//         */
//
//        // @todo: map to FutureAppointemntReminder hnadler
//        // @todo add platform todo;
//
//        foreach ($matches as $match) {
//            $match->last_reminder = Carbon::now();
//            $match->save();
//            $enrollment = $match->getMentorParticipation()->enrollment;
//            $user = $enrollment->user;
//
//            $user->notify(new MatchNoFutureAppointmentNotification($user->brand, $match, $enrollment));
//            $i++;
//        }

        $this->end('Send %s matches ', $i);

    }
}
