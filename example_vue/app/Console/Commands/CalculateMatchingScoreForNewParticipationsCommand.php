<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Matching\Domain\Models\MatchingScore;
use Modules\Matching\Domain\Services\MatchingService;

class CalculateMatchingScoreForNewParticipationsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'matching:calculate:scores {--reset}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    protected $matchingService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(MatchingService $matchingService)
    {
        parent::__construct();
        $this->matchingService = $matchingService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        if ($this->option('reset')) {
            MatchingScore::query()->delete();
        }

//        $es = Enrollment::all();
//        foreach ($es as $e) {
//            if ($e->transitionAllowed('participate'))
//                $e->transition('participate');
//        }
        $result = null;

        try {
            $result = $this->matchingService->generateMissingCombinations();
        } catch (\Exception $exception){
            echo $exception->getMessage();
            echo $exception->getTraceAsString();
            report($exception);
        }


        $this->info(sprintf('processed %s pairs', $result));
    }
}
