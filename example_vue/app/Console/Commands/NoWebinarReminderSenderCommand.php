<?php

namespace App\Console\Commands;

use App\Notifications\Program\ProgramManagerNoWebinarNotification;
use App\Notifications\Webinar\WebinarChoiceReminderNotification;
use App\Services\MultiTenant\Facades\MultiTenant;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\WebinarService;
use Modules\Core\Reminder\EnrollmentIncompleteReminderHandler;
use Modules\Core\Reminder\WebinarReminderHandler;

class NoWebinarReminderSenderCommand extends AbstractCronjobCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:reminder:nowebinar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    protected $enrollmentService;
    protected $webinarService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(EnrollmentService $enrollmentService, WebinarService $webinarService)
    {
        parent::__construct();
        $this->enrollmentService = $enrollmentService;
        $this->webinarService = $webinarService;
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {

        $this->init();
        /** @var Collection $enrollments */
        $enrollments = $this->enrollmentService->findWithoutFutureTraining();
        /** @var WebinarReminderHandler $handler */
        $handler = App(WebinarReminderHandler::class);
        $eventsFired = $handler->applyActions($enrollments);
        $this->end('[WebinarReminer] Events fired: %s of %s checked entities ', $eventsFired, $enrollments->count());
        return;
    }

    private function informProgramManager(Program $program, User $user)
    {
        if (!$program->manager) {
            return;
        }

        /** @var User $manager */
        $manager = $program->manager;
        $manager->notify(new ProgramManagerNoWebinarNotification($manager->brand, $program, $user));

    }
}
