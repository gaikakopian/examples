<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Services\BrandedDocumentService;
use Modules\Core\Domain\Services\DeeplService;

class TranslateDocumentsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translate:unexisting:es';

    /**
     * The console command description.
     *
     * @var string
     *
     * protected $deeplService;
     * protected $brandedDocumentService;
     *
     * /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DeeplService $deeplService, BrandedDocumentService $brandedDocumentService)
    {
        parent::__construct();
        $this->deeplService = $deeplService;
        $this->brandedDocumentService = $brandedDocumentService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        $documents = BrandedDocument::query()->where('language', 'en')->limit(20)->get();
        $this->info('found ' . $documents->count() . ' documents');

        foreach ($documents as $document) {
            $esdocument = $this->brandedDocumentService->findDocumentInLanguage($document, 'es');
            if ($esdocument) {
                $this->info('found ' . $documents->key . ' skipped; already exists');
                continue;
            }
            $doc = $this->brandedDocumentService->findOrCreateTranslationForDocument('es', $document);
            $this->info('created document ' . $doc->id . ' with key '. $doc->key);

        }

    }
}
