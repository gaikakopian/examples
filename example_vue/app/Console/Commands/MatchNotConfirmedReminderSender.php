<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;
use Modules\Core\Reminder\MatchNotConfirmedReminderHandler;

use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;

class MatchNotConfirmedReminderSender extends AbstractCronjobCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:reminder:match:notconfirmed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $matchService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(MatchService $matchService)
    {
        parent::__construct();
        $this->matchService = $matchService;


    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        //
        $this->init();

        $matches = $this->matchService->listHangingOnState(Match::STATES['UNCONFIRMED'], 24);

        /** @var MatchNotConfirmedReminderHandler $handler */
        $handler = App(MatchNotConfirmedReminderHandler::class);
        $eventsFired = $handler->applyActions($matches);

        $this->end('Reminded %s matches %s eventsFired', $matches->count(),$eventsFired);

    }
}
