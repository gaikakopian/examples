<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\GoogleMapsService;
use Modules\Core\Domain\Services\UserService;

class FindGeoLocationOfMissingUsersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:findgeolocations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    protected $userService;
    protected $googleMapsService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserService $userService, GoogleMapsService $googleMapsService)
    {
        parent::__construct();
        $this->userService = $userService;
        $this->googleMapsService = $googleMapsService;

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::query()->whereNull('lat')->get();
        $users = $users->reverse();

        /** @var User $user */
        foreach ($users as $user) {

            $addr = $user->getAddressString();
            if (!$addr) {
                continue;
            }
            echo 'Looking up ' . $user->id . ' : ' . $user->email . ' : ' . $addr . PHP_EOL;
            $coordinates = $this->googleMapsService->lookUpAddress($addr);
            if ($coordinates == null) {
                //avoid to download it a second time;
                $user->lat = 0;
                $user->lng = 0;
            } else {
                $user->lat = $coordinates->lat;
                $user->lng = $coordinates->lng;
            }
            $user->save();
            sleep(1);
        }
    }
}
