<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\UserService;
use Modules\Matching\Domain\Models\Match;

class DeleteUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:user:forcedelete {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'delete user by id or email';

    protected $userService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserService $userService)
    {
        parent::__construct();
        $this->userService = $userService;

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $id = $this->argument('id');
        /**
         * @var User $user
         * @var Enrollment $enrollment
         * @var Match $match
         * @var Participation $particpation
         */
        $user = $this->userService->get($id);

        Log::warning("Deleting User : $user->email ");
        $this->info(sprintf('found user to delete  %s', $user->email));


        if ($user->isAdmin()) {
            $this->error("cannot delete admin user");
            return;
        }
        // relationships:


        $this->userService->forceDeleteUser($user);

        $this->info(sprintf('User %i was successfully delete', $user->id));


    }
}
