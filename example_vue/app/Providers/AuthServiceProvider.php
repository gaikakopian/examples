<?php

namespace App\Providers;

use App\Services\MultiTenant\MultiTenantEloquentUserProvider;
use Carbon\Carbon;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Webinar;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Comment;
use Modules\Core\Domain\Policies\OrganizationPolicy;
use Modules\Core\Domain\Policies\ProgramPolicy;
use Modules\Core\Domain\Policies\WebinarPolicy;
use Modules\Core\Domain\Policies\UserPolicy;
use Modules\Core\Domain\Policies\CommentPolicy;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Policies\GroupPolicy;
use Modules\Matching\Domain\Policies\MatchPolicy;
use Modules\Scheduling\Domain\Models\Appointment;
use Modules\Scheduling\Domain\Policies\AppointmentPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Appointment::class => AppointmentPolicy::class,
        Group::class => GroupPolicy::class,
        Match::class => MatchPolicy::class,
        Program::class => ProgramPolicy::class,
        Webinar::class => WebinarPolicy::class,
        User::class => UserPolicy::class,
        Comment::class => CommentPolicy::class,
        Organization::class => OrganizationPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::tokensExpireIn(Carbon::now()->addDays(30));


//        Auth::provider('multiTenantEloquentUserProvider', function ($app, $config) {
//            return new MultiTenantEloquentUserProvider(new ApiUserService());
//        });

        //
    }
}
