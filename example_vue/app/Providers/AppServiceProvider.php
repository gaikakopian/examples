<?php

namespace App\Providers;

use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Queue\Events\JobFailed;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\ServiceProvider;
use Laravel\Passport\Http\Controllers\AccessTokenController as PassportAccessTokenController;
use Modules\Core\Http\Controllers\AccessTokenController;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //

        $this->app->bind('\\' . PassportAccessTokenController::class, AccessTokenController::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // The ServiceProvider is loaded as a dev-dependenciy via composer and can therefore only
        // be loaded in development environments, where composer dev-dependencies are installed.
        if ($this->app->environment() === 'local') {
//            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }

        if ($this->app->environment() !== 'local') {

            // log to syslog in production so papertrail receives the mesages
            $monolog = Log::getMonolog();
            $syslog = new \Monolog\Handler\SyslogHandler('vvapi2');
            $formatter = new \Monolog\Formatter\LineFormatter('%channel%.%level_name%: %message% %extra%');
            $syslog->setFormatter($formatter);
            $monolog->pushHandler($syslog);
        }

//  @todo:
//        Queue::failing(function (JobFailed $event) {
//            // $event->connectionName
//            // $event->job
//            // $event->exception
//            report($event->exception);
//        });



    }
}
