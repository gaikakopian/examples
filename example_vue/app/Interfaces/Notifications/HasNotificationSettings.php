<?php

namespace App\Interfaces\Notifications;

interface HasNotificationSettings
{
    /**
     * Get the correct communication channel for a user in a more readable abstract way.
     *
     * For example, instead of a concrete provider like 'twilio' simply return 'sms' and
     * you're good to go. It's also the right place to decide for a single channel if
     * a user accepts all channels. There can only be one channel per entity, e.g.
     * 'sms'. This is a magic Eloquent method which generates a getter
     * `$object->preferred_notification_channel`.
     *
     * @return string|null
     */
    public function getPreferredNotificationChannelAttribute();

    /**
     * This is needed for intersecting available notification channels and accepted channels.
     *
     * This is a magic Eloquent method which generates a getter $object->accepted_channels
     *
     * @return string
     */
    public function getAcceptedChannelsAttribute() : array;

    /**
     * This is needed for finding the best fitting (branded) notifications for the user.
     *
     * This is a magic Eloquent method which generates a getter $object->preferred_language
     *
     * @return string
     */
    public function getPreferredLanguageAttribute() : string;
}
