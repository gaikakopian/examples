<?php

namespace App\Services\Tests;

/**
 * Mock class for testing the EventSubscriberProxy.
 */
class TestHandler
{
    public function handlerMethod($event)
    {
        $event->setName('handledByTestHandler');  // So we can assert the event ends up here
    }
}
