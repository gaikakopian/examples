<?php

namespace App\Services\Tests;

use App\Infrastructure\AbstractTests\LaravelTest;
use App\Infrastructure\Domain\EloquentBreadService;
use App\Infrastructure\Http\Query;
use App\Infrastructure\Http\QueryParams;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Test the functionality of the ContentChooser Service.
 */
class EloquentBreadServiceTest extends LaravelTest
{
    /**
     * A mocked subclass of the abstract EloquentBreadService
     *
     * @var EloquentBreadService
     */
    protected $service;

    /**
     * A mocked Eloquent Model.
     *
     * @var Model
     */
    protected $model;

    /** {@inheritdoc} */
    protected function setUp()
    {
        parent::setUp();

        $this->model = $this->createMock(Model::class);
        $this->service = $this->getMockForAbstractClass(EloquentBreadService::class, [$this->model]);

    }

    /** {@inheritdoc} */
    public function test_it_lists_models()
    {
        // Set up expectations on the model
        // First, a Builder is requested from the model
        $builder = $this->createMock(Builder::class);
        $this->model->expects($this->once())
            ->method('newQuery')
            ->will($this->returnValue($builder));

        // Then, the records are fetched from the builder
        $collection = new Collection(['does', 'not', 'matter', 'whats', 'in', 'here']);
        $builder->expects($this->once())
            ->method('get')
            ->will($this->returnValue($collection));

        // …and we can perform the actual call
        $result = $this->service->list();
        $this->assertEquals($collection, $result);
    }

    /** {@inheritdoc} */
    public function test_it_gets_a_model()
    {
        // Set up expectations on the model
        // First, a Builder is requested from the model
        $builder = $this->createMock(Builder::class);
        $this->model->expects($this->once())
            ->method('newQuery')
            ->will($this->returnValue($builder));

        // Then, the record is fetched from the builder
        $record = $this->createMock(Model::class);
        $builder->expects($this->once())
            ->method('findOrFail')
            ->with(123)
            ->will($this->returnValue($record));

        // …and we can perform the actual call
        $result = $this->service->get(123);
        $this->assertEquals($record, $result);
    }

    /** {@inheritdoc} */
    public function test_it_creates_a_model()
    {
        $attributes = [
            'name' => 'Jack o Lantern',
            'motto' => 'Glow with the flow',
            'moods' => ['easy', 'happy', 'grumpy'],
        ];

        // Set up expectations on the model
        $this->model->expects($this->once())
            ->method('fill')
            ->with($attributes)
            ->will($this->returnSelf());
        $this->model->expects($this->once())
            ->method('fresh')
            ->will($this->returnSelf());

        // Now we perform the actual call
        $result = $this->service->create($attributes);
        $this->assertEquals($this->model, $result);
    }

    /** {@inheritdoc} */
    public function test_it_updates_a_model()
    {
        $attributes = [
            'name' => 'Jack o Lantern',
            'motto' => 'Glow with the flow',
            'moods' => ['easy', 'happy', 'grumpy'],
        ];

        // First, a Builder is requested from the model
        $builder = $this->createMock(Builder::class);
        $this->model->expects($this->once())
            ->method('newQuery')
            ->will($this->returnValue($builder));

        // Then, the record is fetched from the builder
        $record = $this->createMock(Model::class);
        $builder->expects($this->once())
            ->method('findOrFail')
            ->with(123)
            ->will($this->returnValue($record));

        // …and updated
        $record->expects($this->once())
            ->method('update')
            ->with($attributes)
            ->will($this->returnSelf());

        // …and we can perform the actual call
        $result = $this->service->update(123, $attributes);
        $this->assertEquals($this->model, $result);
    }

    /** {@inheritdoc} */
    public function test_it_deletes_a_model()
    {
        // First, a Builder is requested from the model
        $builder = $this->createMock(Builder::class);
        $this->model->expects($this->once())
            ->method('newQuery')
            ->will($this->returnValue($builder));

        // Then, the record is fetched from the builder
        $record = $this->createMock(Model::class);
        $builder->expects($this->once())
            ->method('findOrFail')
            ->with(123)
            ->will($this->returnValue($record));

        // …and deleted
        $record->expects($this->once())
            ->method('delete')
            ->will($this->returnSelf());

        // …and we can perform the actual call
        $result = $this->service->delete(123);
        $this->assertEquals($this->model, $result);
    }

    /**
     * This test will just check if the service accepts custom
     * queries, but not if they were applied.
     * This will be checked in a specific implementation (UserService)
     */
    public function test_it_uses_search_params(){


        $collection = new Collection(['does', 'not', 'matter', 'whats', 'in', 'here']);
        $builder = $this->createMock(Builder::class);
        $builder->method('get')
            ->will($this->returnValue($collection));

        $this->model->expects($this->once())
            ->method('newQuery')
            ->will($this->returnValue($builder));


        $query = new QueryParams();
        $query->setSearch("Hello World");

        $this->service->list($query);


    }
}
