<?php

namespace App\Infrastructure\Traits\Tests;

use App\Infrastructure\AbstractTests\LaravelTest;
use App\Infrastructure\Tests\Mocks\StatableModel;
use Illuminate\Support\Facades\Config;
use SM\StateMachine\StateMachine;

class StatableTest extends LaravelTest
{
    protected $model;

    protected function setUp()
    {
        parent::setUp();

        // Set StateMachine config vars for our fake model
        Config::set('state-machine', [
            'TestState' => [
                'class' => StatableModel::class,
                'graph' => 'default',
                'property_path' => 'state',
                'states' => ['INITIAL', 'CHANGED', 'FINAL'],
                'transitions' => [
                    'change' => [
                        'from' => ['INITIAL'],
                        'to' => 'CHANGED'
                    ],
                    'finalize' => [
                        'from' => ['INITIAL', 'CHANGED'],
                        'to' => 'FINAL'
                    ]
                ]
            ]
        ]);

        $this->model = new StatableModel(['state' => 'INITIAL']);
    }

    public function test_it_creates_a_state_machine_for_a_model()
    {
        $this->assertInstanceOf(StateMachine::class, $this->model->stateMachine());
    }

    public function test_it_retrieves_current_state()
    {
        $this->assertEquals('INITIAL', $this->model->currentState());
    }

    public function test_it_checks_if_a_transition_is_allowed()
    {
        $this->assertTrue($this->model->transitionAllowed('change'));
        $this->assertTrue($this->model->transitionAllowed('finalize'));

        $this->model->state = 'CHANGED';
        $this->assertFalse($this->model->transitionAllowed('change'));
        $this->assertTrue($this->model->transitionAllowed('finalize'));

        $this->model->state = 'FINAL';
        $this->assertFalse($this->model->transitionAllowed('change'));
        $this->assertFalse($this->model->transitionAllowed('finalize'));
    }

    public function test_it_applies_a_transition()
    {
        $this->assertEquals('INITIAL', $this->model->currentState());

        $this->assertTrue($this->model->transition('change'));
        $this->assertEquals('CHANGED', $this->model->currentState());

        $this->assertTrue($this->model->transition('finalize'));
        $this->assertEquals('FINAL', $this->model->currentState());
    }

    public function test_it_knows_whether_it_logs_transitions()
    {
        $this->assertTrue($this->model->logsTransitions());
    }
}
