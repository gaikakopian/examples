<?php

namespace App\Infrastructure\Contracts;

/**
 * Describes a numeric interval.
 */
interface Interval
{
    /**
     * Lower end of the interval.
     * @param $tz string Timezone
     * @return int
     */
    public function intervalStart() : int;

    /**
     * Upper end of the interval.
     * @param $tz string Timezone
     * @return int
     */
    public function intervalEnd() : int;
}
