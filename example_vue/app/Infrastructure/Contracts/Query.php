<?php

namespace App\Infrastructure\Contracts;

/**
 * Describes a more specific query to a Service's `index()` method.
 *
 * Possible query options are:
 * - filters
 * - sorting
 * - sideloads
 * - pagination
 */
interface Query
{
    /**
     * Bruno-compatible representation of the query.
     *
     * Should return an array of the following structure:
     * ```
     * [
     *      'includes' => [],
     *      'sort' => [],
     *      'limit' => null,
     *      'page' => null,
     *      'mode' => 'embed',
     *      'filter_groups' => [],
     *      'search' => ''
     * ]
     * ```
     *
     * @return array
     */
    public function options() : array;

    /**
     * Determine whether a query requests a paginated response.
     *
     * @return bool
     */
    public function isPaginated() : bool;


}
