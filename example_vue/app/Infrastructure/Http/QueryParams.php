<?php

namespace App\Infrastructure\Http;

use App\Infrastructure\Contracts\Query as QueryInterface;
use Illuminate\Http\Request;
use InvalidArgumentException;

/**
 * An interface to pass paramater to pass options to
 * breadservice->get() ... e.g. to enable eagerloading.
 *
 */
class QueryParams implements QueryInterface
{

    private $parsed_options;


    /**
     * QueryParams constructor.
     */
    public function __construct()
    {

        $this->parsed_options = Query::$defaults;
    }

    public function setSearch($search){
        $this->parsed_options['search'] = $search;

    }
    public function setIncludes($includes)
    {
        if (is_string($includes)) {
            $includes = [$includes];
        }
        $this->parsed_options['includes'] = $includes;
    }

    /**
     * Parse GET parameters into resource options.
     *
     * @return array
     */
    public function options(): array
    {
        return $this->parsed_options;
    }

    public function isPaginated(): bool
    {

    }
}
