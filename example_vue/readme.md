# Volunteer Vision API v2

> Replacement of JavaPlay backend

## Table of contents


### General
* [About this documentation](_docs/meta.md)
* …

### Admin Guide

* [Admin Guide](_docs/admin/index.md)
    * [Programs](_docs/admin/programs.md)

### API Documentation
* [API Documentation](_docs/api/index.md)
    * [API Reference](https://documenter.getpostman.com/view/830159/volunteer-vision-api-v2/7LuZeQf)
    * [Authentication](_docs/api/authentication.md)
    * [Query Params (filter, sort, include, paginate)](_docs/api/query-params.md)
    * [Uploading images](_docs/image-attachments.md)

### Developer Documentation

#### Guidelines & How-Tos
* [Setup](_docs/setup.md)
* [Testing](_docs/testing.md)
* [GIT Workflow](_docs/git-workflow.md)
* [Development Setup](_docs/development-setup.md)
* [Style & Conventions](_docs/style-conventions.md)

#### Concepts & Resources
* [Folder Structure](_docs/folder-structure.md)
* [Actions & Controllers](_docs/actions-controller.md)
* [Image Attachments](_docs/image-attachments.md#uploading-images-through-api)
* [Hints & Tricks](_docs/hints-tricks.md)
* [Cheatsheet](_docs/cheatsheet.md)
* [Notes](_docs/notes.md)

### Admin Documentation
* [Operations Manual](_docs/operations.md)
* …



https://github.com/barryvdh/laravel-ide-helper/issues/637




