<?php

/*
|--------------------------------------------------------------------------
| Module routes
|--------------------------------------------------------------------------
|
| Here's the place to register API routes for the current module. These routes
| are loaded by the RouteServiceProvider within a group which is assigned
| the "api" middleware group and a version prefix, eg. `/api/v1`.
|
 */

/**
 * /example
 */
// Route::group(['prefix' => '/example', 'middleware' => ['auth']], function () {
//     Route::get('/', 'Actions\ExampleAction');
// });
