<?php

namespace Modules\ModuleBlueprint\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Modules\ModuleBlueprint\Domain\Services\ExampleService;
use Modules\ModuleBlueprint\Http\Resources\ExampleHttpResource;

class ExampleAction extends Action
{
    protected $exampleService;
    protected $responder;

    public function __construct(ExampleService $exampleService, ResourceResponder $responder)
    {
        $this->exampleService = $exampleService;
        $this->responder = $responder;
    }

    public function __invoke(Request $request)
    {
        $results = $this->exampleService->list();

        return $this->responder->send($me, ExampleHttpResource::class);
    }
}
