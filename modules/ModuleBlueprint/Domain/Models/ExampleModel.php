<?php

namespace Modules\ModuleBlueprint\Domain\Models;

use Illuminate\Database\Eloquent\Model;

class ExampleModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];
}
