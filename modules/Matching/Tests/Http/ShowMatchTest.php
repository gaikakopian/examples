<?php

namespace Modules\Matching\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Matching\Domain\Models\Match;

/**
 * Test the functionality of the `/api/v1/matches/{id}/confirm` endpoint.
 */
class ShowMatchTest extends EndpointTest
{
    public function test_it_confirms_a_match()
    {
        $match = factory(Match::class)->create(
            ['state' => Match::STATES['UNCONFIRMED']]
        );
        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'role' => 'mentor',
        ]);
        $participation = factory(Participation::class)->create([
            'enrollment_id' => $enrollment->id,
            'match_id' => $match->id,
            'match_confirmed_at' => Carbon::now(),
        ]);

        $response = $this->actingAs($this->user)->get("/api/v2/matches/$match->id");

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJsonStructure([
            'data' => [
                'id',
                'completedLection',
                'program',
                'matchedParticipations' => [[]],
            ]
        ]);

        // Make sure the match has been confirmed.
    }
}
