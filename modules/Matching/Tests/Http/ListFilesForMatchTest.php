<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\Role;
use Modules\Matching\Domain\Models\Match;

/**
 * Test the functionality of the `/api/v1/matches/{id}/cancel` endpoint.
 */
class ListFilesForMatchTest extends EndpointTest
{
    public function test_it_lists_files_for_mentees()
    {
        Match::flushEventListeners();
        // Disable model events, as we don't want to call the ConferenceService during this test


        $match = factory(Match::class)->create();

        $testenrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'role' => Role::ROLES['mentee']
        ]);

        $participation = factory(Participation::class)->create([
            'enrollment_id' => $testenrollment->id,
            'match_id' => $match->id,
            'match_confirmed_at' => Carbon::now(),
        ]);


        $response = $this->actingAs($this->user)->getJson("/api/v2/matches/$match->id/files");
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


        // does only show mentee data;
        $response = json_decode($response->getContent());
        $data = $response->data;

        foreach ($data as $row) {
            if ($row->targetAudience != null) {
                $this->assertEquals($row->targetAudience, Role::ROLES['mentee']);
            }
        }

    }

    public function test_it_lists_files_for_mentees_lection()
    {
        Match::flushEventListeners();
        // Disable model events, as we don't want to call the ConferenceService during this test


        $match = factory(Match::class)->create();

        $testenrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'role' => 'mentee',
        ]);

        $participation = factory(Participation::class)->create([
            'enrollment_id' => $testenrollment->id,
            'match_id' => $match->id,
            'match_confirmed_at' => Carbon::now(),
        ]);


        $response = $this->actingAs($this->user)->getJson("/api/v2/matches/$match->id/lections/1/files");
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        // general documetns
        $response = $this->actingAs($this->user)->getJson("/api/v2/matches/$match->id/lections/0/files");
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
    }
}
