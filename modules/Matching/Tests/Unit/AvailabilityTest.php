<?php

namespace Modules\Matching\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;
use ErrorException;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Availability;
use Carbon\Carbon;

class AvailabilityTest extends LaravelTest
{
    public function test_minute_of_week()
    {
        $availability = new Availability();

        $availability->day_of_week = 0;
        $availability->start_minute_of_day = 0;
        $this->assertEquals(0, $availability->getMinuteOfWeek(), 'Beginning of week');

        $availability->day_of_week = 1;
        $availability->start_minute_of_day = 0;
        $this->assertEquals(24 * 60, $availability->getMinuteOfWeek(), 'One day');

        $availability->day_of_week = 1;
        $availability->start_minute_of_day = 18 * 60;
        $this->assertEquals(24 * 60 + 18 * 60, $availability->getMinuteOfWeek(), 'One day - 18:00');

        $availability->day_of_week = 3;
        $availability->start_minute_of_day = 18 * 60;
        $this->assertEquals(3 * 24 * 60 + 18 * 60, $availability->getMinuteOfWeek(), 'One day - 18:00');
    }


    public function test_it_uses_same_tz_correctly()
    {
        $winerTime = Carbon::create(2017, 02, 01, 12);
        Carbon::setTestNow($winerTime);

        // no timezone offset in greenwich;
        $tz1 = 'Etc/Greenwich';

        $dayOfWeek = 1;
        $hourOfDay = 10;
        $correctResult = $dayOfWeek * 1440 + $hourOfDay * 60;

        /** @var Availability $availability */
        $availability = factory(Availability::class)
            ->create(['day_of_week' => 1, 'start_minute_of_day' => 600, 'timezone' => $tz1]);

        $result = $availability->getMinuteOfWeekInTz($tz1);

        // back and forth converted
        $this->assertEquals($correctResult, $result);
    }


    public function test_it_converts_timezone_correctly()
    {
        $winerTime = Carbon::create(2017, 02, 01, 12);
        Carbon::setTestNow($winerTime);

        $tz1 = 'Etc/Greenwich';
        $tz2 = 'Europe/Berlin';
        $realOffset = 60;

        $dayOfWeek = 1;
        $hourOfDay = 10;
        $correctResult = $dayOfWeek * 1440 + $hourOfDay * 60 + $realOffset;

        /** @var Availability $availability */
        $availability = factory(Availability::class)
            ->create([
                'day_of_week' => $dayOfWeek,
                'start_minute_of_day' => $hourOfDay * 60,
                'timezone' => $tz1
            ]);

        $result = $availability->getMinuteOfWeekInTz($tz2);
        $this->assertEquals($correctResult, $result);
    }

    public function test_it_throws_error_on_wrong_tz()
    {
        $this->expectException(ErrorException::class);

        $availability = new Availability();

        $availability->timezone = 'Etc/Greenwich';
        $res = $availability->getMinuteOfWeekInTz('Etcasdsad/Greenwiasdasdsasdasch');
    }

//    public function test_it_computes_correct_interval_start_in_utc()
    public function test_it_computes_correct_interval_start_in_utc_when_it_is_summer_in_germany()
    {
        $tz = 'Etc/Greenwich';

        /** @var  \Modules\Matching\Domain\Models\Availability $availability1 */

        $pastHotDayInGermany = Carbon::create(2017, 06, 15, 12);
        Carbon::setTestNow($pastHotDayInGermany);


        $availability1 = factory(Availability::class)
            ->create([
                'day_of_week' => 2,
                'start_minute_of_day' => 18.5 * 60,
                'duration_minutes' => 120,
                'timezone' => 'Etc/Greenwich'
            ]);

        $this->assertEquals(3990, $availability1->intervalStart());


        $availability2 = factory(Availability::class)
            ->create([
                'day_of_week' => 2,
                'start_minute_of_day' => 18.5 * 60,
                'duration_minutes' => 120,
                'timezone' => 'Europe/Berlin'
            ]);
        $this->assertEquals(3870, $availability2->intervalStart());
    }

    public function test_it_computes_correct_interval_start_in_utcy()
    {
        $pastChristmasInGermany = Carbon::create(2017, 12, 24, 12);
        Carbon::setTestNow($pastChristmasInGermany);

        $tz = 'Etc/Greenwich';

        /** @var Availability $availability1 */
        $availability1 = factory(Availability::class)
            ->create([
                'day_of_week' => 2,
                'start_minute_of_day' => 16.5 * 60,
                'duration_minutes' => 120,
                'timezone' => $tz,
            ]);



        // 2days + 16,5hrs - timezoneoffiset
        $minuteOfWeek = (int)2 * 24 * 60 + 16.5 * 60;

        $this->assertEquals($minuteOfWeek, $availability1->intervalStart());
    }
    public function test_it_computes_correct_interval_start_in_utc_when_it_is_winter_in_germany()
    {
        $pastChristmasInGermany = Carbon::create(2017, 12, 24, 12);
        Carbon::setTestNow($pastChristmasInGermany);

        $tz = 'Europe/Berlin';
        /** @var  \Modules\Matching\Domain\Models\Availability $availability2 */
        $availability2 = factory(Availability::class)->create([
            'day_of_week' => 2,
            'start_minute_of_day' => 16.5 * 60,
            'duration_minutes' => 120,
            'timezone' => $tz
        ]);

        // 2days + 16,5hrs - timezoneoffiset
        $minuteOfWeek = (int)2 * 24 * 60 + 16.5 * 60;
        $this->assertEquals($minuteOfWeek - 60, $availability2->intervalStart());
    }


    public function test_it_computes_correct_interval_end_in_utc_when_it_is_summer_in_germany()
    {
        $tz = 'Etc/Greenwich';

        /** @var  \Modules\Matching\Domain\Models\Availability $availability1 */
        $pastHotDayInGermany = Carbon::create(2017, 06, 15, 12);
        Carbon::setTestNow($pastHotDayInGermany);


        $availability1 = factory(Availability::class)
            ->create([
                'day_of_week' => 2,
                'start_minute_of_day' => 16.5 * 60,
                'duration_minutes' => 120,
                'timezone' => 'Etc/Greenwich'
            ]);

        $minuteOfWeek = $availability1->getMinuteOfWeek() + $availability1->duration_minutes;
        $this->assertEquals($minuteOfWeek, $availability1->intervalEnd(), 'it should not convert tz');


        $expectedOffset = 120;

        /** @var Availability $availability2 */
        $availability2 = factory(Availability::class)
            ->create([
                'day_of_week' => 2,
                'start_minute_of_day' => 18.5 * 60,
                'duration_minutes' => 120,
                'timezone' => 'Europe/Berlin'
            ]);

        $minuteOfWeek = $availability2->getMinuteOfWeek() + $availability2->duration_minutes;
        $this->assertEquals($minuteOfWeek - $expectedOffset, $availability2->intervalEnd(), 'it should convert the tz'); //@todo: should have an offset.
    }

    public function test_it_computes_correct_interval_end_in_utc_when_it_is_winter_in_germany()
    {
        $pastChristmasInGermany = Carbon::create(2017, 12, 24, 12);
        Carbon::setTestNow($pastChristmasInGermany);
        $tz = 'Etc/Greenwich';


        $availability1 = factory(Availability::class)
            ->create([
                'day_of_week' => 2,
                'start_minute_of_day' => 16.5 * 60,
                'duration_minutes' => 120,
                'timezone' => $tz
            ]);
        $this->assertEquals(3990, $availability1->intervalEnd());

        $tz = 'Europe/Berlin';
        /** @var  \Modules\Matching\Domain\Models\Availability $availability2 */
        $availability2 = factory(Availability::class, 10)->create([
            'day_of_week' => 2,
            'start_minute_of_day' => 17.5 * 60,
            'duration_minutes' => 120,
            'timezone' => $tz,
        ])->first();
        $this->assertEquals(3990, $availability2->intervalEnd());
    }
}
