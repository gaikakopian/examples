<?php

namespace Modules\Matching\Tests\Unit\MatchingLogic;

use App\Infrastructure\AbstractTests\EndpointTest;
use App\Infrastructure\AbstractTests\LaravelTest;
use Faker\Factory;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\ProfileField;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Services\MatchingLogic\Suggester;

class PictureEnrollmentFujitsuTest extends EndpointTest
{
    /**
     * @var Suggester
     */
    protected $suggester;

    /**
     * @var Program
     */
    protected $program;


    protected $mentor;
    protected $mentee;


    /**
     * @var Enrollment
     */
    protected $mentorEnrollment;
    protected $menteeEnrollment;


    protected $mentorParticipation;
    protected $menteeParticipation;


    public function setUp()
    {
        parent::setUp();

        $this->suggester = app(Suggester::class);


        $this->program = Program::find(1);
        $this->program->matching_logic = 'PictureEnrollmentLogic';
        $this->program->save();
//

        $this->mentor = User::where('email', 'mentor@volunteer-vision.com')->firstOrFail();
        $this->mentee = User::where('email', 'mentee@volunteer-vision.com')->firstOrFail();

        $this->addProfileFields($this->mentor);
        $this->addProfileFields($this->mentee);


        $this->mentorEnrollment = factory(Enrollment::class)->create([
            'user_id' => $this->mentor->id,
            'program_id' => $this->program->id,
            'state' => Enrollment::STATES['ACTIVE'],
            'role' => 'mentor',
        ]);

        $this->menteeEnrollment = factory(Enrollment::class)->create([
            'user_id' => $this->mentee->id,
            'program_id' => $this->program->id,
            'state' => Enrollment::STATES['ACTIVE'],
            'role' => 'mentee',
        ]);

        $this->mentorParticipation = factory(Participation::class)->create([
            'enrollment_id' => $this->mentorEnrollment->id
        ]);
        $this->menteeParticipation = factory(Participation::class)->create([
            'enrollment_id' => $this->menteeEnrollment->id
        ]);


    }

    private function addProfileFields(User $user)
    {

        $user->profileFields()->delete();


        $answers = [
            ['code' => 'organize_desk', 'value' => '[{"code": "structuring", "answer": "unstructured"}]'],
            ['code' => 'morning_question', 'value' => '[{"code": "punctuality", "answer": "ontime"}]'],
            ['code' => 'morning_form', 'value' => '{"education": "vocational", "currentjob": "other", "profession": "noexperience"}'],
            ['code' => 'mentoring_time', 'value' => '[{"code": "mentoring_type", "answer": "relationship"}, {"code": "preparation", "answer": "prepared"}, {"code": "mentoring_struggle", "answer": "relaxed"}]'],
            ['code' => 'lunch_time', 'value' => '[{"code": "eat", "answer": "alone"}, {"code": "extraversion", "answer": "introvert"}]'],
            ['code' => 'languages', 'value' => '["ar", "en"]'],
            ['code' => 'hobbies', 'value' => '["friends", "reading", "computergames", "culture","sample"]'],
            ['code' => 'assignment', 'value' => '[{"code": "energy_invest", "answer": "first_draft"}, {"code": "stressfulness", "answer": "keep_waiting"}]'],
        ];

        $answers = array_map(function ($a) {
            return new ProfileField($a);
        }, $answers);

        $user->profileFields()->saveMany($answers);
    }

    public function test_it_handles_fujitsu_users_genderInprference_prefernce_correctly()
    {
        $this->mentor->gender = 'male';
        $this->mentor->save();
        (new ProfileField(['user_id' => $this->mentee->id, 'code' => 'organization_questions', 'value' => '{"gender": "female", "levels": "7", "department2": "consulting", "department3": "projectManagement", "department4": "management", "accessibilityRequirements": "no"}']))->save();
        (new ProfileField(['user_id' => $this->mentor->id, 'code' => 'organization_questions', 'value' => '{"levels": "10", "jobFamily": "projectManagement", "accessibilityRequirements": "no"}']))->save();

        $logic = $this->suggester->getLogicForProgram($this->program);

        $matchingScore = $this->suggester->processOne($this->mentorParticipation, $this->menteeParticipation, $logic);
        $this->assertContains('INVALID_GENDER_PREFERENCE', $matchingScore->notes);
        $this->assertNotNull($matchingScore->algorithm_value);
    }


    public function test_it_handles_fujitsu_users_gender_prefernce_correctly()
    {
        $this->mentor->gender = 'female';
        $this->mentor->save();
        (new ProfileField(['user_id' => $this->mentee->id, 'code' => 'organization_questions', 'value' => '{"gender": "female", "levels": "7", "department1": "consulting", "department2": "projectManagement", "department4": "management", "accessibilityRequirements": "no"}']))->save();
        (new ProfileField(['user_id' => $this->mentor->id, 'code' => 'organization_questions', 'value' => '{"levels": "10", "jobFamily": "projectManagement", "accessibilityRequirements": "no"}']))->save();

        $logic = $this->suggester->getLogicForProgram($this->program);

        $matchingScore = $this->suggester->processOne($this->mentorParticipation, $this->menteeParticipation, $logic);
        $this->assertNotContains('INVALID_GENDER_PREFERENCE', $matchingScore->notes);
        $this->assertNotContains('DEPARTMENT1_MATCH', $matchingScore->notes);
        $this->assertNotNull($matchingScore->algorithm_value);
    }

    public function test_it_handles_fujitsu_frf_correctly()
    {
        $this->addProfileFields($this->mentor);
        $this->addProfileFields($this->mentee);

        (new ProfileField(['user_id' => $this->mentee->id, 'code' => 'organization_questions', 'value' => '{"gender": "female", "levels": "13+", "department2": "consulting", "department3": "projectManagement", "department4": "management", "accessibilityRequirements": "no"}']))->save();
        (new ProfileField(['user_id' => $this->mentor->id, 'code' => 'organization_questions', 'value' => '{"levels": "10", "jobFamily": "projectManagement", "accessibilityRequirements": "no"}']))->save();

        $logic = $this->suggester->getLogicForProgram($this->program);

        $matchingScore = $this->suggester->processOne($this->mentorParticipation, $this->menteeParticipation, $logic);
        $this->assertContains('MENTEE_HIGHER_LEVEL', $matchingScore->notes);
        $this->assertContains('DEPARTMENT2_MATCH', $matchingScore->notes);
        $this->assertNotNull($matchingScore->algorithm_value);
    }

    public function test_it_handles_fujitsu_users_correctly()
    {
//        organization_questions
        (new ProfileField(['user_id' => $this->mentee->id, 'code' => 'organization_questions', 'value' => '{"gender": null, "levels": "7", "department2": "consulting", "department3": "projectManagement", "department4": "management", "accessibilityRequirements": "no"}']))->save();
        (new ProfileField(['user_id' => $this->mentor->id, 'code' => 'organization_questions', 'value' => '{"levels": "10", "jobFamily": "projectManagement", "accessibilityRequirements": "no"}']))->save();

        $logic = $this->suggester->getLogicForProgram($this->program);

        $matchingScore = $this->suggester->processOne($this->mentorParticipation, $this->menteeParticipation, $logic);
        // maximum equal sum;
//        $this->assertEquals($matchingScore->algorithm_value, 84, 'The matching score is invalid');
        $this->assertNotNull($matchingScore->algorithm_value);
    }

}
