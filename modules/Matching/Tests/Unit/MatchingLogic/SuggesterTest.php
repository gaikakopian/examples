<?php

namespace Modules\Matching\Tests\Unit\MatchingLogic;

use App\Infrastructure\AbstractTests\EndpointTest;
use App\Infrastructure\AbstractTests\LaravelTest;
use Faker\Factory;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Services\MatchingLogic\Suggester;

class SuggesterTest extends EndpointTest
{
    /**
     * @var Suggester
     */
    protected $suggester;

    /**
     * @var Program
     */
    protected $program;

    /**
     * @var Enrollment
     */
    protected $enrollment;


    public function setUp()
    {
        parent::setUp();

        $this->markTestSkipped('suggester update');
        $this->suggester = app(Suggester::class);


//        $program = Program::find(1);
//        $program->matching_logic = 'DefaultLogic';
//        $program->save();
//
        $this->program = Program::find(2);

        $this->enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user,
            'program_id' => $this->program->id,
            'state' => Enrollment::STATES['ACTIVE'],
            'role' => 'mentor',
        ]);

        $mentorParticipation = factory(Participation::class)->create([
            'enrollment_id' => $this->enrollment->id
        ]);
    }

    public function test_it_suggests_mentee_participations_for_a_mentor()
    {
        $this->markTestSkipped('suggester update');

        $newProgram = factory(Program::class)->create();

        $mentorEnrollment = factory(Enrollment::class)->create([
            'program_id' => $newProgram->id,
            'state' => Enrollment::STATES['ACTIVE'],
            'role' => 'mentor',
        ]);
        $mentorParticipation = factory(Participation::class)->create([
            'enrollment_id' => $mentorEnrollment->id
        ]);

        $allMentees = Participation::whereHas('enrollment', function ($query) use ($newProgram) {
            $query->where('role', 'mentee')
                ->where('program_id', $newProgram->id);
        });

        // For a mentor in Program#1 we should get all mentors in Program#1
        $menteeSuggestions = $this->suggester->suggest($mentorParticipation->id);
        $this->assertEquals($allMentees->pluck('id'), $menteeSuggestions->pluck('id'));
    }

    public function test_it_shows_correct_tz_difference()
    {
        $this->markTestSkipped('suggester update');

        $newProgram = factory(Program::class)->create();

        $this->user->timezone = 'America/New_York';
        $this->user->save();
        $userB = User::where('email', '=', 'mentee@volunteer-vision.com')->firstOrFail();
        $userB->timezone = 'America/Vancouver'; // without summer/witne r time
        $userB->save();


        $mentorEnrollment = factory(Enrollment::class)->create([
            'program_id' => $newProgram->id,
            'state' => Enrollment::STATES['ACTIVE'],
            'role' => 'mentor',
            'user_id' => $this->user->id
        ]);
        $mentorParticipation = factory(Participation::class)->create([
            'enrollment_id' => $mentorEnrollment->id
        ]);

        $menteeEnrollment = factory(Enrollment::class)->create([
            'program_id' => $newProgram->id,
            'state' => Enrollment::STATES['ACTIVE'],
            'role' => 'mentee',
            'user_id' => $userB->id
        ]);
        factory(Participation::class)->create([
            'enrollment_id' => $mentorEnrollment->id
        ]);
        factory(Participation::class)->create([
            'enrollment_id' => $menteeEnrollment->id
        ]);


        // For a mentor in Program#1 we should get all mentors in Program#1
        $menteeSuggestions = $this->suggester->suggest($mentorParticipation->id);
        $this->assertEquals($menteeSuggestions->first()->timezone_difference, 180);
    }

    public function test_it_suggests_mentor_participations_for_a_mentee()
    {

        // find open participataion of mentor
        $this->markTestSkipped('suggester update');

        $program = $this->program;


        $menteeEnrollment = factory(Enrollment::class)->create([
            'program_id' => $program->id,
            'state' => Enrollment::STATES['ACTIVE'],
            'role' => 'mentee',
        ]);
        $menteeParticipation = factory(Participation::class)->create([
            'enrollment_id' => $menteeEnrollment->id
        ]);

        $allMentors = Participation::whereNull('match_id')
            ->whereHas('enrollment', function ($query) use ($program) {
                $query->where('role', 'mentor')
                    ->where('state', '=', Enrollment::STATES['ACTIVE'])
                    ->where('program_id', $program->id);
            });

        // For a mentee in Program#1 we should get all mentees in Program#1
        $mentorSuggestions = $this->suggester->suggest($menteeParticipation->id);

        if ($allMentors->count() === 0) {
            $this->markTestIncomplete('Missing Mentors to match');
        }
        $mentorIds = $allMentors->pluck('id')->sort();
        $menteeIds = $mentorSuggestions->pluck('id')->sort();

        $this->assertEquals($allMentors->count(), $mentorSuggestions->count(),
            ' the amount of mentors should equal the amount of suggested mnetees');

        $mentorIds = $mentorIds->toArray();
        $menteeIds = $menteeIds->toArray();

        sort($mentorIds);
        sort($menteeIds);
        $this->assertEquals($mentorIds, $menteeIds);
    }

    public function test_it_uses_a_custom_matching_logic_if_specified_so_by_the_program()
    {

        // find open participataion of mentor
//        $openEnrollment = Enrollment::join('participations', 'enrollments.id', '=', 'participations.enrollment_id')
//            ->whereNull('participations.match_id')
//            ->where('enrollments.role', '=', Role::ROLES['mentor'])
//            ->firstOrFail();

        $program = $this->program;

        // Create a male mentor that we'll try to find matches for
        $mentor = factory(User::class)->create(['gender' => 'male']);
        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $mentor->id,
            'program_id' => $program->id,
            'role' => 'mentor'
        ]);
        $participation = factory(Participation::class)->create(['enrollment_id' => $enrollment->id]);

        // Now create some users that should match (male) and some that shouldn't (female)
        $users = [
            'male' => factory(User::class, 3)->create(['gender' => 'male']),
            'female' => factory(User::class, 3)->create(['gender' => 'female']),
        ];
        $enrollments = [
            'male' => $users['male']->map(function ($user) use ($program) {
                return factory(Enrollment::class)->create([
                    'user_id' => $user->id,
                    'program_id' => $program->id,
                    'role' => 'mentee',
                    'state' => Enrollment::STATES['ACTIVE']
                ]);
            }),
            'female' => $users['female']->map(function ($user) use ($program) {
                return factory(Enrollment::class)->create([
                    'user_id' => $user->id,
                    'program_id' => $program->id,
                    'role' => 'mentee',
                    'state' => Enrollment::STATES['ACTIVE']
                ]);
            }),
        ];
        $participations = [
            'male' => $enrollments['male']->map(function ($enrollment) {
                return factory(Participation::class)->create(['enrollment_id' => $enrollment->id]);
            }),
            'female' => $enrollments['female']->map(function ($enrollment) {
                return factory(Participation::class)->create(['enrollment_id' => $enrollment->id]);
            }),
        ];

        // For a male mentor in Program#1 we should get all male mentees in Program#1 but no women
        $allMaleMentees = Participation::
        whereNull('match_id')
            ->whereHas('enrollment', function ($query) use ($program) {
                $query->where('role', 'mentee')
                    ->where('program_id', $program->id)
                    ->whereHas('user', function ($query) {
                        $query->where('gender', 'male');
                    });
            });

        // Now set the custom logic before we ask for the suggestions
        $program->matching_logic = 'SameGenderLogic';
        $program->save();

        // Ask for suggestions
        $menteeSuggestions = $this->suggester->suggest($participation->id)->pluck('id');

        $this->assertEquals($allMaleMentees->count(), $menteeSuggestions->count(),
            ' the amount of male mentees should equal the amount of suggested mnetees');
        $this->assertEquals($allMaleMentees->pluck('id'), $menteeSuggestions, 'all male Mentees should get suggested');

        // None of the females should be among the suggestions
        foreach ($participations['female']->pluck('id') as $participationId) {
            $this->assertNotContains($participationId, $menteeSuggestions);
        }
    }
}
