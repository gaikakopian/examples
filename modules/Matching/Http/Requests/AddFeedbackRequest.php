<?php

namespace Modules\Matching\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class AddFeedbackRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules() : array
    {
        return [
            'question_code' => 'required|string',
            'response_scalar' => 'integer',
            'response_text' => 'string'
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function authorize() : bool
    {
        // Authentication is done via the Scope Middleware
        return true;
    }
}
