<?php

namespace Modules\Matching\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Http\Resources\EnrollmentMinimalHttpResource;
use Modules\Core\Http\Resources\EnrollmentHttpResource;
use Modules\Core\Http\Resources\ProgramHttpResource;
use Modules\Scheduling\Http\Resources\AppointmentHttpResource;

class MatchHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'completedLection' => $this->lections_completed ? $this->lections_completed : 0 , //@depreacted
            'lectonsCompleted' => $this->lections_completed ? $this->lections_completed : 0 ,
            'state' => $this->state,
            'lastStateChangeAt' => $this->last_state_change_at ? $this->last_state_change_at->toIso8601String() : null ,
            'program' => new ProgramHttpResource($this->program),
            'matchedParticipations' => MatchedParticipationHttpResource::collection($this->participations),
            'appointments' => AppointmentHttpResource::collection($this->appointments),
            'currentEnrollment' => new EnrollmentMinimalHttpResource($this->currentEnrollment),
            'conferenceRoomUri' => $this->conferenceRoomUri,
            'classroomVersion' => $this->classroom_version
        ];
    }
}
