<?php

namespace Modules\Matching\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Participation;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Matching\Http\Resources\MatchHttpResource;
use App\Exceptions\Client\NotAuthorizedException;
use Modules\Core\Domain\Services\ParticipationService;

class CancelMatchAction extends Action
{
    protected $participationService;
    protected $matchService;
    protected $responder;

    public function __construct(
        ParticipationService $participationService,
        MatchService $matchService,
        ResourceResponder $responder
    )
    {
        $this->participationService = $participationService;
        $this->matchService = $matchService;
        $this->responder = $responder;
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws NotAuthorizedException
     * @throws \App\Exceptions\Client\ClientException
     * @throws \App\Exceptions\Server\ServerException
     */
    public function __invoke($id, Request $request)
    {
        $user = Auth::user();
        /** @var Match $match */
        $match = $this->matchService->get($id);
        $comment = $request->comment;


        /** @var Participation $participation */
        $participation = $this->participationService->getForUserAndMatch($user->id, $match->id);

        if ($user->cannot('cancel', [$match, $participation])) {
            throw new NotAuthorizedException;
        }

        $this->matchService->userCancelsMatch($user, $participation, $match, $comment);

        return response('', 200);
    }
}
