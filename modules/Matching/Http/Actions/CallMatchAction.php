<?php

namespace Modules\Matching\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Services\ConferenceService;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Matching\Http\Resources\MatchHttpResource;
use App\Exceptions\Client\NotAuthorizedException;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Scheduling\Domain\Models\Appointment;

/**
 * Class CallMatchAction
 * @package Modules\Matching\Http\Actions
 */
class CallMatchAction extends Action
{
    protected $matchService;
    protected $conferenceService;


    public function __construct(
        MatchService $matchService,
        ConferenceService $conferenceService
    ) {
        $this->matchService = $matchService;
        $this->conferenceService = $conferenceService;

    }

    /**
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @deprecated
     * @throws \App\Exceptions\Server\ServerException
     */
    public function __invoke(int $id, Request $request)
    {
        $user = Auth::user();

        /** @var Match $match */
        $match = $this->matchService->get($id);

//        /** @var Appointment $appointment */
//        $appointment = $match->getCurrentAppointment();
//        if ($appointment && $appointment->transitionAllowed('start')) {
//            $appointment->transition('start');
//        }

        $response = [];
        $response['url'] = $this->matchService->getClassroomLink($match, $user);

        return response()->json(['data' => $response]);
    }
}
