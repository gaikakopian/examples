<?php

namespace Modules\Matching\Listeners\Match;

use App\Infrastructure\Domain\EventSubscriberProxy;
use Symfony\Component\EventDispatcher\Event;

class MatchStateSubscriber extends EventSubscriberProxy
{
    /** {@inheritDoc} */
    protected function mapEvents(): array
    {
        return [
            'Match.suggest' => 'suggest',
            'Match.confirm' => 'confirm',
            'Match.reject' => 'reject',
            'Match.activate' => 'activate',
            'Match.pause' => 'pause',
            'Match.resume' => 'resume',
            'Match.autoreject' => 'autoreject',
            'Match.request' => 'request',
            'Match.mentee_reject' => 'mentee_reject',
            'Match.mentee_reject_auto' => 'mentee_reject_auto',
            'Match.mentee_approve' => 'mentee_approve',
            'Match.finish' => 'finish',
            'Match.cancel' => 'cancel',
        ];
    }

    /** {@inheritDoc} */
    protected function chooseHandler(Event $event)
    {
        return new DefaultMatchStateHandler();
    }
}
