<?php

namespace Modules\Matching\Domain\Models;

use App\Infrastructure\Contracts\Interval;
use App\Services\TimeHelper;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Domain\Models\User;

/*
Weekdays:
0 = monday
6 = sunday
 */

/***
 * Class Availability
 * Please Note: the only public setters should be :
 *  setDayAndTime()
 *  setMinuteOfWeek()
 * because they convert users data into UTC;
 *
 * @package Modules\Matching\Domain\Models
 */
class Availability extends Model implements Interval
{
    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
    */
    const MINUTES_OF_DAY = 1440;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['owner_id', 'owner_type', 'day_of_week', 'start_minute_of_day', 'duration_minutes', 'timezone'];

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    /**
     * Get all of the owning commentable models.
     */
    public function owner()
    {
        return $this->morphTo();
    }


    /**
     * Express the start of the Availability in minutes since Monday 00:00 UTC+0.
     *
     * @return int
     */
    public function intervalStart(): int
    {
        $timezoneOffsetToUtcInMinutes = TimeHelper::timezoneAsUtcOffsetInMinutes($this->timezone);

        $minutesAfterStartOfMondayInUtc = $this->getMinuteOfWeek() - $timezoneOffsetToUtcInMinutes;

        return $this->normalizeMinutesInWeek($minutesAfterStartOfMondayInUtc);
    }

    /**
     * Express end of the Availability in minutes since Monday 00:00 UTC+0.
     *
     * @return int
     */
    public function intervalEnd(): int
    {
        return $this->normalizeMinutesInWeek($this->intervalStart() + $this->duration_minutes);
    }

    /**
     * Convert negative value to positive and wrap at 10080 (numer of minutes in 7 days).
     *
     * @param int $minutes
     * @return int
     */
    public function normalizeMinutesInWeek(int $minutes)
    {
        $normalized = $minutes % 10080;

        if ($normalized < 0) {
            $normalized = 10080 + $normalized;
        }

        return $normalized;
    }

    public function getMinuteOfWeek(): int
    {
        return ($this->day_of_week * self::MINUTES_OF_DAY) + $this->start_minute_of_day;
    }

    /**
     * user requests to see value in his tz
     * @param $clientTz
     * @return int
     */
    public function getMinuteOfWeekInTz(string $clientTz): int
    {
        // if $clientTz == $this->tz we do not need to convert anything.

        if ($this->timezone === $clientTz) {
            return $this->getMinuteOfWeek();
        }

        $startInUtc = $this->intervalStart(); // convert into UTC;

        $offset = TimeHelper::timezoneAsUtcOffsetInMinutes($clientTz);

        return $this->normalizeMinutesInWeek($startInUtc + $offset);
    }

    public function getInTz(string $clientTz): array
    {
        $minuteOfWeek = $this->getMinuteOfWeekInTz($clientTz);

        return [
            'dayOfWeek'  => (int) floor($minuteOfWeek / self::MINUTES_OF_DAY),
//            'minuteOfWeek'  => $minuteOfWeek,
            'durationMinutes' => $this->duration_minutes,
            'startMinuteOfDay' => $minuteOfWeek % self::MINUTES_OF_DAY,
            'timezone' => $clientTz
        ];
    }
}
