<?php

namespace Modules\Matching\Domain\Events;

use Modules\Matching\Domain\Models\Match;
use Symfony\Component\EventDispatcher\Event;

class MatchCreated extends Event
{
    /**
     * The Match instance that this Event refers to.
     *
     * @var Modules\Matching\Domain\Models\Match
     */
    public $match;

    public function __construct(Match $match)
    {
        $this->match = $match;
    }
}
