<?php

namespace Modules\Matching\Domain\Listeners;

use Modules\Matching\Domain\Events\MatchCreated;
use Modules\Matching\Domain\Jobs\CreateConferenceRoomForMatch;

class CreateConferenceRoomOnMatchCreation
{
    /**
     * Handle the event.
     *
     * @param  Modules\Matching\Domain\Events\MatchCreated  $event
     * @return void
     */
    public function handle(MatchCreated $event)
    {
//        dispatch(new CreateConferenceRoomForMatch($event->match));
    }
}
