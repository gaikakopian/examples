<?php

namespace Modules\Matching\Domain\Services;

use App\Infrastructure\Domain\EloquentBreadService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Domain\Models\Activity;
use Modules\Core\Domain\Models\User;
use Illuminate\Database\Eloquent\Builder;


class MatchingScoreService extends EloquentBreadService
{
    public function filterParticipation(Builder $query, $method, $clauseOperator, $value)
    {
        // if clauseOperator is idential to false,
        //     we are using a specific SQL method in its place (e.g. `in`, `between`)

        if ($clauseOperator === false) {
            $clauseOperator = '=';
        }


        $query->orWhere('mentee_participation_id', $value);
        $query->orWhere('mentor_participation_id', $value);

        return [];
    }

}
