<?php

namespace Modules\Matching\Domain\Services\MatchingLogic;

use Modules\Core\Domain\Models\Participation;
use Modules\Matching\Domain\Models\MatchingScore;

/**
 * A matching logic that only matches people by the levenshtein distance of their names,
 * besides respecting a participation's role to match mentors with mentees.
 *
 * Implemented only for testing and demonstration purposes.
 */
class StringDistanceLogic implements MatchingLogic
{
    /** {@inheritDoc} */
    public function calculateScore(MatchingScore $matchingScore, Participation $reference, Participation $candidate): float
    {
        // Bail if roles are equal (eg. mentor+mentor)
        if ($reference->enrollment->role == $candidate->enrollment->role) {
            return -1;
        }
        if (!$reference->enrollment->user || !$candidate->enrollment->user){
            return -1;
        }

        return levenshtein($reference->enrollment->user->first_name, $candidate->enrollment->user->first_name) / 10;
    }
    public function calculateFinalScore(MatchingScore $matchingScore){

    }
}
