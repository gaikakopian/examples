<?php

namespace Modules\Matching\Domain\Services\MatchingLogic;


use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\ProfileField;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\MatchingScore;

/**
 * Default matching logic that only respects a participation's role to match mentors with mentees.
 */
class PictureEnrollmentLogic implements MatchingLogic
{

    /**
     * @var Participation $mentee
     * @var Participation $mentor
     */
    protected $mentor;
    protected $mentee;


    protected $valuesMentor;
    protected $valuesMentee;


    /** {@inheritDoc} */
    public function calculateScore(MatchingScore $matchingScore, Participation $reference, Participation $candidate): float
    {

        if ($this->isMentee($candidate)) {
            $this->mentee = $candidate;
            $this->mentor = $reference;
        } else {
            $this->mentee = $reference;
            $this->mentor = $candidate;
        }

        $this->valuesMentee = $this->reduceValues($this->mentee);
        $this->valuesMentor = $this->reduceValues($this->mentor);

        $matchingScore->algorithm_value = $this->algorithmValues();


        if ($this->mentor->enrollment->role == $this->mentee->enrollment->role) {
            $matchingScore->notes .= 'SAME_ROLE;';
        }

        if ($this->valuesMentee && $this->valuesMentor
            && isset($this->valuesMentee['profession'])
            && isset($this->valuesmentor['profession'])
            && $this->valuesMentee['profession'] == $this->valuesMentor['profession']) {
            $matchingScore->notes .= 'SAME_PROFESSION;';
        }
        return 0;

    }

    private function isMentee(Participation $candidate)
    {
        return $candidate->enrollment->role == Role::ROLES['mentee'];
    }


    public function calculateFinalScore(MatchingScore $matchingScore)
    {

        $finalValue = $matchingScore->algorithm_value;

        if ($matchingScore->time_overlap > 100) {
            $finalValue += 20;

        }
        if ($matchingScore->time_overlap < 30) {
            $finalValue -= 50;
        }

        /**
         * @var User $mentor
         * @var User $mentee
         */
        $mentor = $this->mentor->enrollment->user;
        $mentee = $this->mentee->enrollment->user;


        if ($mentor->getAge() && $mentee->getAge()
            && $mentor->getAge() < $mentee->getAge() + 2) {
            $finalValue -= 50;
            $matchingScore->notes .= 'MENTORYOUNG;';
        }

        // 3 years mentorAge;
        if ($matchingScore->distance && $matchingScore->distance < 100000) { // not user if distnace is in km?
            $finalValue += 20;
            $matchingScore->notes .= 'NEARBY;';
        }

        if ($mentor->country === $mentee->country && strlen($mentor->country) > 0) {
            $finalValue += 20;
            $matchingScore->notes .= 'SAME_COUNTRY;';
        }

        if ($mentor->country_of_origin === $mentee->country_of_origin && strlen($mentor->country_of_origin) > 0) {
            $finalValue += 20;
            $matchingScore->notes .= 'SAME_COUNTRY_OF_ORIGIN;';
        }

        // Fujitsu exentsions -> should be moved into a separte class

        $finalValue += $this->fujitsuQuestions($matchingScore, $mentor, $mentor);

        $matchingScore->final_value = $finalValue;


    }

    private function fujitsuQuestions(MatchingScore $matchingScore, User $mentor, User $mentee)
    {
        $value = 0;

        if (isset($this->valuesMentee['genderPreference'])) {
            if ($mentor->gender != $this->valuesMentee['genderPreference']) {
                $matchingScore->notes .= 'INVALID_GENDER_PREFERENCE;';
                $value -= 50;
            }
        }
        if (isset($this->valuesMentee['frflevel']) && isset($this->valuesMentor['frflevel'])) {
            if ($this->valuesMentee['frflevel'] >= $this->valuesMentor['frflevel']) {
                $matchingScore->notes .= 'MENTEE_HIGHER_LEVEL;';
                $value -= 100;
            }

        }
        if (isset($this->valuesMentor['jobFamily'])) {
            if (isset($this->valuesMentee['department1']) &&
                $this->valuesMentee['department1'] == $this->valuesMentor['jobFamily']) {
                $matchingScore->notes .= 'DEPARTMENT1_MATCH;';
                $value += 35;
            }
            if (isset($this->valuesMentee['department2']) &&
                $this->valuesMentee['department2'] == $this->valuesMentor['jobFamily']) {
                $matchingScore->notes .= 'DEPARTMENT2_MATCH;';
                $value += 20;
            }
            if (isset($this->valuesMentee['department3']) &&
                $this->valuesMentee['department3'] == $this->valuesMentor['jobFamily']) {
                $matchingScore->notes .= 'DEPARTMENT3_MATCH;';
                $value += 10;
            }
        }


        return $value;

    }

    private function algorithmValues()
    {
        $questionWithScores = [
            'punctuality' => 10,

            'structuring' => 7,

            'energy_invest' => 7.5,
            'stressfulness' => 7.5,

            'eat' => 6,
            'extraversion' => 6,

            'preparation' => 5,
            'mentoring_type' => 5,
            'mentoring_struggle' => 5,
        ];


        $totalScore = 0;

        foreach ($questionWithScores as $q => $score) {
            if (isset($this->valuesMentee[$q]) && isset($this->valuesMentor[$q])
                && $this->valuesMentee[$q] === $this->valuesMentor[$q]) {
                $totalScore += $score;
            }
        }

        if (isset($this->valuesMentee['hobbies']) && isset($this->valuesMentor['hobbies'])) {

            foreach ($this->valuesMentee['hobbies'] as $h1) {
                foreach ($this->valuesMentor['hobbies'] as $h2) {
                    if ($h1 === $h2) $totalScore += 5;
                }
            }
        }
        return $totalScore;


    }

    /**
     *
     * @param Participation $participation
     * @return array
     *  ['hobbies' => ... 'structuring' => 'unstructured']
     */
    private function reduceValues(Participation $participation)
    {

        /** @var User $user */
        $user = $participation->enrollment->user;


        $fields = $user->profileFields;
        Log::info("[PictureEnrollment] Processing participation " . $participation->id);

        $codesAndAnswers = [];
        /** @var ProfileField $field */
        foreach ($fields as $field) {
            // code, valu


            if (is_string($field->value)) {
                $values = json_decode($field->value, True);
            } else {
                $values = $field->value;
            }


            if ($field->code === 'hobbies' || $field->code === 'languages') {
                $codesAndAnswers[$field->code] = $values;
                continue;
            }
            if ($field->code === 'morning_form') {
                $codesAndAnswers = array_merge($codesAndAnswers, $values);
                continue;
            }

            if ($field->code === 'organization_questions') {
                //@todo: do be moved in a subclass;
                $codesAndAnswers = array_merge($codesAndAnswers, $values);
                $codesAndAnswers = $this->reduceFujitsuFunctionality($codesAndAnswers);
                continue;
            }

            // otherwise we expect [ code: .. answer: .. ] as format;
            foreach ($values as $questionRow) {
                $codesAndAnswers [$questionRow['code']] = $questionRow['answer'];
            }


        }

        return $codesAndAnswers;

    }

    private function reduceFujitsuFunctionality($codesAndAnswers)
    {


        if (isset($codesAndAnswers['levels'])) {
            if ($codesAndAnswers['levels'] === '13+') {
                $codesAndAnswers['levels'] = 14;

            }
            $codesAndAnswers['frflevel'] = (int)$codesAndAnswers['levels'];
        }

        if (isset($codesAndAnswers['gender'])) {
            $codesAndAnswers['genderPreference'] = $codesAndAnswers['gender'];
        }
        if (isset($codesAndAnswers['department2']) && !isset($codesAndAnswers['department1'])) {
            if (isset($codesAndAnswers['department2']))
                $codesAndAnswers['department1'] = $codesAndAnswers['department2'];

            if (isset($codesAndAnswers['department3']))
                $codesAndAnswers['department2'] = $codesAndAnswers['department3'];

            if (isset($codesAndAnswers['department4']))
                $codesAndAnswers['department3'] = $codesAndAnswers['department4'];
        }

        return $codesAndAnswers;

    }
}

/*
 *
 *
 *
 * code,value
organize_desk,"[{""code"": ""structuring"", ""answer"": ""unstructured""}]"
morning_question,"[{""code"": ""punctuality"", ""answer"": ""ontime""}]"
morning_form,"{""education"": ""vocational"", ""currentjob"": ""other"", ""profession"": ""noexperience""}"
mentoring_time,"[{""code"": ""mentoring_type"", ""answer"": ""relationship""}, {""code"": ""preparation"", ""answer"": ""prepared""}, {""code"": ""mentoring_struggle"", ""answer"": ""relaxed""}]"
lunch_time,"[{""code"": ""eat"", ""answer"": ""alone""}, {""code"": ""extraversion"", ""answer"": ""introvert""}]"
languages,"[""ar"", ""en""]"
hobbies,"[""friends"", ""reading"", ""computergames"", ""culture""]"
assignment,"[{""code"": ""energy_invest"", ""answer"": ""first_draft""}, {""code"": ""stressfulness"", ""answer"": ""keep_waiting""}]"



        // part 1
        if (havesamevalue("personality_1")) score = score + 7;
        if (havesamevalue("personality_2")) score = score + 6;
        if (havesamevalue("personality_3")) score = score + 6;

        // part 2
        if (havesamevalue("punctuality")) score = score + 10;
        if (havesamevalue("deadline")) score = score + 10;

        // part 3
        if (havesamevalue("personality_goals")) score = score + 7.5;
        if (havesamevalue("personality_goals_energy")) score = score + 7.5;

        // part 4
        if (havesamevalue("mentor_primary")) score = score + 15;

        // part 6
        if (havesamevalue("mentor_type1")) score = score + 5;
        if (havesamevalue("mentor_type2")) score = score + 5;
        if (havesamevalue("mentor_type3")) score = score + 5;

        // part 7
        score = score + (5 * numberofsamematches("leisure"));

        // normalize
        score = score/this.maxscore;

        this.matchingresult.setscore(score);

//        return score;
    }

    public double getbonusscore() {
        double bonusscore = 0.0;

        // mentor must have top german skills (must have 5.0)
        registrationanswer registrationanswermentor = this.getanswer(mentor, "languagelevelenglish");
        registrationanswer registrationanswermentee = this.getanswer(mentee, "languagelevelenglish");

        if (registrationanswermentor != null && registrationanswermentee != null) {
            integer languagelevelmentor = registrationanswermentor.getanswerasinteger();
            integer languagelevelmentee = registrationanswermentee.getanswerasinteger();
            if (languagelevelmentee >= 3 && languagelevelmentor >= 3) {
                bonusscore = 10.0;
            }
        }

        return bonusscore;
    }


 */