<?php

namespace Modules\Matching\Domain\Services\MatchingLogic;

use Modules\Core\Domain\Models\Participation;
use Modules\Matching\Domain\Models\MatchingScore;

/**
 * A matching logic that only matches people of the same gender, besides
 * respecting a participation's role to match mentors with mentees.
 *
 * Implemented only for testing and demonstration purposes.
 */
class SameGenderLogic implements MatchingLogic
{
    /** {@inheritDoc} */
    public function calculateScore(MatchingScore $matchingScore, Participation $reference, Participation $candidate): float
    {
        // Bail if roles are equal (eg. mentor+mentor)
        if ($reference->enrollment->role == $candidate->enrollment->role) {
            return -1;
        }

        // Bail if sex is different
        if ($reference->enrollment->user->gender != $candidate->enrollment->user->gender) {
            return -1;
        }

        // In all other cases, we have a possible match
        return 1;
    }
    public function calculateFinalScore(MatchingScore $matchingScore){

    }


}
