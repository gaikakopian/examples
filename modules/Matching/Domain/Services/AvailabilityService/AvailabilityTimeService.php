<?php

namespace Modules\Matching\Domain\Services\AvailabilityService;

use DateTimeZone;
use Illuminate\Support\Collection;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Interfaces\HasAvailabilityInterface;
use Modules\Matching\Domain\Models\Availability;
use App\Services\ArcIntersection\ArcIntersection;

/*
Weekdays:
0 = monday
6 = sunday
 */

class AvailabilityTimeService
{
    /**
     * The service for calculating intersections
     *
     * @var App\Services\ArcIntersection\ArcIntersection
     */
    protected $arcIntersectionService;

    public function __construct()
    {
        $this->arcIntersectionService = new ArcIntersection(10080);
    }

    /**
     * Filter users that have overlapping availabilities with a user and order them by overlap amount
     *
     * @param Modules\Core\Domain\Models\User $user
     * @param Illuminate\Support\Collection $other_users No eloquent collection because that makes testing easier.
     * @return Illuminate\Support\Collection
     */
    public function findOverlappingAvailbilityUsers(HasAvailabilityInterface $user, Collection $other_users): Collection
    {
        $available_users = new Collection();

        foreach ($other_users as $other_user) {
            $overlap = $this->getOverlappingMinutes($user, $other_user);
            if ($overlap) {
                $other_user->overlapping_minutes = $overlap;
                $available_users->push($other_user);
            }
        }

        $sorted_available_users = $available_users->sortByDesc(function ($user, $key) {
            return $user->overlapping_minutes;
        });

        return $sorted_available_users->values();
    }

    /**
     * @param $timezone1
     * @param $timezone2
     * @return int minutes
     */
    public function getTimezoneDifference($timezone1, $timezone2): int
    {
        /**
         * Sometimes tz is missing.
         */
        if (empty($timezone1) || empty($timezone2)){
            return 0;
        }
        try{
            $origin_dtz = new DateTimeZone($timezone1);
            $remote_dtz = new DateTimeZone($timezone2);
            return ($origin_dtz->getOffset(new \DateTime()) - $remote_dtz->getOffset(new \DateTime())) / 60;
        } catch (\Exception $exception){
            report ($exception);
            return 0;
        }
    }

    /**
     * Get amount of overlapping minutes from all availabilities of two users
     *
     * @param HasAvailabilityInterface $user
     * @param HasAvailabilityInterface $other_user
     * @return int
     */
    public function getOverlappingMinutes(HasAvailabilityInterface $user, HasAvailabilityInterface $other_user): int
    {
        $overlapping_minutes = 0;

        foreach ($user->availabilities as $user_availability) {
            foreach ($other_user->availabilities as $other_user_availability) {
                $overlapping_minutes += $this->arcIntersectionService->computeIntersection($user_availability,
                    $other_user_availability);
            }
        }

        return $overlapping_minutes;
    }

    /**
     *
     * @param HasAvailabilityInterface $user
     * @return int
     */
    public function getMinutesAvailabilitesTotal(HasAvailabilityInterface $user):int
    {
        return $user->availabilities->count() * 30; // could be solved smarter, see method above;
    }
}
