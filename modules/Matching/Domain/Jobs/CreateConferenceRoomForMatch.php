<?php

namespace Modules\Matching\Domain\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Modules\Core\Domain\Services\ConferenceService;
use Modules\Matching\Domain\Models\Match;

/**
 * @unused : this will now be done later (e.g. on appointemnt creation etc.)
 *
 * Class CreateConferenceRoomForMatch
 * @package Modules\Matching\Domain\Jobs
 */
class CreateConferenceRoomForMatch implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The match instance for which to create a room.
     *
     * @var Modules\Matching\Domain\Models\Match
     */
    protected $match;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Match $match)
    {
        $this->match = $match;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ConferenceService $conferenceService)
    {

        // adds the room id to the match internally
        $conferenceService->createRoomForMatch($this->match);
        $this->match->save();
    }
}
