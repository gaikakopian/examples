<?php

namespace Modules\Supervisor\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\QueryParams;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Resources\GroupHttpResource;
use Modules\Supervisor\Http\Resources\UserSupervisorHttpResource;

class ShowUserToSupervisorAction extends Action
{
    protected $userService;
    protected $responder;

    public function __construct(UserService $userService, ResourceResponder $responder)
    {
        $this->userService = $userService;
        $this->responder = $responder;
    }

    public function __invoke($id, Request $request)
    {
        // @todo: check if user is allowed to to so.

        $query = new QueryParams();
        // @todo: how to  includes WHERE type == SUPERVISOR_COMMENT ?..
        $query->setIncludes(['comments']);

        /**
         * @var $me User
         */
        $me = $this->userService->get($id, $query);

        return $this->responder->send($me, UserSupervisorHttpResource::class);
    }
}
