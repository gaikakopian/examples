<?php

namespace Modules\Supervisor\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ActivityService;
use Modules\Core\Http\Resources\EnrollmentHttpResource;
use Modules\Core\Http\Resources\RoleOnlyHttpResource;

class UserSupervisorHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'displayName'=>$this->first_name . ' ' . $this->last_name,
            'firstName' => $this->first_name,
            'lastName' => $this->last_name,
            'email' => $this->email,
            'state' => $this->state,
            'gender' => $this->gender,
            'primaryRole' => $this->primary_role,
            'phoneNumber' => $this->phone_number,
            'phoneNumberPrefix' => $this->phone_number_prefix,
            'whatsappNumber' => $this->whatsapp_number,
            'whatsappNumberPrefix' => $this->whatsapp_number_prefix,
            'address' => $this->address,
            'city' => $this->city,
            'postcode' => $this->postcode,
            'country' => $this->country,
            'countryOfOrigin' => $this->country_of_origin,
            'birthday' => $this->birthday,
            'brand' => $this->brand,
            'roles' => RoleOnlyHttpResource::Collection($this->whenLoaded('roles')),
            'enrollments' => EnrollmentSupervisorHttpResource::Collection($this->whenLoaded('enrollments')),
            'organization' => $this->organization,
            'language' => $this->language,
            'acceptSms' => $this->accept_sms,
            'acceptEmail' => $this->accept_email,
            'acceptPush' => $this->accept_push,
            'preferredChannel' => $this->preferred_channel,
            'avatar' => $this->avatar,
            'points' => $this->points,
            'position' => $this->position,
            'aboutMe' => $this->about_me,
            'pointsNextLevel' => ActivityService::getPointsForNextLevel($this->points),
            'level' => (isset(User::GAMIFICATION_LEVELS[$this->level]) ? User::GAMIFICATION_LEVELS[$this->level] : 'DEFAULT'),
        ];
    }
}
