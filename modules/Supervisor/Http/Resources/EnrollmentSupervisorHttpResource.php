<?php

namespace Modules\Supervisor\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Admin\Http\Resources\UserMinimalHttpResource;
use Modules\Core\Http\Resources\ProgramHttpResource;

class EnrollmentSupervisorHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'program' => new ProgramHttpResource($this->whenLoaded('program')),
            'user' => new UserMinimalHttpResource($this->whenLoaded('user')),
            'participations' => ParticipationSupervisorHttpResource::Collection($this->whenLoaded('participations')),
            'role' => $this->role,
            'state' => $this->state,
            'lastStateChange' => $this->last_state_change
        ];
    }
}
