<?php

namespace Modules\Supervisor\Test\Http;

use App\Infrastructure\AbstractTests\EndpointTest;

/**
 * Test the functionality of the `/api/v1/users/me/groups` endpoint.
 */
class ShowUserToSupervisorTest extends EndpointTest
{
    /** @test */
    public function test_i_can_access_detail_view_of_user()
    {
        $response = $this->actingAs($this->user)->getJson('/api/v2/supervisor/users/1');
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJsonStructure([
            'data' => [
                    'id',
                    'displayName',
                    'email'

            ]
        ]);
    }
}
