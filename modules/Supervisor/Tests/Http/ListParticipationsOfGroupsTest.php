<?php

namespace Modules\Supervisor\Test\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v1/users/me/groups` endpoint.
 */
class ListParticipationsOfGroupsTest extends EndpointTest
{
    /** @test */
    public function test_it_lists_participants_of_group()
    {
        $user = User::where('email', 'supervisor@volunteer-vision.com')->firstOrFail();

        $response = $this->actingAs($user)->getJson('/api/v2/supervisor/groups/1/participations');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


        $response->assertJsonStructure([
            'data' => [
                [
                    'id'
                ]
            ]
        ]);
    }
}
