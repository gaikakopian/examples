<?php

namespace Modules\Supervisor\Test\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v1/users/me/groups` endpoint.
 */
class AddCommentToUserTest extends EndpointTest
{
    /** @test */
    public function test_i_can_add_a_comment_to_a_user()
    {
        $user = User::where('email', 'supervisor@volunteer-vision.com')->firstOrFail();

        $group = $user->groups()->where('role', 'supervisor')->first();
        $otherUserId = $group->users()->first()->id;

        $response = $this->actingAs($user)->postJson(
            '/api/v2/supervisor/users/' . $otherUserId . '/comments',
            [
                'body' => 'Comment'
            ]
        );

        $this->assertEquals(201, $response->getStatusCode(), $response->getContent());


        $response->assertJsonStructure([
            'data' => [
                'id',
                'body',
                'type',
                'pinToTop'
            ]
        ]);
    }
}
