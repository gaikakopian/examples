<?php

namespace Modules\Reporting\Services;

use App\Infrastructure\Domain\EloquentBreadService;
use Carbon\Carbon;
use DateTime;
use Doctrine\DBAL\Query\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

class ReportingService
{
    public function __construct()
    {
    }

    public function getHighlightedFeedbackOfOrganization(Organization $organization)
    {
        return UserFeedback::where('is_highlighted', '=', true)
            ->join('users', 'user_id', '=', 'users.id')
            ->where('users.organization_id', '=', $organization->id)
            ->inRandomOrder()
            ->limit(1)
            ->firstOrFail();
    }

    /**
     * @param Organization $organization
     * @return QueryBuilder
     */
    public function getMatchOfOrganizationQuery(Organization $organization)
    {
        return DB::table('matches')
            ->join('participations', 'matches.id', '=', 'participations.match_id')
            ->join('enrollments', 'enrollments.id', '=', 'participations.enrollment_id')
            ->join('users', 'users.id', '=', 'enrollments.user_id')
            ->where('users.organization_id', $organization->id);
    }

    public function getActivatedLicencesOfOrganizationQuery(Organization $organization): int
    {
        return $this->getMatchOfOrganizationQuery($organization)
            ->whereNotNull('matches.licence_activated')
            ->count();
    }

    public function getUsersOfOrgnanization(Organization $organization)
    {
        // @todo: does this make a select COUNT or select and then count?
        return $organization->users()->count();
    }

    public function getActiveMatchesOfQuery(Organization $organization)
    {
        return $this->getMatchOfOrganizationQuery($organization)
            ->where('matches.state', '=', Match::STATES['ACTIVE'])
            ->count();
    }

    public function getActiveMatchesCount()
    {
        return DB::table('matches')
            ->whereIn('state', Match::ACTIVESTATES)
            ->count();
    }

//    public function getDoneMatchesOfOrganiation(Organization $organization)
//    {
//        return $this->getMatchOfOrganizationQuery($organization)
//            ->where('matches.state', '=', Match::STATES['DONE'])
//            ->count();
//    }
    public function getMatchesWithStateOfOrganiation(Organization $organization, $states)
    {
        if (!is_array($states)) {
            $states = [$states];
        }
        return $this->getMatchOfOrganizationQuery($organization)
            ->whereIn('matches.state', $states)
            ->count();
    }

    public function getMatchableOfOrganization(Organization $organization)
    {
        return DB::table('participations')
            ->join('enrollments', 'enrollments.id', '=', 'participations.enrollment_id')
            ->join('users', 'users.id', '=', 'enrollments.user_id')
            ->where('users.organization_id', $organization->id)
            ->where('enrollments.state', Enrollment::STATES['ACTIVE'])
            ->whereNull('match_id')// @todo: where NULL wird nicht geprüft? And/or fehler?
            ->whereNull('participations.deleted_at')
            ->whereNull('enrollments.deleted_at')
            ->where(function ($query) {
                $query->where('start_matching_after', '>', Carbon::now())
                    ->orWhereNull('start_matching_after');
            })->count();
    }

    public function getMatchableParticipationsCount()
    {
        return DB::table('participations')
            ->whereNull('match_id')
            ->where(function ($query) {
                $query->where('start_matching_after', '>', Carbon::now())
                    ->orWhereNull('start_matching_after');
            })->count();
    }

    public function getAverageRatingOfOrganization(Organization $organization)
    {
        return DB::table('users')
            ->join('user_feedbacks', 'users.id', '=', 'user_id')
            ->where('users.organization_id', '=', $organization->id)
            ->where('question_code', '=', UserFeedback::CODES['LIKE_MENTORSHIP'])
            ->average('response_scalar');
    }


    public function getReachedPeople($startDate = null, $endDate = null)
    {

        $query = DB::table('users')
            ->select(DB::raw('count(DISTINCT users.id) as total'))
            ->join('enrollments', 'users.id', '=', 'enrollments.user_id')
            ->join('participations', 'enrollments.id', '=', 'participations.enrollment_id')
            ->join('matches', 'matches.id', '=', 'participations.match_id')
            ->whereNotNull('licence_activated')
            ->where('users.count_for_reporting', true);


        if ($startDate) {
            $query->where('matches.licence_activated', '>=', $startDate);
        }
        if ($endDate) {
            $query->where('matches.licence_activated', '<=', $endDate);
        }

        return $query->first()->total;

        /*
         * Query in old plattform
         *
SELECT
EXTRACT(year FROM p."created_at") as year ,
COUNT(*) as reachedPerson,
COUNT(*) / 2 as mentorships  FROM users u
JOIN participation p ON par_mentor = usr_id OR par_mentee = usr_id
WHERE
licence_activated IS NOT NULL
AND count_for_reporting = true
GROUP BY year
ORDER BY year ASC

         */
    }


    public function getSuccessfulAppoinmentsByMonth($startDate = null, $endDate = null)
    {

        $query = DB::table('appointments')
            ->select(DB::raw('count(DISTINCT appointments.id) as total'),
                DB::raw('date_trunc(\'month\', planned_start) as monthyear')
            )
            ->groupby('monthyear')
            ->join('matches', 'matches.id', '=', 'appointments.match_id')
            ->join('participations', 'matches.id', '=', 'participations.match_id')
            ->join('enrollments', 'enrollments.id', '=', 'participations.enrollment_id')
            ->join('users', 'users.id', '=', 'enrollments.user_id')
            ->where('appointments.state', '=', Appointment::STATES['SUCCESS'])
            ->where('users.count_for_reporting', true)
            ->whereNotNull('planned_start');

        if ($startDate) {
            $query->where('matches.created_at', '>=', $startDate);
        }
        if ($endDate) {
            $query->where('matches.created_at', '<=', $endDate);
        }
        return $query->get();
    }

    public function getReachedMentorships($startDate = null, $endDate = null)
    {

        $query = DB::table('users')
            ->select(DB::raw('count(*) as total'))
            ->join('enrollments', 'users.id', '=', 'enrollments.user_id')
            ->join('participations', 'enrollments.id', '=', 'participations.enrollment_id')
            ->join('matches', 'matches.id', '=', 'participations.match_id')
            ->whereNotNull('licence_activated')
            ->where('users.count_for_reporting', true);

        if ($startDate) {
            $query->where('matches.created_at', '>=', $startDate);
        }
        if ($endDate) {
            $query->where('matches.created_at', '<=', $endDate);
        }

        return $query->first()->total;
    }

    private function licenceActivatedBaseQuery($startDate = false, $endDate = false, $includeFreeLicence = true)
    {
        $query = DB::table('users')
            ->join('enrollments', 'users.id', '=', 'enrollments.user_id')
            ->join('participations', 'enrollments.id', '=', 'participations.enrollment_id')
            ->join('matches', 'matches.id', '=', 'participations.match_id')
            ->whereNotNull('licence_activated')
            ->where('users.count_for_reporting', true);

        if (!$includeFreeLicence) {
            $query->where('licence_free', false);
        }

        if ($startDate) {
            $query->where('matches.licence_activated', '>=', $startDate);
        }
        if ($endDate) {
            $query->where('matches.licence_activated', '<=', $endDate);
        }
        return $query;
    }

    public function getLicenceActivatedByMonth($includeFreeLicences)
    {
        return $this->getLicenceActivatedByMonthAndProgram($includeFreeLicences, false);
    }

    public function getLicenceActivatedByMonthAndProgram($includeFreeLicences, Program $program = null, Organization $organization = null)
    {

        $query = $this->licenceActivatedBaseQuery(false, false, $includeFreeLicences)
            ->select(DB::raw('date_trunc(\'month\', licence_activated) as month'), DB::raw('count(distinct matches.id) as count'))
//            ->select()
            ->groupby(\DB::raw('month'));
        if ($program) {
            $query->where('enrollments.program_id', $program->id);
        }
        if ($organization) {
            $query->where('users.organization_id', $organization->id);
        }

        return $query->get();

    }

    public function getLicencesActivated($startDate = null, $endDate = null, $includeFreeLicneces = false)
    {
        $query = $this->licenceActivatedBaseQuery($startDate, $endDate, $includeFreeLicneces)
            ->select(DB::raw('count(distinct matches.id) as total'));

        return $query->first()->total;
    }

    public function getMentorshipsWithOneOrMoreSessions($startDate = null, $endDate = null)
    {

        $query = DB::table('users')
            ->select(DB::raw('count(matches.id) as total'))
            ->join('enrollments', 'users.id', '=', 'enrollments.user_id')
            ->join('participations', 'enrollments.id', '=', 'participations.enrollment_id')
            ->join('matches', 'matches.id', '=', 'participations.match_id')
            ->where('lections_completed', '>', 0)
            ->whereNotNull('licence_activated')
            ->where('users.count_for_reporting', true);

        if ($startDate) {
            $query->where('matches.created_at', '>=', $startDate);
        }
        if ($endDate) {
            $query->where('matches.created_at', '<=', $endDate);
        }

        return $query->first()->total;
    }


    public function getRegisteredUsers()
    {
        return DB::table('users')->where('count_for_reporting', true)->count();
    }

    public function getUsersByPrimaryRole($startDate = null, $endDate = null)
    {

        $query = DB::table('users')
            ->select('primary_role as primaryRole', DB::raw('count(*) as total'))
            ->groupBy('primary_role')
            ->where('count_for_reporting', true);

        if ($startDate) {
            $query->where('users.created_at', '>=', $startDate);
        }
        if ($endDate) {
            $query->where('users.created_at', '<=', $endDate);
        }
        return $query->get();


    }

    public function getLicencesBookedOfOrganizations(Organization $organization)
    {
        return DB::table('matches')
            ->select(
                DB::raw('count(DISTINCT matches.id) as total')
            )
            ->join('participations', 'matches.id', '=', 'participations.match_id')
            ->join('enrollments', 'enrollments.id', '=', 'participations.enrollment_id')
            ->join('users', 'users.id', '=', 'enrollments.user_id')
            ->where('users.count_for_reporting', '=', 'true')
            ->where('licence_free', 'false')
            ->whereNotNull('licence_activated')
            ->where('users.organization_id', $organization->id)
            ->first()->total;


    }

    public function getSuccessfulSessionsCount($startDate = null, $endDate = null)
    {

        $query = DB::table('users')
            ->select(
                DB::raw('count(DISTINCT appointments.id) as appointments'),
                DB::raw('count(DISTINCT matches.id) as matchesWithSuccessfulAppointment')
            )
            ->join('enrollments', 'users.id', '=', 'enrollments.user_id')
            ->join('participations', 'enrollments.id', '=', 'participations.enrollment_id')
            ->join('matches', 'matches.id', '=', 'participations.match_id')
            ->join('appointments', 'matches.id', '=', 'appointments.match_id')
            ->where('appointments.state', '=', Appointment::STATES['SUCCESS'])
            ->where('users.count_for_reporting', true);

        if ($startDate) {
            $query->where('appointments.planned_start', '>=', $startDate);
        }
        if ($endDate) {
            $query->where('appointments.planned_start', '<=', $endDate);
        }
        return $query->first();
    }

    public function getUpcomingAppointmentsCount()
    {
        return DB::table('appointments')->where('planned_start', '>', new DateTime())->count();
    }
}
