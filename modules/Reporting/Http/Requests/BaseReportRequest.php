<?php

namespace Modules\Reporting\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

/**
 * This abstract class contains common validation rules for creating
 * and updating users and is not intended to be used directly.
 */
abstract class BaseReportRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'organization' => 'sometimes|integer|exists:organizations,id',
            'startDate' => 'required|sometimes|date',
            'endDate' => 'required|sometimes|date',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function authorize()
    {
        return true;
    }
}
