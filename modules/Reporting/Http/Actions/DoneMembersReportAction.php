<?php

namespace Modules\Reporting\Http\Actions;

use Illuminate\Support\Facades\DB;
use Modules\Matching\Domain\Models\Match;

class DoneMembersReportAction extends BaseReportAction
{
    /** {@inheritDoc} */
    protected function mainQuery()
    {
        return DB::table('users')
            ->join('enrollments', 'users.id', '=', 'enrollments.user_id')
            ->join('participations', 'enrollments.id', '=', 'participations.enrollment_id')
            ->join('matches', 'participations.match_id', '=', 'matches.id')
            ->join('match_state_logs', 'matches.id', '=', 'match_state_logs.match_id')
            ->where('match_state_logs.to', Match::STATES['DONE'])
            ->selectRaw('COUNT(DISTINCT users.id) AS data');
    }

    /** {@inheritDoc} */
    protected function scopeOrganization(int $organizationId)
    {
        $this->query->where('users.organization_id', '=', $organizationId);
    }

    /** {@inheritDoc} */
    protected function scopeStartDate(string $startDate)
    {
        $this->query->where('users.created_at', '>=', $startDate);
    }

    /** {@inheritDoc} */
    protected function scopeEndDate(string $endDate)
    {
        $this->query->where('users.created_at', '<=', $endDate);
    }

    protected function report()
    {
        $data = $this->query->get();

        return $data[0]->data;
    }
}
