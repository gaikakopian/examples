<?php

namespace Modules\Reporting\Http\Actions;

use Modules\Core\Domain\Models\User;
use App\Infrastructure\Http\PlainHttpResource;

class SatisfactionReportAction extends BaseReportAction
{
    /**
     * {@inheritDoc}
     */
    protected function mainQuery()
    {
        return User::join('user_feedbacks', 'users.id', '=', 'user_feedbacks.user_id')
            ->whereNotNull('user_feedbacks.response_scalar')
            ->whereIn('user_feedbacks.question_code', ['LIKE_SESSION', 'MENTORSHIP_HAPPINESS', 'LIKE_MENTORSHIP'])
            ->selectRaw('DISTINCT on users.id');
    }

    /**
     * {@inheritDoc}
     */
    protected function scopeOrganization(int $organizationId)
    {
        $this->query->where('users.organization_id', '=', $organizationId);
    }

    /**
     * {@inheritDoc}
     */
    protected function scopeStartDate(string $startDate)
    {
        $this->query->where('user_feedbacks.created_at', '>=', $startDate);
    }

    /**
     * {@inheritDoc}
     */
    protected function scopeEndDate(string $endDate)
    {
        $this->query->where('user_feedbacks.created_at', '<=', $endDate);
    }

    /**
     * {@inheritDoc}
     */
    protected function report()
    {
        $votes = $this->query->count();
        $rating = $this->query->avg('user_feedbacks.response_scalar');

        $data = [
            'votes' => $votes,
            'rating' => round($rating, 1),
        ];

        return $this->responder->send($data, PlainHttpResource::class);
    }
}
