<?php

namespace Modules\Reporting\Http\Actions;

use App\Exceptions\Client\NotAuthorizedException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Http\Resources\ProgramHttpResource;

class ShowProgramsToCSRAction extends Action
{
    protected $programService;
    protected $responder;

    public function __construct(ProgramService $programService, ResourceResponder $responder)
    {
        $this->programService = $programService;
        $this->responder = $responder;
    }

    public function __invoke(Request $request)
    {
        $user = Auth::user();

        $programs = $user->organization->programs; //@todo: maybe show also ones which are not avialble;


        return $this->responder->send($programs, ProgramHttpResource::class);
    }
}
