<?php

namespace Modules\Reporting\Http\Actions;

use Modules\Core\Domain\Models\OrganizationBooking;
use App\Infrastructure\Http\PlainHttpResource;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class LicencesReportAction extends BaseReportAction
{
    protected $organizationId;

    /** {@inheritDoc} */
    protected function mainQuery()
    {
        return (new OrganizationBooking())->newQuery();
    }

    /** {@inheritDoc} */
    protected function scopeOrganization(int $organizationId)
    {
        $this->organizationId = $organizationId;
        $this->query->where('organization_id', '=', $this->organizationId);
    }

    /** {@inheritDoc} */
    protected function report()
    {
        $all_licences = $this->query->sum('licences');
        $activated_licences = $this->getActivatedLicenses();
        $ready_members = $this->getReadyMembersCount();

        $data = [
            'total' => $all_licences,
            'activated' => $activated_licences,
            'ready' => $ready_members,
            'open' => $all_licences - $activated_licences,
        ];

        return $this->responder->send($data, PlainHttpResource::class);
    }

    private function getMatchesQuery()
    {
        return DB::table('matches')->join(
                'participations',
                'matches.id',
                '=',
                'participations.match_id'
            )
            ->join(
                'enrollments',
                'participations.enrollment_id',
                '=',
                'enrollments.id'
            )
            ->join(
                'users',
                'enrollments.user_id',
                '=',
                'users.id'
            )
            ->where('users.organization_id', '=', $this->organizationId);
    }

    private function getActivatedLicenses()
    {
        return $this->getMatchesQuery()
            ->where('matches.licence_activated', '<', Carbon::now())
            ->where('matches.licence_free', '=', false)
            ->selectRaw('COUNT(DISTINCT matches.id) AS data')
            ->get()[0]->data;
    }

    private function getReadyMembersCount()
    {
        return $this->getMatchesQuery()
            ->whereNull('matches.licence_activated')
            ->where('matches.licence_free', '=', false)
            ->selectRaw('COUNT(DISTINCT matches.id) AS data')
            ->get()[0]->data;
    }
}
