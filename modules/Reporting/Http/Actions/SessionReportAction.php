<?php

namespace Modules\Reporting\Http\Actions;

use Carbon\Carbon;
use Illuminate\Database\Query\Expression;
use Illuminate\Support\Facades\DB;
use Modules\Reporting\Http\Requests\GenericReportRequest;
use Illuminate\Database\Query\Builder;

abstract class SessionReportAction extends BaseReportAction
{
    /**
     * {@inheritDoc}
     */
    protected function mainQuery()
    {
        $query = DB::table('appointments');
        $this->sessionSelect($query);
        return $query;
    }

    /**
     * {@inheritDoc}
     */
    protected function scopeStartDate(string $startDate)
    {
        $this->query->where('actual_start', '>=', $startDate);
    }

    /**
     * {@inheritDoc}
     */
    protected function scopeEndDate(string $endDate)
    {
        $this->query->where($this->getEndDate(), '<=', $endDate);
    }

    /**
     * Select the desired datapoint from the appointments table.
     *
     * @param Builder $query
     * @return void
     */
    abstract protected function sessionSelect(Builder $query);

    /**
     * {@inheritDoc}
     */
    protected function scopeOrganization(int $organizationId)
    {
        $this->query->join(
            'participations',
            'appointments.match_id',
            '=',
            'participations.match_id'
        )
        ->join(
            'enrollments',
            'participations.enrollment_id',
            '=',
            'enrollments.id'
        )
        ->join(
            'users',
            'enrollments.user_id',
            '=',
            'users.id'
        )
        ->where('users.organization_id', '=', $organizationId);
    }

    /**
     * Get the end date of the appointment: Add the duration to the the actual start.
     *
     * @return Expression
     */
    private function getEndDate() : Expression
    {
        return DB::raw('actual_start + (actual_duration_minutes::text || \' minute\')::interval');
    }
}
