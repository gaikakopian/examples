<?php

namespace Modules\Admin\Domain\Models;

use App\Domain\Models\BaseModel;
use App\Infrastructure\Traits\Statable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\User;

class OrganizationInteraction extends BaseModel
{

    /*
    |--------------------------------------------------------------------------
    | Constants
    |--------------------------------------------------------------------------
    */


    protected $fillable = ['preparation', 'organization_id', 'coordinator_id', 'documentation', 'type', 'interaction_at', 'state','satisfaction_estimate'];

    protected $casts = [
        'interaction_at' => 'datetime'


    ];


    /***
     * @return mixed
     */
    public function coordinator()
    {
        return $this->belongsTo(User::class);
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }


}