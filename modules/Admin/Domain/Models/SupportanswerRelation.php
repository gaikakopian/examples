<?php

namespace Modules\Admin\Domain\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Distribution\Domain\Contracts\Distributable;
use Modules\Distribution\Domain\Traits\HasDistributionLogic;

class SupportanswerRelation extends Model implements Distributable
{
    use SoftDeletes;
    use HasDistributionLogic;


    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['answer', 'class',
        'supportanswer_id',
        'links_to',
        'distribute_to_tenants',
        'distributor_source_id'];

    /**
     * The attributes that should be hidden.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];


    /**
     * An Comment belongs to a User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function answer()
    {
        return $this->belongsTo(Supportanswer::class);
    }

    /**
     * An Comment belongs to a User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function link()
    {
        return $this->belongsTo(Supportanswer::class, 'links_to');
    }


}
