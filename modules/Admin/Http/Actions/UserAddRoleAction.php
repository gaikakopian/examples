<?php

namespace Modules\Admin\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Http\Requests\AddUserToGroupRequest;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Http\Resources\ProgramHttpResource;

class UserAddRoleAction extends Action
{
    /**
     * @var GroupService $groupService
     */
    protected $groupService;


    public function __construct()
    {
    }

    public function __invoke(int $id, int $role_id)
    {
        // @todo: check if user is allowed to add this role

        $user = User::findOrFail($id);
        $role = Role::findOrFail($role_id);

        $user->roles()->attach($role);

        return response('ok', 200);
    }
}
