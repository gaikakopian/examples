<?php

namespace Modules\Admin\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Resources\ProgramHttpResource;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Reporting\Services\ReportingService;
use Illuminate\Http\Request;

class ShowFunnelKpisAction extends Action
{
    protected $organizationService;
    protected $reportingService;


    public function __construct(OrganizationService $organizationService, ReportingService $reportingService)
    {
        $this->reportingService = $reportingService;
        $this->organizationService = $organizationService;
    }

    public function __invoke(Request $request)
    {

//        $startDate = $request->startDate ? new Carbon($request->startDate) : null;
//        $endDate = $request->endDate ? new Carbon($request->endDate) : null;
//
//        $data = [
//            'reachedPeople' => $this->reportingService->getReachedPeople($startDate, $endDate),
//            'registeredUsersByRole' => $this->reportingService->getUsersByPrimaryRole(),
//            'newRegistrationsByRole' => $this->reportingService->getUsersByPrimaryRole($startDate, $endDate),
//
//            'successfulSessionsCount' => $this->reportingService->getSuccessfulSessionsCount($startDate, $endDate),
//            'activatedLicences' => $this->reportingService->getLicencesActivated($startDate, $endDate),
//            'mentoringsWithOneSession' => $this->reportingService->getMentorshipsWithOneOrMoreSessions($startDate, $endDate),
//            'appointmentsPerMonth' => $this->reportingService->getSuccessfulAppoinmentsByMonth($startDate, $endDate)
//
//        ];

        return response()->json($data);
    }
}
