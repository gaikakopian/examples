<?php

namespace Modules\Admin\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Resources\ProgramHttpResource;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Reporting\Services\ReportingService;
use Illuminate\Http\Request;

class ShowLicenceKpisAction extends Action
{
    protected $organizationService;
    protected $reportingService;
    protected $programService;


    public function __construct(OrganizationService $organizationService, ReportingService $reportingService, MatchService $matchService, ProgramService $programService)
    {
        $this->reportingService = $reportingService;
        $this->organizationService = $organizationService;
        $this->programService = $programService;
        $this->matchService = $matchService;

    }

    public function __invoke($program, Request $request)
    {

        // @todo: optional filter by company?
        if ($program !== 'all') {
            $program = $this->programService->get($program);
        } else {
            $program = null;
        }

        $organization = $request->organization ? $this->organizationService->get($request->organization) : null;


        $data = [
            'result' => $this->reportingService->getLicenceActivatedByMonthAndProgram(false, $program, $organization)
        ];
        return response()->json($data);
    }
}
