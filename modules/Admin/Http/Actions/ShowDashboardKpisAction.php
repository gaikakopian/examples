<?php

namespace Modules\Admin\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use App\Services\WaboboxService;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Domain\Services\CoordinatorTodoService;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Resources\ProgramHttpResource;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Reporting\Services\ReportingService;

class ShowDashboardKpisAction extends Action
{
    protected $organizationService;
    protected $reportingService;
    protected  $coordinatorTodoService;
    protected  $waboxService;



    public function __construct(OrganizationService $organizationService, ReportingService $reportingService, CoordinatorTodoService $coordinatorTodoService, WaboboxService $waboboxService)
    {
        $this->reportingService = $reportingService;
        $this->organizationService = $organizationService;
        $this->coordinatorTodoService = $coordinatorTodoService;
        $this->waboxService = $waboboxService;


    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\Server\ServerException
     */
    public function __invoke()
    {
        $user = Auth::user();
        $data = [
            'userCount' => $this->reportingService->getRegisteredUsers(),
            'upcomingAppointmentsCount' => $this->reportingService->getUpcomingAppointmentsCount(),
            'matchableParticipations' => $this->reportingService->getMatchableParticipationsCount(),
            'activeMatches' => $this->reportingService->getActiveMatchesCount(),
            'openTodos' => $this->coordinatorTodoService->getOpenTodosForUserCount($user),
            'closedThisWeek' => $this->coordinatorTodoService->getThisWeekCompletedTodos($user),
            'overdueTodos' => $this->coordinatorTodoService->getOverdueTodosAssignedTo($user),
            'whatsappStatus'=> $this->waboxService->getWhatsappDeviceStatus()
        ];

        return response()->json($data);
    }
}
