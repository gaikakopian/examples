<?php

namespace Modules\Admin\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Http\Resources\ProgramHttpResource;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;

/**
 * @todo: add test;
 *
 * Class ActivateMatchLicenceAction
 * @package Modules\Admin\Http\Actions
 */
class ActivateMatchLicenceAction extends Action
{
    /**
     * @var MatchService $matchService
     */
    protected $matchService;


    public function __construct(MatchService $matchService)
    {
        $this->matchService = $matchService;
    }

    /**
     * Unmatching a match...
     *  1. match state changes;
     *  2. create new enrollment for both parties.
     *
     */
    public function __invoke(int $id)
    {

        /**
         * @var $match Match
         */
        $match = $this->matchService->get($id);

        if ($match->state === Match::STATES['UNCONFIRMED'] || $match->state === Match::STATES['CONFIRMED']){
            $match->state = Match::STATES['ACTIVE'];
        }

        $match->activateLicence('MANUAL');

        $match->save();

        return response('ok', 200);
    }
}
