<?php

namespace Modules\Admin\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Http\Requests\AddUserToGroupRequest;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Http\Resources\ProgramHttpResource;

class UserAddGroupAction extends Action
{
    /**
     * @var GroupService $groupService
     */
    protected $groupService;


    public function __construct(GroupService $groupService)
    {
        $this->groupService = $groupService;
    }

    public function __invoke(int $id, int $group_id, AddUserToGroupRequest $request)
    {
        $user = User::findOrFail($id);
        $group = Group::findOrFail($group_id);
        $role = $request->input('role');


        $this->groupService->attachUserToGroup($user, $group, $role);

        return response('ok', 200);
    }
}
