<?php

namespace Modules\Admin\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Resources\ProgramHttpResource;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Reporting\Services\ReportingService;

class ShowKpisOfOrganizationAction extends Action
{
    protected $organizationService;
    protected $reportingService;


    public function __construct(OrganizationService $organizationService, ReportingService $reportingService)
    {
        $this->reportingService = $reportingService;
        $this->organizationService = $organizationService;
    }

    public function __invoke(int $id)
    {

        /** @var Organization $organization */
        $organization = $this->organizationService->get($id);

        $data = [
            'userCount' => $this->reportingService->getUsersOfOrgnanization($organization),
            'matchablePaticipations' => $this->reportingService->getMatchableOfOrganization($organization),
            'activeMentorships' => $this->reportingService->getMatchesWithStateOfOrganiation($organization, Match::STATES['ACTIVE']),
//                $this->reportingService->getActivatedLicencesOfOrganizationQuery($organization),
            'doneMentorships' =>  $this->reportingService->getMatchesWithStateOfOrganiation($organization, Match::STATES['DONE']),
            'averageRating' => $this->reportingService->getAverageRatingOfOrganization($organization),
            'licencesUsed' => $this->reportingService->getLicencesBookedOfOrganizations($organization)
//            'licencesBook' => rand(4, 5) * 10 // @todo: Maybe resuse Jalils Reporting Service;
        ];

        return response()->json($data);
    }
}
