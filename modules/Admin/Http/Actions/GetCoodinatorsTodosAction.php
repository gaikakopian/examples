<?php

namespace Modules\Admin\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Domain\Models\CoordinatorTodo;
use Modules\Admin\Http\Resources\CoordinatorTodoHttpResource;
use Modules\Admin\Http\Resources\ParticipationAdminHttpResource;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Domain\Services\WebinarService;
use Modules\Core\Http\Resources\ParticipationHttpResource;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchingLogic\Suggester;
use Modules\Matching\Domain\Services\MatchService;

/**
 * @todo: this endpoint has no test yet.
 *
 * Class ListMatchSuggestionsAdminAction
 * @package Modules\Admin\Http\Actions
 */
class GetCoodinatorsTodosAction extends Action
{
    protected $suggester;
    protected $responder;


    /**
     * @var Collection
     */
    protected $todos;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var MatchService
     */
    protected $matchService;


    protected $enrollmentService;


    public function __construct(UserService $userService, MatchService $matchService, EnrollmentService $enrollmentService, ResourceResponder $responder)
    {


        $this->responder = $responder;
        $this->userService = $userService;
        $this->enrollmentService = $enrollmentService;
        $this->matchService = $matchService;


    }

    public function __invoke(Request $request)
    {
        $this->todos = new Collection();
        $user = Auth::user();
//        $this->findUsersOnRegisterState();
//        $this->findNoWebinarScheduledEnrollment();
//        $this->findNotMatchedUsers();
//        $this->findMatchesWithoutFirstAppointment();


        $this->todos->sortBy('actionDate');

        /**
         *
         * Sie ist denke ich der Auffassung, dass das über SQL nicht geht, aber wenn Simon uns dabei hilft einen Code zu erstellen der uns zeigt welche Mentoren
         *  -- Registrierung nicht abgeschlossen?
         *  -- Kein Webinar Termin?
         * -- Webinar abgeschlossen und nicht gematched
         *--  Nicht Match confirmed
         *---- noch nicht gestartet sind (0/5) und noch keinen 1. Termin eingetragen haben,
         * noch nicht gestartet sind (0/5), 1. Termin eingetragen (individuelle Betreuung!) ?? Warum ??
         * das Programm neu gestartet, bzw. die erste Session nun abgeschlossen haben, ???? WArum ???
         *  --- keinen weiteren Termin eingetragen haben und der letzte Termin 5 (?) Tage zurückliegt.
         *
         * bei welcher Sessionanzahl sie stehen,
         * welche User Kommentare wir dazugeschrieben haben
         *
         * Wer 3 Sterne oder schlechter als Feedback gegeben hat
         * Wer hat kein Feedback gegeben und 3 Sessions (nachhalten!)
         *
         * und welche das Programm nun abgeschlossen haben,
         */
        // @todo sort;

        return $this->responder->send($this->todos, CoordinatorTodoHttpResource::class);

    }

//
//
//    private function findNoWebinarScheduledEnrollment()
//    {
//
//        // @otdo: to check;
//        $enrollments = $this->enrollmentService->findWithoutFutureTraining();
//        foreach ($enrollments as $enrollment) {
//            $this->addTodo($enrollment->user, CoordinatorTodo::REASONS['NO_FUTURE_WEBINAR'], $enrollment->last_state_change_at, ['enrollment_id' => $enrollment->id]);
//        }
//    }
//
//    private function findNotMatchedUsers()
//    {
//
//        $enrollments = $this->enrollmentService->findWithoutMatchFor7Days();
//        foreach ($enrollments as $enrollment) {
//            $this->addTodo($enrollment->user, CoordinatorTodo::REASONS['WITHOUT_MATCH'], $enrollment->last_state_change_at, ['enrollment_id' => $enrollment->id]);
//        }
//    }
//
//
//    /**
//     *
//     */
//    private function findMatchesWithoutFirstAppointment()
//    {
//        /**
//         * @var Match $match ;
//         */
//        $matches = $this->matchService->findMatchesWithoutPlannedAppointments(Match::STATES['CONFIRMED']);
//        foreach ($matches as $match) {
//            $user = $match->getMentorParticipation()->enrollment->user;
//            if ($user) {
//                $this->addTodo($user, CoordinatorTodo::REASONS['NO_FIRST_APPOINTMENT'], $match->last_state_change_at, ['match_id' => $match->id]);
//            }
//
//        }
//
//        $matches = $this->matchService->findMatchesWithoutPlannedAppointments(Match::STATES['ACTIVE'], 24 * 3);
//        foreach ($matches as $match) {
//            $user = $match->getMentorParticipation()->enrollment->user;
//            if ($user) {
//                $this->addTodo($user, CoordinatorTodo::REASONS['NO_FUTURE_APPOINTMENT'], $match->last_state_change_at, ['match_id' => $match->id]);
//            }
//
//
//        }
//
//    }
//
//    private function findAllUsersWithReminderDate()
//    {
//        $users = $this->userService->findUsersWithPendingReminderDate();
//
//        foreach ($users as $user) {
//            $this->addTodo($user, CoordinatorTodo::REASONS['REMINDER'], $user->remind_again_at, ['text' => 'remind_comment']);
//        }
//
//
//    }
//
//
//    /**** ---------------------------------------------
//     *  Exlusion reasons
//     */
//
//    /**
//     * @param User $user
//     * @param $reason
//     * @param $actionDate
//     */
//    private function addTodo(User $user, $reason, $lastUserAction, $meta = []) // @todo: last user seen...
//    {
//
//        if ($reason != CoordinatorTodo::REASONS['REMINDER']) {
//
//            $threeDaysAgo = Carbon::now()->subDays(5); // at least 5 days between user gets again on reminder list;
//
//            if ($user->last_manual_interaction_at && $user->last_manual_interaction_at > $threeDaysAgo) {
//                return;
//            }
//        }
//
//
//        // We skip all users having an activation within xx days;
//        $actionDate = $this->getReactionTime($lastUserAction, $reason);
//        $todo = new CoordinatorTodo();
//        $todo->targetUser = $user;
//        $todo->lastUserAction = new Carbon($lastUserAction);
//        $todo->actionDate = $actionDate;
//        $todo->reason = $reason;
//        $todo->meta = $meta;
//
//        $this->todos->add($todo);
//
//    }
//
//    private function getReactionTime($actionDate, $reason)
//    {
//        $actionDate = new Carbon($actionDate);
//
//        /**
//         * in Hours
//         */
//        $reactionTime = 0;
//
//        switch ($reason) {
//            case CoordinatorTodo::REASONS['NO_APPOINTMENT_PLANNED']:
//            case CoordinatorTodo::REASONS['NO_FIRST_APPOINTMENT']:
//                $reactionTime = 24 * 3;
//                break;
//
//
//        }
//
//        return $actionDate->addHours($reactionTime);
//
//    }
}
