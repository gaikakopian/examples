<?php

namespace Modules\Admin\Http\Actions;

use App\Exceptions\Client\ValidationException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Webinar;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Domain\Services\WebinarService;
use Modules\Core\Http\Resources\ProgramHttpResource;

/**
 * @todo add test;
 *
 * Class AddEnrollmentToWebinarAction
 * @package Modules\Admin\Http\Actions
 */
class AddEnrollmentToWebinarAction extends Action
{
    /**
     * @var OrganizationService $organizationService
     */
    protected $webinarService;


    protected $enrollmentService;

    public function __construct(WebinarService $webinarService, EnrollmentService $enrollmentService)
    {
        $this->webinarService = $webinarService;
        $this->enrollmentService = $enrollmentService;
    }

    public function __invoke(int $id, int $webinar_id)
    {

        /** @var Webinar $webinar */
        $webinar = $this->webinarService->get($webinar_id);
        /** @var Enrollment $enrollment */
        $enrollment = $this->enrollmentService->get($id);

        $existsalready = $webinar->enrollments()->where('enrollment_id','=', $enrollment->id)->exists();

        if ($existsalready) {
            throw new ValidationException('This user is already enrolled to this webinar');
        }

        $webinar->enrollments()->attach($enrollment);

        return response('ok', 200);
    }
}
