<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Carbon\Carbon;
use Modules\Core\Http\Resources\EnrollmentHttpResource;

class UserleadResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {

        //@todo: no idea why we need carbon here?
        $startsAt = $this->starts_at ? (new Carbon($this->starts_at))->toIso8601String() : null;
        return [
            'id' => $this->id,
            'firstName' => $this->first_name,
            'lastName' => $this->last_name,
            'email' => $this->email,
            'state' => $this->state,
            'source' => $this->source,
            'code' => $this->code,
            'language' => $this->language,
            'lastReminder' => $this->last_reminder ? $this->last_reminder->toIso8601String() : $this->last_reminder,
            'lastStateChangeAt' => $this->last_state_change_at ? $this->last_state_change_at->toIso8601String() : $this->last_state_change_at,
            'registrationCodeId' => $this->registration_code_id,
            'updatedAt' => $this->updated_at->toIso8601String(),
            'createdAt' => $this->created_at->toIso8601String(),
            'registrationCode' => new RegistrationCodeHttpResource($this->whenLoaded('registrationCode'))
        ];
    }
}

/*
 * {{$greetings}}

Nach unseren Angaben war heute einer deiner Mentoringtermine geplant. Leider konnten wir nicht sehen, ob alles geklappt hat und wollten uns deshalb erkundigen ob wir etwas für dich tun können?

Bitte wähle hier kurz aus, wie der Termin verlaufen ist:

Wir haben uns nicht getroffen, aber das war so abgesprochen
Wir haben uns ohne Plattform getroffen (Skype etc.)
Wir hatten technische Probleme

Falls wir dir diese E-Mail nicht mehr schicken sollen, klick bitte hier.

Danke





 */