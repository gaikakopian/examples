<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Http\Resources\StateLogHttpResource;

class OrganizationHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type,
            'logo' => $this->logo,
            'code' => $this->code,
            'keyaccountId' => $this->keyaccount_id,
            'keyaccount' => new UserMinimalHttpResource($this->whenLoaded('keyaccount')),
            'servicelevel' => $this->servicelevel,
            'state' => $this->state,
            'lastStateChangeAt' => $this->last_state_change_at,
            'programs' => ProgramHttpResource::collection($this->whenLoaded('programs')),
            'videoProvider' => $this->video_provider,

            'privateWebinars'=> $this->private_webinars,
            'technicalWebinarCheck'=> $this->technical_webinar_check,
            'technicalGuestWifiCheck'=> $this->technical_guest_wifi_check,
            'technicalDeviceCheck'=> $this->technical_device_check,
            'technicalCompanyNetworkCheck'=> $this->technical_company_network_check,
            'technicalUsageComment' => $this->technical_usage_comment,
            'history' => StateLogHttpResource::Collection($this->whenLoaded('history')),

            //
            'lastCustomerInteraction' => $this->last_customer_interaction,
            'classification' => $this->classification,
            'dpaStatus' => $this->dpa_status,
            'contractStatus' => $this->contract_status,
            'licenceLogic' => $this->licence_logic,
            'contractExpiration' => $this->contract_expiration,
            'satisfactionLevel' => $this->satisfaction_level,

            'whatsappEnabled' => $this->whatsapp_enabled,
            'communityEnabled'  => $this->community_enabled,



            'churnPredictor' => $this->churn_predictor,
            'jfInterval' => $this->jf_interval,
            'jfLastAt' => $this->jf_last_at ? $this->jf_last_at->toIso8601String() : $this->jf_last_at,
            'jfPlannedAt' => $this->planned_jf_at ? $this->planned_jf_at->toIso8601String() : null

        ];
    }
}
