<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class BrandHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'logo' => $this->logo,
            'primaryColour' => $this->primary_colour,
            'frontendUrl' => $this->frontend_url,
            'emailSignature' => $this->email_signature,
            'tenantId' => $this->tenant_id,


        ];
    }
}
