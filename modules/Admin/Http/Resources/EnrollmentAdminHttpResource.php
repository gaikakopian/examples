<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Http\Resources\StateLogHttpResource;

class EnrollmentAdminHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'role' => $this->role,
            'state' => $this->state,
            'lastStateChangeAt' => $this->last_state_change_at ? $this->last_state_change_at->toIso8601String() : $this->last_state_change_at,
            'lastReminderType' => $this->last_reminder_type,
            'user' => new UserMinimalHttpResource($this->whenLoaded('user')),
            'participations' =>  ParticipationAdminHttpResource::collection($this->whenLoaded('participations')),
            'history' => StateLogHttpResource::Collection($this->whenLoaded('history')),
            'program' => new ProgramHttpResource($this->whenLoaded('program')),
            'updatedAt' => $this->updated_at ? $this->updated_at->toIso8601String() : $this->updated_at,
            'createdAt' => $this->created_at ? $this->created_at->toIso8601String() : $this->created_at,
        ];
    }
}
