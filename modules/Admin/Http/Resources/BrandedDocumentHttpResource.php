<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class BrandedDocumentHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'key' => $this->key,
            'type' => $this->type,
            'subject' => $this->subject,
            'content' => $this->content,
            'markdown' => $this->markdown,
            'language' => $this->language,
            'audience' => $this->audience,
            'reviewedAt' => $this->reviewed_at ? $this->reviewed_at->toIso8601String() : $this->reviewed_at,
            'brand_id' => $this->brand_id,
            'brandId' => $this->brand_id,
            'reviewer' => new UserMinimalHttpResource($this->whenLoaded('reviewer')),
            'internalTitle' => $this->internal_title,
            'programId' => $this->program_id,
            'program' => new ProgramHttpResource($this->whenLoaded('program')),
            'distributeToTenants' =>  $this->distribute_to_tenants,
            'distributorSourceId' =>  $this->distributor_source_id
        ];
    }
}
