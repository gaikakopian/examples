<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Domain\Models\Activity;

class SupportanswerHttpResource extends HttpResource
{
    /**
     * Unset the default "data" wrapper, so that we can return
     * a value without casting it to an array.
     *
     * @var string
     */
    public static $wrap = '';

    /**
     * Transform the resource.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'question' => $this->question,
            'description' => $this->description,
            'descriptionMarkdown' => $this->description_markdown,
            'solution' => $this->solution,
            'likes' => $this->likes,
            'dislikes' => $this->dislikes,
            'views' => $this->views,
            'type' => $this->type,
            'answers' => SupportanswerRelationHttpResource::collection($this->whenLoaded('relations')),
        ];
    }
}
