<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class ProgramHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'displayName' => $this->title,
            'title' => $this->title,
            'description' => $this->description,
            'logo' => $this->logo,
            'colorCode' => $this->color_code,
            'managerId' => $this->manager_id,
            'lectionsTotal' => $this->lections_total,
            'cover' => $this->cover,
            'detailPhoto' => $this->detail_photo,
            'matchingAlgorithm' => $this->matching_algorithm,
            'hasMentorWebinar' => $this->has_mentor_webinar,
            'hasMenteeWebinar' => $this->has_mentee_webinar,
            'previewRoomId' => $this->demo_room_id,
            'conferenceEntryUrl' => $this->conference_entry_url,
            'cooperatingOrganization' => $this->cooperating_organization,
            'customLinks' => $this->custom_links,

            'enrollmentType' => ($this->pivot && $this->pivot->enrollment_type ? $this->pivot->enrollment_type : null),
        ];
    }
}
