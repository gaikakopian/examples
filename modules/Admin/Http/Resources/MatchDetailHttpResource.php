<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Http\Resources\StateLogHttpResource;
use Modules\Matching\Http\Resources\MatchedParticipationHttpResource;

class MatchDetailHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'lectionsCompleted' => $this->lections_completed,
            'licenceFree' => $this->licence_free,
            'licenceRescindedAt' => $this->licence_rescinded_at ? $this->licence_rescinded_at->toIso8601String() : $this->licence_rescinded_at,
            'licenceActivated' => $this->licence_activated ? $this->licence_activated->toIso8601String() : $this->licence_activated,
            'licenceReason' => $this->licence_reason,
            'lastStateChangeAt' => $this->last_state_change_at ? $this->last_state_change_at->toIso8601String() : $this->last_state_change_at,
            'lastReminder' => $this->last_reminder ? $this->last_reminder->toIso8601String() : $this->last_reminder,

            'userComment' => $this->user_comment,
            'mentorComment' => $this->mentor_comment,

            'state' => $this->state,
            'hash' => $this->hash,
            'externalRoomId' => $this->external_room_id,
            'lections_completed' => $this->lections_completed,//old
            'distributeToTenants' => $this->distribute_to_tenants,
            'distributorSourceId' => $this->distributor_source_id,
            'program' => new ProgramHttpResource($this->whenLoaded('program')),
            'matchedParticipations' => MatchedParticipationHttpResource::collection($this->whenLoaded('participations')),
            'history' => StateLogHttpResource::Collection($this->whenLoaded('history')),
            'appointments' => AppointmentHttpResource::collection($this->whenLoaded('appointments')),
            'conferenceRoomUri' => $this->conferenceRoomUri,
            'classroomVersion' => $this->classroom_version,

            'createdAt' => $this->created_at->toIso8601String(),
            'updatedAt' => $this->updated_at->toIso8601String(),


        ];
    }
}
