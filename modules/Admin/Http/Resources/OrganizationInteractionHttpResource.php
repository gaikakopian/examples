<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Http\Resources\EnrollmentHttpResource;
use Modules\Core\Http\Resources\StateLogHttpResource;

class OrganizationInteractionHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'preparation' => $this->preparation,
            'documentation' => $this->documentation,
            'type' => $this->type,
            'state' => $this->state,
            'satisfactionEstimate' => $this->satisfaction_estimate,

            'interactionAt' => $this->interaction_at ? $this->interaction_at->toIso8601String() : $this->interaction_at,
//            'customer' => new UserAdminHttpResource($this->whenLoaded('customer')),
            'coordinator' => new UserMinimalHttpResource($this->whenLoaded('coordinator')),
//            'creator' => new UserMinimalHttpResource($this->whenLoaded('creator')),
        ];
    }
}
