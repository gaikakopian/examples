<?php

//@todo Joseph+ Simon, figure out a better solution


/**
 * @param $name (has to be CamelCase with UCfirst)
 * @param $plural (has to be locercase (url)
 */
$addBreadRoute = function ($name, $plural, $subRoutes = []) {
    Route::group(['prefix' => '/' . $plural], function () use ($name, $subRoutes) {
        Route::get('/', 'Controllers\\' . $name . 'Controller@index');
        Route::post('/', 'Controllers\\' . $name . 'Controller@store');
        Route::get('/{id}', 'Controllers\\' . $name . 'Controller@show');
        Route::put('/{id}', 'Controllers\\' . $name . 'Controller@update');
        Route::delete('/{id}', 'Controllers\\' . $name . 'Controller@destroy');

        foreach ($subRoutes as $route) {
            $method = $route['method'];
            $path = $route['path'];
            $action = $route['action'];

            Route::{$method}($path, 'Controllers\\' . $name . 'Controller@' . $action);
        }
    });
};


Route::get('/admin/brandeddocuments/{id}/preview', 'Actions\PreviewBrandedDocumentAction')->name('admin.previewbrandeddocument');



/**
 * Admin Scope
 */
Route::group(['prefix' => '/admin', 'middleware' => ['auth', 'scope:admin']], function () use ($addBreadRoute) {
    Route::post('/brandeddocuments/{id}/review', 'Actions\ReviewBrandedDocumentAction');
    Route::get('/brandeddocuments/{id}/log', 'Actions\ShowLogOfBrandedDocumentAction');

    $addBreadRoute('User', 'users', [
        ['method' => 'POST', 'path' => '/{id}/impersonate', 'action' => 'impersonate']
    ]);
    /**
     * Users (/admin/users)
     */
    // @todo: umbaen zu
    // Route::resource('users', 'Controllers\\UsersController');

    // @todo; NO Idea why this is invalid?

    Route::put('/files/{id}/cover', 'Actions\UploadFileCoverAction')->where('id', '[0-9]+');
    Route::post('/files/{id}/cover', 'Actions\UploadFileCoverAction')->where('id', '[0-9]+');

    Route::post('/files/{id}/upload', 'Actions\UploadFileAction');

    Route::get('/todos', 'Actions\GetCoodinatorsTodosAction');


    $addBreadRoute('User', 'users');
    $addBreadRoute('CoordinatorTodo', 'coordinatortodos');
    $addBreadRoute('Organization', 'organizations');
    $addBreadRoute('Appointment', 'appointments');
    $addBreadRoute('Translation', 'translations');
    $addBreadRoute('Enrollment', 'enrollments');
    $addBreadRoute('Brand', 'brands');
    $addBreadRoute('Tenant', 'tenants');
    $addBreadRoute('Group', 'groups');
    $addBreadRoute('Smslog', 'smslogs');
    $addBreadRoute('Participation', 'participations');
    $addBreadRoute('Program', 'programs');
    $addBreadRoute('Webinar', 'webinars');
    $addBreadRoute('BrandedDocument', 'brandeddocuments');
    $addBreadRoute('Supportanswer', 'supportanswers');
    $addBreadRoute('UserFeedback', 'userfeedbacks');
    $addBreadRoute('Userlead', 'userleads');

    $addBreadRoute('RegistrationCode', 'registrationcodes');
    $addBreadRoute('Notification', 'notifications');
    $addBreadRoute('File', 'files');
    $addBreadRoute('Match', 'matches');
    $addBreadRoute('OrganizationInteraction', 'organizationinteractions');


    Route::delete('users/{id}', 'Controllers\\UserController@unregister');


    Route::post('/enrollments/{id}/enrollWebinar/{webinar_id}', 'Actions\AddEnrollmentToWebinarAction');

    //@todo: requires super admin scope;
    Route::group(['prefix' => '/admin', 'middleware' => ['auth', 'scope:superadmin']], function () use ($addBreadRoute) {
        $addBreadRoute('Tenant', 'Tenants');
    });

    Route::get('/options', 'Actions\ShowOptionsAction');
    Route::get('/organizations/{id}/kpis', 'Actions\ShowKpisOfOrganizationAction');
    Route::post('/reporting/investorkpis', 'Actions\ShowInvestorsKpisAction');
    Route::get('/reporting/programs/{id}/kpis', 'Actions\ShowProgramKpisAction');
    Route::get('/reporting/programs/{id}/licences', 'Actions\ShowLicenceKpisAction');



    Route::get('/dashboardkpis', 'Actions\ShowDashboardKpisAction');
    Route::post('/organizations/{id}/programs/{progrm_id}', 'Actions\AddProgramToOrganizationAction');
    Route::delete('/organizations/{id}/programs/{progrm_id}', 'Actions\ProgramRemoveOrganizationAction');
    Route::put('/organizations/{id}/programs/{progrm_id}', 'Actions\ProgramUpdateOrganizationAction');


    Route::delete('/users/{id}/groups/{group_id}', 'Actions\UserRemoveGroupAction');
    Route::post('/users/{id}/groups/{group_id}', 'Actions\UserAddGroupAction');
    Route::post('/users/{id}/resetpassword', 'Actions\ResetPasswordOfUserAction');
    Route::post('/users/{id}/applyRegistrationCode/{codeid}', 'Actions\ApplyRegistrationCodeToUserAction')->name("admin.user.applyRegistrationCode");
    Route::post('/users/{id}/sendwhatsapp', 'Actions\SendWhatsappMessageAction');
    Route::post('/users/{id}/quit', 'Actions\SetUserToQuitAction');
    Route::post('/users/{id}/noreply', 'Actions\SetUserToNoReplyAction');

    Route::delete('/users/{id}/roles/{role_id}', 'Actions\UserRemoveRoleAction');
    Route::post('/users/{id}/roles/{role_id}', 'Actions\UserAddRoleAction');

    Route::post('/matches/{id}/cancel', 'Actions\CancelMatchAction');
    Route::post('/matches/{id}/activateLicence', 'Actions\ActivateMatchLicenceAction');
    Route::post('/programs/{id}/createpreviewroom', 'Actions\CreatePreviewRoomForProgramAction');

    Route::post('/smslogs/{id}/review', 'Actions\ReviewSmsLogAction');

    Route::post('/supportanswers/{id}/relations', 'Controllers\SupportanswerController@linkSupportAnswer');
    Route::delete('/supportanswers/{id}/relations/{linkid}', 'Controllers\SupportanswerController@unlinkSupportAnswer');
//    Route::get('/matches/{id}/previewroomlink', 'Actions\ShowRoomLinkAction');
    Route::get('/conference/{type}/{id}/previewroomlink', 'Actions\ShowRoomLinkAction');

    Route::get('/roles', 'Actions\ListRolesAction');
    Route::get('/me/2faqr', 'Actions\Show2FAImageAction');


    Route::post('/participations/{id}/match', 'Actions\MatchParticipationAction')->name('admin.match.participation');
    Route::get('/participations/{id}/match-suggestions', 'Actions\ListMatchSuggestionsAdminAction');

});
