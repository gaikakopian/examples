<?php

namespace Modules\Admin\Http\Controllers;

use App\Exceptions\Client\NotAuthorizedException;
use App\Infrastructure\Http\BreadServiceController;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Http\Requests\ImpersonateUserRequest;
use Modules\Admin\Http\Requests\UserRequest;
use Modules\Admin\Http\Resources\UserAdminHttpResource;
use Modules\Admin\Http\Resources\UserMapHttpResource;
use Modules\Admin\Http\Resources\UserMinimalHttpResource;
use Modules\Admin\Http\Resources\UserResource;
use Modules\Core\Domain\Models\SecurityIncident;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\SecurityIncidentService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Requests\FileUploadRequest;

class UserController extends BreadServiceController
{
    /** {@inheritdoc} */
    protected function getResourceClass(string $view): string
    {
        if ($view === 'list') {
            return UserMinimalHttpResource::class;
        }
        if ($view === 'map') {
            return UserMapHttpResource::class;
        }
        return UserAdminHttpResource::class;
    }

    /** {@inheritdoc} */
    protected function getStoreRequestClass(): string
    {
        return UserRequest::class;
    }

    /** {@inheritdoc} */
    protected function getUpdateRequestClass(): string
    {
        return UserRequest::class;
    }

    public function unregister($id)
    {
        /** @var UserService $service */
        $service = $this->service;
        $user = $service->get($id);;
        $res = $service->removeUser($user);
        return $this->makeResource($res);
    }


    /**
     * Upload an user avatar image.
     *
     * @param int $id
     * @param Modules\Core\Http\Requests\FileUploadRequest $request
     * @return Modules\Admin\Http\Resources\UserResource
     */
    protected function uploadAvatar(int $id, FileUploadRequest $request): UserResource
    {
        $user = $this->service->get($id);
        $user->updateImageAttachment('avatar', $request->fileContent(), $request->fileType());

        return $this->makeResource($user);
    }

    /**
     * Remove an user avatar image.
     *
     * @param int $id
     * @return Modules\Admin\Http\Resources\UserResource
     */
    protected function deleteAvatar(int $id)
    {
        $user = $this->service->get($id);
        $user->removeImageAttachment('avatar');

        return $this->makeResource($user);
    }

    /**
     * Retreive an access token for another user (admin only).
     *
     * @param  ImpersonateUserRequest $request
     * @return Response
     */
    public function impersonate($userId, ImpersonateUserRequest $request)
    {


        /** @var SecurityIncidentService $incidentService */
        $incidentService = app(SecurityIncidentService::class);
        $incidentService->saveSecurityIncident(
            SecurityIncident::TYPES['IMPERSONATE'],
            ['target_user' => $userId],
            Auth::id()
        );

        /** @var User $user */
        $user = User::findOrFail($userId);

        if ($user->isAdmin() || $user->isCoordinator()) {
            throw new NotAuthorizedException("It is not possible to impersonate Admin or Coordinators.");
        }
        
        $token = $user->createToken('Admin Impersonate Token');

        return [
            'access_token' => $token->accessToken,
            'token_type' => 'Bearer'
        ];
    }
}
