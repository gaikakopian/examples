<?php

namespace Modules\Admin\Http\Controllers;

use App\Infrastructure\Http\BreadServiceController;
use Modules\Admin\Http\Requests\BrandRequest;
use Modules\Admin\Http\Resources\BrandHttpResource;

class TenantController extends BreadServiceController
{
    /** {@inheritdoc} */
    protected function getResourceClass(string $view): string
    {
        return BrandHttpResource::class;
    }

    /** {@inheritdoc} */
    protected function getStoreRequestClass() : string
    {
        return BrandRequest::class;
    }

    /** {@inheritdoc} */
    protected function getUpdateRequestClass() : string
    {
        return BrandRequest::class;
    }
}
