<?php

namespace Modules\Admin\Http\Controllers;

use App\Infrastructure\Http\BreadServiceController;
use Modules\Admin\Http\Requests\MatchRequest;
use Modules\Admin\Http\Resources\MatchAdminListHttpResource;
use Modules\Admin\Http\Resources\MatchDetailHttpResource;
use Modules\Matching\Http\Resources\MatchHttpResource;

class MatchController extends BreadServiceController
{
    /** {@inheritdoc} */
    protected function getResourceClass(string $view): string
    {
//        return MatchAdminListResource::class;
        return MatchDetailHttpResource::class;
    }

    /** {@inheritdoc} */
    protected function getStoreRequestClass() : string
    {
        return MatchRequest::class;
    }

    /** {@inheritdoc} */
    protected function getUpdateRequestClass() : string
    {
        return MatchRequest::class;
    }
}
