<?php

namespace Modules\Admin\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class GroupRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'name'=>'string|required',
            'organization_id'=>'required',
            'description' => 'string|max:500',
        ];
    }

    /** {@inheritDoc} */
    public function authorize()
    {
        return true;
    }
}
