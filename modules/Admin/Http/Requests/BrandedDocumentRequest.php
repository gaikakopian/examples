<?php

namespace Modules\Admin\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class BrandedDocumentRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'internal_title' => 'required',
            'type' => 'required',
            'key' => 'required',
            'language' => 'required',
            'content' => 'required'
        ];
    }

    /** {@inheritDoc} */
    public function authorize()
    {
        return true;
    }
}
