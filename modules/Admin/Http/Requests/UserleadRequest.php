<?php

namespace Modules\Admin\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class UserleadRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'first_name' =>  'required|string|min:2',
            'last_name' =>  'required|string|min:2',
            'email' => 'required|email|unique:user_leads',
            'state' => 'string',
            'source' => 'required|string|min:2',
            'language' => 'required|string|min:2',
            'registration_code_id' => 'required|exists:registration_codes,id',

        ];
    }

    /** {@inheritDoc} */
    public function authorize()
    {
        return true;
    }
}
