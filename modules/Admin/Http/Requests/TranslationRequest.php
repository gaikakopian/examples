<?php

namespace Modules\Admin\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class TranslationRequest extends DeserializedFormRequest
{
    protected function deserialize(array $input): array
    {
        if (!empty($input['json_value']) && is_array($input['json_value'])) {
            $input['json_value'] = json_encode($input['json_value']);
        }
        return parent::deserialize($input);
    }

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'tenant_id' => 'integer',
            'locale' => 'string|min:2|max:2',
            'scope' => 'string|min:1',
            'json_value' => 'json'
        ];
    }

    /** {@inheritDoc} */
    public function authorize()
    {
        return true;
    }
}
