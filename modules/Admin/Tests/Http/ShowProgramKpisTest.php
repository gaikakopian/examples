<?php

namespace Modules\Admin\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Support\Carbon;
use Modules\Core\Domain\Models\User;

class ShowProgramKpisTest extends EndpointTest
{
    public function test_it_shows_the_kpis_with_different_dates()
    {

//        $startDate = Carbon::now()->subMonth(3);
//        $endDate = Carbon::now();
//
//        $options = [
//            'body' => [
//                'startDate' => $startDate->toIso8601String(),
//                'endDate' => $endDate->toIso8601String()
//            ]
//        ];

        $response = $this->actingAs($this->admin)
            ->json('GET', "/api/v2/admin/reporting/programs/1/kpis");

        $this->assertEquals($response->getStatusCode(), 200, $response->getContent());

        $response->assertJsonStructure([
            'enrollmentStates',
            'matchStates',
            'matchableParticipations'
        ]);
    }
}
