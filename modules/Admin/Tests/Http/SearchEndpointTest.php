<?php

namespace Modules\Admin\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Faker\Factory;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v2/admin/users` endpoints.
 */
class SearchEndpointTest extends EndpointTest
{
    public function test_it_finds_user_containing_string()
    {
        $uniqueString = 'asdalhffkfkshfhirohhrwefhwelfhewlflkfc';

        $searchUser = factory(User::class)->create(['email' => $uniqueString.'@test.com']);

        // List all Users via API
        $response = $this->actingAs($this->admin)
            ->get('/api/v2/admin/users?search='.$uniqueString)
        ;

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


        // According to UsersTableSeeder we have 25 Users
        $this->assertCount(1, $response->json()['data']);

        // Don't check every single record, just the structure of each
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'firstName',
                    'lastName',
                ]
            ],
            'meta' => [
                'current_page',
                'from',
                'last_page',
                'path',
                'per_page',
                'to',
                'total',
            ],
            'links' => [
                'first',
                'last',
                'prev',
                'next',
            ],
        ]);
    }
}
