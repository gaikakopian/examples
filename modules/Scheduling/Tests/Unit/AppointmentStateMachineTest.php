<?php

namespace Modules\Scheduling\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;
use App\Notifications\Appointment\AppointmentCanceledNotification;
use App\Notifications\Appointment\AppointmentPlannedNotification;
use App\Notifications\Appointment\AppointmentRescheduledNotification;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Notification;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

class AppointmentStateMachineTest extends LaravelTest
{
    use DatabaseTransactions;

    /**
     * @var Appointment
     */
    protected $model;
    protected $user;

    public function test_it_counts_successful_appointments()
    {
        Notification::fake();

        $user = User::query()->where('email', 'mentor@volunteer-vision.com')->firstOrFail();

        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $user->id,
        ]);
        $match = factory(Match::class)->create(['state' => Match::STATES['ACTIVE']]);

        factory(Participation::class)->create([
            'enrollment_id' => $enrollment->id,
            'match_id' => $match->id,
        ]);
        $appointment = factory(Appointment::class)->create([
            'match_id' => $match->id,
            'state' => Appointment::STATES['ONLINE']
        ]);



        $appointment->transition('finish');
        $match = $appointment->match;
        $this->assertEquals(2, $match->lections_completed,'Appointment counted first successful appointment');

        $appointment = factory(Appointment::class)->create([
            'match_id' => $match->id,
            'current_lecture' => 3,
            'state' => Appointment::STATES['ONLINE']
        ]);

        $appointment->transition('finish');
        $match = $appointment->match;
        $this->assertEquals(3, $match->lections_completed,'Appointment counted manually transfered lecture number');

        $appointment = factory(Appointment::class)->create([
            'match_id' => $match->id,
            'state' => Appointment::STATES['SUCCESS']
        ]);

        // total of 4 successful sessions now.



        $appointment = factory(Appointment::class)->create([
            'match_id' => $match->id,
            'state' => Appointment::STATES['ONLINE']
        ]);

        $appointment->transition('finish');
        $match = $appointment->match;
        $this->assertEquals(5, $match->lections_completed,'Appointment counted manually transfered lecture number');






    }

    public function test_it_applies_a_transition()
    {
        Notification::fake();

        $this->assertEquals('NEW', $this->model->currentState());

        //------------------------- test transition 'plan'-------------------
        $result = $this->model->transition('plan');

        $this->assertTrue($result);
        $this->assertEquals(Appointment::STATES['PLANNED'], $this->model->currentState());

        //asserting that the transition has been logged in the history
        $this->assertEquals(1, $this->model->history()->count());
        $this->assertNotNull($this->model->history()->first()->actor_id);

        //asserting that the 'last_state_change_at' column is updated
        $this->assertNotNull($this->model->last_state_change_at);

        //--------------------------------------------------------------------

        //----------------------------- test transition 'reschedule' --------------
        $result = $this->model->transition('reschedule');

        $this->assertTrue($result);
        $this->assertEquals(Appointment::STATES['PLANNED'], $this->model->currentState());

        //asserting that a cancel notification has been sent
        Notification::assertSentTo(
            $this->user,
            AppointmentRescheduledNotification::class
        );
        //asserting that the transition has been logged in the history
        $this->assertEquals(2, $this->model->history()->count());

        //--------------------------------------------------------------------

        //----------------------------- test transition 'cancel' --------------
        $result = $this->model->transition('cancel');

        $this->assertTrue($result);
        $this->assertEquals(Appointment::STATES['CANCELED'], $this->model->currentState());

        //asserting that a cancel notification has been sent
        Notification::assertSentTo(
            $this->user,
            AppointmentCanceledNotification::class
        );
        //asserting that the transition has been logged in the history
        $this->assertEquals(3, $this->model->history()->count());

        //--------------------------------------------------------------------
    }

    protected function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create(['accept_email' => true, 'accept_sms' => true, 'preferred_channel' => null]);
        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
        ]);
        factory(Participation::class)->create([
            'enrollment_id' => $enrollment->id,
            'match_id' => 1,
        ]);
        $this->model = factory(Appointment::class)->create([
            'match_id' => 1,
            'state' => Appointment::STATES['NEW']
        ]);


        //setting a fake_actor that represents the authenticated user who is applying changes
        $fake_actor = factory(User::class)->create();
        $this->be($fake_actor);
    }
}
