<?php

namespace Modules\Scheduling\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;
use Modules\Scheduling\Domain\Models\Appointment;

/**
 * Test the functionality of the `/api/v1/appointments/{id}/reschedule` endpoint.
 */
class RescheduleAppointmentTest extends EndpointTest
{
    public function test_it_reschedules_an_appointment()
    {

        //@todo: seppi fragen:

        //using a mock for the notifications
        Notification::fake();

        $appointment = factory(Appointment::class)->create([
            'planned_start' => Carbon::now(),
            'planned_duration_minutes' => 30,
            'match_id' => 1,
            'state' => 'PLANNED',
        ]);



        $params =  [
            'plannedStart' => Carbon::tomorrow()->toDateTimeString(),
            'plannedDurationMinutes' => 90,
            'match_id' => 2, // Make sure that changing match isn't possible
        ];

        $response = $this->actingAs($this->user)->json(
            'POST',
            "/api/v2/appointments/$appointment->id/reschedule",
           $params
        );

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        return;
        $response->assertJsonFragment([
            'plannedStart' => $tomorrow,
            'plannedDurationMinutes' => 90,
        ]);

        $this->assertDatabaseHas('appointments', [
            'id' => $appointment->id,
            'planned_start' => $tomorrow,
            'planned_duration_minutes' => 90,
            'match_id' => 1
        ]);
    }
}
