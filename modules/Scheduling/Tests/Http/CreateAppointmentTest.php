<?php

namespace Modules\Scheduling\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;

/**
 * Test the functionality of the `/api/v1/matches/{id}/appointments` endpoint.
 */
class CreateAppointmentTest extends EndpointTest
{
    public function test_it_creates_a_new_appointment()
    {
        //using a mock for the notifications
        Notification::fake();

        $response = $this->actingAs($this->user)->json(
            'POST',
            '/api/v2/matches/1/appointments',
            [
                'plannedStart' => Carbon::tomorrow()->toDateTimeString(),
            ]
        );

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJsonStructure([
            'data' => [
                'id',
                'plannedStart',
                'plannedDurationMinutes',
            ]
        ]);

        $this->assertDatabaseHas('appointments', [
            'id' => $response->json()['data']['id']
        ]);

    }
}
