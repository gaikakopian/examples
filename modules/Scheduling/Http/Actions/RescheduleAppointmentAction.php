<?php

namespace Modules\Scheduling\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Modules\Scheduling\Domain\Models\Appointment;
use Modules\Scheduling\Domain\Services\AppointmentService;
use Modules\Scheduling\Http\Requests\UpdateAppointmentRequest;
use Modules\Scheduling\Http\Resources\AppointmentHttpResource;

class RescheduleAppointmentAction extends Action
{
    protected $appointmentService;
    protected $responder;

    public function __construct(AppointmentService $appointmentService, ResourceResponder $responder)
    {
        $this->appointmentService = $appointmentService;
        $this->responder = $responder;
    }

    public function __invoke(int $id, UpdateAppointmentRequest $request)
    {
        /** @var Appointment $appointment */
        $appointment = $this->appointmentService->update($id, [
            'planned_start' => $request->input('planned_start'),
            'planned_duration_minutes' => $request->input('planned_duration_minutes', 60),
        ]);

        //statemachine
        $appointment->transition('reschedule');

        return $this->responder->send($appointment, AppointmentHttpResource::class);
    }
}
