<?php

namespace Modules\Scheduling\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Modules\Scheduling\Domain\Services\AppointmentService;
use Modules\Scheduling\Http\Requests\CancelAppointmentRequest;
use Modules\Scheduling\Domain\Models\Appointment;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Comment;
use Modules\Scheduling\Http\Resources\AppointmentHttpResource;

class CancelAppointmentAction extends Action
{
    protected $appointmentService;
    protected $responder;

    public function __construct(AppointmentService $appointmentService, ResourceResponder $responder)
    {
        $this->appointmentService = $appointmentService;
        $this->responder = $responder;


    }

    public function __invoke(int $id, CancelAppointmentRequest $request)
    {
        /** @var Appointment $appointment */
        $appointment = $this->appointmentService->get($id);

        $this->appointmentService->cancelAppointent(Auth::id(), $appointment, $request->comment);

        return $this->responder->send($appointment, AppointmentHttpResource::class);


    }
}
