<?php

namespace Modules\Scheduling\Http\Actions;

use App\Infrastructure\Http\Action;
use Illuminate\Support\Facades\Auth;
use Modules\Scheduling\Domain\Models\Appointment;
use Modules\Scheduling\Domain\Services\AppointmentService;
use Modules\Scheduling\Http\Requests\FinishAppointmentRequest;

class FinishAppointmentAction extends Action
{
    protected $appointmentService;

    public function __construct(AppointmentService $appointmentService)
    {
        $this->appointmentService = $appointmentService;
    }

    public function __invoke(int $id, FinishAppointmentRequest $request)
    {
        /** @var Appointment $appointment */
        $appointment = $this->appointmentService->get($id);


        $this->appointmentService->finishAppointment($appointment, $request->type);


        return response('', 200);
    }
}
