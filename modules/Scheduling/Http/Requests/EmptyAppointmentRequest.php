<?php

namespace Modules\Scheduling\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;
use Modules\Scheduling\Domain\Models\Appointment;

class EmptyAppointmentRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules() : array
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    public function authorize() : bool
    {
        $appointment = Appointment::findOrFail(request()->id);

        return $this->user()->can('change', $appointment);
    }
}
