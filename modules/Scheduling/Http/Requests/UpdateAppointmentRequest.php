<?php

namespace Modules\Scheduling\Http\Requests;

use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

class UpdateAppointmentRequest extends AbstractAppointmentRequest
{
    /**
     * {@inheritDoc}
     */
    public function authorize() : bool
    {
        $appointment = Appointment::findOrFail(request()->id);

        return $this->user()->can('update', $appointment);
    }
}
