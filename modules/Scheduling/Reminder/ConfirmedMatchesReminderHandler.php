<?php

namespace Modules\Scheduling\Reminder;

use App\Notifications\Match\MatchNoFirstAppointment2Notification;

use App\Notifications\Match\MatchNoFirstAppointment3Notification;
use App\Notifications\Match\MatchNoFirstAppointmentNotification;

use App\Reminders\Handler\AbstractReminderHandler;
use Illuminate\Support\Carbon;
use Modules\Admin\Domain\Models\CoordinatorTodo;
use Modules\Admin\Domain\Services\CoordinatorTodoService;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

class ConfirmedMatchesReminderHandler extends AbstractReminderHandler
{


    protected $coordinatorTodoService;

    public function __construct(CoordinatorTodoService $coordinatorTodoService)
    {
        parent::__construct();
        $this->coordinatorTodoService = $coordinatorTodoService;

        $this->setDateCallback(function ($match) {
            return $match->last_state_change_at;
        });

        $this->registerEvents();
    }

    public function registerEvents()
    {
        $this->registerEvent(24 * 2, [$this, 'firstReminder']);
        $this->registerEvent(24 * 4, [$this, 'secondReminder']);
        $this->registerEvent(24 * 9, [$this, 'thirdReminder']);
        $this->registerEvent(24 * 14, [$this, 'coordinatorAction']);

    }

  /**
 * @param Match $match
 */
    public function firstReminder($match)
    {
        $this->sendReminder($match, MatchNoFirstAppointmentNotification::class);
    }

    public function secondReminder($match)
    {
        $this->sendReminder($match, MatchNoFirstAppointment2Notification::class);
    }

    //@todo: does this exist?
    public function thirdReminder($match)
    {
        $this->sendReminder($match, MatchNoFirstAppointment3Notification::class);
    }

    public function coordinatorAction($match)
    {

        $participation = $match->getMentorParticipation();

        if (!$participation) { // no idea how this could happen..
            return;
        }
        $enrollment = $participation->enrollment;

        $match->last_reminder = Carbon::now();
        $match->save();

        $data = [
            'meta' => ['match_id' => $match->id],
            'type' => CoordinatorTodo::TYPES['NO_FIRST_APPOINTMENT'],
            'customer_id' => $enrollment->user->id,
            'duedate' => Carbon::now(),
        ];
        $todo = new CoordinatorTodo($data);
        $this->coordinatorTodoService->createTodoIfNotExistsWithinDays($todo, 7);

    }

    private function sendReminder($match, $class)
    {
        /**
         * @var Match $match
         * @var User $mentor
         */

        $participation = $match->getMentorParticipation();

        if (!$participation) { // no idea how this could happen..
            return;
        }
        $enrollment = $participation->enrollment;
        $match->last_reminder = Carbon::now();
        $match->save();

        $mentor = $enrollment->user;

        $notification = new $class($mentor->brand, $match, $enrollment);
        $mentor->notify($notification);

    }
}