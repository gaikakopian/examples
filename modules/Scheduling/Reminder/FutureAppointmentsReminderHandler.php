<?php

namespace Modules\Scheduling\Reminder;


use App\Notifications\Match\MatchNoFutureAppointment2Notification;
use App\Notifications\Match\MatchNoFutureAppointment3Notification;
use App\Notifications\Match\MatchNoFutureAppointmentNotification;
use App\Reminders\Handler\AbstractReminderHandler;
use Carbon\Carbon;
use Modules\Admin\Domain\Models\CoordinatorTodo;
use Modules\Admin\Domain\Services\CoordinatorTodoService;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

class FutureAppointmentsReminderHandler extends AbstractReminderHandler
{


    protected $coordinatorTodoService;

    public function __construct(CoordinatorTodoService $coordinatorTodoService)
    {
        parent::__construct();
        $this->coordinatorTodoService = $coordinatorTodoService;


        $this->setDateCallback(function ($match) {
            /** @var Match $match */
            $latestAppointment = $match->appointments()
                ->where('state', Appointment::STATES['SUCCESS'])
                ->orderBy('planned_start', 'desc')
                ->first();

            if ($latestAppointment) {
                return $latestAppointment->planned_start;
            }

            return $match->last_state_change_at;
        });

        $this->registerEvents();
    }

    public function registerEvents()
    {
        $this->registerEvent(24 * 7, [$this, 'firstReminder']);
        $this->registerEvent(24 * 14, [$this, 'secondReminder']);

        //

        $this->registerEvent(24 * 28, [$this, 'coordinatorAction']);

    }

    /**
     * @param Match $match
     */
    public function firstReminder($match)
    {
        $this->sendReminder($match, MatchNoFutureAppointmentNotification::class);
    }

    public function secondReminder($match)
    {
        $this->sendReminder($match, MatchNoFutureAppointment2Notification::class);
    }


    public function thirdReminder($match)
    {
        // dows not exist currently.
        $this->sendReminder($match, MatchNoFutureAppointment3Notification::class);
    }


    public function coordinatorAction(Match $match)
    {
        $participation = $match->getMentorParticipation();
        if (!$participation) { // no idea how this could happen..
            return;
        }

        $enrollment = $participation->enrollment;
        $user = $enrollment->user;

        $data = [
            'meta' => ['match_id' => $match->id],
            'type' => CoordinatorTodo::TYPES['NO_FUTURE_APPOINTMENT'],
            'customer_id' => $user->id,
            'duedate' => Carbon::now(),
        ];
        $todo = new CoordinatorTodo($data);
        $this->coordinatorTodoService->createTodoIfNotExistsWithinDays($todo, 7);

    }

    private function sendReminder($match, $class)
    {


        /** @var Match $match */
        $participation = $match->getMentorParticipation();
        if (!$participation) { // no idea how this could happen..
            return;
        }
        $enrollment = $participation->enrollment;

        $mentor = $enrollment->user;

        // tdb: needed?


        $mentor->last_reminder = Carbon::now();
        $mentor->save();


        $notification = new $class($mentor->brand, $match, $enrollment);

        $mentor->notify($notification);

    }
}