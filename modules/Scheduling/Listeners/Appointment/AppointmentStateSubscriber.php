<?php

namespace Modules\Scheduling\Listeners\Appointment;

use App\Infrastructure\Domain\EventSubscriberProxy;
use Symfony\Component\EventDispatcher\Event;

class AppointmentStateSubscriber extends EventSubscriberProxy
{
    /** {@inheritDoc} */
    protected function mapEvents(): array
    {
        return [
            'Appointment.plan' => 'plan',
            'Appointment.reschedule' => 'reschedule',
            'Appointment.cancel' => 'cancel',
            'Appointment.start' => 'start',
            'Appointment.report_noattendance' => 'reportNoattendance',
            'Appointment.report_noshow' => 'reportNoshow',
            'Appointment.undo_noshow' => 'undoNoshow',
            'Appointment.finish' => 'finish',
            'Appointment.finish_offline' => 'finishOffline',
            'Appointment.hangup' => 'hangup',
        ];
    }

    /** {@inheritDoc} */
    protected function chooseHandler(Event $event)
    {
        return new DefaultAppointmentStateHandler();
    }
}
