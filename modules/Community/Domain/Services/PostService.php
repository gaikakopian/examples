<?php

namespace Modules\Community\Domain\Services;

use App\Infrastructure\Domain\EloquentBreadService;
use Illuminate\Support\Facades\DB;
use Modules\Community\Domain\Models\PostComment;
use Modules\Community\Domain\Models\PostLike;
use Modules\Core\Domain\Models\Group;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class PostService
 *
 * @package Modules\Community\Domain\Services
 */
class PostService extends EloquentBreadService
{
    /**
     *
     */
    const MIN_POSTS_TO_SHOW = 3;

    /**
     * @param int $id
     * @return mixed
     */
    public function getPost(int $id, int $userId)
    {
        $post = $this->model->with('user')->find($id);

        $postLikes = $this->getPostLikesByUser($userId);
        $post->isLiked = !is_null($postLikes->where('post_id', $post->id)->first());



        if (is_null($post)) {
            throw new NotFoundHttpException(sprintf('Post with id %d could not be found', $id));
        }

        return $post;
    }

    /**
     * @param array $groupIds
     * @param int $userId
     * @param int|null $lastPostId
     * @param null $limit
     * @return \Illuminate\Support\Collection
     */
    public function getPostsByGroupId(array $groupIds, int $userId, int $lastPostId = null, $limit = null)
    {
        if (false === is_null($lastPostId)) {
            $lastPost = $this->model->find($lastPostId);
            if (is_null($lastPost)) {
                throw new NotFoundHttpException(sprintf('Post with id %d could not be found', $lastPostId));
            }
        } else {
            $lastPostId = 0;
        }

        $limit = is_null($limit) ? self::MIN_POSTS_TO_SHOW : $limit;

        $posts = $this->model
            ->with('user')
            ->whereIn('group_id', $groupIds)
            ->where('id', '>', $lastPostId)
            ->orderBy('created_at', 'DESC')
            ->limit($limit)
            ->get();
        $posts = $this->mapPostAndCheckIsLiked($posts, $userId);

        return $posts;
    }

    /**
     * @param int $userId
     * @param int|null $limit
     * @return mixed
     */
    public function getUserAllPosts(int $userId, int $limit = null)
    {
        $limit = is_null($limit) ? self::MIN_POSTS_TO_SHOW : $limit;

        $groupIds = DB::table('group_user')
            ->where('user_id', $userId)
            ->select('group_id')
            ->get()
            ->pluck('group_id')
            ->toArray();
        $posts = $this->model
            ->whereIn('group_id', $groupIds)
            ->with('user')
            ->limit($limit)
            ->orderBy('created_at', 'DESC')
            ->get();

        $posts = $this->mapPostAndCheckIsLiked($posts, $userId);

        return $posts;
    }

    /**
     * @param $posts
     * @param int $userId
     * @return mixed
     */
    public function mapPostAndCheckIsLiked($posts, int $userId)
    {
        $postLikes = $this->getPostLikesByUser($userId);

        return $posts->map(function ($post) use ($postLikes) {
            $post->isLiked = !is_null($postLikes->where('post_id', $post->id)->first());

            return $post;
        });
    }

    /**
     * @param int $userId
     * @return \Illuminate\Support\Collection
     */
    public function getPostLikesByUser(int $userId)
    {
        return DB::table('post_likes')
            ->where('user_id', $userId)
            ->get();
    }

    /**
     * @param int $postId
     * @param int $userId
     *
     * @return PostLike
     */
    public function likePost(int $postId, int $userId): PostLike
    {
        $postLike = new PostLike();
        $postLike->post_id = $postId;
        $postLike->user_id = $userId;
        $postLike->created_at = new \DateTime('now');
        $postLike->save();

        return $postLike;
    }

    /**
     * @param int $postId
     * @param int $userId
     *
     * @return bool
     */
    public function disLikePost(int $postId, int $userId): bool
    {
        PostLike::where(['post_id' => $postId, 'user_id' => $userId])->delete();

        return true;
    }

    /**
     * @param int $postId
     * @param int $userId
     * @param string $body
     * @param int|null $parentCommentId
     *
     * @return PostComment
     */
    public function commentOnPost(int $postId, int $userId, string $body, int $parentCommentId = null): PostComment
    {
        $postComment = new PostComment();
        $postComment->post_id = $postId;
        $postComment->user_id = $userId;
        $postComment->body = $body;
        $postComment->parent_id = $parentCommentId;
        $postComment->save();

        return $postComment;
    }

    /**
     * @param int $commentId
     *
     * @return bool
     */
    public function deleteComment(int $commentId): bool
    {
        PostComment::find($commentId)->delete();

        return true;
    }
}
