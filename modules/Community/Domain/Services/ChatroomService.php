<?php

namespace Modules\Community\Domain\Services;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Domain\EloquentBreadService;
use Carbon\Carbon;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Modules\Community\Domain\Models\Chatroom;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\User;

class ChatroomService extends EloquentBreadService
{
    public function findChatRoomForUsers($userids)
    {
        if ($userids[0] == $userids[1]) {
            throw new ClientException('CANNOT_CHAT_WITH_YOURSELF');
        }
        if (count($userids) < 2) {
            throw new ClientException('MINIMUM_OF_TWO_PARTICIPANTS_NEEDED');
        }
        // actually I wanted here to search for WHERE user_id IN (?), then it would also work with more than 2.
        // but eloquent didnt let me use prepare for arrays
        $chatroomId = DB::select(
            "SELECT max(chatroom_id) AS id  FROM chatroom_user  WHERE user_id = ? OR user_id = ? GROUP BY chatroom_id HAVING COUNT(*) = ? LIMIT 1",
            [$userids[0], $userids[1], count($userids)]
        );

        if (count($chatroomId) > 0) {
            return Chatroom::find($chatroomId[0]->id);
        }
        return null;
    }

    /**
     * @param $userids
     * @return mixed
     */
    public function createChatRoomForUsers($userids, $room_id)
    {
        $chatroom = $this->findChatRoomForUsers($userids);
        if ($chatroom != null) {
            $chatroom->external_room_id = $room_id;
            $chatroom->save();
            return $chatroom;
        }


        $chatroom = new Chatroom();
        $chatroom->external_room_id = $room_id;
        $chatroom->save();
        $chatroom->users()->sync($userids);

        return $chatroom;
    }
}
