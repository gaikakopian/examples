<?php

namespace Modules\Community\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Community\Domain\Models\Post;
use Modules\Community\Domain\Models\PostComment;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\User;

/**
 * Class CreatePostCommentTest
 *
 * @package Modules\Community\Tests\Http
 */
class CreatePostCommentTest extends EndpointTest
{
    public function test_it_returns_error_when_post_does_not_exist()
    {
        $this->user->community_enabled = true;
        $response = $this->actingAs($this->user)->postJson('/api/v2/community/users/me/posts/0/comments', [
            'body' => 'Comment #1',
        ]);

        self::assertEquals(404, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'message' => 'Post with id 0 could not be found',
        ]);
    }

    public function test_it_returns_error_when_user_comments_on_post_of_group_he_is_not_member_of()
    {
        $this->user->community_enabled = true;

        $group = factory(Group::class)->create([
            'name' => 'Private group',
            'organization_id' => 1,
            'privacy' => Group::PRIVACY_PUBLIC,
        ]);

        $otherUser = User::where('email', '=', 'community@volunteer-vision.com')->firstOrFail();
        $post = factory(Post::class)->create([
            'user_id' => $otherUser->id,
            'group_id' => $group->id,
            'body' => 'Hello',
        ]);

        $response = $this
            ->actingAs($this->user)
            ->postJson('/api/v2/community/users/me/posts/' . $post->id . '/comments', [
                'body' => 'Comment #1',
            ]);

        $response->assertJson([
            'message' => 'User can comment on posts of groups he is member of.',
        ]);
    }

    public function test_it_returns_error_when_parent_comment_does_not_exist()
    {
        $this->user->community_enabled = true;

        $group = factory(Group::class)->create([
            'name' => 'Private group',
            'organization_id' => 1,
            'privacy' => Group::PRIVACY_PUBLIC,
        ]);

        $group->users()->attach($this->user);

        $otherUser = User::where('email', '=', 'community@volunteer-vision.com')->firstOrFail();
        $post = factory(Post::class)->create([
            'user_id' => $otherUser->id,
            'group_id' => $group->id,
            'body' => 'Hello',
        ]);

        $response = $this
            ->actingAs($this->user)
            ->postJson('/api/v2/community/users/me/posts/' . $post->id . '/comments', [
                'body' => 'Comment #1',
                'parent_id' => 0,
            ]);

        self::assertEquals(404, $response->getStatusCode());
        $response->assertJson([
            'message' => 'Comment with id 0 could not be found',
        ]);
    }

    public function test_it_creates_comment()
    {
        $this->user->community_enabled = true;

        $group = factory(Group::class)->create([
            'name' => 'Private group',
            'organization_id' => 1,
            'privacy' => Group::PRIVACY_PUBLIC,
        ]);

        $group->users()->attach($this->user);

        $otherUser = User::where('email', '=', 'community@volunteer-vision.com')->firstOrFail();
        $post = factory(Post::class)->create([
            'user_id' => $otherUser->id,
            'group_id' => $group->id,
            'body' => 'Hello',
        ]);

        $response = $this
            ->actingAs($this->user)
            ->postJson('/api/v2/community/users/me/posts/' . $post->id . '/comments', [
                'body' => 'Comment #1',
            ]);

        self::assertEquals(201, $response->getStatusCode());
        $response->assertJsonStructure([
            'data' => [
                'id',
                'post_id',
                'body',
                'parent_id',
                'created_at',
            ],
        ]);

        self::assertEquals(1, $post->comments()->count());
    }

    public function test_it_creates_reply_comment()
    {
        $this->user->community_enabled = true;

        $group = factory(Group::class)->create([
            'name' => 'Private group',
            'organization_id' => 1,
            'privacy' => Group::PRIVACY_PUBLIC,
        ]);

        $group->users()->attach($this->user);

        $otherUser = User::where('email', '=', 'community@volunteer-vision.com')->firstOrFail();
        $post = factory(Post::class)->create([
            'user_id' => $otherUser->id,
            'group_id' => $group->id,
            'body' => 'Hello',
        ]);

        $parentComment = factory(PostComment::class)->create([
            'user_id' => $otherUser->id,
            'post_id' => $post->id,
        ]);

        $response = $this
            ->actingAs($this->user)
            ->postJson('/api/v2/community/users/me/posts/' . $post->id . '/comments', [
                'body' => 'Comment #1',
                'parent_id' => $parentComment->id,
            ]);

        self::assertEquals(201, $response->getStatusCode());
        $response->assertJsonStructure([
            'data' => [
                'id',
                'post_id',
                'body',
                'parent_id',
                'created_at',
            ],
        ]);

        self::assertEquals(2, $post->comments()->count());
    }
}
