<?php

namespace Modules\Community\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Group;

/**
 * Class CreatePostTest
 *
 * @package Modules\Community\Tests\Http
 */
class CreatePostTest extends EndpointTest
{
    public function test_it_returns_error_when_group_does_not_exist()
    {
        $this->user->community_enabled = true;
        $response = $this->actingAs($this->user)->postJson('/api/v2/community/users/me/posts', [
            'group_id' => 0,
            'body' => 'Lorem Ipsum'
        ]);

        self::assertEquals(404, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'message' => 'Group with id 0 could not be found',
        ]);
    }

    public function test_it_returns_error_when_user_creates_post_in_group_he_is_not_member_of()
    {
        $this->user->community_enabled = true;
        $organization = factory(\Modules\Core\Domain\Models\Organization::class)->create([
            'name' => 'Test Volunteer Vision',
            'type' => 'company',
        ]);

        $group = factory(Group::class)->create([
            'name' => 'Private group',
            'organization_id' => $organization->id,
            'privacy' => Group::PRIVACY_PUBLIC,
        ]);

        $response = $this->actingAs($this->user)->postJson('/api/v2/community/users/me/posts', [
            'group_id' => $group->id,
            'body' => 'Lorem Ipsum'
        ]);

        $response->assertJson([
            'message' => 'User can create posts only in groups he is member of.',
        ]);
    }

    public function test_it_creates_post()
    {
        $this->user->community_enabled = true;
        $organization = factory(\Modules\Core\Domain\Models\Organization::class)->create([
            'name' => 'Test Volunteer Vision',
            'type' => 'company',
        ]);

        $group = factory(Group::class)->create([
            'name' => 'Private group',
            'organization_id' => $organization->id,
            'privacy' => Group::PRIVACY_PUBLIC,
        ]);

        $group->users()->attach($this->user->id);

        $response = $this->actingAs($this->user)->postJson('/api/v2/community/users/me/posts', [
            'group_id' => $group->id,
            'body' => 'Lorem Ipsum'
        ]);

        self::assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJsonStructure([
           'data' => [
               'id',
               'group_id',
               'body',
               'created_at',
           ],
        ]);
    }
}
