<?php

namespace Modules\Community\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Community\Domain\Models\Post;
use Modules\Community\Domain\Models\PostComment;
use Modules\Core\Domain\Models\User;

/**
 * Class UpdateCommentTest
 *
 * @package Modules\Community\Tests\Http
 */
class UpdateCommentTest extends EndpointTest
{
    public function test_it_returns_error_when_post_does_not_exist()
    {
        $this->user->community_enabled = true;
        $response = $this->actingAs($this->user)->putJson('/api/v2/community/users/me/posts/0/comments/1', [
            'body' => 'Comment #1 update',
        ]);

        self::assertEquals(404, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'message' => 'Post with id 0 could not be found',
        ]);
    }

    public function test_it_returns_error_when_comment_does_not_exist()
    {
        $this->user->community_enabled = true;
        $post = factory(Post::class)->create([
            'user_id' => $this->user->id,
            'group_id' => 1,
            'body' => 'Hello',
        ]);

        $response = $this
            ->actingAs($this->user)
            ->putJson('/api/v2/community/users/me/posts/' . $post->id . '/comments/0', [
                'body' => 'Comment #1 update',
            ]);

        self::assertEquals(404, $response->getStatusCode());
        $response->assertJson([
            'message' => 'Comment with id 0 could not be found',
        ]);
    }

    public function test_it_returns_error_when_user_tries_to_edit_another_user_comment()
    {
        $this->user->community_enabled = true;

        $post = factory(Post::class)->create([
            'user_id' => $this->user->id,
            'group_id' => 1,
            'body' => 'Hello',
        ]);

        $otherUser = User::where('email', '=', 'community@volunteer-vision.com')->firstOrFail();
        $comment = factory(PostComment::class)->create([
            'user_id' => $otherUser->id,
            'post_id' => $post->id,
            'body' => 'Comment #1',
        ]);

        $response = $this
            ->actingAs($this->user)
            ->putJson('/api/v2/community/users/me/posts/' . $post->id . '/comments/' . $comment->id, [
                'body' => 'Comment #1 update',
            ]);

        $response->assertJson([
            'message' => 'User can edit only his own comments.',
        ]);
    }

    public function test_it_updates_comment()
    {
        $this->user->community_enabled = true;

        $post = factory(Post::class)->create([
            'user_id' => $this->user->id,
            'group_id' => 1,
            'body' => 'Hello',
        ]);

        $comment = factory(PostComment::class)->create([
            'user_id' => $this->user->id,
            'post_id' => $post->id,
            'body' => 'Comment #1',
        ]);

        $response = $this
            ->actingAs($this->user)
            ->putJson('/api/v2/community/users/me/posts/' . $post->id . '/comments/' . $comment->id, [
                'body' => 'Comment #1 update',
            ]);

        self::assertEquals(200, $response->getStatusCode());
        $response->assertJsonStructure([
            'data' => [
                'id',
                'post_id',
                'body',
                'parent_id',
                'created_at',
            ],
        ]);

        $updatedComment = PostComment::find($comment->id);
        self::assertEquals('Comment #1 update', $updatedComment->body);
    }
}
