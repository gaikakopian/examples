<?php

namespace Modules\Community\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

/**
 * Class UpdatePostRequest
 *
 * @package Modules\Community\Http\Requests
 */
class UpdatePostRequest extends DeserializedFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'body' => 'required|string|min:1',
        ];
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
