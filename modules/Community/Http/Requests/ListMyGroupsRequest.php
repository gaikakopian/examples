<?php

namespace Modules\Community\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

/**
 * Class ListMyGroupsRequest
 *
 * @package Modules\Community\Http\Requests
 */
class ListMyGroupsRequest extends DeserializedFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'page' => 'integer|min:1',
            'per_page' => 'integer|min:1',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function authorize()
    {
        return true;
    }
}
