<?php

namespace Modules\Community\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

/**
 * Class CreatePostCommentRequest
 *
 * @package Modules\Community\Http\Requests
 */
class CreatePostCommentRequest extends DeserializedFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'body' => 'required|string|min:1',
            'parent_id' => 'integer',
        ];
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
