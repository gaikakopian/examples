<?php

namespace Modules\Community\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

/**
 * Class ListPostCommentRequest
 *
 * @package Modules\Community\Http\Requests
 */
class ListPostCommentRequest extends DeserializedFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'limit' => 'integer|min:1',
            'last_comment_id' => 'integer',
            'parent_comment_id' => 'integer',
        ];
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
