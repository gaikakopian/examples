<?php

namespace Modules\Community\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

/**
 * Class ListPostsRequest
 *
 * @package Modules\Community\Http\Requests
 */
class ListPostsRequest extends DeserializedFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'group_id' => 'required|array',
            'group_id.*' => 'exists:groups,id',
            'limit' => 'integer|min:1',
            'last_post_id' => 'integer',
        ];
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
