<?php

namespace Modules\Community\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class AddChatRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'external_room_id' => 'required|integer'
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function authorize()
    {
        // Authorization handled via Scope Middleware.
        return true;
    }
}
