<?php

namespace Modules\Community\Http\Resources;


use App\Infrastructure\Http\HttpResource;
use Modules\Core\Http\Resources\UserPublicHttpResource;

class GroupCommunityHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'users' => UserPublicHttpResource::collection($this->users),
        ];
    }
}
