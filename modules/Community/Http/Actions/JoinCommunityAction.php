<?php

namespace Modules\Community\Http\Actions;

use App\Infrastructure\Http\Action;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Services\GroupService;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class JoinGroupAction
 *
 * @package Modules\Community\Http\Actions
 */
class JoinCommunityAction extends Action
{
    /** @var GroupService */
    private $groupService;

    /**
     * JoinGroupAction constructor.
     *
     * @param GroupService $groupService
     */
    public function __construct(GroupService $groupService)
    {
        $this->groupService = $groupService;
    }

    /**
     * @param int $id Group id
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke()
    {
        $user = Auth::user();

        $user->community_enabled = true;
        $user->save();

        return response([], 201);
    }
}
