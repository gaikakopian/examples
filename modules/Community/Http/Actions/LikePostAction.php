<?php

namespace Modules\Community\Http\Actions;

use App\Infrastructure\Http\Action;
use Illuminate\Support\Facades\Auth;
use Modules\Community\Domain\Models\Post;
use Modules\Community\Domain\Services\PostService;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class LikePostAction
 *
 * @package Modules\Community\Http\Actions
 */
class LikePostAction extends Action
{
    /** @var PostService */
    private $postService;

    /**
     * LikePostAction constructor.
     *
     * @param PostService $postService
     */
    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    /**
     * @param int $postId
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(int $postId)
    {
        $loggedInUserId = Auth::id();
        $post = Post::find($postId);
        if (is_null($post)) {
            throw new NotFoundHttpException(sprintf('Post with id %d could not be found', $postId));
        }

        if ($post->user_id === $loggedInUserId) {
            throw new AccessDeniedHttpException('User can not like his own post.');
        }

        $postLike = $post->likes()->where(['user_id' => $loggedInUserId, 'post_id' => $post->id])->first();
        if (false === is_null($postLike)) {
            throw new AccessDeniedHttpException('User can not like the same post twice.');
        }

        $this->postService->likePost($post->id, $loggedInUserId);

        return response([], 201);
    }
}
