<?php

namespace Modules\Community\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Community\Domain\Models\Post;
use Modules\Community\Domain\Services\PostService;
use Modules\Community\Http\Requests\UpdatePostRequest;
use Modules\Community\Http\Resources\PostHttpResource;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class UpdatePostAction
 *
 * @package Modules\Community\Http\Actions
 */
class UpdatePostAction extends Action
{
    /** @var PostService */
    private $postService;

    /** @var ResourceResponder */
    private $resourceResponder;

    /**
     * UpdatePostAction constructor.
     *
     * @param PostService $postService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(PostService $postService, ResourceResponder $resourceResponder)
    {
        $this->postService = $postService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * @param int $postId
     * @param UpdatePostRequest $request
     *
     * @return mixed
     */
    public function __invoke(int $postId, UpdatePostRequest $request)
    {
        $post = Post::find($postId);
        if (is_null($post)) {
            throw new NotFoundHttpException(sprintf('Post with id %d could not be found', $postId));
        }

        $loggedInUser = Auth::user();
        if ($post->user_id !== $loggedInUser->id) {
            throw new AccessDeniedHttpException('User can only edit own posts.');
        }

        $postData = [];
        $postData['body'] = $request->get('body');
        $updatedPost = $this->postService->update($postId, $postData);

        return $this->resourceResponder->send($updatedPost, PostHttpResource::class);
    }
}
