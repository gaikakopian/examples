<?php

namespace Modules\Community\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Community\Domain\Models\Post;
use Modules\Community\Domain\Models\PostComment;
use Modules\Community\Domain\Services\PostService;
use Modules\Community\Http\Requests\CreatePostCommentRequest;
use Modules\Community\Http\Resources\PostCommentHttpResource;
use Modules\Core\Domain\Services\GroupService;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class CreatePostCommentAction
 *
 * @package Modules\Community\Http\Actions
 */
class CreatePostCommentAction extends Action
{
    /** @var PostService */
    private $postService;

    /** @var GroupService */
    private $groupService;

    /** @var ResourceResponder */
    private $resourceResponder;

    /**
     * CommentOnPostAction constructor.
     *
     * @param PostService $postService
     * @param GroupService $groupService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(
        PostService $postService,
        GroupService $groupService,
        ResourceResponder $resourceResponder
    ) {
        $this->postService = $postService;
        $this->groupService = $groupService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * @param int $postId
     * @param CreatePostCommentRequest $request
     *
     * @return mixed
     */
    public function __invoke(int $postId, CreatePostCommentRequest $request)
    {
        $post = Post::find($postId);
        if (is_null($post)) {
            throw new NotFoundHttpException(sprintf('Post with id %d could not be found', $postId));
        }

        $loggedInUser = Auth::user();
        if (false === $this->groupService->isMemberOf($loggedInUser, $post->group_id)) {
            throw new AccessDeniedHttpException('User can comment on posts of groups he is member of.');
        }

        $parentCommentId = $request->get('parent_id', null);
        if (false === is_null($parentCommentId)) {
            $parentComment = PostComment::find($parentCommentId);
            if (is_null($parentComment)) {
                throw new NotFoundHttpException(sprintf('Comment with id %d could not be found', $parentCommentId));
            }
        }

        $commentBody = $request->get('body');
        $createdComment = $this->postService->commentOnPost($post->id, $loggedInUser->id, $commentBody, $parentCommentId);

        return $this->resourceResponder->send($createdComment, PostCommentHttpResource::class);
    }
}
