<?php

namespace Modules\Community\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Modules\Community\Domain\Models\Post;
use Modules\Community\Domain\Models\PostComment;
use Modules\Community\Http\Requests\ListPostCommentRequest;
use Modules\Community\Http\Resources\PostCommentsListHttpResource;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ListPostCommentsAction
 *
 * @package Modules\Community\Http\Actions
 */
class ListPostCommentsAction extends Action
{
    /** @var ResourceResponder */
    private $resourceResponder;

    /**
     * ListPostCommentsAction constructor.
     *
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(ResourceResponder $resourceResponder)
    {
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * @param int $postId
     * @param ListPostCommentRequest $request
     *
     * @return mixed
     */
    public function __invoke(int $postId, ListPostCommentRequest $request)
    {
        $post = Post::find($postId);
        if (is_null($post)) {
            throw new NotFoundHttpException(sprintf('Post with id %d could not be found', $postId));
        }

        $lastCommentId = $request->get('last_comment_id', null);
        if (false === is_null($lastCommentId)) {
            $lastComment = PostComment::find($lastCommentId);
            if (is_null($lastComment)) {
                throw new NotFoundHttpException(sprintf('Comment with id %d could not be found', $lastCommentId));
            }
        } else {
            $lastCommentId = 0;
        }

        $parentCommentId = $request->get('parent_comment_id', null);
        if (false === is_null($parentCommentId)) {
            $parentComment = PostComment::find($parentCommentId);
            if (is_null($parentComment)) {
                throw new NotFoundHttpException(sprintf('Comment with id %d could not be found', $parentCommentId));
            }
        }

        $limit = $request->get('limit');
        $postComments = $post->comments()
            ->where('id', '>', $lastCommentId)
            ->where('parent_id', $parentCommentId)
            ->orderBy('created_at', 'DESC')
            ->limit($limit)
            ->with('user')
            ->get();

        return $this->resourceResponder->send($postComments, PostCommentsListHttpResource::class);
    }
}
