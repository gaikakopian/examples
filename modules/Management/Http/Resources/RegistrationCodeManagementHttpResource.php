<?php

namespace Modules\Management\Http\Resources;


use Illuminate\Http\Resources\Json\ResourceCollection;
use Modules\Core\Http\Resources\FileHttpResource;

class RegistrationCodeManagementHttpResource extends ResourceCollection
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            // public link is missing!
            'publicLink' => 'https://TODO',
            'primaryRole' => $this->primary_role,
            'usersCountLimit' => $this->users_count_limit,
            'usersCountPlanned' => $this->users_count_planned,
            'usersCount' => isset($this->users_count) ? $this->users_count : null,
        ];
    }
}
