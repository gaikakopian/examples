<?php

namespace Modules\Management\Http\Resources;


use App\Infrastructure\Http\HttpResource;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ActivityService;

class UserManagementMinimalResource extends HttpResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'displayName' => $this->first_name . ' ' . $this->last_name,
            'firstName' => $this->first_name,
            'lastName' => $this->last_name, 'avatar' => $this->avatar,
            'createdAt' => $this->created_at ? $this->created_at->toIso8601String() : $this->created_at,
        ];
    }
}
