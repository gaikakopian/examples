<?php

namespace Modules\Management\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class ProgramHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'language' => $this->language,
            'code' => $this->code,
            'title' => $this->title,
            'description' => $this->description,
            'completed_lections' => $this->whenLoaded('matches', function() {
                return $this->matches()->sum('lections_completed');
            }),
            'is_activated_license' => $this->whenLoaded('matches', function() {
                return $this->matches()->whereNotNull('licence_activated')->count() > 0;
            }),
            'totalLections' => $this->lections_total,
            'logo' => $this->logo,
            'cover' => $this->cover,
            'detailPhoto' => $this->detail_photo,
            'matchingAlgorithm' => $this->matching_algorithm,
            'hasMentorWebinar' => $this->has_mentor_webinar,
            'hasMenteeWebinar' => $this->has_mentee_webinar
        ];
    }
}
