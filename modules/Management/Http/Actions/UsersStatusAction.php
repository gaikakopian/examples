<?php

namespace Modules\Management\Http\Actions;

use App\Infrastructure\Http\PlainHttpResource;
use Modules\Reporting\Http\Actions\BaseReportAction;
use Modules\Reporting\Http\Actions\DoneMembersReportAction;
use Modules\Reporting\Http\Actions\MatchingMembersReportAction;
use Modules\Reporting\Http\Actions\RegisteredMembersReportAction;
use Modules\Reporting\Http\Actions\TrainedMembersReportAction;
use Modules\Reporting\Http\Actions\WorkingMembersReportAction;

class UsersStatusAction extends BaseReportAction
{
    protected $organizationId;

    /** {@inheritDoc} */
    protected function mainQuery()
    {
        //
    }

    /** {@inheritDoc} */
    protected function report()
    {
        $registered = new RegisteredMembersReportAction();
        $trained = new TrainedMembersReportAction();
        $matching = new MatchingMembersReportAction();
        $active = new WorkingMembersReportAction();
        $done = new DoneMembersReportAction();

        $data = [
            'registered' => $registered(),
            'trained' => $trained(),
            'matching' => $matching(),
            'active' => $active(),
            'completed' => $done(),
        ];

        return $this->responder->send($data, PlainHttpResource::class);
    }
}
