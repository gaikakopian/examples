<?php

namespace Modules\Management\Http\Actions;


use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Modules\Management\Domain\Services\FileService;
use Modules\Management\Http\Resources\FileGroupHttpResource;

class DownloadsAction extends Action
{
    /**
     * @var FileService
     */
    protected $fileService;

    /**
     * @var ResourceResponder
     */
    private $resourceResponder;

    /**
     * DownloadsAction constructor.
     * @param FileService $fileService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(FileService $fileService, ResourceResponder $resourceResponder)
    {
        $this->fileService = $fileService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        $organizationId = auth()->user()->organization_id;
        $groupedFiles = $this->fileService->getFiles($organizationId);

        return $this->resourceResponder->send($groupedFiles, FileGroupHttpResource::class);
    }
}
