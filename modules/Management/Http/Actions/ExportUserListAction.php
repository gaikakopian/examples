<?php

namespace Modules\Management\Http\Actions;


use App\Infrastructure\Http\Action;
use Modules\Management\Domain\Services\ExcelService;

class ExportUserListAction extends Action
{
    /**
     * @var ExcelService
     */
    protected $excelService;

    /**
     * ManagementDashboardAction constructor.
     * @param ExcelService $excelService
     */
    public function __construct(ExcelService $excelService)
    {
        $this->excelService = $excelService;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        return $this->excelService->exportUsers();
    }
}
