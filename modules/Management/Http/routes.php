<?php

/*
|--------------------------------------------------------------------------
| Module routes
|--------------------------------------------------------------------------
|
| Here's the place to register API routes for the current module. These routes
| are loaded by the RouteServiceProvider within a group which is assigned
| the "api" middleware group and a version prefix, eg. `/api/v1`.
|
 */

/**
 * /management
 */

Route::group(['prefix' => '/management', 'as' => 'management.','middleware' => ['auth', 'scope:manager']], function () {
    Route::get('/key-numbers', 'Actions\ManagementDashboardAction')->name('keyNumbers');
    Route::get('/newest-users', 'Actions\GetNewestUsersAction')->name('getNewestUsers');
    Route::get('/registered-users', 'Actions\GetRegisteredUsersAction')->name('getRegisteredUsers');
    Route::get('/latest-feedbacks', 'Actions\GetLatestFeedbackListAction')->name('getLatestFeedbacks');
    Route::get('/registrationcodes', 'Actions\GetRegistrationLinksAction')->name('getRegistrationCodes');
    Route::get('/export-users', 'Actions\ExportUserListAction')->name('exportUsers');
    Route::get('/users-status', 'Actions\UsersStatusAction')->name('getUsersStatus');
    Route::get('/progress', 'Actions\ProgressAction')->name('progress');
    Route::get('/downloads', 'Actions\DownloadsAction')->name('getDownloadsList');
    Route::get('/users-programs', 'Actions\UsersProgramSearchAction')->name('getUsersWithPrograms');
    Route::get('/licenses', 'Actions\GetLicensesReportAction')->name('getLicensesReport');
});
