<?php

namespace Modules\Management\Domain\Services;


use Illuminate\Database\Eloquent\Model;

class FileService
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * FileService constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function getFiles(int $organizationId)
    {
        return  $this->model
            ->select('files.*')
            ->join('programs', 'programs.id', '=', 'files.program_id')
            ->join('users', 'users.id', '=', 'programs.manager_id')
            ->where('users.organization_id', $organizationId)
            ->get();
    }
}
