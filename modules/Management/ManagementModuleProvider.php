<?php

namespace Modules\Management;

use App\Infrastructure\Providers\ModuleServiceProvider;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Domain\Models\File;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Management\Domain\Services\FileService;
use Modules\Management\Domain\Services\LicenseService;
use Modules\Management\Domain\Services\UserFeedbackService;
use Modules\Management\Domain\Services\UserManagementService;
use Modules\Matching\Domain\Models\Match;

class ManagementModuleProvider extends ModuleServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(UserManagementService::class)->needs(Model::class)->give(User::class);
        $this->app->when(UserFeedbackService::class)->needs(Model::class)->give(UserFeedback::class);
        $this->app->when(FileService::class)->needs(Model::class)->give(File::class);
        $this->app->when(LicenseService::class)->needs(Model::class)->give(Match::class);
    }
}
