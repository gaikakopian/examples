<?php

namespace Modules\Management\Tests\Http;


use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\File;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;

class GetDownloadsListTest extends EndpointTest
{
    public function test_it_lists_downloads()
    {
        factory(File::class)->create(['category' => 'video']);

        /** @var User $user */
        $user = $this->user;

        $managerRole = Role::whereName(Role::ROLES['manager'])->first();
        $this->user->roles()->attach($managerRole);


        $response = $this->actingAs($this->user)->getJson(route('management.getDownloadsList'));
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' =>
                [
                    'video' =>
                        [
                            [
                                'id',
                                'title',
                                'source',
                                'link',
                                'cover',
                                'targetAudience',
                                'programId',
                                'mime',
                                'lectionNumber',
                                'order',
                                'placement',
                                'language',
                                'filesize',
                            ]
                        ]
                ]
        ]);
    }
}
