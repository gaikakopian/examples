<?php

namespace Modules\Management\Tests\Http;


use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Role;

class GetNewestUsersListTest extends EndpointTest
{
    public function test_it_lists_newest_users()
    {
        $managerRole = Role::whereName(Role::ROLES['manager'])->first();
        $this->user->roles()->attach($managerRole);


        $response = $this->actingAs($this->user)->getJson(route('management.getNewestUsers'));
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


        $response->assertJsonStructure([
            'data' =>
                [
                    [
                        'id',
                        'displayName',
                        'firstName',
                        'lastName',
                    ]
                ]
        ]);
    }
}
