<?php

namespace Modules\Management\Tests\Http;


use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Role;

class GetRegistrationLinksTest extends EndpointTest
{
    public function test_it_lists_users_status()
    {
        $managerRole = Role::whereName(Role::ROLES['manager'])->first();
        $this->user->roles()->attach($managerRole);


        $response = $this->actingAs($this->user)->getJson(route('management.getRegistrationCodes'));
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJsonStructure([
            'data' => [
                [
                    'id'
                ]
            ]
        ]);
    }
}
