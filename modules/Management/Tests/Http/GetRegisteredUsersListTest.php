<?php

namespace Modules\Management\Tests\Http;


use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Role;

class GetRegisteredUsersListTest extends EndpointTest
{
    public function test_it_lists_registered_users()
    {
        $managerRole = Role::whereName(Role::ROLES['manager'])->first();
        $this->user->roles()->attach($managerRole);



        $response = $this->actingAs($this->user)->getJson(route('management.getRegisteredUsers'));
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' =>
                [
                    [
                        'id',
                        'displayName',
                        'firstName',
                        'lastName',
                        'email',
                        'gender',
                        'primaryRole',
                        'phoneNumber',
                        'phoneNumberPrefix',

                        'whatsappSetup',
                        'whatsappNumber',
                        'whatsappNumberPrefix',
                        'address',
                        'state',
                        'city',
                        'postcode',
                        'country',
                        'countryOfOrigin',
                        'birthday',
                        'brand',
                        'language',
                        'acceptSms',
                        'acceptEmail',
                        'acceptPush',
                        'preferredChannel',
                        'countForReporting',
                        'avatar',
                        'points',
                        'position',
                        'aboutMe',
                        'askForTosAcceptance',
                        'pointsNextLevel',
                        'level',
                        'emailOptinAt',
                        'createdAt',
                    ]
                ]
        ]);
    }
}
