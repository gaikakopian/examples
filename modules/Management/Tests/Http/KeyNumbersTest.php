<?php

namespace Modules\Management\Tests\Http;


use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Modules\Core\Domain\Models\Role;

class KeyNumbersTest extends EndpointTest
{
    public function test_it_get_key_numbers()
    {
        $managerRole = Role::whereName(Role::ROLES['manager'])->first();
        $this->user->roles()->attach($managerRole);



        $response = $this->actingAs($this->user)->getJson(route('management.keyNumbers'));
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'engagedUsersCount',
                'countriesCount',
                'happyFeedbacksPercentage',
                'mentoringHours'
            ]
        ]);
    }
}
