<?php

namespace Modules\Management\Tests\Http;


use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Role;

class GetUsersStatusTest extends EndpointTest
{
    public function test_it_lists_users_status()
    {
        $managerRole = Role::whereName(Role::ROLES['manager'])->first();
        $this->user->roles()->attach($managerRole);


        $response = $this->actingAs($this->user)->getJson(route('management.getUsersStatus'));
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'registered',
                'trained',
                'matching',
                'active',
                'completed'
            ]
        ]);
    }
}
