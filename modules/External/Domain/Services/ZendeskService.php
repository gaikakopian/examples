<?php

namespace Modules\External\Domain\Services;

use App\Exceptions\Client\NotAuthenticatedException;
use App\Exceptions\Server\ServerException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\SmsLog;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

/**
 * Class WaboboxService
 * @docs: https://www.waboxapp.com/assets/doc/waboxapp-API-v2.pdf
 * @package App\Services
 */
class ZendeskService
{

    private $token = null;
    private $user = null;
    private $baseUrl = null;

    private $enabled = false;


    /**
     * An HTTP client instance for making requests to the conference service.
     *
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;

    /**response
     * Construct an instance of the Service.
     *
     * @param \GuzzleHttp\Client $httpClient
     */
    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
        $this->enabled = Config::get('zendesk.enabled');
        $this->user = Config::get('zendesk.user');
        $this->token = Config::get('zendesk.token');
        $this->baseUrl = self::getBaseUrl();


    }

    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param Collection $users
     * @return mixed|null|\Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function updateOrCreateUsers(Collection $users)
    {
        if ($users->count() > 100) {
            throw new \Exception('Maximum of 100 users at once allowed');
        }

        $rows = [];
        foreach ($users as $user) {
            $rows[] = $this->transformUser($user);
        }
        $payload = [
            'users' => $rows
        ];

        $response = $this->makeRequest('users/create_or_update_many.json', $payload);

        if (!$response) {
            throw new \Exception('Error on sending users to zendesk - no response');
        }

        if ($response->getStatusCode() !== 200 && $response->getStatusCode() !== 201) {
            throw new \Exception('Error on sending users to zendsk:' . $response->getBody());
        }

        return $response;

    }

    public function updateOrCreateUser(User $user)
    {

        $payload = [
            'user' => $this->transformUser($user)
        ];

        $response = $this->makeRequest('users/create_or_update.json', $payload);

        if ($response->getStatusCode() !== 200 && $response->getStatusCode() !== 201) {
            throw new \Exception('Error on sending users to zendsk:' . $response->getBody());
        }

        $responseBody = $response->getBody()->getContents();
        $response = json_decode($responseBody);
        $user->zendesk_user_id = $response->user->id;

        $user->zendesk_last_sync = Carbon::now();
        $user->save();

        return $response;

    }

    public function createOrUpdateOrganization(Organization $organization)
    {
//        organizations/create_many.json

        $payload = [
            'organization' => $this->transformOrganization($organization)
        ];

        $response = $this->makeRequest('organizations/create_or_update.json', $payload);

        $responseBody = $response->getBody()->getContents();
        $response = json_decode($responseBody);

        $organization->zendesk_id = $response->organization->id;
        $organization->zendesk_last_sync = Carbon::now();
        $organization->save();


    }

    protected function transformUser(User $user)
    {
        return [
            'name' => $user->getDisplayNameAttribute(),
            'email' => $user->email,
            'external_id' => $user->id,
            'verified' => true,
            'organization' => ['name' => $user->organization->name],
            'user_fields' => ['role' => $user->primary_role]
        ];
    }

    protected function transformOrganization(Organization $organization)
    {
        return [
            'name' => $organization->name,
            'external_id' => (string)$organization->id
        ];
    }

    protected function makeRequest($endpoint, $payload, $method = 'POST')
    {


        $auth = base64_encode($this->user . '/token:' . $this->token);

        $headers = [
            'Authorization' => 'Basic ' . $auth
        ];

        $options = [
            'json' => $payload,
            'headers' => $headers
        ];
        $response = null;

        try {
            $response = $this->httpClient->request($method, $this->baseUrl . $endpoint, $options);
        } catch (\Exception $e) {
            report($e);
        }

        return $response;
    }

    public static function getInstanceUrl()
    {
        $instanceName = Config::get('zendesk.instancename');
        return 'https://' . $instanceName . '.zendesk.com/';
    }

    public static function getBaseUrl()
    {
        $instanceName = Config::get('zendesk.instancename');
        return 'https://' . $instanceName . '.zendesk.com/api/v2/';

    }


}
