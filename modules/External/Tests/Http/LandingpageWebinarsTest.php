<?php

namespace Modules\External\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\Webinar;
use Modules\Matching\Domain\Models\Match;

/**
 * Test the functionality of the `/api/v1/matches/{id}/confirm` endpoint.
 */
class LandingpageWebinarsTest extends EndpointTest
{
    public function test_it_shows_webinar_for_code()
    {
        // Disable model events, as we don't want to call the ConferenceService during this test
        Match::flushEventListeners();


        $code = factory(RegistrationCode::class)
            ->create([
                'auto_enroll_in' => 1,
                'group_id' => 1,
                'primary_role' => Role::ROLES['mentor']
            ]);

        $tomorrow = Carbon::tomorrow();

        // make sure there is a fitting webinar
        factory(Webinar::class)
            ->create([
                'program_id' => $code->auto_enroll_in,
                'target_audience' => $code->primary_role,
                'starts_at' => $tomorrow
            ]);



        // api/v2/external/landingpages/codes/{code}/webinars
        $response = $this->getJson("/api/v2/external/landingpages/codes/" . $code->code . "/webinars");



        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'state',
                    'startsAt',
                    'title',
                    'description'
                ]
            ]
        ]);
    }
}
