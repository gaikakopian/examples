<?php

namespace Modules\External\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\Role;
use Modules\Matching\Domain\Models\Match;

/**
 * Test the functionality of the `/api/v1/matches/{id}/confirm` endpoint.
 */
class LandingpageRegisterTest extends EndpointTest
{
    public function test_it_registers_a_user()
    {
        // Disable model events, as we don't want to call the ConferenceService during this test
        Match::flushEventListeners();


        $code = factory(RegistrationCode::class)
            ->create([
                'auto_enroll_in' => 1,
                'group_id' => 1,
                'primary_role' => Role::ROLES['mentor']
            ]);

        $data = [
            'name' => 'Simon Fakir',
            'email' => 'simon.fakir@volunteer-vision.com',
            'webinarId' => 3
        ];

        // api/v2/external/landingpages/codes/{code}/webinars
        $response = $this->postJson("/api/v2/external/landingpages/codes/" . $code->code . "/register", $data);


        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


        $response->assertJsonStructure([
            'data' =>
                [
                    'id'
                ]

        ]);
    }
}
