<?php

namespace Modules\External\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Matching\Domain\Models\Match;

/**
 * Test the functionality of the `/api/v1/matches/{id}/confirm` endpoint.
 */
class ShowTokboxCredentailsTest extends EndpointTest
{
    public function test_it_shows_tokbox_crendentials()
    {
        // Disable model events, as we don't want to call the ConferenceService during this test
        Match::flushEventListeners();

        $this->markTestSkipped('Should not be used so often');


        $route = route('callcheck.gettokboxcredentials');


//        $response = $this->actingAs($this->user)->getJson($route);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


        $response->assertJsonStructure(['data' => ['sessionId', 'apiKey']]);

    }
}
