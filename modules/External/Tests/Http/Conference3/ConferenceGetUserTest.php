<?php

namespace Modules\External\Tests\Http\Conference3;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Services\ConferenceService;

/**
 * Test the functionality of the `/api/v1/conference/auth/user` endpoint.
 */
class ConferenceGetUserTest extends EndpointTest
{
    /**
     * @var ConferenceService
     */
    protected $conferenceService;

    public function setUp()
    {
        parent::setUp();

        $this->conferenceService = app(ConferenceService::class);
    }

    public function test_it_responds_with_200_when_a_user_may_log_in()
    {

        $response = $this->json('POST', '/api/v2/conference3/users/' . $this->user->id);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
    }
}
