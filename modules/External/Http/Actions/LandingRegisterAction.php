<?php

namespace Modules\External\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use App\Notifications\External\LandingPageRegistrationNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\UserLead;
use Modules\Core\Domain\Models\Webinar;
use Modules\Core\Domain\Services\RegistrationCodeService;

use Modules\Core\Domain\Services\WebinarService;
use Modules\External\Http\Requests\LandingRegisterRequest;
use Modules\Matching\Http\Requests\AddFeedbackRequest;


/**
 * Class CallMatchAction
 * @package Modules\Matching\Http\Actions
 */
class LandingRegisterAction extends Action
{

    /**
     * @var RegistrationCodeService
     */
    private $codeService;


    /**
     * @var
     */
    private $responder;


    public function __construct(
        RegistrationCodeService $codeService,
        WebinarService $webinarService,
        ResourceResponder $responder
    ) {
        $this->codeService = $codeService;
        $this->webinarService = $webinarService;
        $this->responder = $responder;
    }

    /**
     *
     * @param $id
     * @param AddFeedbackRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ClientException
     */
    public function __invoke(string $code, LandingRegisterRequest $request)
    {

        /** @var RegistrationCode $registrationCode */
        $registrationCode = $this->codeService->getByCode($code);
        $webinarId = $request->get('webinar_id');
        $name = $request->get('name');
        $email = $request->get('email');


        /** @var Webinar $webinar */
        $webinar = $webinarId ? $this->webinarService->get($webinarId) : null;
        $programId = $registrationCode->auto_enroll_in;

        if ($programId) {
            $program = Program::find($programId);
            $manager = $program->manager;
            $notification = new LandingPageRegistrationNotification($manager->brand, $code,
                $name, $email, $program, $webinar);
            $manager->notify($notification);
        } else {
            Log::info("Registration code for landing page is not connected to a program " . $programId);
        }


        $lead = new UserLead();
        $lead->fill(['firstName' => $name, 'email' => $email, 'source' => 'landingpage', 'code' => $code]);
        $lead->save();

        return response()->json(['data' => ['id' => $lead->id]]);
    }
}
