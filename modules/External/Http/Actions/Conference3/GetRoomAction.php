<?php

namespace Modules\External\Http\Actions\Conference3;

use App\Exceptions\Client\NotAuthorizedException;
use App\Exceptions\Client\NotFoundException;
use App\Infrastructure\Http\Action;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\UserService;
use Modules\External\Domain\Services\Conference3Service;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Twilio\TwiML\Voice\Conference;

/**
 * Sends room details to conference room to make sure
 * it has the correct content etc.
 * Class GetRoomAction
 * @package Modules\External\Http\Actions\Conference3
 */
class GetRoomAction extends Action
{
    /**
     * An instance of the MatchService.
     *
     * @var MatchService
     */
    protected $matchService;

    protected $conference3Service;

    /**
     * Construct an instance of the action.
     *
     * @param MatchService $matchService
     */
    public function __construct(MatchService $matchService, Conference3Service $conference3Service)
    {
        $this->matchService = $matchService;
        $this->conference3Service = $conference3Service;
    }

    /**
     * Handle the request.
     *
     * @param Request $request
     * @return Response
     * @throws NotAuthorizedException
     */
    public function __invoke(Request $request, $id)
    {

        if (strpos($id, 'DEMO') !== false) {
            $data = $this->demoException($id);
            return response()
                ->json(['data' => $data]);

        }


        /** @var Match $match */
        $match = $this->find_and_try_match($id);

        if (!$match) {
            throw new NotFoundHttpException('This roomId is unknown');
        }


        /** @var Enrollment $enrollment */
        $enrollment = $match->getMentorParticipation()->enrollment;

        /** @var Program $program */
        $program = $enrollment->program;

        /** @var Organization $organisation */
        $organisation = $enrollment->user->organization;

        $provider = $organisation->video_provider ?: 'twilio';


        $response = [
            'content' =>
                [
                    'entry' => $program->conference_entry_url,
                    'root' => $program->conference_entry_url
                ],
            'access' => 'private', // should be public for demo;
            'video' => ['provider' => $provider]
        ];

        return response()
            ->json(['data' => $response]);


    }

    private function demoException($roomName)
    {
        $settigns = $this->conference3Service->findSettingsForDemoRoom($roomName);
        $settigns['access'] = 'public';
        $settigns['video'] = ['provider' => 'twilio'];
        return $settigns;
    }

    private function find_and_try_match($externalRoomId)
    {
        try {
            $match = $this->matchService->getByExternalRoomId($externalRoomId);
        } catch (ModelNotFoundException $e) {
            // match does not exist
            return false;
        }
        return $match;
    }

}
