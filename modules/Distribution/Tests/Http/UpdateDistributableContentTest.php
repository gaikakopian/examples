<?php

namespace Modules\Distribution\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;
use Modules\Distribution\Domain\Services\DistributionService;

class UpdateDistributableContentTest extends EndpointTest
{
    protected $url = '/api/v2/distribution/distributable-content';

    public function test_it_denies_access_to_unauthenticated_users()
    {
        $response = $this->json('POST', $this->url . '/branded-documents');
        $response->assertStatus(401);
    }

    public function test_it_denies_access_to_unauthorized_users()
    {
        $response = $this->actingAs($this->user)->json('POST', $this->url . '/branded-documents');
        $response->assertStatus(403);
    }

    public function test_it_fails_with_a_bad_type()
    {
        $distributor = factory(User::class)->create([
            'primary_role' => Role::ROLES['distributionservice']
        ]);
        $response = $this->actingAs($distributor)->json('POST', $this->url . '/bad-type');
        $response->assertStatus(404);
    }

    public function test_it_updates_branded_documents()
    {
        $distributor = factory(User::class)->create([
            'primary_role' => Role::ROLES['distributionservice']
        ]);

        $samples = [
            [
                'id' => 14,
                'attributes' => [
                    'content' => 'replaced by distributor',
                    'deleted_at' => Carbon::now()->toDateTimeString(),
                    'markdown' => 'Mark me up before you go go',
                ],
                'document' => factory(BrandedDocument::class)->create(['distributor_source_id' => 14])
            ],
            [
                'id' => 20,
                'attributes' => [
                    'content' => 'replaced by external distributor',
                    'deleted_at' => Carbon::now()->toDateTimeString(),
                    'markdown' => 'Mark me up before you go!',
                ],
                'document' => factory(BrandedDocument::class)->create(['distributor_source_id' => 20])
            ]
        ];

        foreach ($samples as ['id' => $id, 'attributes' => $attributes, 'document' => $document]) {
            $payload = array_merge($attributes, ['id' => $id]);
            $response = $this
                ->actingAs($distributor)
                ->json('POST', $this->url . '/branded-documents', ['data' => $payload]);
            $response->assertStatus(200);

            $targetAttributes = array_merge($document->toArray(), $attributes);

            $this->assertDatabaseHas('branded_documents', ['distributor_source_id' => $id]);

            $this->assertDatabaseHas('branded_documents', $targetAttributes);

            $this->assertEquals($response->getContent(),'ok', $response->getContent());
        }
    }
}
