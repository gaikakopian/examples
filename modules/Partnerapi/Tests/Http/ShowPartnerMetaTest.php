<?php

namespace Modules\Partnerapi\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

/**
 * Test the functionality of the `/api/v1/matches/{id}/confirm` endpoint.
 */
class ShowPartnerMetaTest extends EndpointTest
{



    public function test_it_shows_meta_info()
    {

        // CustardappleService

        $user = User::query()->where('email', 'kironapi@volunteer-vision.com')->firstOrFail();

        $route = route('partnerapi.meta');
        $response = $this->actingAs($user)->getJson($route);
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJsonStructure([
            'data' => []
        ]);

    }

}
