<?php

namespace Modules\Partnerapi\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use App\Notifications\External\LandingPageRegistrationNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\UserLead;
use Modules\Core\Domain\Models\Webinar;
use Modules\Core\Domain\Services\RegistrationCodeService;

use Modules\Core\Domain\Services\WebinarService;
use Modules\External\Http\Requests\LandingRegisterRequest;
use Modules\Matching\Http\Requests\AddFeedbackRequest;


/**
 * Class CallMatchAction
 * @package Modules\Matching\Http\Actions
 */
class SamlErrorAction extends Action
{

    /**
     * @var RegistrationCodeService
     */
    private $codeService;


    /**
     * @var
     */
    private $responder;


    public function __construct(
        RegistrationCodeService $codeService,
        WebinarService $webinarService,
        ResourceResponder $responder
    ) {
        $this->codeService = $codeService;
        $this->webinarService = $webinarService;
        $this->responder = $responder;
    }

    /**
     *
     * @param $id
     * @param AddFeedbackRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ClientException
     */
    public function __invoke()
    {

        return response()->json(['message' => 'error']);
    }
}
