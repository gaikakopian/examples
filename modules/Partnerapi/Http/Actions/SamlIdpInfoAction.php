<?php

namespace Modules\Partnerapi\Http\Actions;

use Aacotroneo\Saml2\Events\Saml2LoginEvent;
use Aacotroneo\Saml2\Saml2Auth;
use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;


use Modules\Core\Domain\Services\UserService;
use Modules\Matching\Http\Requests\AddFeedbackRequest;
use Modules\Partnerapi\Domain\Services\SamlService;


/**
 * Class CallMatchAction
 * @package Modules\Matching\Http\Actions
 */
class SamlIdpInfoAction extends Action
{

    private $samlService;
    private $saml2Auth;

    private $userService;


    public function __construct(
        SamlService $samlService,
        Saml2Auth $saml2Auth,
        UserService $userService,
        ResourceResponder $responder
    )
    {
        $this->samlService = $samlService;
        $this->userService =  $userService;

        $this->responder = $responder;
        $this->saml2Auth = $saml2Auth;
    }

    /**
     *
     * @param $id
     * @param AddFeedbackRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ClientException
     * @throws \Exception
     */
    public function __invoke($slug)
    {

        $organizationSaml  = $this->samlService->findBySlug($slug);
        $settings = $this->samlService->toSettings($organizationSaml);

        // @todo: inject settings;


        return response($settings, 200);

    }
}
