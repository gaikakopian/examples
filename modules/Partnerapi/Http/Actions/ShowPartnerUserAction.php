<?php

namespace Modules\Partnerapi\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;

use App\Services\WaboboxService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Log;


use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\SmsLog;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Services\ConferenceService;
use Modules\Core\Domain\Services\UserFeedbackService;
use Modules\Core\Domain\Services\UserService;
use Modules\Partnerapi\Http\Resources\PartnerUserHttpResource;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Matching\Http\Requests\AddFeedbackRequest;
use Modules\Matching\Http\Resources\MatchHttpResource;
use App\Exceptions\Client\NotAuthorizedException;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Scheduling\Domain\Models\Appointment;

/**
 * Class CallMatchAction
 * @package Modules\Matching\Http\Actions
 */
class ShowPartnerUserAction extends Action
{

    protected $userService;
    protected $matchService;
    protected $responder;


    public function __construct(
        MatchService $matchService,
        UserService $userService,
        ResourceResponder $responder
    )
    {
        $this->matchService = $matchService;
        $this->userService = $userService;
        $this->responder = $responder;
    }

    /**
     *
     * @param $id
     * @param AddFeedbackRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ClientException
     */
    public function __invoke($id, Request $request)
    {

        $user = Auth::user();

        if (!$user->hasRole('partnerapi')){
            throw new NotAuthorizedException('You do not have partner api access');
        }

        /** @var User $partnerUser */
        $partnerUser = $this->userService->get($id);

        if ($partnerUser->organization_id != $user->organization_id) {
            throw new NotAuthorizedException();
        }

        $partnerUser->load(
            'enrollments',
            'enrollments.participations',
            'enrollments.participations.match',
            'enrollments.participations.match.appointments'
        );

        return $this->responder->send($partnerUser, PartnerUserHttpResource::class);


    }


}
