<?php

namespace Modules\Partnerapi\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Http\Resources\CommentHttpResource;
use Modules\Core\Http\Resources\EnrollmentHttpResource;
use Modules\Core\Http\Resources\StateLogHttpResource;

class PartnerProgramHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'displayName' => $this->title,
            'description' => $this->description,
            'code' => $this->code,
            'logo' => $this->logo,
        ];
    }
}
