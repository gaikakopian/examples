<?php

namespace Modules\Partnerapi\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Http\Resources\CommentHttpResource;
use Modules\Core\Http\Resources\EnrollmentHttpResource;
use Modules\Core\Http\Resources\StateLogHttpResource;

class PartnerMatchHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'lectionsCompleted' => $this->lections_completed,
            'lastStateChangeAt' => $this->last_state_change_at ? $this->last_state_change_at->toIso8601String() : $this->last_state_change_at,
            'state' => $this->state,
            'appointments' => PartnerAppointmentHttpResource::collection($this->whenLoaded('appointments')),
            'createdAt' => $this->created_at->toIso8601String(),
            'updatedAt' => $this->updated_at->toIso8601String(),


        ];
    }
}
