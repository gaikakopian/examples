<?php

namespace Modules\Partnerapi\Domain\Services;

use GuzzleHttp\Client;

use http\Url;
use Modules\Core\Domain\Services\UserService;
use OneLogin\Saml2\Auth;
use Aacotroneo\Saml2\Saml2Auth;


class SamlConfigProvider
{

    public function __construct(Client $httpClient, UserService $userService)
    {
        $this->httpClient = $httpClient;
        $this->userService = $userService;


    }

    public function getSaml2ConfigBySlug($slug)
    {
//        try {
//            $idpResolver = $this->getIdpResolver();
//            $idpSettings = call_user_func([$idpResolver, 'idpSettings'], $slug);
//        } catch (ModelNotFoundException $exception){
//            throw new ValidationException( "Provided idp client not found");
//        }

        /** @var SamlService $samlService */
        $samlService = app(SamlService::class);
        $organizationSaml = $samlService->findBySlug($slug);

        $idpSettings = $samlService->toSettings($organizationSaml);
        $spSettings = $this->getBaseSettings($slug);

        $settings = array_merge($spSettings, ['idp' => $idpSettings]);

        return $settings;
    }

    /**
     * @param $slug
     * @return Saml2Auth
     * @throws \OneLogin\Saml2\Error
     */
    public function getSaml2AuthBySlug($slug)
    {
        // putting saml auth on request;
        $settings = $this->getSaml2ConfigBySlug($slug);
        $saml2 = new Auth($settings);
        return new Saml2Auth($saml2);
    }


    protected function getBaseSettings($slug)
    {
        $config = config('saml2_settings');


        if (empty($config['sp']['entityId'])) {
            $config['sp']['entityId'] = route('saml.metadata', ['slug' => $slug]);
        }

        if (empty($config['sp']['assertionConsumerService']['url'])) {
            $config['sp']['assertionConsumerService']['url'] = route('saml.acs', ['slug' => $slug]);
        }
        if (!empty($config['sp']['singleLogoutService']) &&
            empty($config['sp']['singleLogoutService']['url'])) {
            $config['sp']['singleLogoutService']['url'] = route('saml.sls', ['sls' => $slug]);
        }
        if (strpos($config['sp']['privateKey'], 'file://') === 0) {
            $config['sp']['privateKey'] = $this->extractPkeyFromFile($config['sp']['privateKey']);
        }
        if (strpos($config['sp']['x509cert'], 'file://') === 0) {
            $config['sp']['x509cert'] = $this->extractCertFromFile($config['sp']['x509cert']);
        }
        return $config;
    }

    protected function extractPkeyFromFile($path)
    {
        $res = openssl_get_privatekey($path);
        if (empty($res)) {
            throw new \Exception('Could not read private key-file at path \'' . $path . '\'');
        }
        openssl_pkey_export($res, $pkey);
        openssl_pkey_free($res);
        return $this->extractOpensslString($pkey, 'PRIVATE KEY');
    }

    protected function extractCertFromFile($path)
    {
        $res = openssl_x509_read(file_get_contents($path));
        if (empty($res)) {
            throw new \Exception('Could not read X509 certificate-file at path \'' . $path . '\'');
        }
        openssl_x509_export($res, $cert);
        openssl_x509_free($res);
        return $this->extractOpensslString($cert, 'CERTIFICATE');
    }

    protected function extractOpensslString($keyString, $delimiter)
    {
        $keyString = str_replace(["\r", "\n"], "", $keyString);
        $regex = '/-{5}BEGIN(?:\s|\w)+' . $delimiter . '-{5}\s*(.+?)\s*-{5}END(?:\s|\w)+' . $delimiter . '-{5}/m';
        preg_match($regex, $keyString, $matches);
        return empty($matches[1]) ? '' : $matches[1];
    }

}
