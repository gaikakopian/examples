<?php

namespace Modules\Partnerapi\Domain\Models;

use App\Services\MultiTenant\BelongsToTenant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Matching\Domain\Interfaces\HasAvailabilityInterface;
use Modules\Matching\Domain\Models\Traits\HasAvailability;

class PartnerAccess extends Model
{

    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'organization_id',
        'registration_code_id',
        'api_base',
        'secret',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];



    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */
    /**
     * A Group belongs to an Organization.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    /**
     * A User can be member of a Brand.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function registrationCode()
    {
        return $this->belongsTo(RegistrationCode::class);
    }


}
