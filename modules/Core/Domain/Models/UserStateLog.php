<?php

namespace Modules\Core\Domain\Models;

use App\Infrastructure\Traits\LogsState;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Domain\Models\User;

class UserStateLog extends Model
{
    use LogsState;

    protected $fillable = ['user_id', 'actor_id', 'transition', 'from', 'to'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function actor()
    {
        return $this->belongsTo(User::class);
    }
}
