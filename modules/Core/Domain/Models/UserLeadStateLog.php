<?php

namespace Modules\Core\Domain\Models;

use App\Infrastructure\Traits\LogsState;
use Illuminate\Database\Eloquent\Model;

class UserLeadStateLog extends Model
{
    use LogsState;

    protected $fillable = ['lead_id', 'actor_id', 'transition', 'from', 'to'];

    public function lead()
    {
        return $this->belongsTo(UserLead::class);
    }
}
