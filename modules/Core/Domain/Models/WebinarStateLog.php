<?php

namespace Modules\Core\Domain\Models;

use App\Infrastructure\Traits\LogsState;
use Illuminate\Database\Eloquent\Model;

class WebinarStateLog extends Model
{
    use LogsState;

    protected $fillable = ['webinar_id', 'actor_id', 'transition', 'from', 'to'];

    public function webinar()
    {
        return $this->belongsTo(Webinar::class);
    }
}
