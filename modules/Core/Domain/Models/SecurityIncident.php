<?php

namespace Modules\Core\Domain\Models;

use Illuminate\Database\Eloquent\Model;

class SecurityIncident extends Model
{

    const TYPES = [
        'INVALID_LOGIN' => 'INVALID_LOGIN',
        'INVALID_CODE' => 'INVALID_CODE',
        'IMPERSONATE' => 'IMPERSONATE'
    ];

    protected $fillable = [
        'affected_user_id',
        'type',
        'meta',
        'created_at',
        'updated_at'
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'meta' => 'json',
    ];
}
