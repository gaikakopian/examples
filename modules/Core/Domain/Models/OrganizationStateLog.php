<?php

namespace Modules\Core\Domain\Models;

use App\Infrastructure\Traits\LogsState;
use Illuminate\Database\Eloquent\Model;

class OrganizationStateLog extends Model
{
    use LogsState;

    protected $fillable = ['organization_id', 'actor_id', 'transition', 'from', 'to'];

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }
}
