<?php

namespace Modules\Core\Domain\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Domain\Models\Traits\HasImageAttachments;

class SmsLog extends Model
{

    const WHATSAPP_STATUS =
        [
            'IdontKnow' => -1,
            'SENDING' => 0,
            'SENT_TO_WHATSAPP' => 1,
            'DELIVERED' => 2,
            'READ_BY_RECEIPIENT' => 3 // only if user accepts;
        ];

    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', //
        'from',
        'to',
        'channel', // sms, whatsapp etc,
        'direction',
        'body',
        'sent_at',
        'user_id',
        'status',
        'whatsapp_uid',
        'whatsapp_status', // -1; 0; 3 ??? document meanings..
        'unique_message_id'
    ];


    /**
     * The attributes that should be cast to another type.
     *
     * @var array
     */
    protected $casts = [
        'sent_at' => 'datetime',
        'reviewed_at' => 'datetime'
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }


}
