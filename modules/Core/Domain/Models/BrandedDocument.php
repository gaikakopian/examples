<?php

namespace Modules\Core\Domain\Models;

use App\Compilers\StringCompiler;
use App\Domain\Models\BaseModel;
use App\Exceptions\Server\ParseErrorException;
use App\Services\ClassFinder;
use App\Services\StringHelper;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;
use Modules\Distribution\Domain\Contracts\Distributable;
use Modules\Distribution\Domain\Traits\HasDistributionLogic;
use Parsedown;
use ReflectionClass;

/**
 * Class BrandedDocument
 *
 *  type: ['email', 'article', 'sms']
 *
 * @package Modules\Core\Domain\Models
 */
class BrandedDocument extends BaseModel implements Distributable
{
    use SoftDeletes;
    use HasDistributionLogic;

    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
     */

    const TYPES = ['article' => 'article', 'sms' => 'sms', 'email' => 'email', 'whatsapp' => 'whatsapp'];

    const KNOWN_ARTICLES = [

        'certificate_text' => 'Text shown on Mentor/Mentee Certificate',

        'supervisor_account_created' => 'Supervisor: Account was created',
        'supervisor_weekly_report' => 'Supervisor: Weekly report',

        // article
        'supervisor_help' => 'Article: Help in the supervisor account',
        'user_help' => 'Article: General Help',
        'next_steps' => 'Article: Shown in Enrollment, if there is no webinar',
        'statementofgoodconduct' => 'Article: Statement of good conduct',
        'terms' => 'Article: Terms of Service',
        'privacypolicy' => 'Article: Privacy Policy',

        // articles per program:
        'registration_welcome' => 'Article: First page in Regisration',
        'program_info' => 'Article: Program Info for interessted people',
        'program_requirements' => 'Article: What are the requirements to participate?',
        'enrollment_intro' => 'Article: Explanation befor the enrollment starts',
        'waitinglist_info' => 'Shown if user sees the waiting list',
    ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'type',
        'subject',
        'content',
        'language',
        'markdown',
        'views',
        'program_id',
        'internal_title',
        'audience',
        'brand_id',
        'program_id',
        'reviewed_at',
        'distribute_to_tenants',
        'distributor_source_id',
        'deleted_at'
    ];

    /**
     * used by the eloquent service to fuzzy search entities
     * @var array
     */
    public $searchable = [
        'key',
        'internal_title',
        'subject'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['deleted_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'reviewed_at',
        'created_at',
        'updated_at'
    ];


    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
     */

    /**
     * A BrandedDocument can belong to one Brand.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * A BrandedDocument can belong to one Brand.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reviewer()
    {
        return $this->belongsTo(User::class);
    }

    /**
     *  A BrandedDocument can belong to one Program.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function program()
    {
        return $this->belongsTo(Program::class);
    }

    /*
    |--------------------------------------------------------------------------
    | Query Scopes
    |--------------------------------------------------------------------------
     */

    /**
     * Limit a query to documents of type `article`.
     *
     * @param Builder $query
     * @return void
     */
    public function scopeArticles(Builder $query)
    {
        $query->where('type', 'article');
    }

    /*
    |--------------------------------------------------------------------------
    | Custom Getters
    |--------------------------------------------------------------------------
     */

    /**
     * Replaces the placeholders with concrete dataand returns the final string.
     *
     * Example: ['the_program' => $program]
     * 'the_program' might be used within the subject (e.g. $the_program->title)
     * $program contains the data that will replace the placeholders
     *
     * @param Array $data
     * @return String
     */
    public function parseSubject(array $data = [])
    {
        if (!$this->subject) {
            return '';
        }

        try {
            return StringCompiler::render($this->subject, $data);
        } catch (\Exception $e) {
            Log::warning(
                "[BrandedDocuments] Error while parsing the subject of BrandedDocument #{$this->id}.",
                ['data' => $data]
            );
            report($e);
        }

        return $this->subject;
    }

    /**
     * Replaces the placeholders with concrete data and returns the final string.
     *
     * Example: ['the_program' => $program]
     * 'the_program' might be used within the content (e.g. $the_program->title)
     * $program contains the data that will replace the placeholders
     *
     * @param Array $data
     * @return String
     */
    public function parseContent(array $data = [], $verbose = false)
    {
        if (!$this->content) {
            return '';
        }


        try {
            return StringCompiler::render($this->content, $data);
        } catch (\Exception $e) {
            Log::error(
                "[BrandedDocuments] Error while parsing the content of BrandedDocument #{$this->id}.",
                ['data' => $e->getMessage(), 'template' => $this->content]
            );

            throw new ParseErrorException($e->getMessage() . " in file " . $this->id . ":" . $this->key);

        }

        return $this->content;
    }

    public function isNotification()
    {
        return in_array($this->type, [self::TYPES['email'], self::TYPES['sms'], self::TYPES['whatsapp']]);

    }

    public static function getAllKnownKeysWithComments()
    {
        $notificationClasses = ClassFinder::getClassesInNamespace('app/Notifications');
        $classesWithComment = [];

        foreach ($notificationClasses as $class) {
//            $comment$comment = $class::getComment();
            $comment = method_exists($class, 'getComment') ? $class::getComment() : 'no comment';
            $key = StringHelper::classNameToDocumentKey($class);;
            $classesWithComment[$key] = $comment;
        }
        return array_merge($classesWithComment, self::KNOWN_ARTICLES);

    }

    public static function getAllKnownKeys()
    {
        $notificationClasses = ClassFinder::getClassesInNamespace('app/Notifications');
        $classesWithComment = [];
        foreach ($notificationClasses as $class) {
            $classesWithComment[] = StringHelper::classNameToDocumentKey($class);;
        }
        $articleKeys = array_keys(self::KNOWN_ARTICLES);

        return array_merge($classesWithComment, $articleKeys);
    }

    public function parseMarkdown()
    {

        $Parsedown = new Parsedown();

        $this->content = $Parsedown->text($this->markdown);
        $this->content = str_replace('::', '->', $this->content);

        return $this->content;
    }
}
