<?php

namespace Modules\Core\Domain\Models;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    /**
     *  Activities
     *
     * @var array
     */
    const ACTIVITIES = [
        'USER_REGISTERED' => 'USER_REGISTERED', // done
        'USER_PARTICIPATED_IN_PROGRAM' => 'USER_PARTICIPATED_IN_PROGRAM',
        'ENROLLMENT_DONE' => 'ENROLLMENT_DONE', // done
        'ENROLLMENT_READY' => 'ENROLLMENT_READY', // done
        'COMPLETED_FIRST_SESSION' => 'COMPLETED_FIRST_SESSION',
        'COMPLETED_SESSION' => 'COMPLETED_SESSION',

        'MATCH_ACCEPTED' =>'MATCH_ACCEPTED',
        'MATCH_CONFIRMED_MENTEE' =>'MATCH_CONFIRMED_MENTEE', // done
        'MATCH_CONFIRMED_MENTOR' =>'MATCH_CONFIRMED_MENTOR', // done
        'MATCH_OFFERED' => 'MATCH_OFFERED', // done for mentors
        'MATCH_REJECTED' => 'MATCH_REJECTED',

        'APPOINTMENT_SCHEDULED' => 'APPOINTMENT_SCHEDULED', // done
        'APPOINTMENT_CANCELED' => 'APPOINTMENT_CANCELED', // done
        'APPOINTMENT_COMPLETED' => 'APPOINTMENT_COMPLETED', // done

        'INVITED_USER' => 'INVITED_USER', // done

        'TRAINING_COMPLETED' => 'TRAINING_COMPLETED',

        'JOINED_COMMUNITY' => 'JOINED_COMMUNITY',
    ];
    public static $ACTIVITY_CONFIG = [
        'APPOINTMENT_SCHEDULED' => ['type' => 'success'],
        'APPOINTMENT_CANCELED' => ['type' => 'cancel'],
        'MATCH_REJECTED' => ['type' => 'cancel'],
        'MATCH_ACCEPTED' => ['type' => 'success'],
        'MATCH_CONFIRMED_MENTOR' => ['type' => 'success'],
        'MATCH_CONFIRMED_MENTEE' => ['type' => 'success'],
        'USER_REGISTERED' => ['type' => 'success'],
        'JOINED_COMMUNITY' => ['type' => 'success'],
        'DEFAULT' => ['type' => 'success'],
    ];


    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'name', 'variables'];

    /**
     * The fields that should be cast to a specific type.
     *
     * @var array
     */
    protected $casts = [
        'variables' => 'array'
    ];

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    /**
     * An Activity belongs to a User.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
