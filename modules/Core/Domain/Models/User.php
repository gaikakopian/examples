<?php

namespace Modules\Core\Domain\Models;

use App\Exceptions\Client\NotAuthenticatedException;
use App\Infrastructure\Traits\Statable;
use App\Interfaces\Notifications\HasNotificationSettings;

use App\Reminders\Traits\Remindable;
use App\Services\MultiTenant\BelongsToTenant;
use App\Services\MultiTenant\TenantScope;
use App\Services\StringHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Modules\Admin\Domain\Models\CoordinatorTodo;
use Modules\Community\Domain\Models\Chatroom;
use Modules\Core\Domain\Models\Traits\Commentable;
use Modules\Core\Domain\Models\Traits\HasImageAttachments;

use Modules\External\Domain\Services\ZendeskService;
use Modules\Matching\Domain\Interfaces\HasAvailabilityInterface;
use Modules\Matching\Domain\Models\Traits\HasAvailability;
use OwenIt\Auditing\Contracts\Auditable;

class User extends Authenticatable implements HasNotificationSettings, HasAvailabilityInterface, Auditable
{
    use HasApiTokens;
    use Notifiable;
    use SoftDeletes;
    use HasImageAttachments;
    use Commentable;
    use Statable;
    use HasAvailability;
    use BelongsToTenant;
    use Remindable;
    use \OwenIt\Auditing\Auditable;

    /*
     * State Machine Configuration
     */

    const HISTORY_MODEL = UserStateLog::class;
    const SM_CONFIG = 'user'; // the SM graph to use

    /*
    |--------------------------------------------------------------------------
    | Constants
    |--------------------------------------------------------------------------
    */

    /**
     * All possible states that this model can have.
     *
     * @var array
     */
    const STATES = [
        'NEW' => 'NEW',
        'INMIGRATION' => 'INMIGRATION',
        'REGISTERED' => 'REGISTERED',
        'QUIT' => 'QUIT',       // user quit by themselves
        'BANNED' => 'BANNED',   // user was disabled (banned)
    ];

    /**
     * All possible states that this model can have.
     *
     * @var array
     */

    const SERVICELEVELS = [
        'normal' => 'normal',
        'noservice' => 'noservice',
        'premium' => 'premium',       // user quit by themselves
        'priority' => 'priority',   // user was disabled (banned)
    ];

    /**
     * All possible states that this model can have.
     *
     * @var array
     */
    const GAMIFICATION_LEVELS = [
        'NEW',
        'BEGINNER',
        'EXPERT'
    ];

    const ROLES = Role::ROLES;

    /**
     * All possible state transitions that can be performed on the model.
     *
     * @var array
     */
    const TRANSITIONS = [
        'complete_registration' => [
            'from' => ['NEW'],
            'to' => 'REGISTERED'
        ],
        'quit' => [
            'from' => ['REGISTERED'],
            'to' => 'QUIT'
        ],
        'confirmmigration' => [
            'from' => ['INMIGRATION'],
            'to' => 'REGISTERED'
        ],
        'return' => [
            'from' => ['QUIT'],
            'to' => 'REGISTERED'
        ],
        'ban' => [
            'from' => ['REGISTERED'],
            'to' => 'BANNED'
        ],
        'unban' => [
            'from' => ['BANNED'],
            'to' => 'REGISTERED'
        ],
    ];

    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
    */

    protected $image_attachments = [
        'avatar' => [
            'path' => 'uploads/user/avatar',
            'defaults' => [
                'small' => '.jpg',
                'medium' => '.jpg',
                'large' => '.jpg',
            ],
            'styles' => [
                'small' => '100',
                'medium' => '500',
                'large' => '1000'
            ],
            'visibility' => 'public',
        ]
    ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'accept_email',
        'accept_push',
        'accept_sms',
        'supervisor_weekly_enabled',
        'supervisor_daily_enabled',
        'sms_disabled',
        'brand_id',

        // sso/saml
        'external_user_id',
        'external_attributes',

        'first_name',
        'last_name',
        'address',
        'city',
        'country',
        'country_of_origin',
        'postcode',

        'gender',
        'phone_number',
        'phone_number_prefix',
        'birthday',

        'preferred_channel',
        'whatsapp_status',
        'whatsapp_setup',
        'whatsapp_number',
        'whatsapp_number_prefix',

        'language',
        'email',

        'servicelevel',
        'is_anonymized',

        'last_login',
        'last_reminder',
//        'last_reminder_type',
        'last_state_change_at',
        'zendesk_last_sync',
        'zendesk_user_id',
        'general_comment',
        'remind_again_at',
        'last_manual_interaction_at',
        'email_optin_at',

        'level',
        'anonymized_reporting',

        'organization_id',
        'password',
        'primary_role',
        'registration_code_id',
        'state',
        'about_me',
        'tenant_id',
        'timezone',
        'count_for_reporting',
        'invite_sent',
        'old_id',
        'tos_accepted_at',
        'pp_accepted_at',
        'chatbot_state',
        'auth_secret',
        'valid_until',

        'lng',
        'lat',
        'community_enabled',

    ];

    /**
     * Attributes to exclude from the Audit.
     *
     * @var array
     */
    protected $auditExclude = [
        'password',
        'level',
        'anonymized_reporting',
        'servicelevel',
        'is_anonymized',
        'primary_role',
        'registration_code_id',
        'timezone',
        'count_for_reporting',
        'invite_sent',
        'language',
        'state',
        'points',
        'timezone',
        'count_for_reporting',
        'invite_sent',
        'last_state_change_at',
        'state',
        'chatbot_state',
        'email_optin_at',
        'points'
    ];

    /**
     * used by the eloquent service to fuzzy search entities
     * @var array
     */
    public $searchable = [
        'first_name',
        'last_name',
        'email'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'supervisor_daily_sent_at',
        'supervisor_weekly_sent_at',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be cast to another type.
     *
     * @var array
     */
    protected $casts = [
        'language' => 'string',
        'accept_sms' => 'boolean',
        'accept_email' => 'boolean',
        'accept_push' => 'boolean',
        'last_manual_interaction_at' => 'datetime',
        'remind_again_at' => 'datetime',
        'last_state_change_at' => 'datetime',
        'zendesk_last_sync' => 'datetime',
        'email_optin_at' => 'datetime',
        'valid_until' => 'datetime',
        'chatbot_state' => 'json',
        'external_attributes' => 'json',
        'community_enabled' => 'boolean',
    ];


    public $firebaseToken;

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    /**
     * A User can be member of a Brand.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * A User can be member of a Brand.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function registrationCode()
    {
        return $this->belongsTo(RegistrationCode::class);
    }

    /**
     * A User is member of exactly one Organization.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    /**
     * A User can have many ProfileFields.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function profileFields()
    {
        return $this->hasMany(ProfileField::class);
    }

    public function callcheckResults()
    {
        return $this->hasMany(UserCallcheckResult::class);
    }

    /**
     * A User can have many Availabilities.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function feedbacks()
    {
        return $this->hasMany(UserFeedback::class);
    }

    /**
     * A User can have many Enrollments.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function enrollments()
    {
        return $this->hasMany(Enrollment::class);
    }

    /**
     * A User can have many Invitations.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invitations()
    {
        return $this->hasMany(UserInvitation::class, 'inviting_user_id');
    }

    /**
     * A User can be a member of many Groups.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function groups()
    {
        return $this->belongsToMany(Group::class)->withPivot('role');
    }

    /**
     * A User can have many chatrooms
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function chatrooms()
    {
        return $this->belongsToMany(Chatroom::class);
    }

    /**
     * A User can have many smslogs
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function smslogs()
    {
        return $this->hasMany(SmsLog::class);
    }

    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_roles');
    }

    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function relatedTodos()
    {
        return $this->hasMany(CoordinatorTodo::class, 'customer_id');
    }

    public function passwordHistories()
    {
        return $this->hasMany(PasswordHistory::class);
    }

    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function launches()
    {
        return $this->hasMany(UserLaunch::class, 'user_id');
    }

    /**
     * A User can have many accepted Terms.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function acceptedTerms()
    {
        return $this->belongsToMany(Terms::class, 'terms_user')
            ->withPivot('accepted_at', 'ip_address');
    }

    /*
    |--------------------------------------------------------------------------
    | Notification Preferences
    |--------------------------------------------------------------------------
    */

    /** {@inheritDoc} */
    public function getPreferredNotificationChannelAttribute()
    {
        return $this->preferred_channel;
    }

    /** {@inheritDoc} */
    public function getAcceptedChannelsAttribute(): array
    {
        $channels = [];
        if ($this->accept_email) {
            $channels[] = 'email';
        }

        if ($this->accept_push) {
            $channels[] = 'push';
        }


        if ($this->getQualifiedWhatsappNumber()) {
            $channels[] = 'whatsapp';
        }


        if ($this->accept_sms && !$this->sms_disabled) {
            // only send sms if user does not have whatsapp...
            if (!in_array('whatsapp', $channels)) {
                $channels[] = 'sms';
            }

        }

        return $channels;
    }

    public function getZendeskLinkAttribute()
    {

        if (!$this->zendesk_user_id) {
            return null;
        }

        $baseurl = ZendeskService::getInstanceUrl();
        return $baseurl . 'agent/users/' . $this->zendesk_user_id . '/requested_tickets';

        // https://volunteer-vision.zendesk.com/agent/users/366539947234


    }

    /** {@inheritDoc} */
    public function getPreferredLanguageAttribute(): string
    {
        if ($this->language) {
            return $this->language;
        }
        return 'en';
    }

    /** {@inheritDoc} */
    public function getDisplayNameAttribute(): string
    {
        return $this->first_name . '  ' . $this->last_name;
    }

    public function getFullContactDetailsAttribute(): string
    {
        return
            'email: ' . $this->email . PHP_EOL .
            'phone: ' . $this->getQualifiedPhoneNumber() . PHP_EOL .
            ($this->getQualifiedWhatsappNumber() ? 'whatsapp:' . $this->getQualifiedWhatsappNumber() : '') . PHP_EOL;
    }

    /** {@inheritDoc} */
    public function getInitialsAttribute(): string
    {
        return substr($this->first_name, 0, 1) . '  ' . substr($this->last_name, 0, 1);
    }

    /**
     * So far the implementation allows only 1 role,
     * but we are going to have multiple in the future.
     *
     * Please use this instead of User.primaryRole ==
     *
     * @param string $role
     */
    public function hasRole(string $role)
    {
        if (!isset(User::ROLES[$role])) {
            throw new \InvalidArgumentException($role . ' does not exist ');
        }
        if ($this->primary_role === $role) {
            return true;
        }

        return $this->roles->pluck('name')->contains($role);
    }
    /*
    |--------------------------------------------------------------------------
    | Accessors & Mutators
    |--------------------------------------------------------------------------
    */

    /**
     * Accessor providing an `avatar` attribute which contains an array of image urls for every defined dimension.
     *
     * @return array
     */
    public function getAvatarAttribute()
    {
        return $this->getImageAttachmentPaths('avatar');
    }

    public function getQualifiedPhoneNumberAttribute()
    {
        return $this->getQualifiedPhoneNumber();
    }

    /**
     * returns phone number including country code
     * @return null|string
     */
    public function getQualifiedPhoneNumber()
    {
        return !empty($this->phone_number) ? '+' . $this->phone_number_prefix . $this->phone_number : null;
    }

    /**
     * returns whatsapp number including country code
     * @return null|string
     */
    public function getQualifiedWhatsappNumber()
    {
        if ($this->whatsapp_setup === 'mobile') {
            return $this->getQualifiedPhoneNumber();
        }
        if ($this->whatsapp_setup === 'notused') {
            return null;
        }
        if (empty($this->whatsapp_number)) {
            return null;
        }

        return '+' . $this->whatsapp_number_prefix . $this->whatsapp_number;
    }

    public function getAuthSecret()
    {

    }

    public function getQualifiedWhatsappNumberAttribute()
    {
        return $this->getQualifiedWhatsappNumber();
    }

    public function routeNotificationForTwilio()
    {
        return $this->getQualifiedPhoneNumber();
    }

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower($value);
    }

    public function requiresTwoFactorAuth(): bool
    {
        //@todo: implemet here, user has Role COORDINATOR/ADMIN ?
        return false;
    }

    public function setWhatsappNumberAttribute($value)
    {
        if (substr($value, 0, 1) === '0') {
            $value = substr($value, 1);
        }

        $this->attributes['whatsapp_number'] = strtolower($value);
    }

    public function setPhoneNumberAttribute($value)
    {
        if (substr($value, 0, 1) === '0') {
            $value = substr($value, 1);
        }

        $this->attributes['phone_number'] = strtolower($value);
    }

    public function setCountryAttribute($value)
    {
        $this->attributes['country'] = strtoupper($value);
    }

    public function getAddressString()
    {
        if (empty($this->postcode) && empty($this->city)) {
            return null;
        }
        $string = '';
        if ($this->address) {
            $string .= $this->address . ',';
        }
        $string .= $this->postcode . ' ';
        if ($this->city) {
            $string .= $this->city . ',';
        }
        if ($this->country) {
            $string .= $this->country;
        }
        return $string;
    }
    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
    */

    /**
     * Find a user by its username for passport login.
     *
     * We implement this method to enable login via email OR phone number.
     * By default, passport would only check against the `email` field.
     *
     * @see \Laravel\Passport\Bridge\UserRepository::getUserEntityByUserCredentials()
     * @param string $username
     * @return self|null
     */
    public function findForPassport(string $username)
    {
        return $this->withoutGlobalScope(TenantScope::class)
            ->where('email', $username)
            ->orWhere('phone_number', $username)
            ->first();
    }

    /*
    |--------------------------------------------------------------------------
    | Role Helpers
    |--------------------------------------------------------------------------
    */

    /**
     * Check whether the User is a mentee.
     *
     * @return bool
     */
    public function isMentee(): bool
    {
        return $this->primary_role === 'mentee';
    }

    /**
     * Check whether the User is a mentor.
     *
     * @return bool
     */
    public function isMentor(): bool
    {
        return $this->primary_role === 'mentor';
    }

    /**
     * Check whether t`he User is an admin.
     *
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->primary_role === 'admin' || $this->hasRole(Role::ROLES['admin']);
    }


    public function emailSecret($action)
    {
        $key = strtolower($this->id . $this->email . $action);
        return substr(sha1($key), 0, 10);
    }

    /**
     * Check whether the User is an admin.
     *
     * @return bool
     */
    public function isCoordinator(): bool
    {
        return $this->hasRole(Role::ROLES['coordinator']);
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        if (!$this->birthday) {
            return null;
        }
        return Carbon::parse($this->birthday)->age;
    }

    /**
     * Check whether the User is an admin.
     *      admin or coodrianteor
     * @return bool
     */
    public function isSupport(): bool
    {
        return $this->isCoordinator() || $this->isAdmin();
    }

    public function hasAdminAccess(): bool
    {
        return $this->isSupport() || $this->isAdmin() || $this->hasRole(Role::ROLES['editor']);
    }

    public function getSaluation($lang = 'en')
    {
        //@todo
        return 'Hello ' . $this->first_name;
    }

    protected function asDateTime($value)
    {
        if (is_string($value)) {
            // parses json zulu strings;
            $date = Carbon::parse($value);
            return $date;
        }
        return parent::asDateTime($value);
    }


    public function anonymize()
    {
        $this->first_name = StringHelper::generateRandomString();
        $this->last_name = StringHelper::generateRandomString();
        $this->email = StringHelper::generateRandomString() . '@' . StringHelper::generateRandomString() . '.com';
        $this->is_anonymized = true;
        $this->address = '';
        $this->phone_number = rand(0, 1111111111);
        $this->whatsapp_number = rand(0, 1111111111);
    }

    /**
     * @throws NotAuthenticatedException
     */
    public function isAllowedToLogin()
    {

        if ($this->is_anonymized) {
            throw new NotAuthenticatedException('ACCOUNT_WAS_ANONYMIZED');
        }
        if ($this->valid_until) {
            if ($this->valid_until->isPast()){
                throw new NotAuthenticatedException('ACCOUNT_IS_NOT_VALID_ANYMORE');

            }
        }
        return true;
//


    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function programs()
    {
        return $this->hasMany(Program::class, 'manager_id');
    }
}
