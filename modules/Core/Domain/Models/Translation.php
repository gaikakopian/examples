<?php

namespace Modules\Core\Domain\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Distribution\Domain\Contracts\Distributable;
use Modules\Distribution\Domain\Traits\HasDistributionLogic;

class Translation extends Model implements Distributable
{
    use SoftDeletes;
    use HasDistributionLogic;

    /**
     * The attributes that are mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['tenant_id', 'locale', 'scope', 'json_value',
        'distribute_to_tenants',
        'distributor_source_id'];

    /**
     * Translation constructor.
     *
     * @param array $fillable
     */
    public function setTranslation($tenant_id, $locale, $scope, $json_value)
    {
        $this->tenant_id = $tenant_id;
        $this->locale = $locale;
        $this->scope = $scope;
        $this->json_value = $json_value;

        $this->save();
    }
}
