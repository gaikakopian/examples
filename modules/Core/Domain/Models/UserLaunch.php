<?php

namespace Modules\Core\Domain\Models;

use Illuminate\Database\Eloquent\Model;

class UserLaunch extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['timezone', 'useragent', 'user_id', 'timezone2', 'offset', 'client_type', 'client_browser', 'client_os', 'client_engine'];


    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    /**
     * A ProfileField belongs to a User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function setUseragentAttribute($value)
    {

        $this->attributes['useragent'] = $value;

        $result = new \WhichBrowser\Parser($value);

        $this->attributes['client_browser'] = $result->browser->toString();
        $this->attributes['client_engine'] = $result->engine->toString();
        $this->attributes['client_os'] = $result->os->toString();
        $this->attributes['client_type'] = $result->getType();
        try {
            $this->attributes['client_version'] = $result->browser->version->value;
        } catch (\Exception $e) {

        }


    }
}
