<?php

namespace Modules\Core\Domain\Models;

use Illuminate\Database\Eloquent\Model;

class UserCallcheckResult extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['success', 'download_speed', 'upload_speed', 'mos_audio', 'mos_video', 'packet_loss_ratio', 'meta'];


    protected $casts = [
        'meta' => 'array',
        'packet_loss_ratio' => 'float',
        'upload_speed' => 'float',
        'download_speed' => 'float',
        'mos_video' => 'float',
        'mos_audio' => 'float',
    ];
    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    /**
     * A ProfileField belongs to a User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
