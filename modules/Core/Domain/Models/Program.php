<?php

namespace Modules\Core\Domain\Models;

use Config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Domain\Models\Traits\HasImageAttachments;
use Modules\Distribution\Domain\Traits\HasDistributionLogic;
use Modules\Matching\Domain\Models\Match;

class Program extends Model
{
    use SoftDeletes, HasImageAttachments;
    use HasDistributionLogic;


    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
    */

    protected $image_attachments = [
        'logo' => [
            'path' => 'uploads/program/logo',
            'styles' => [
                'medium' => '500',
            ],
            'visibility' => 'public',
        ],
        'cover' => [
            'path' => 'uploads/program/cover',
            'styles' => [
                'small' => '100',
                'medium' => '500',
                'large' => '1000'
            ],
            'visibility' => 'public',
        ],
        'detail_photo' => [
            'path' => 'uploads/program/detail_photo',
            'styles' => [
                'small' => '100',
                'medium' => '500',
                'large' => '1000'
            ],
            'visibility' => 'public',
        ],
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'language', 'title', 'description', 'matching_algorithm',
        'manager_id', 'color_code', 'has_mentor_webinar', 'has_mentee_webinar',
        'lections_total', 'conference_entry_url', 'old_id', 'cooperating_organization',
        'custom_links',
        'distribute_to_tenants',
        'distributor_source_id'
    ];


    /**
     * used by the eloquent service to fuzzy search entities
     * @var array
     */
    public $searchable = [
        'code',
        'language',
        'title'
    ];
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['deleted_at', 'created_at', 'updated_at'];

    protected $casts = [
        'custom_links' => 'array',
    ];


    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    /**
     * A Program can have many Articles.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articles()
    {
        return $this->hasMany(User::class);
    }

    /**
     * A Program can have many Files.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany(File::class);
    }

    /**
     * A Program can have many Webinars.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function webinars()
    {
        return $this->hasMany(Webinar::class);
    }

    /**
     * A Program can have many Enrollments.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function enrollments()
    {
        return $this->hasMany(Enrollment::class);
    }

    /**
     * A Program can be used by many Organizations.
     *
     * @return BelongsToMany
     */
    public function organizations()
    {
        return $this->belongsToMany(Organization::class);
    }

    /**
     *
     *
     * @return BelongsTo
     */
    public function manager()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param $type
     * @param $role
     * @param $language
     * @return null
     * @throws \Exception
     */
    public function getCustomLink($type, $role, $language): string
    {

        if (!$this->custom_links) {
            throw new \Exception("Custom Link not found on program" . $this->id . ' link ' . $type);
        }


        foreach ($this->custom_links as $link) {
            if ($link['type'] == $type && $link['role'] === $role && $link['language'] === $language) {
                return $link['url'];
            }
        }

        foreach ($this->custom_links as $link) {
            if ($link['type'] == $type && $link['role'] === $role && $link['language'] === 'en') {
                return $link['url'];
            }
        }

        throw new \Exception("Custom Link not found on program" . $this->id . ' link ' . $type);
    }

    public function addCustomLink($type, $role, $language, $link)
    {
        $links = $this->custom_links ? $this->custom_links : [];

        $links[] = ['type' => $type, 'role' => $role, 'language' => $language, 'url' => $link];

        $this->custom_links = $links;

    }

    /*
    |--------------------------------------------------------------------------
    | Accessors & Mutators
    |--------------------------------------------------------------------------
     */

    /**
     * Accessor providing a `logo` attribute which contains an array of image urls for every defined dimension.
     *
     * @return array
     */
    public function getLogoAttribute()
    {
        return $this->getImageAttachmentPaths('logo');
    }

    public function getEnrollmentTypeAttribute()
    {
        switch ($this->matching_algorithm) {

            case 'inhouse':
                return 'inhouse'; // to be change if the pivot table implementation is ready.

            case 'inhouseFujitsu':
                return 'inhouseFujitsu';
            case 'default':
            default:
                return 'classic';


        }
    }

    public function secretHash() {
        $salt = Config::get('app.key');

        return sha1($salt . $this->id);

    }
    /**
     * Accessor providing a `cover` attribute which contains an array of image urls for every defined dimension.
     *
     * @return array
     */
    public function getCoverAttribute()
    {
        return $this->getImageAttachmentPaths('cover');
    }

    /**
     * Accessor providing a `detail_photo` attribute which contains an array of image urls for every defined dimension.
     *
     * @return array
     */
    public function getDetailPhotoAttribute()
    {
        return $this->getImageAttachmentPaths('detail_photo');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function matches()
    {
        return $this->hasMany(Match::class);
    }
}
