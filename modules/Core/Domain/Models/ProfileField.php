<?php

namespace Modules\Core\Domain\Models;

use Illuminate\Database\Eloquent\Model;

class ProfileField extends Model
{
    /**
     * Base profile field codes.
     *
     * @var array
     */
    public static $KNOWN_CODES = [
        'PERSONALITY',
        'INTERESTS',
        'LANGUAGES',
        'GOALS'
    ];

    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'code', 'value'];

    /**
     * The fields that should be cast to a specific type.
     *
     * @var array
     */
    protected $casts = [
        'value' => 'array'
    ];

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    /**
    * A ProfileField belongs to a User.
    *
    * @return Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
