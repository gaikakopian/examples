<?php

namespace Modules\Core\Domain\Models;

use App\Services\MultiTenant\BelongsToTenant;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{

    use BelongsToTenant;

    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'frontend_url', 'email_signature', 'email_greetings', 'logo'];

    /**
     * The attributes that should be hidden.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];


    public function getLinkTo($url, User $user){
        // optpnal, add auth token;


    }
}
