<?php

namespace Modules\Core\Domain\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserInvitation
 * @package Modules\Core\Domain\Models
 *
 * A class to keep track of the invitations to use VV platform sent by existing users to any email
 *
 * The 'state' of Invitation could be 'PENDING' or 'ACCEPTED'
 *
 * the 'state' will change to 'ACCEPTED' when the invited user registers on the platform
 */
class UserInvitation extends Model
{
    /**
     * All possible states that this model can have.
     *
     * @var array
     */
    const STATES = [
        'PENDING' => 'PENDING',
        'CLICKED' => 'CLICKED',
        'ACCEPTED' => 'ACCEPTED',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'inviting_user_id',
        'invitation_sent_at',
        'invited_user_email',
        'invited_user_id',
        'state',
        'last_clicked_at'
    ];


    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    /**
     *  A UserInvitation belongs to exactly one User.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function invited_user()
    {
        return $this->belongsTo(User::class);
    }
}
