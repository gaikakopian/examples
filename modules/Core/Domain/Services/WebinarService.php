<?php

namespace Modules\Core\Domain\Services;

use App\Infrastructure\Domain\EloquentBreadService;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Webinar;

class WebinarService extends EloquentBreadService
{
    /**
     * Return all webinars for a program and user.
     *
     * @param int $userId
     * @param int $programId
     * @return Collection
     */
    public function listForUserAndProgram(int $userId, int $programId): Collection
    {
        return $this->model->newQuery()
            ->where(['program_id' => $programId])
            ->get();
    }

    /**
     * List for Enrollment
     * (to use in enrollmentPRocess)
     * Logic:
     *  - find if there are special webinars for the user's company.
     *  - if not
     *
     * @param Enrollment $enrollment
     * @return Collection
     */
    public function listForEnrollment(Enrollment $enrollment): Collection
    {
        $programId = $enrollment->program_id;
        $organization = $enrollment->user->organization;

        return $this->listBy($programId, $organization, $enrollment->role);
    }


    public function listBy(int $programId, Organization $organization, string $role)
    {

        $filterOrganization = ($organization->private_webinars ? $organization->id : null);
        return $this->model->newQuery()
            ->where([
                'program_id' => $programId,
                'organization_id' => $filterOrganization,
                'target_audience' => $role,
                'state' => Webinar::STATES['PLANNED']
            ])
            ->where('starts_at', '>', Carbon::now())
            ->get();
    }

    /**
     * Return a specific webinar for a user.
     *
     * @param int $webinarId
     * @param int $userId
     * @param int $programId
     * @return Arrayable
     */
    public function getForUserAndProgram(int $webinarId, int $userId, int $programId): Arrayable
    {
        return $this->model->newQuery()
            ->where('id', $webinarId)
            ->firstOrFail();
    }

    /**
     * Enroll a User to a Webinar via their Enrollment.
     *
     * @param integer $enrollmentId
     * @param Webinar $webinar
     * @return void
     */
    public function enrollUser(int $enrollmentId, Webinar $webinar)
    {
        $webinar->enrollments()->attach($enrollmentId);
    }

    /**
     * Un a User to a Webinar via their Enrollment.
     *
     * @param integer $enrollmentId
     * @param Webinar $webinar
     * @return void
     */
    public function unenrollUser(int $enrollmentId, Webinar $webinar)
    {
        $webinar->enrollments()->detach($enrollmentId);
    }

    /***
     *
     * Find upcoming webinars to send out invitation emails
     * @return Collection
     */
    public function findWebinarsToRemind(): Collection
    {
        return $this->model->query()
            ->where('state', Webinar::STATES['PLANNED'])
            ->whereNotNull('starts_at')
            ->whereBetween('starts_at', [Carbon::now(), Carbon::now()->addDay(1)])
            ->with('enrollments')
            ->whereHas('enrollments', function ($q) {
                $q->whereNull('enrollment_webinar.invitation_sent');
            })
            ->get();
    }


    /**
     * A User has attended/missed a Webinar.
     *
     * @param integer $enrollmentId
     * @param Webinar $webinar
     * @param boolean $attended `true` for attended, `false` for missed
     * @return void
     */
    public function markAttended(int $enrollmentId, Webinar $webinar, bool $attended = true)
    {
        $webinar->enrollments()->updateExistingPivot($enrollmentId, [
            'attended' => $attended,
        ]);
    }
}
