<?php

namespace Modules\Core\Domain\Services;

use App\Exceptions\Server\ServerException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Support\Facades\Config;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

/**
 * Class ConferenceService
 * @package Modules\Core\Domain\Services
 * @deprecated to be replaced by conference3
 */
class ConferenceService
{
    /**
     * An HTTP client instance for making requests to the conference service.
     *
     * @var GuzzleHttp\Client
     */
    protected $httpClient;


//    const DefaultVideoProvider = 'twilio';
    const DefaultVideoProvider = 'tokbox';


    /**
     * Construct an instance of the Service.
     *
     * @param GuzzleHttp\Client $httpClient
     */
    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function createRoomForMatch(Match $match): string
    {

        // get video provider from mentor;
        $videoProvider = $match->getMentorParticipation()->enrollment->user->organization->video_provider;

        $contentRoot = $match->program->conference_entry_url;
        $match->external_room_id = $this->createRoom($contentRoot, $videoProvider);
        return $match->external_room_id;
    }

    /**
     * Create a new room at the conference backend, returning its roomId.
     *
     * @doNotUse To make the interface more clear, we should use createRoomForMatch if there
     *             is no other usecase
     * @var string $contentPath Path of the topmost content that may be accessed in the room
     * @var array $hooks Event callbacks that we want to subscribe to
     * @return string Id of the newly created room
     */
    public function createRoom(string $contentPath, $videoprovider = false): string
    {
        if (!$videoprovider) {
            $videoprovider = self::DefaultVideoProvider;
        }
        $url = $this->makeServiceUrl('/rooms');
        $payload = [
            'content' => [
                'root' => $contentPath,
                'entry' => $contentPath,
                'videoprovider' => $videoprovider
            ]
        ];

        $headers = ['Authorization' => 'Bearer ' . Config::get('conference.access_token')];


        try {
            $options = [
                'json' => $payload,
                'headers' => $headers
            ];

            $response = $this->httpClient->request('POST', $url, $options);
            $parsedResponse = json_decode($response->getBody());
        } catch (RequestException $e) {
            report($e);
            return '0';
        }
        // @todo: add firestore here;



        return $parsedResponse->id;
    }

    /**
     * Get the token with which a user can log into the conference app.
     *
     * @param int $userId
     * @return string A token that doesn't expire, but is unique per user
     */
    public static function getConferenceTokenForUser(int $userId): string
    {
        return $userId . '-' . hash_hmac('sha512', "User:$userId", Config::get('app.key'));
    }

    /**
     * Check the validity of a conference token.
     *
     * @param string $token
     * @return bool
     */
    public function isValidConferenceToken(string $token): bool
    {
        $userId = $this->getUserIdFromToken($token);

        return self::getConferenceTokenForUser($userId) === $token;
    }

    /**
     * Get the corresponding user to a token.
     *
     * @param string $token
     * @return Modules\Core\Domain\Models\User
     */
    public function getUserForToken(string $token): User
    {
        $userId = (int)explode('-', $token)[0];
        return User::find($userId);
    }

    /**
     * @param string $token
     * @return int
     */
    public function getUserIdFromToken(string $token): int
    {
        return (int)explode('-', $token)[0];
    }

    /**
     * Returns the room Id of the frontend.
     *
     * @param int $userid
     * @param Match $match
     * @return string external Room Id
     * @throws ServerException
     */
    public function getRoomLinkForMatch($userid, Match $match)
    {
        if (empty($match->external_room_id)) {
            $contentRoot = $match->program->conference_entry_url;
            $this->createRoomForMatch($match);
//            $this->createRoom($contentRoot); // $match->external_room_id =
            $match->save();
        }

        if (empty($match->external_room_id)) {
            throw new ServerException('COULD_NOT_CREATE_ROOM');
        }

        return self::getLinkForRoom($match->external_room_id, $userid);
    }

    /**
     * get Link for room;
     * @param $roomId
     * @param $userid
     * @return string
     */
    public static function getLinkForRoom($roomId, $userid)
    {
        $token = self::getConferenceTokenForUser($userid);
        return Config::get('conference.frontend_url') . 'room/' . $roomId . '?token=' . $token;
    }

    /**
     * Generate a full URL to the conference backend with given path component.
     *
     * @param string $path
     * @return string
     */
    protected function makeServiceUrl(string $path): string
    {
        if (!starts_with($path, '/')) {
            $path = '/' . $path;
        }

        return Config::get('conference.backend_url') . $path;
    }
}
