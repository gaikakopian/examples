<?php

namespace Modules\Core\Domain\Services;

use App\Infrastructure\Domain\EloquentBreadService;
use Carbon\Carbon;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\User;

class GroupService extends EloquentBreadService
{
    /**
     * Return all groups for a user.
     *
     * @param int $userId
     * @return Collection
     */
    public function listForUser(int $userId): Collection
    {
        return $this->model->newQuery()
            ->whereHas('users', function ($query) use ($userId) {
                $query->where('user_id', $userId);
            })
            ->get();
    }

    /**
     * Return user's public groups
     *
     * @param int $userId
     * @param int $offset
     * @param int $limit
     * @return Collection
     */
    public function listUserPublicGroups(int $userId, int $offset, int $limit): Collection
    {
        return $this->model->newQuery()
            ->whereHas('users', function ($query) use ($userId) {
                $query->where('user_id', $userId);
            })
            ->offset($offset)
            ->limit($limit)
            ->get();
    }

    /**
     * List all Groups for a User where the User is a supervisor.
     *
     * @param integer $userId
     * @return Collection
     */
    public function listSupervisingForUser(int $userId): Collection
    {
        return $this->model->newQuery()
            ->whereHas('users', function ($query) use ($userId) {
                $query->where([
                    'user_id' => $userId,
                    'role' => 'supervisor',
                ]);
            })
            ->get();
    }

    /**
     * Return a specific group for a user.
     *
     * @param int $userId
     * @param int $groupId
     * @return Arrayable
     */
    public function getForUser(int $userId, int $groupId): Arrayable
    {
        return $this->model->newQuery()
            ->where('id', $groupId)
            ->whereHas('users', function ($query) use ($userId) {
                $query->where(['user_id' => $userId]);
            })
            ->firstOrFail();
    }

    /**
     * List Groups that have at least one of the given Users as a member.
     *
     * @param array $userIds
     * @return Collection
     */
    public function listForUsers(array $userIds): Collection
    {
        $query = $this->model->newQuery();

        $query->whereHas('users', function ($query) use ($userIds) {
            $query->whereIn('user_id', $userIds);
        });

        return $query->get();
    }

    /**
     * Check if a User is supervisor of antoher User.
     *
     * @param int $applicantId
     * @param int $objectId
     * @return bool
     */
    public function isSupervisorOf(int $applicantId, int $objectId): bool
    {
        $applicantGroups = $this->listSupervisingForUser($applicantId);
        $objectGroups = $this->listForUser($objectId);
        $intersection = $applicantGroups->intersect($objectGroups);

        return $intersection->count() > 0;
    }

    /**
     * Check if a User is Member of
     *
     * @param User $user
     * @param int $objectId
     * @return bool
     */
    public function isMemberOf(User $user, int $groupId): bool
    {
        return $user->groups()->where(['group_id' => $groupId, 'role' => 'member'])->exists();
    }

    /**
     * Generate a unique human readable password for a given group and today's date.
     *
     * @param integer $groupId
     * @return string
     */
    public function getTodaysPasswordForGroup(int $groupId): string
    {
        $faker = $this->getSeededFakerForTodayAndGroup($groupId);

        return str_slug($faker->words(3, true), '-');
    }

    /**
     * Get a seeded instance of Faker which we can use to generate a new password
     * for every day and group.
     *
     * @param integer $groupId
     * @return \Faker\Generator
     */
    private function getSeededFakerForTodayAndGroup(int $groupId): Generator
    {
        $today = Carbon::today()->startOfDay()->timestamp;
        $seedString = $groupId . $today;

        $faker = Factory::create();
        $faker->seed($this->generateSecretSeedNumber($seedString));

        return $faker;
    }

    /**
     * Generate a secret number for a given string.
     *
     * @param string $string
     * @return integer
     */
    private function generateSecretSeedNumber(string $string): int
    {
        $hash = hash_hmac('sha256', $string, env('APP_SECRET'));

        // Take only 8 hex characters to be safe in even with 32bit integers
        $seedString = substr($hash, 0, 8);

        return base_convert($seedString, 16, 10);
    }

    /**
     * Add user to group
     * @param User $user
     * @param Group $group
     */
    public function attachUserToGroup(User $user, Group $group, string $role): void
    {
        $group->users()->attach($user->id, ['role' => $role]);
    }

    /**
     * Remove user from group
     *
     * @param User $user
     * @param Group $group
     */
    public function detachUserToGroup(User $user, Group $group): void
    {
        $group->users()->detach($user->id);
    }


    /**
     *
     */
    public function findGroupsToSendReport(): Collection
    {
        return $this->model->newQuery()
            ->whereNull('report_weekly_sent_at')
            ->orWhere('report_weekly_sent_at', '<', Carbon::now()->subDays(6))
            ->get();
    }


    public function findGroupsForDailyReport(): Collection
    {

        // @todo: find logic when to send..
        return $this->model->newQuery()
            ->whereNull('report_daily_sent_at')
            ->orWhere('report_daily_sent_at', '<', Carbon::now()->subHours(24))
            ->get();


    }
}
