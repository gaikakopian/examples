<?php

namespace Modules\Core\Domain\Services;

use App\Services\MultiTenant\TenantScope;
use Illuminate\Contracts\Support\Arrayable;
use App\Infrastructure\Domain\EloquentBreadService;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\User;
use Illuminate\Database\Eloquent\Builder;


class RegistrationCodeService extends EloquentBreadService
{
    /**
     * Retrieve a single RegistrationCode by its `code`.
     *
     * @param  string $code `code` of the entity
     * @return Illuminate\Contracts\Support\Arrayable
     */
    public function getByCode(string $code): Arrayable
    {
        $code = strtoupper($code);

        return $this->model
            ->withoutGlobalScope(TenantScope::class)
            ->where('code', 'ILIKE', $code)
            ->firstOrFail();
    }


    public function applyRegistrationCode(User $user, RegistrationCode $registrationCode, $ignoreWaitinglist = false)
    {

        $program = $registrationCode->auto_enroll_in;
        $user->groups()->attach($registrationCode->group_id);

        $user->primary_role = $registrationCode->primary_role;
        $user->registration_code_id = $registrationCode->id;
        $user->save();


        if (!$program) {
            return;
        }


//        $data = [
//            'user_id' => $user->id,
//            'program_id' => $program,
//            'role' => $registrationCode->primary_role,
//            'state' => Enrollment::DEFAULT_STATE
//        ];
//        $e = new Enrollment($data);
//        $e->save();
//
//        if ($registrationCode->users_count_limit >= 1) {
//            $registeredUserCount = $registrationCode->users()->count();
//
//            if ($registrationCode->users_count_limit < $registeredUserCount) {
//                $e->transition('overbooked');
//            }

        /** @var EnrollmentService $enrollmentService */
        $enrollmentService = app(EnrollmentService::class);
        $enrollment = $enrollmentService->firstOrCreate($user->id, $program, $registrationCode->primary_role);

        if (!$ignoreWaitinglist) {
            $this->ensureWaitinglist($registrationCode, $enrollment);
        }

    }

    private function ensureWaitinglist(RegistrationCode $registrationCode, Enrollment $enrollment)
    {
        if ($registrationCode->users_count_limit < 1) {
            return;
        }
        $registeredUserCount = $registrationCode->users()->count();
        if ($registrationCode->users_count_limit >= $registeredUserCount) {
            return;
        }

        $enrollment->transition('overbooked');
    }

    public function findCodesForOrganization(Organization $organization){

        return $this->model
                ->query() // @todo: filter by organization id;
                ->get();
    }

    public
    function filterOrganization(Builder $query, $method, $clauseOperator, $value)
    {
        // if clauseOperator is idential to false,
        //     we are using a specific SQL method in its place (e.g. `in`, `between`)
        if ($clauseOperator === false) {
            call_user_func([$query, $method], 'groups.organization_id', $value);
        } else {
            call_user_func([$query, $method], 'groups.organization_id', $clauseOperator, $value);
        }

        // @add join tables
        return ['group' => 'group'];
    }


}
