<?php

namespace Modules\Core\Domain\Services;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Domain\EloquentBreadService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Core\Domain\Models\Translation;

use Illuminate\Contracts\Support\Arrayable;
use Modules\Core\Domain\Models\TranslationLog;
use Swaggest\JsonDiff\JsonDiff;

class TranslationService extends EloquentBreadService
{
    /**
     * @param $tenant_id
     * @param $locale
     * @param $scope
     * @param $json_value
     */
    public static function addTranslation($tenant_id, $locale, $scope, $json_value)
    {
        $translation = new Translation();
        $translation->setTranslation($tenant_id, $locale, $scope, $json_value);
    }

    public function update($id, array $attributes): Arrayable
    {
        $record = $this->model->newQuery()->findOrFail($id);

        $newLength = strlen($attributes['json_value']);
        $oldLength = strlen($record->json_value);

        $allowedChange = $oldLength * -0.2;

        if ($newLength - $oldLength < $allowedChange) {
            throw new ClientException('not allowed to remove so many fields. Please ask Simon to remove fields.');
        }

        $this->saveDiff($attributes['json_value'], $record);

        $response = parent::update($id, $attributes);
        // @todo: disallow reductions greater then 10 oder so was...

        // @todo: create log etc?
        return $response;
    }

    private function saveDiff($newValue, $record)
    {
        $newValue = json_decode($newValue);
        $oldValue = json_decode($record->json_value);

        $diff = new JsonDiff($oldValue, $newValue);
        $removed = $diff->getRemoved();
        $added = $diff->getAdded();

        if ($removed || $added) {
            $log = new TranslationLog();
            $log->tenant_id = $record->tenant_id;
            $log->user_id = Auth::id();
            $log->locale = $record->locale;
            $log->scope = $record->scope;
            $log->json_added = json_encode($added);
            $log->json_removed = json_encode($removed);
            $log->save();
        }

    }

    /**
     * @param $locale
     * @param $scope
     * @return mixed
     */
    public static function retrieveTranslationForLocaleAndScope(string $locale, string $scope)
    {
        if (!$locale || strlen($locale) != 2) {
            throw new \InvalidArgumentException('Expecting two-digit isocode as $locale, got:' . $locale);
        }
        $locale = strtolower($locale);
        $fallbackLocale = 'en';

        $result = Translation::query()
            ->where('locale', $fallbackLocale)
            ->where('scope', $scope)
            ->firstOrFail();
        $json = $result->json_value;

        if ($fallbackLocale === $locale) {
            return $json;
        }

        $result = Translation::query()
            ->where('locale', $locale)
            ->where('scope', $scope)
            ->first();
        if (!$result) {
            return $json;
        }

        $secondJson = $result->json_value;

        return self::mergeJson($json, $secondJson);
    }

    private static function mergeJson($json1, $json2)
    {
        $a1 = json_decode($json1, true);
        $a2 = json_decode($json2, true, 512, JSON_UNESCAPED_UNICODE);


//        $res = array_merge_recursive($a1, $a2);

        $res = self::my_merge($a1, $a2);


        return json_encode($res, JSON_UNESCAPED_UNICODE);


    }

    private static function my_merge($arr1, $arr2)
    {
        $keys = array_keys($arr2);
        foreach ($keys as $key) {
            if (isset($arr1[$key])
                && is_array($arr1[$key])
                && is_array($arr2[$key])
            ) {
                $arr1[$key] = self::my_merge($arr1[$key], $arr2[$key]);
            } else {
                $arr1[$key] = $arr2[$key];
            }
        }
        return $arr1;
    }
}

