<?php

namespace Modules\Core\Domain\Services;

use App\Infrastructure\Domain\EloquentBreadService;
use Illuminate\Contracts\Support\Arrayable;

class TermsService extends EloquentBreadService
{
    /**
     * Get the newest Terms by type.
     *
     * @param string $type
     * @return Arrayable
     */
    public function getNewestForType(string $type) : Arrayable
    {
        return $this->model->newQuery()
            ->where('type', $type)
            ->orderBy('updated_at', 'desc')
            ->firstOrFail();
    }
}
