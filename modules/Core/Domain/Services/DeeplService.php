<?php

namespace Modules\Core\Domain\Services;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Domain\EloquentBreadService;
use GuzzleHttp\Client;
use GuzzleHttp;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Admin\Domain\Models\TranslationCache;
use Modules\Core\Domain\Models\Translation;

use Illuminate\Contracts\Support\Arrayable;
use Modules\Core\Domain\Models\TranslationLog;
use Swaggest\JsonDiff\JsonDiff;

class DeeplService
{


    /**
     * An HTTP client instance for making requests to the conference service.
     *
     * @var GuzzleHttp\Client
     */
    protected $httpClient;


    protected $key;

    /**
     * Construct an instance of the Service.
     *
     * @param GuzzleHttp\Client $httpClient
     */
    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
        $this->key = env('DEEPL_KEY', 'a664bd66-6d94-fc97-15ec-9b2dc7fc18c9');
    }

    public function translate($input, $sourceLang, $targetLang) : TranslationCache
    {


//        protected $fillable = ['input_hash', 'input_text', 'output_text', 'reviewed_at', 'target_lang'];
        $hash = md5($input);
        $cache = TranslationCache::query()->where(['input_hash' => $hash, 'target_language' => $targetLang])->first();

        if ($cache) {
            echo 'cache hit';
            return $cache;
        }
        echo 'no cache';

        $output = $this->translateNoCache($input, $sourceLang, $targetLang);

        $cache = new TranslationCache(['input_hash' => $hash, 'target_language' => $targetLang, 'output_text' => $output, 'input_text' => $input]);
        $cache->save();

        return $cache;

    }

    private function translateNoCache($input, $sourceLang, $targetLang)
    {

        $url = 'https://api.deepl.com/v2/translate';
        $parameters = [
            'source_lang' => strtoupper($sourceLang),
            'text' => $input,
            'target_lang' => strtoupper($targetLang),
            'auth_key' => $this->key
        ];

        $response = $this->httpClient->post($url, [
            'form_params' => $parameters
        ]);
        $result = json_decode($response->getBody()->getContents());


        return $result->translations[0]->text;
        // { "translations": [ { "detected_source_language": "DE", "text": "Hello World!" } ] }


    }
}

