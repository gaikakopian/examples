<?php

namespace Modules\Core\Domain\Services\ContentChooser;

use App\Exceptions\Client\ClientException;
use App\Exceptions\Client\NotFoundException;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;

use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Program;
use Illuminate\Support\Facades\Log;

class ContentChooser
{
    /**
     * Finds a branded document (or fallback) for given search attributes
     *
     * @param Modules\Core\Domain\Services\ContentChooser\SearchObject $params
     * @return BrandedDocument|\Exception
     */
    public static function findDocument(SearchObject $params)
    {
        $documents = self::searchDocuments($params);

        if (!$documents) {
            $msg = "Can't find any Branded Documents for the key '$params->key' in format '$params->type'; Search: " . $params->toString();
            Log::warning("[BrandedDocuments] $msg");
            throw new NotFoundException($msg);
        }

        return self::chooseDocument($documents, $params);

    }

    /**
     * Finds the right document that fits the main parameters key and type (+ optional brand and program)
     * Info: If the program or brand are desired we still wanna find null records for later fallbacks.
     *
     * @param Modules\Core\Domain\Services\ContentChooser\SearchObject $params
     * @return Illuminate\Database\Eloquent\Collection|null
     */
    protected static function searchDocuments(SearchObject $params)
    {
        $builder = BrandedDocument::query()
            ->where('key', $params->key)
            ->where('type', $params->type);


        if ($params->brand) {
            $builder->where(function ($query) use ($params) {
                $query->where('brand_id', $params->brand->id)
                    ->orWhereNull('brand_id');
            });
        } else {
            $builder->whereNull('brand_id');
        }

        if ($params->program) {
            $builder->where(function ($query) use ($params) {
                $query->where('program_id', $params->program->id)
                    ->orWhereNull('program_id');
            });
        } else {
            $builder->whereNull('program_id');
        }

        if ($params->role) {
            $builder->where(function ($query) use ($params) {
                $query->where('audience', $params->role)
                    ->orWhereNull('audience');
            });
            // note: role is optional!
        }

        $all_documents = $builder->get();

        if (!$all_documents || $all_documents->count() === 0) {
            return null;
        }
        return $all_documents;
    }

    /**
     * Either returns the reduced collection or if the filter has no
     * results it returns the total collection.
     *
     * @param EloquentCollection $documents
     * @param $filter
     * @return EloquentCollection|static
     */
    private static function filterOthersIfExists(EloquentCollection $documents, $filter)
    {
        $filteredDocuments = $documents->filter($filter);

        if (count($filteredDocuments)) {
            return $filteredDocuments;
        }
        return $documents;

    }

    /**
     * Decides which document of all results from basic query has the hightest priority.
     *
     * // @todo check priorities ?
     *
     * @param Illuminate\Database\Eloquent\Collection $documents
     * @param Modules\Core\Domain\Services\ContentChooser\SearchObject $params
     * @return BrandedDocument
     */
    protected static function chooseDocument(EloquentCollection $documents, SearchObject $params)
    {

        // first check if there is a brand existing and remove all others;
        if ($params->brand) {
            $documents = self::filterOthersIfExists($documents, function ($document) use ($params) {
                return $document->brand_id == $params->brand->id;
            });
        }

        // check if in this subset is a program match
        if ($params->program) {
            $documents = self::filterOthersIfExists($documents, function ($document) use ($params) {
                return $document->program_id == $params->program->id;
            });
        }


        // check if in this subset is a role match
        if ($params->role) {
            $documents = self::filterOthersIfExists($documents, function ($document) use ($params) {
                return $document->audience == $params->role;
            });
        }


        // check if in this subset is a role match
        if ($params->lang) {
            $documents = self::filterOthersIfExists($documents, function ($document) use ($params) {
                return strtolower($document->language) == strtolower($params->lang);
            });
        }   // check if in this subset is a role match

        // choose en als fallback;
        $documents = self::filterOthersIfExists($documents, function ($document) use ($params) {
            return $document->language == 'en';
        });


        if ($documents->count() === 0) {
            return null; // should actually not happen!
        }
        return $documents->first();


//
//        // Prio 1: Branding + Programm + Language
//        $matches_brand_program_lang = $documents->filter(function ($document) use ($params) {
//            return ($params->brand and $document->brand_id === $params->brand->id) and
//                ($params->program and $document->program_id === $params->program->id) and
//                ($document->language === $params->lang);
//        });

//        if (count($matches_brand_program_lang)) {
//            return $matches_brand_program_lang->first();
//        }


        // Prio 2: Branding + Language
//        $matches_brand_lang = $documents->filter(function ($document) use ($params) {
//            return ($params->brand and $document->brand_id === $params->brand->id) and
//                ($document->language === $params->lang);
//        });
//
//        if (count($matches_brand_lang)) {
//            return $matches_brand_lang->first();
//        }
//
//        // Prio 3: Programm + Language
//        $matches_program_lang = $documents->filter(function ($document) use ($params) {
//            return ($params->program and $document->program_id === $params->program->id) and
//                ($document->language === $params->lang);
//        });
//
//        if (count($matches_program_lang)) {
//            return $matches_program_lang->first();
//        }
//
//        // Prio 4: Language
//        $matches_lang = $documents->filter(function ($document) use ($params) {
//            return $document->language === $params->lang;
//        });
//
//        if (count($matches_lang)) {
//            return $matches_lang->first();
//        }
//
//        // Fallback
//        return $documents->first();
    }
}
