<?php

namespace Modules\Core\Domain\Services;

use App\Infrastructure\Domain\EloquentBreadService;
use Carbon\Carbon;
use Doctrine\DBAL\Query\QueryBuilder;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Program;

class EnrollmentService extends EloquentBreadService
{
    /**
     * Return all Enrollments for a User.
     *
     * @param int $userId
     * @return Collection
     */
    public function listForUser(int $userId): Collection
    {
        return $this->model->newQuery()
            ->where(['user_id' => $userId])
            ->get();
    }


    /**
     * Return a specific Enrollment for a User.
     *
     * @param int $userId
     * @param int $enrollmentId
     * @return Arrayable
     */
    public function getForUser(int $userId, int $enrollmentId): Arrayable
    {
        return $this->model->newQuery()
            ->where('user_id', $userId)
            ->where('id', $enrollmentId)
            ->firstOrFail();
    }

    /**
     * Get the Enrollment between given User and Match.
     *
     * @param int $userId
     * @param int $matchId
     * @return Arrayable
     */
    public function getForUserAndMatch(int $userId, int $matchId): Arrayable
    {
        return $this->model->newQuery()
            ->where('user_id', $userId)
            ->whereHas('participations', function ($query) use ($matchId) {
                $query->where('match_id', $matchId);
            })
            ->firstOrFail();
    }

    /**
     * Get an Enrollment for a User and Program.
     *
     * @param integer $userId
     * @param integer $programId
     * @deprecated  since this result is not unique (think of doing a program as mentor and mentee)
     *  this method is deprecedated.
     * @return Arrayable
     */
    public function getForUserAndProgram(int $userId, int $programId): Arrayable
    {
        return $this->model->newQuery()
            ->where('user_id', $userId)
            ->where('program_id', $programId)
            ->firstOrFail();
    }

    /**
     * find or Create
     *
     */
    public function firstOrCreate(int $userId, int $programId, string $role): Enrollment
    {
        $query = [
            'user_id' => $userId,
            'program_id' => $programId,
            'role' => $role
        ];
        return Enrollment::firstOrCreate($query);
    }


    /**
     *
     */
    public function findWithoutFutureTraining(): Builder
    {

        // @todo: cehck the two cases;

        $query = $this->model->newQuery()
//            ->select('enrollments.*')
//            ->leftJoin('enrollment_webinar', 'enrollment_webinar.enrollment_id', '=', 'enrollments.id')
//            ->leftJoin('webinars', function ($join) {
//                $join->on('enrollment_webinar.webinar_id', '=', 'webinars.id')
//                    ->where('webinars.starts_at', '>', 'now()');
//            })
            ->where('enrollments.state', Enrollment::STATES['TRAINING'])
            ->whereNull('webinars.id')
            ->get();

        return $query;

    }

    public function findWithoutMatchFor7Days()
    {

        // @todo: also find with only inactive/disabled matches


        return $this->model->newQuery()
            ->leftJoin('participations', 'participations.enrollment_id', '=', 'enrollments.id')
            ->whereNull('participations.match_id')
            ->get();

    }

    public function getProgramReport(Program $program, $organization = null)
    {


        $query = DB::table('enrollments')
            ->select(DB::raw('count(*) as total'), 'enrollments.role', 'enrollments.state')
            ->join('users', 'users.id', '=', 'enrollments.user_id')
            ->where('users.count_for_reporting', true)
            ->where('enrollments.program_id', $program->id)
            ->groupBy(['enrollments.role', 'enrollments.state']);

        if ($organization) {
            $query->where('users.organization_id', $organization->id);
        }


        return $query;

//        SELECT COUNT(*), primary_role, enrollments.state FROM enrollments
// JOIN users u on enrollments.user_id = u.id
//GROUP BY primary_role, enrollments.state


    }


}
