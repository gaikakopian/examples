<?php

namespace Modules\Core\Domain\Services;

use App\Infrastructure\Domain\EloquentBreadService;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Role;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;

class ParticipationService extends EloquentBreadService
{

    public function hadToManyRejections($enrollment)
    {
        /** @var MatchService $matchService */
        $matchService = app(MatchService::class);

        $count = $matchService->countRejectedMentorships($enrollment->user);
        return $count >= 3;

    }

    public function createNewParticipationIfAllowed(Enrollment $enrollment, $inDays = 7)
    {

        if ($this->hadToManyRejections($enrollment)) {
            return false;
        }

        return $this->createFromEnrollment($enrollment, $inDays);

    }

    /**
     * Return all participations for a user.
     *
     * @param int $userId
     * @return Collection
     */
    public function listForUser(int $userId): Collection
    {
        return $this->model->newQuery()
            ->whereHas('enrollment', function ($query) use ($userId) {
                $query->where('user_id', $userId);
            })
            ->get();
    }

    public function listActiveForUser(int $userId): Collection
    {
        return $this->model->newQuery()
            ->whereHas('enrollment', function ($query) use ($userId) {
                $query->where('user_id', $userId);
            })
            ->get();
    }

    public function filterInactiveParticipations(Collection $participations): Collection
    {

        return $participations->filter(function ($participation) {

            $hideStates = [Match::STATES['REJECTED']];
            $hideMentor = [Match::STATES['REQUESTED']];
            $hideMentee = [Match::STATES['UNCONFIRMED'], Match::STATES['REQUESTED']];

            if (!$participation->match) {
                return true;
            }

            $state = $participation->match->state;

            if (in_array($state, $hideStates))
                return false;

            if ($participation->enrollment->role === Role::ROLES['mentor'] && in_array($state, $hideMentor))
                return false;

            if ($participation->enrollment->role === Role::ROLES['mentee'] && in_array($state, $hideMentee))
                return false;

            return true;
        })->values();

    }

    /**
     * Return a specific participation for a user.
     *
     * @param int $userId
     * @param int $participationId
     * @return Arrayable
     */
    public function getForUser(int $userId, int $participationId): Arrayable
    {
        return $this->model->newQuery()
            ->whereHas('enrollment', function ($query) use ($userId) {
                $query->where('user_id', $userId);
            })
            ->where('id', $participationId)
            ->firstOrFail();
    }

    /**
     * Return the Participation that is matched to given Match id.
     *
     * @param integer $userId
     * @param integer $matchId
     * @return Arrayable
     */
    public function getForUserAndMatch(int $userId, int $matchId): Arrayable
    {
        return $this->model->newQuery()
            ->whereHas('enrollment', function ($query) use ($userId) {
                $query->where('user_id', $userId);
            })
            ->where('match_id', $matchId)
            ->firstOrFail();
    }


    /**
     * @param Enrollment $enrollment
     * @return \App\Infrastructure\Domain\Illuminate\Contracts\Support\Arrayable|Arrayable
     */
    public function createFromEnrollment(Enrollment $enrollment, $inDays = 0)
    {
        $after = ($inDays > 0) ? $after = Carbon::now()->addDays($inDays) : null;
        return $this->model->updateOrCreate([
            'enrollment_id' => $enrollment->id,
            'match_id' => null
        ],
            ['start_matching_after' => $after]
        );
    }

    /**
     *
     * This filter was specially made to find Partiicpations which should be mached.
     *
     * @param Builder $query
     * @param $method where
     * @param $clauseOperator something like =
     * @param $value is passed value
     * @return array
     */
    public function filterMatchable(Builder $query, $method, $clauseOperator, $value)
    {
        $query
            ->where('enrollments.state', Enrollment::STATES['ACTIVE'])
            ->whereNull('match_id')
            ->where(function ($query) {
                $query->where('start_matching_after', '>', Carbon::now())
                    ->orWhereNull('start_matching_after');
            });

        return ['enrollment' => 'enrollment'];
    }

    public function findMatchableParticipationForProgram(Program $program)
    {

        return \DB::query()
            ->select(['participations.id', 'enrollments.role'])
            ->from('participations')
            ->join('enrollments', 'enrollments.id', '=', 'participations.enrollment_id')
            ->where('enrollments.program_id', $program->id)
            ->where('enrollments.state', Enrollment::STATES['ACTIVE'])
            ->whereNull('match_id')
            ->whereNull('participations.deleted_at')
            ->where(function ($query) {
                $query->where('start_matching_after', '>', Carbon::now())
                    ->orWhereNull('start_matching_after');
            })
            ->get();

    }

    private function getMatchableBaseQuery()
    {
        return $this->model->newQuery()
            ->from('participations')
            ->whereNull('match_id')
            ->whereNull('participations.deleted_at')
            ->where(function ($query) {
                $query->where('start_matching_after', '>', Carbon::now())
                    ->orWhereNull('start_matching_after');
            });
    }

    public function findMatchableParticipationsOlderThan(int $hours)
    {

        return $this->getMatchableBaseQuery()
            ->where('participations.created_at', '<', Carbon::now()->subHours($hours))
            ->get();


    }

    public function getProgramReport(Program $program, $organization = null)
    {
        $query = \DB::query()
            ->select(DB::raw('count(distinct participations.id) as total'), 'enrollments.role')
            ->from('participations')
            ->join('enrollments', 'enrollments.id', '=', 'participations.enrollment_id')
            ->where('enrollments.program_id', $program->id)
            ->where('enrollments.state', Enrollment::STATES['ACTIVE'])
            ->whereNull('match_id')
            ->whereNull('participations.deleted_at')
            ->where(function ($query) {
                $query->where('start_matching_after', '>', Carbon::now())
                    ->orWhereNull('start_matching_after');
            })
            ->groupBy(['enrollments.role']);;

//        if ($organization) {
//            $query->where('users.organization_id', $organization->id);
//        }


        return $query;

    }


    public function findMatchableParticipationsForParticiption(Participation $participation)
    {

        /** @var Enrollment $enrollment */
        $enrollment = $participation->enrollment;

        $otherRole = $enrollment->role == Role::ROLES['mentor'] ? Role::ROLES['mentee'] : Role::ROLES['mentor'];

        return Participation::
        join('enrollments', 'enrollments.id', '=', 'participations.enrollment_id')
            ->where('enrollments.program_id', $participation->enrollment->program_id)
            ->where('participations.id', '<>', $participation->id)
            ->where('enrollments.role', '=', $otherRole)
            //@todo: exclude whenever they have already been matched;

            // searching for match condition:
            ->where('enrollments.state', Enrollment::STATES['ACTIVE'])
            ->whereNull('match_id')
            ->where(function ($query) {
                $query->where('start_matching_after', '>', Carbon::now())
                    ->orWhereNull('start_matching_after');
            })
            ->get(['participations.*']);

    }

    /**
     * List Participations for Group including
     *
     * @param int $groupId
     * @return Collection
     */
    public function listParticipationsForGroup(int $groupId, $includes = []): Collection
    {
        return $this->model->newQuery()
            ->with($includes)
            ->whereHas('enrollment.user.groups', function ($query) use ($groupId) {
                $query->where('group_id', $groupId);
            })
            ->get();

        return $this->model->newQuery()
            ->join('matches', 'appointments.match_id', '=', 'matches.id')
            ->join('participations', 'participations.match_id', '=', 'matches.id')
            ->join('enrollments', 'enrollments.id', '=', 'participations.enrollment_id')
            ->join('program', 'enrollments.program_id', '=', 'program.id')
            ->join('users', 'users.id', '=', 'enrollments.user_id')
            ->join('group_user', 'group_user.user_id', '=', 'users.id')
            ->where('group_user.group_id', $group->id)
            ->select([
                'program.sessions_total as sessionsTotal',
                'matches.lections_completed as sessionsCompleted',
                'matches.state',
                'appointments.id',
                'appointments.planned_start as plannedStart',
                'appointments.state',
                'users.id as userId',
                'users.first_name as firstName',
                'users.last_name as lastName',
                'users.created_at as registered'
            ])
            ->get();;
    }
}
