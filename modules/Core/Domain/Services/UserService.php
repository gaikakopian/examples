<?php

namespace Modules\Core\Domain\Services;

use App\Exceptions\Client\ClientException;
use App\Exceptions\Client\ValidationException;
use App\Infrastructure\Domain\EloquentBreadService;
use App\Notifications\User\UserPasswordResetNotification;
use App\Notifications\User\UserRegistrationMobileAcceptedNotification;
use Carbon\Carbon;
use FontLib\TrueType\Collection;
use GuzzleHttp\Client;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Modules\Core\Domain\Models\Activity;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\PasswordHistory;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchingService;
use Modules\Matching\Domain\Services\MatchService;
use Illuminate\Database\Eloquent\Builder;

class UserService extends EloquentBreadService
{
    protected $termsService;
    /**
     * @var PasswordBroker
     */
    protected $passwordBroker;
    protected $activityService;

    protected $httpClient;

    const USER_ROLE_ID = 1;

    public function __construct(
        TermsService $termsService,
        PasswordBroker $passwordBroker,
        ActivityService $activityService,
        Model $model,
        Client $client
    )
    {
        $this->termsService = $termsService;
        $this->passwordBroker = $passwordBroker;
        $this->httpClient = $client;

        $this->activityService = $activityService;

        parent::__construct($model);
    }

    /**
     * Accept the newest Terms of a given type.
     *
     * @param int $userId
     * @param string $userIpAddress
     * @param string $type
     * @return void
     */
    public function acceptTerms(int $userId, string $userIpAddress, string $type)
    {
        $user = $this->get($userId);
        $newestTerm = $this->termsService->getNewestForType($type);

        $user->acceptedTerms()->attach($newestTerm->id, [
            'accepted_at' => Carbon::now(),
            'ip_address' => $userIpAddress,
        ]);
    }

    /***
     * Send a password reset link to the user.
     *
     * @param string $email
     */
    public function sendPasswordResetEmail(string $email)
    {
        $user = $this->model->where('email', $email)->firstOrFail();
        $token = $this->passwordBroker->createToken($user);

        $user->notify(new UserPasswordResetNotification($user->brand, $token));
    }

    /***
     * Reset a user's password using their email and reset token.
     *
     * @param array $credentials
     */
    public function resetPassword(array $credentials)
    {
        $onSuccess = function (User $user, string $newPassword) {
            $this->changePassword($user, $newPassword);
        };

        $response = $this->passwordBroker->reset($credentials, $onSuccess);
        if ($response !== PasswordBroker::PASSWORD_RESET) {
            throw new ValidationException('passwordreset.' . $response);
        }
        return true;
    }

    public function changePassword(User $user, string $newPassword)
    {


        $minimumPasswordHistoryCount = Config::get('security.minimum_password_history');
        $passwordHistories = $user->passwordHistories()->take($minimumPasswordHistoryCount)->get();

        $passwordHash = bcrypt($newPassword);
        // check if the user
        foreach ($passwordHistories as $passwordHistory) {
            //      if (Hash::check($newPassword, $passwordHistory->password)) {
            if ($passwordHash == $passwordHistory->password) {
                throw new \InvalidArgumentException("PASSWORD_ALREADY_USED");


            }
        }

        $user->password = $passwordHash;
        $user->save();

        PasswordHistory::create(['user_id' => $user->id,
            'password' => $passwordHash]);

    }

    public
    function welcomeImportedUser(User $user)
    {

        $password = $this->generateAndSetNewPassword($user);

        $this->activityService->logActivityForUser($user, Activity::ACTIVITIES['USER_REGISTERED']);

        $ip = request()->ip();
        $this->getUserDataFromIP($user, $ip);

    }

    public
    function getUserDataFromIP(User $user, $ipAddress)
    {
        try {
            $key = env('IPDATA_KEY', '32ce8f3b467fb005f79e1faa68cd2d3d1ff527a479cc84e1342b9dc7');

            $uri = 'https://api.ipdata.co/' . $ipAddress . '?api-key=' . $key;

            $response = $this->httpClient->get($uri);
            $contents = json_decode($response->getBody()->getContents());

            $user->postcode = $contents->postal;
            $user->city = $contents->city;
            $user->country = $contents->country_code;
            $user->lng = $contents->longitude;
            $user->lat = $contents->latitude;
        } catch (\Exception $e) {
            report($e);
        }


    }

    /**
     * @param User $user
     * @return string
     */
    public
    function generateAndSetNewPassword(User $user): string
    {
        $password = $this->generateNewPassword();
        // @todo: generate random password with more sense;
        $user->password = bcrypt($password);
        $user->save();
        return $password;
    }

    /**
     * @param User $user
     */
    public function forceDeleteUser(User $user)
    {

        /** @var MatchingService $matchService */
        $matchService = app(MatchingService::class);

        $user->enrollments->each(function ($enrollment) use ($matchService) {

            $enrollment->webinars()->sync([]);
            $enrollment->history()->delete();


            $enrollment->participations->each(
            /**
             * @param $particaption Participation
             */
                function ($particaption) use ($matchService) {

                    /** @var Match $match */
                    $match = $particaption->match;
                    if ($particaption->match_id || $match) {
                        throw new ClientException('This user has still active matches; therefore he cannot become quit');
                    }

                    $matchService->removeParticipationFromMatchingPool($particaption);
                    $particaption->forceDelete();

                });
            $enrollment->forceDelete();

        });
        $user->feedbacks()->delete();
        $user->history()->delete();
        $user->smslogs()->delete();
        $user->audits()->delete();
        $user->profileFields()->delete();
        $user->chatrooms()->delete();
        $user->invitations()->delete();
        $user->comments()->delete();
        $user->launches()->delete();
        $user->groups()->sync([]);
        $user->passwordHistories()->delete();
        $user->roles()->sync([]);


        $user->forceDelete();


    }

    public
    function quitUser(User $user)
    {

        /** @var MatchingService $matchService */
        $matchService = app(MatchingService::class);

        $user->enrollments->each(function ($enrollment) use ($matchService) {

//            $enrollment->webinars()->sync([]);

            $enrollment->participations->each(
                function ($particaption) use ($matchService) {

                    /** @var Match $match */
                    $match = $particaption->match;
                    if (!$particaption->match_id) {
                        $matchService->removeParticipationFromMatchingPool($particaption);
                        $particaption->delete();
                        return;
                    }
                    if ($match->isActive()) {
                        throw new ClientException('This user has still active matches; therefore he cannot become quit');
                    }
                });
            if ($enrollment->transitionAllowed('quit')) {
                $enrollment->transition('quit');
            }
        });

        if ($user->transitionAllowed('quit')) {
            $user->transition('quit');
        }

    }

    public function filterEnrollmentState(Builder $query, $method, $clauseOperator, $value)
    {
        $query
            ->where('enrollments.state', $value);
        return ['enrollments' => 'enrollments'];
    }

//    public function filterMatchState(Builder $query, $method, $clauseOperator, $value)
//    {
//        $query
//            ->where('enrollments.participations.match.state', $value);
//        return [
//            'enrollments' => 'enrollments'
//        ];
//    }


    public function generateNewPassword(): string
    {
        $file = File::get(storage_path('positive_words.txt'));
        $words = explode("\n", $file);
        $password = $words[array_rand($words)] . ucfirst($words[array_rand($words)]);
        $password = preg_replace('/\PL/u', '', $password);
        return $password;


    }

    /**
     * Create a new entity by an array of attributes.
     *
     * Attributes missing from the array will be
     * set to their respective default values.
     *
     * @param  array $attributes Attributes of the new entity
     * @return Arrayable Entity created
     */
    public
    function create(array $attributes): Arrayable
    {


        $user = parent::create($attributes);

        if ($user->primary_role === User::ROLES['supervisor']) {
            $role = Role::query()->where('name', '=', Role::ROLES['supervisor'])->firstOrFail();
        } else {
            $role = Role::query()->where('name', '=', Role::ROLES['user'])->firstOrFail();
        }


        PasswordHistory::create([
            'user_id' => $user->id,
            'password' => bcrypt($attributes['password'])
        ]);

        $user->roles()->attach($role->id);

        return $user;
    }


    /**
     * @param $groupId
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public
    function listUsersForOverviewOfGroup($groupId)
    {

        $includes = [
            'enrollments',
            'enrollments.participations',
            'enrollments.participations.match',
            'enrollments.participations.match.appointments'
        ];


        return $this->model->newQuery()
            ->with($includes)
            ->whereHas('groups', function ($query) use ($groupId) {
                $query->where('group_id', $groupId);
                $query->where('role', 'member');
            })
            ->get();


//        includes[]: enrollments
//includes[]: enrollments.participations
//includes[]: enrollments.participations.match
//includes[]: enrollments.participations.match.appointments
//
    }

    public
    function getUsersForPartnerApi(Organization $organization)
    {
        $includes = [
            'enrollments',
//            'enrollments.participations',
//            'enrollments.participations.match',
//            'enrollments.participations.match.appointments'
        ];
        return $this->model->newQuery()
            ->with($includes)
            ->where('users.organization_id', $organization->id)
            ->get();

    }

    public
    function findUserByInternationalPhoneNumber($number)
    {

        $number = preg_replace('/[^0-9]/', '', $number); // remove anythhing but numbers
        $number = ltrim($number, '0'); // trim starting zeros


        return $this->model->newQuery()
            ->orWhere(DB::raw("phone_number_prefix || phone_number"), '=', $number)
            ->orWhere(DB::raw("whatsapp_number_prefix || whatsapp_number"), '=', $number)
            ->first();
    }

    public
    function findUsersWithPendingReminderDate(): Collection
    {
        return $this->model->newQuery()
            ->where('remind_again_at', '<=', Carbon::now())
            ->whereNotNull('remind_again_at')
            ->get();
    }


    public
    function confirmWhatsappNumber(User $user)
    {
        if ($user->whatsapp_status != 'confirmed') {
            $user->whatsapp_status = 'confirmed';
            $user->save();
            $user->notify(new UserRegistrationMobileAcceptedNotification($user->brand, $user));
        }
    }

    /**
     *
     * returns null if user has already opted in
     * returns full qualified url if user has to optin.
     *
     * @param User $user
     * @return null|string
     */
    public
    static function getOptInLink(User $user)
    {

        if ($user->email_optin_at) {
            return null;
        }
        $secret = $user->emailSecret('confirmemail');

        return route('email.response.general', [
            'action' => 'confirmemail',
            'userid' => $user->id,
            'secret' => $secret
        ]);

    }

    public
    function getLoginLink(User $user)
    {
        // https://app.volunteer-vision.com/auth?adminview=true&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjNjYTkwNWZjMjBlNjQ4OGZiNTkyZTZiNzkxNDU0MWU0MmZiMzAyYTA3NTZhYWVmZGE3OTg1OTNhYzZiMTJlMTYxMDBjNDMyNDZmZjUzZjE0In0.eyJhdWQiOiIxIiwianRpIjoiM2NhOTA1ZmMyMGU2NDg4ZmI1OTJlNmI3OTE0NTQxZTQyZmIzMDJhMDc1NmFhZWZkYTc5ODU5M2FjNmIxMmUxNjEwMGM0MzI0NmZmNTNmMTQiLCJpYXQiOjE1MzkzNDAyNTgsIm5iZiI6MTUzOTM0MDI1OCwiZXhwIjoxNTcwODc2MjU4LCJzdWIiOiIxNDM5Iiwic2NvcGVzIjpbXX0.CD1GsNsnTmr2iMav89NrIxiXZ4vDwICC3dUodhf_nooh_mKGBHa1JOBBJi2zeQIvS15SPiICiTh1O7r-Q5FDyjkRsRqdEe5DsU8HliU3Ib5vP6r2a43GbWc9Mmi1QeGkFpniHEOrk1fjZz9sRQVeDw-pAxdR3Yc-1ZQzbX-BAV1pqrqsXD9dL7NbHOwoSirfa2q0HL4wFoU0U5qMwZi7Eckb71hzU-wAP0oIjUpKAhNl44xQRPw4Skli_aR5TlKvOJ3ndOaJ2Dj7lz_IinAjyg3FM2RqcTAPjMbPZPAnT4rGTjQagOiW0N3e-fTcRCZTeYY5Q6bS1HkaJPpnop9eDJJoTGrqhgz8F9_RtJfDOkjvoq3YBIB2f1KH7ODTSBvDNiEk-K_y0q-0ChJsV8CCjXWUpKXPaFdfCUTfnJqkrgpd6UsltRJD8csGYU91RD0_cRCS5QqtcrHZogqQ6D_iD7zkOttKTTcBfdVblKS3MNKD82Um3Lv3g3CGvgju0uUpAcnBHpOTbAuMKA00ryKP2Rfj7ygPC--wGwFhPEU05mGIf3B5RBYg5O2AtYTrDVlXaSjO8sCTfQppxmTSGWfrsTMBKk_1Gxd11lCj44pj82RVEmTsSmINmqo3devoZrgdlJ58WXJ5NH7p1iWIWxFx13oOPBojx08MwlkWQ6Av73s
        $token = $user->createToken('Impersonate Token');
        $url = $user->brand->frontend_url . '/auth?token=' . $token->accessToken;

        return $url;
    }

    public
    function findUserByEmail($email)
    {
        $email = strtolower($email);
        return $this->model->query()->where('email', $email)->first();
    }

    public
    function removeUser(User $user)
    {
        $this->quitUser($user);
        $user->anonymize();
        $user->save();
        return $user;
    }

    /**
     * @param array $userdata
     * @param $registrationCode
     * @throws \Exception
     */
    public
    function createAndInviteUser(array $userdata, RegistrationCode $registrationCode, $isComplete): User
    {
        if (empty($userdata['email'])) {
            throw new \Exception('e-Mail Address is required');
        }
        $userdata['registration_code_id'] = $registrationCode->id;
        $user = $this->createUserWithDefaults($userdata);
        $this->welcomeImportedUser($user);


        if ($isComplete && $user->transitionAllowed('complete_registration')) {
            $user->transition('complete_registration');
        }

        /** @var RegistrationCodeService $registrationCodeService */
        $registrationCodeService = app(RegistrationCodeService::class);
        $registrationCodeService->applyRegistrationCode($user, $registrationCode);

        return $user;

    }

    public function isDemoMentor(User $user)
    {

        return $user->email == 'mentor@volunteer-vision.com';
    }

    public function findDemoMentor(): User
    {
        return $this->model->newQuery()
            ->where('email', 'mentor@volunteer-vision.com')
            ->firstOrFail();


    }

    private
    function createUserWithDefaults(array $userdataAll): User
    {
        $userdata = $userdataAll;
        $userdata['tenant_id'] = 1;
        $userdata['brand_id'] = 1;
        $userdata['password'] = rand(190, 1132132);
        $userdata['whatsapp_setup'] = 'notused';
        /** @var User $user */
        $user = $this->create($userdata);
        $user->email_optin_at = Carbon::now();;
        $user->state = User::STATES['NEW'];
        $user->save();

        // @todo: maybe set whatsapp status here based on organiation setup?


        return $user;

    }


    public function findTestUsers()
    {
        return $this->model->newQuery()
            ->where('last_name', 'ilike', 'test')
            ->get();

    }
}
