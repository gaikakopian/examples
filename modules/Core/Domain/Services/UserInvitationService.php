<?php

namespace Modules\Core\Domain\Services;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Domain\EloquentBreadService;
use App\Notifications\UserInvitation\UserInvitationNotification;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Notification;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserInvitation;

class UserInvitationService extends EloquentBreadService
{

    /**
     *  send an invitation email
     * @param int $inviting_user_id
     * @param $invited_user_email
     */
    public function sendInvitationEmail(User $inviting_user, $invited_user_email)
    {
        $invitation = $this->createUserInvitation($inviting_user->id, $invited_user_email);

        Notification::route('mail', $invited_user_email)
            ->notify(new UserInvitationNotification($inviting_user->brand, $invitation));
    }

    /**
     * @param int $inviting_user_id
     * @param $invited_user_email
     */
    protected function createUserInvitation(int $inviting_user_id, $invited_user_email): UserInvitation
    {
        return Factory(UserInvitation::class)->create([
            'inviting_user_id' => $inviting_user_id,
            'invitation_sent_at' => new \DateTime('NOW'),
            'invited_user_email' => $invited_user_email,
            'state' => 'PENDING'
        ]);
    }

    /**
     * @param $inviting_user_id
     * @return Collection
     */
    public function getAllInvitedByUser($inviting_user_id): Collection
    {
        return UserInvitation::where(['inviting_user_id' => $inviting_user_id])->get();
    }

    /**
     * @param $uid
     * @return UserInvitation
     * @throws ClientException
     */
    public function acceptInvitation($uid) : UserInvitation
    {
        // Look up the invitation
        $invitation = UserInvitation::where(['uid' => $uid])->first();
        if (!$invitation) {
            throw new ClientException("Invitation does not exist");
        }
        $this->update($invitation->id, [
            'last_clicked_at' => new \DateTime('NOW'),
            'state' => 'CLICKED'
        ]);
        return $invitation;
    }
}
