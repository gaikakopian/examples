<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Webinar;
use Modules\Matching\Domain\Models\Match;

/**
 * Test the functionality of the `/api/v2/programs` endpoint.
 */
class ShowNextStepTest extends EndpointTest
{
    protected $user;
    protected $enrollment;

    /** {@inheritdoc} */
    protected function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();

    }

    public function test_it_show_user_without_enrollment()
    {

        $response = $this->actingAs($this->user)->getJson('/api/v2/users/me/nextStep');
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'data' => [
                'state' => 'NO_ENROLLMENT'
            ]
        ]);

    }

    public function test_it_does_show_no_future_appointment()
    {


        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'state' => Enrollment::STATES['ACTIVE'],
            'role' => Role::ROLES['mentor']
        ]);
        $match = factory(Match::class)->create([
            'state' => Match::STATES['ACTIVE']
        ]);

        factory(Participation::class)->create([
            'enrollment_id' => $enrollment->id,
            'match_id' => $match->id
        ]);

        $response = $this->actingAs($this->user)->getJson('/api/v2/users/me/nextStep');
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'data' => [
                'state' => 'NO_UPCOMING_APPOINTMENT'
            ]
        ]);

    }

    public function test_it_does_not_show_no_future_appointment_on_rejected()
    {


        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'state' => Enrollment::STATES['ACTIVE'],
            'role' => Role::ROLES['mentor']
        ]);
        $match = factory(Match::class)->create([
            'state' => Match::STATES['REJECTED']
        ]);

        factory(Participation::class)->create([
            'enrollment_id' => $enrollment->id,
            'match_id' => $match->id
        ]);

        $response = $this->actingAs($this->user)->getJson('/api/v2/users/me/nextStep');
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'data' => [
                'state' => 'DEFAULT'
            ]
        ]);

    }

    /**
     * - INCOMPLETE_ENROLLMENT
     * - WAITING_FOR_TRAINING
     * - NO_TRAINING
     * - UNCONFIRMED_MATCH
     * - NO_UPCOMING_APPOINTMENT
     * ??
     */

    public function test_shows_incomplete_enrollment()
    {
        $this->enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'state' => Enrollment::STATES['NEW']
        ]);

        $response = $this->actingAs($this->user)->getJson('/api/v2/users/me/nextStep');
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'data' => [
                'state' => 'INCOMPLETE_ENROLLMENT'
            ]
        ]);
    }

    public function test_shows_no_training()
    {
        $this->enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'state' => Enrollment::STATES['NEW']
        ]);

        $this->enrollment->state = Enrollment::STATES['TRAINING'];
        $this->enrollment->save();

        $response = $this->actingAs($this->user)->getJson('/api/v2/users/me/nextStep');
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'data' => [
                'state' => 'NO_TRAINING'
            ]
        ]);
    }


    public function test_shows_waiting_for_training()
    {
        $this->enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'state' => Enrollment::STATES['NEW']
        ]);

        $this->enrollment->state = Enrollment::STATES['TRAINING'];
        $this->enrollment->save();
        $webinar = Webinar::where('starts_at', '>', Carbon::now())->firstOrFail();
        $this->enrollment->webinars()->attach($webinar);

        $response = $this->actingAs($this->user)->getJson('/api/v2/users/me/nextStep');
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'data' => [
                'state' => 'WAITING_FOR_TRAINING'
            ]
        ]);

    }

    public function test_it_shows_no_training_for_old_webinar()
    {
        $this->enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'state' => Enrollment::STATES['NEW']
        ]);

        $this->enrollment->state = Enrollment::STATES['TRAINING'];
        $this->enrollment->save();
        $webinar = Webinar::where('starts_at', '<', Carbon::now())->firstOrFail();
        $this->enrollment->webinars()->attach($webinar);

        $response = $this->actingAs($this->user)->getJson('/api/v2/users/me/nextStep');
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'data' => [
                'state' => 'NO_TRAINING'
            ]
        ]);

    }

    public function test_it_shows_unconfirmed_match()
    {

        $this->enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'state' => Enrollment::STATES['ACTIVE']
        ]);


//        $this->enrollment->state = Enrollment::STATES['ACTIVE'];
//        $this->enrollment->save();

        $match = factory(Match::class)->create(['state' => Match::STATES['UNCONFIRMED']]);
        $enrollment = $this->user->enrollments->first();
        $enrollment->role = Role::ROLES['mentor'];
        $enrollment->save();

        factory(Participation::class)->create(['enrollment_id' => $enrollment->id, 'match_id' => $match]);

        $response = $this->actingAs($this->user)->getJson('/api/v2/users/me/nextStep');
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'data' => [
                'state' => 'UNCONFIRMED_MATCH'
            ]
        ]);

    }
}
