<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v2/programs/{id}/enroll` endpoint.
 */
class EnrollToProgramTest extends EndpointTest
{


    public function test_it_enrolls_in_a_program()
    {
        $actor = factory(User::class)->create(['organization_id' => 1, 'primary_role' => Role::ROLES['mentor']]);

        $response = $this->actingAs($actor)->postJson('/api/v2/programs/1/enroll');
        $this->assertEquals(201, $response->getStatusCode(), $response->getContent());
        $response->assertJsonStructure([
            'data' => [
                'id',
                'program' => [],
                'participations' => [],
            ]
        ]);

        $this->assertDatabaseHas('enrollments', [
            'id' => $response->json()['data']['id'],
            'user_id' => $actor->id,
        ]);
    }




}
