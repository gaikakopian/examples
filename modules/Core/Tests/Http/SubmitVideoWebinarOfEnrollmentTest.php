<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v2/programs` endpoint.
 */
class SubmitVideoWebinarOfEnrollmentTest extends EndpointTest
{
    public function test_it_submits_a_videowebinar()
    {

        /** @var Enrollment $enrollment */
        $enrollment = Enrollment::query()->where('state', Enrollment::STATES['TRAINING'])->firstOrFail();

        /** @var User $user */
        $user = $enrollment->user;

        /** @var Program $program */
        $program = $enrollment->program;

        $response = $this->actingAs($this->user)->postJson('/api/v2/enrollments/' . $enrollment->id . '/videowebinar');


        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
    }

}
