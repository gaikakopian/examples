<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v1/users/me` endpoint.
 */
class UpdateMeTest extends EndpointTest
{
    /** @test */
    public function test_it_updates_the_currently_authenticated_user()
    {
        $data = [
            'first_name' => 'Peter1',
            'last_name' => 'Pan2',
            'email' => 'peter@pan.com',
            'password' => 'start123',
            'address' => 'ADDRESSTEST',
            'city' => 'CITYTEST',
            'postcode' => 'POSTCODE',
            'about_me' => 'yolo',
            'country' => 'COUNTRYTEST1',
            'phone_number' => '12345',
            'phone_number_prefix' => '123',
            'whatsapp_number' => '12345',
            'whatsapp_number_prefix' => '00000',
            'country_of_origin' => 'COUNTRYTEST2',
            'gender' => 'male',
            'language' => 'jp',
        ];
        $response = $this->actingAs($this->user)->json('PUT', '/api/v2/users/me', $data);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $this->assertDatabaseHas('users', $data);
    }

    /** @test */
    public function it_throws_an_error_for_wrong_password_when_updating_current_user()
    {
        $response = $this->actingAs($this->user)->json('PUT', '/api/v2/users/me', [
            'newPassword' => 'peterPan',
            'password' => 'secret'
        ]);

        $response->assertStatus(403);
    }

    /** @test */
    public function it_does_not_allow_the_user_to_change_role()
    {

        $originalRole = $this->user->primary_role;

        $response = $this->actingAs($this->user)->json('PUT', '/api/v2/users/me', [
            'primary_role' => 'OTHER_ROLE',
//            'level' => 'TEST'
        ]);


        $newSetup = User::query()->find($this->user->id);
        $this->assertEquals($newSetup->primary_role, $originalRole);


        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

    }
}
