<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Program;

/**
 * Test the functionality of the `/api/v2/programs/{id}` endpoint.
 */
class ShowProgramTest extends EndpointTest
{
    public function test_it_shows_a_program()
    {
        $response = $this->actingAs($this->user)->getJson('/api/v2/programs/1');


        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJsonStructure([
            'data' => [
                'id',
                'language',
                'code',
                'title',
                'description',
            ]
        ]);
    }

    public function test_it_hides_an_unaccessible_program()
    {
        $program = Program::find(1);
        $program->organizations()->detach();

        $response = $this->actingAs($this->user)->get('/api/v2/programs/1');

        $this->assertEquals(404, $response->getStatusCode(), $response->getContent());
    }
}
