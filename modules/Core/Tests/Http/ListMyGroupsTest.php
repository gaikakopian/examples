<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;

/**
 * Test the functionality of the `/api/v1/users/me/groups` endpoint.
 */
class ListMyGroupsTest extends EndpointTest
{
    /** @test */
    public function test_it_lists_all_my_groups()
    {
        $response = $this->actingAs($this->user)->get('/api/v2/users/me/groups');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'users' => [
                        [
                            'groupRole',
                        ]
                    ]
                ]
            ]
        ]);
    }
}
