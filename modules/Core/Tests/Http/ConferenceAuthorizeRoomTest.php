<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v1/conference/auth/room` endpoint.
 */
class ConferenceAuthorizeRoomTest extends EndpointTest
{
    public function test_it_responds_with_200_when_a_user_may_join_a_room()
    {
        $userId = $this->user->id;
        $externalRoomId = '8579d842-309a-4947-a992-fce85db4ce8b';

        $match = $this->user->enrollments->first()->participations->first()->match;
        $match->update([
            'external_room_id' => $externalRoomId
        ]);

        $payload = ['userId' => $userId, 'roomId' => $externalRoomId];
        $response = $this->json('POST', '/api/v2/conference/authorize/room', $payload);
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
    }

    public function test_it_makes_me_a_driver()
    {
        $userId = $this->user->id;
        $externalRoomId = '8579d842-309a-4947-a992-fce85db4ce8b';

        $enrollment = $this->user->enrollments->firstWhere('role', Role::ROLES['mentor']);
        $match = $enrollment->participations->first()->match;
        $match->update([
            'external_room_id' => $externalRoomId
        ]);

        $payload = ['userId' => $userId, 'roomId' => $externalRoomId];
        $response = $this->json('POST', '/api/v2/conference/authorize/room', $payload);

        $response->assertJsonFragment([
            'isDriver' => true
        ]);
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
    }

    public function test_it_makes_me_a_non_driver()
    {

        $externalRoomId = '8579d842-309a-4947-a992-fce85db4ce9b';

        /** @var User $user */
        $user = User::where('email', '=', 'mentee@volunteer-vision.com')->firstOrFail();
        // make sure user is not admin;
        foreach ($user->roles as $role) {
            if ($role->name === Role::ROLES['admin']){
                $user->roles()->detach($role);
            }
        }

        $enrollment = $user->enrollments->firstWhere('role', Role::ROLES['mentee']);

        $match = $enrollment->participations->first()->match;
        $match->update(['external_room_id' => $externalRoomId]);

        $payload = ['userId' => $user->id, 'roomId' => $externalRoomId];
        $response = $this->json('POST', '/api/v2/conference/authorize/room', $payload);

        $response->assertJsonFragment(['isDriver' => false]);
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
    }

    public
    function test_it_responds_with_403_when_a_user_may_not_join_a_room()
    {
        $externalRoomId = '8579d842-309a-4947-a992-fce85db4ce8b';

        $match = $this->user->enrollments->first()->participations->first()->match;
        $match->update([
            'external_room_id' => $externalRoomId
        ]);

        $payload = ['userId' => 26, 'roomId' => $externalRoomId];
        $response = $this->json('POST', '/api/v2/conference/authorize/room', $payload);
        $this->assertEquals(403, $response->getStatusCode(), $response->getContent());
    }

    public
    function test_it_does_return_403_on_random_user()
    {
        $externalRoomId = '8579d842-309a-4947-a992-fce85db4ce8b';

        $match = $this->user->enrollments->first()->participations->first()->match;
        $match->update([
            'external_room_id' => $externalRoomId
        ]);

        $payload = ['userId' => 26, 'roomId' => $externalRoomId];
        $response = $this->json('POST', '/api/v2/conference/authorize/room', $payload);
        $this->assertEquals(403, $response->getStatusCode(), $response->getContent());
    }

    public
    function test_it_allows_admin_to_join_the_room()
    {
        $user = User::where('email', '=', 'admin@volunteer-vision.com')->firstOrFail();

        $externalRoomId = '-NOT-EXISTING-ID-BUT-ADMIN-CAN-ACCESS-ANYTHING!';

        $payload = ['userId' => $user->id, 'roomId' => $externalRoomId];
        $response = $this->json('POST', '/api/v2/conference/authorize/room', $payload);
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
    }
}
