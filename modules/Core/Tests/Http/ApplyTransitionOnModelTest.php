<?php

namespace modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Notification;
use Modules\Core\Domain\Models\User;
use Modules\Scheduling\Domain\Models\Appointment;

/**
 * Test the functionality of the `/api/v2/statemachine/{model_name}/{id}/{transition}` endpoint.
 */
class ApplyTransitionOnModelTest extends EndpointTest
{
    use DatabaseTransactions;

    public function test_it_applies_an_allowed_transition()
    {
        Notification::fake();
        $test_user = factory(User::class)->create(['state' => User::STATES['NEW']]);

        $response = $this->actingAs($this->user)->postJson('/api/v2/statemachine/user/' . $test_user->id . '/complete_registration');
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        //checking if the state was updated in the database
        $this->assertDatabaseHas('users', [
            'id' => $test_user->id,
            'state' => User::STATES['REGISTERED'],
        ]);
    }

    public function test_it_does_not_apply_not_allowed_transitions()
    {
        $test_user = factory(User::class)->create(['state' => User::STATES['NEW']]);
        $response = $this->actingAs($this->user)->post('/api/v2/statemachine/user/' . $test_user->id . '/quit');
        $response->assertStatus(400);
    }


    public function test_it_does_not_apply_wrong_transitions()
    {
        $test_appointment = factory(Appointment::class)->create(['state' => Appointment::STATES['NEW']]);
        $response = $this->actingAs($this->user)->post('/api/v2/statemachine/appointment/' . $test_appointment->id . '/random_wrong_transition');
        $response->assertStatus(400);
    }

    /**
     *tests when wrong model name is passed
     */
    public function test_it_throws_an_error_for_wrong_model()
    {
        $response = $this->actingAs($this->user)->post('/api/v2/statemachine/random_model/1/plan');
        $response->assertStatus(400);
    }

    /**
     *tests when a wrong id is passed
     */
    public function test_it_throws_an_error_for_wrong_id()
    {
        $response = $this->actingAs($this->user)->post('/api/v2/statemachine/user/876423234/quit');
        $response->assertStatus(400);
    }
}
