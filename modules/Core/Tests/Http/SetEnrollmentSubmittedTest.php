<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v1/enrollments/{id}/set-ready` endpoint.
 */
class SetEnrollmentSubmittedTest extends EndpointTest
{
    public function test_it_lets_a_supervisor_set_an_enrollment_to_ready()
    {
        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'state' => Enrollment::STATES['NEW']
        ]);

        $response = $this->actingAs($this->user)->postJson('/api/v2/enrollments/' . $enrollment->id . '/submit');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
    }
}
