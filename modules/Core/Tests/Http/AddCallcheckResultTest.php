<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Support\Facades\Log;

/**
 * Test the functionality of the `/api/v1/users/me/launch` endpoint.
 */
class AddCallcheckResultTest extends EndpointTest
{
    public function test_it_saves_callcheck_in_database()
    {

        $route = route('me.addCallcheck');


        $data = [
            'success' => true,
            'downloadSpeed' => 10.5,
            'uploadSpeed' => 5.5,
            'mosAudio' => 3.3,
            'mosVideo' => 3.5,
            'meta' => ['hello' => 'world']
        ];

        $response = $this->actingAs($this->user)->json('POST', $route, $data);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $this->assertDatabaseHas('user_callcheck_results', [
            'user_id' => $this->user->id,
            'success' => true,
            'download_speed' => 10.5,
            'upload_speed' => 5.5,
            'mos_audio' => 3.3,
            'mos_video' => 3.5,

        ]);
    }

}
