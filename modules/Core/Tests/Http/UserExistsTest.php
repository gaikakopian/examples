<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\Core\Domain\Models\User;


class UserExistsTest extends EndpointTest
{
    use DatabaseTransactions;

    /**
     * Test the functionality of the POST `/api/v2/invites` endpoint.
     */
    public function test_a_user_exists()
    {
        $user = User::inRandomOrder()->first();
        $response = $this->postJson('/api/v2/auth/exists', [
            'email' => $user->email
        ]);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
    }

    /**
     * Test the functionality of the POST `/api/v2/invites` endpoint.
     */
    public function test_a_user_does_not_exist()
    {
        $response = $this->postJson('/api/v2/auth/exists', [
            'email' => 'asdadadsadadadadadinvited.user@test.de'
        ]);

        $this->assertEquals(404, $response->getStatusCode(), $response->getContent());
    }

}
