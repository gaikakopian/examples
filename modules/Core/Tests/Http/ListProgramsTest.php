<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Program;

/**
 * Test the functionality of the `/api/v2/programs` endpoint.
 */
class ListProgramsTest extends EndpointTest
{
    public function test_it_lists_accessible_programs()
    {
        $program = Program::find(1);
        $program->organizations()->detach(1);

        $response = $this->actingAs($this->user)->get('/api/v2/programs');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJsonStructure([
            'data' => [[]]
        ]);
        $response->assertJsonMissing(['id' => 1]);
        $response->assertJsonFragment(['id' => 2]);
    }
}
