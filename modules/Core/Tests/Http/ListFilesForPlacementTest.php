<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\File;

/**
 * Test the functionality of the `/api/v2/programs/1/files` endpoint.
 */
class ListFilesForPlacementTest extends EndpointTest
{
    public function test_it_lists_files_for_a_csr_manager()
    {
        // Generate a known file for our User
        factory(File::class)->create([
            'title' => 'Test File',
            'source' => 'https://example.com',
            'target_audience' => 'mentor',
            'program_id' => 1,
        ]);

        $response = $this->actingAs($this->user)->getJson('/api/v2/files/reporting');

        $this->assertEquals($response->getStatusCode(), 200, $response->getContent());
    }


    public function test_it_lists_files_for_a_embassador()
    {
        // Generate a known file for our User
        factory(File::class)->create([
            'title' => 'Test File',
            'source' => 'https://example.com',
            'target_audience' => 'mentor',
            'program_id' => 1,
        ]);

        $response = $this->actingAs($this->user)->getJson('/api/v2/files/embassador');

        $this->assertEquals($response->getStatusCode(), 200, $response->getContent());
    }
}
