<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v2/groups/{id}/members` endpoint.
 */
class AddUserToGroupTest extends EndpointTest
{
    public function test_it_adds_a_user_to_a_group()
    {
        $admin = User::where('email', 'admin@volunteer-vision.com')->first();

        $response = $this->actingAs($admin)->json('POST', '/api/v2/groups/1/members', [
            'userId' => 3
        ]);
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $this->assertDatabaseHas('group_user', [
            'user_id' => 3,
            'role' => 'member',
        ]);

        $response = $this->actingAs($admin)->json('POST', '/api/v2/groups/1/members', [
            'userId' => 4,
            'isSupervisor' => true,
        ]);
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $this->assertDatabaseHas('group_user', [
            'user_id' => 4,
            'role' => 'supervisor',
        ]);
    }
}
