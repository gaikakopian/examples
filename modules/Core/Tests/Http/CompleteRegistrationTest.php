<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Support\Facades\Notification;
use Modules\Core\Domain\Models\User;

class CompleteRegistrationTest extends EndpointTest
{
    /**
     * Test the functionality of the `/api/v2/users/me/complete-registration` endpoint.
     */
    public function test_it_completes_registration_action()
    {
        Notification::fake();

        $this->user->state = User::STATES['NEW'];
        $this->user->save();

        $response = $this->actingAs($this->user)->json('POST', '/api/v2/users/me/complete-registration');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $this->assertDatabaseHas('users', [
            'id' => $this->user->id,
            'state' => 'REGISTERED',
        ]);
    }
}
