<?php

namespace modules\Core\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;
use App\Notifications\Webinar\WebinarFinishedNotification;
use App\Notifications\Webinar\WebinarInvitationNotification;
use App\Notifications\Webinar\WebinarRescheduledNotification;
use Illuminate\Support\Facades\Notification;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Webinar;

class WebinarStateMachineTest extends LaravelTest
{

    /**
     * @var Webinar
     */
    protected $model;
    protected $user;

    public function test_it_applies_a_transition()
    {
        Notification::fake();


        $this->model->state = Webinar::STATES['NEW'];

        //------------------------- test transition 'plan'-------------------
        $result = $this->model->transition('plan');

        $this->assertTrue($result);
        $this->assertEquals(Webinar::STATES['PLANNED'], $this->model->currentState());

        //asserting that the transition has been logged in the history
        $this->assertEquals(1, $this->model->history()->count());
        $this->assertNotNull($this->model->history()->get()->last()->actor_id);

        //asserting that the 'last_state_change_at' column is updated
        $this->assertNotNull($this->model->last_state_change_at);

        // will be send in cronjob
//        Notification::assertSentTo(
//            $this->user,
//            WebinarInvitationNotification::class
//        );

        //--------------------------------------------------------------------


        //----------------------------- test transition 'reschedule' --------------
        $result = $this->model->transition('reschedule');

        $this->assertTrue($result);
        $this->assertEquals(Webinar::STATES['PLANNED'], $this->model->currentState());

        //asserting that a cancel notification has been sent
        Notification::assertSentTo(
            $this->user,
            WebinarRescheduledNotification::class
        );
        //asserting that the transition has been logged in the history
        $this->assertEquals(2, $this->model->history()->count());

        //--------------------------------------------------------------------


        //----------------------------- test transition 'finish' --------------
        $result = $this->model->transition('finish');

        $this->assertTrue($result);
        $this->assertEquals(Webinar::STATES['SUCCESS'], $this->model->currentState());

        //asserting that a cancel notification has been sent
        // we do not have this transition anyre;
//        Notification::assertSentTo(
//            $this->user,
//            WebinarFinishedNotification::class
//        );
//        //asserting that the transition has been logged in the history
        $this->assertEquals(3, $this->model->history()->count());

        //--------------------------------------------------------------------
    }

    protected function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create(['accept_email' => true, 'accept_sms' => true, 'preferred_channel' => null]);

        $program = factory(Program::class)->create();

        //tested Model
        $this->model = factory(Webinar::class)->create([
            'program_id' => $program->id,
            'state' => Webinar::STATES['PLANNED']
        ]);

        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'program_id' => $program->id,
        ]);
        $enrollment->webinars()->attach($this->model->id);


        //setting a fake_actor that represents the authenticated user who is applying changes
        $fake_actor = factory(User::class)->create();
        $this->be($fake_actor);
    }
}
