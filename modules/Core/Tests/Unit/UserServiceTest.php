<?php

namespace Modules\Core\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;
use App\Infrastructure\Http\QueryParams;
use Carbon\Carbon;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Domain\Services\UserService;

class UserServiceTest extends LaravelTest
{
    /**
     * @var UserService
     */
    protected $userService;

    public function setUp()
    {
        parent::setUp();

        $this->userService = app(UserService::class);
    }

    public function test_it_fetches_users()
    {
        /**
         * @var $user User
         * @var $group Group
         * @var $otherGroup Group
         */
        $result = $this->userService->list();

        $this->assertGreaterThan(1, $result->count());
        $user = $result->first();

        $this->assertEquals(User::class, get_class($user));
    }


    public function it_quits_everything_when_a_user_quits()
    {

        $user = User::query()->inRandomOrder()->first();
        $this->userService->quitUser($user);
        $this->assertEquals(User::class, get_class($user));
    }

    /**
     *
     */
    public function test_it_searches_user()
    {

        // should only find 1 user containing this string.
        $uniqueString = 'idfgkbakvsanfdfdskfkslfjpeiorqewewq';
        $mail = $uniqueString . '@mail.com';

        factory(User::class)->create(['email' => $mail]);

        $query = new QueryParams();
        $query->setSearch($uniqueString);

        $response = $this->userService->list($query);

        $this->assertEquals(1, $response->count());
        $this->assertEquals($response->first()->email, $mail);
    }


    /**
     *
     */
    public function test_it_finds_user_by_intl_phonenumber()
    {

        // should only find 1 user containing this string.
        $uniquePhone = '991234567890';
        $countryCode = '49';

        $fullNumber = $countryCode . $uniquePhone;

        $user = factory(User::class)->create(['phone_number' => $uniquePhone, 'phone_number_prefix' => $countryCode]);

        $result = $this->userService->findUserByInternationalPhoneNumber($fullNumber);
        $this->assertNotNull($result);
        $this->assertEquals($result->id, $user->id);

        // try with + in front
        $result = $this->userService->findUserByInternationalPhoneNumber('+' . $fullNumber);
        $this->assertEquals($result->id, $user->id);

        // try with /  in the middle

        $result = $this->userService->findUserByInternationalPhoneNumber('+' . $countryCode . '/' . $uniquePhone);
        $this->assertEquals($result->id, $user->id);

        // try with starting 00

        $result = $this->userService->findUserByInternationalPhoneNumber('00' . $countryCode . '/' . $uniquePhone);
        $this->assertEquals($result->id, $user->id);


    }




    public function test_it_finds_user_data_from_ip()
    {

        $ip = '185.17.205.235';
        $user = new User();

        $this->userService->getUserDataFromIP($user, $ip);

        $this->assertNotNull($user->postcode);
        $this->assertNotNull($user->city);
        $this->assertNotNull($user->lat);
        $this->assertNotNull($user->lng);
    }

}
