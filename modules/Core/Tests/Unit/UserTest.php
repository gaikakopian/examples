<?php

namespace Modules\Core\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;
use Modules\Core\Domain\Models\User;

class UserTest extends LaravelTest
{
    public function test_it_finds_a_user_by_email_or_phone()
    {
        $user = factory(User::class)->create([
            'email' => 'test@example.com',
            'phone_number' => '1-805-651-3136',
        ]);

        $this->assertEquals($user->id, (new User())->findForPassport('test@example.com')->id);
        $this->assertEquals($user->id, (new User())->findForPassport('1-805-651-3136')->id);
        $this->assertNull((new User())->findForPassport('fooBar'));
    }
}
