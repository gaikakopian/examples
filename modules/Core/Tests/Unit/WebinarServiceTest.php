<?php

namespace Modules\Core\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;
use Carbon\Carbon;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Webinar;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Domain\Services\WebinarService;

class WebinarServiceTest extends LaravelTest
{
    /**
     * @var WebinarService
     */
    protected $webinarService;

    public function setUp()
    {
        parent::setUp();
        $this->webinarService = app(WebinarService::class);
    }

    public function test_it_finds_correct_webinar()
    {
        $user = User::where('email', 'client@volunteer-vision.com')->firstOrFail();
        $enrollment = $user->enrollments->first();

        $results = $this->webinarService->listForEnrollment($enrollment);
        // @todo: test cases:
        // 1.  orgnization->private webinrs
        // 2. multiple programs
        // 3. multiple target audiences;

        $this->assertNotNull($results);
    }
}
