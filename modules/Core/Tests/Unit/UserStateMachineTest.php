<?php

namespace Modules\Core\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\Core\Domain\Models\User;
use SM\StateMachine\StateMachine;

class UserStateMachineTest extends LaravelTest
{
    use DatabaseTransactions;

    protected $model;


    protected function setUp()
    {
        parent::setUp();

        $this->model = factory(User::class)->create(['accept_email' => true, 'preferred_channel' => null]);

        //setting a fake_actor that represents the authenticated user who is applying changes
        $fake_actor = factory(User::class)->create();
        $this->be($fake_actor);
    }


    public function test_it_creates_a_state_machine_for_a_model()
    {
        $this->assertInstanceOf(StateMachine::class, $this->model->stateMachine());
    }

    public function test_it_retrieves_current_state()
    {
        $this->assertEquals('REGISTERED', $this->model->currentState());
    }

    public function test_it_checks_if_a_transition_is_allowed()
    {
        $this->model->state = User::STATES['NEW'];
        $this->assertTrue($this->model->transitionAllowed('complete_registration'));

        $this->model->state = User::STATES['REGISTERED'];
        $this->assertFalse($this->model->transitionAllowed('complete_registration'));
        $this->assertTrue($this->model->transitionAllowed('ban'));
        $this->assertTrue($this->model->transitionAllowed('quit'));

        $this->model->state = User::STATES['QUIT'];
        $this->assertFalse($this->model->transitionAllowed('complete_registration'));

        $this->assertFalse($this->model->transitionAllowed('quit'));
        $this->assertFalse($this->model->transitionAllowed('ban'));
        $this->assertTrue($this->model->transitionAllowed('return'));
    }

    public function test_it_applies_a_transition()
    {
        $this->model->state = User::STATES['NEW'];

        $this->assertEquals('NEW', $this->model->currentState());

        // test transition 'complete_registration'
        $result = $this->model->transition('complete_registration');

        $this->assertTrue($result);
        $this->assertEquals(User::STATES['REGISTERED'], $this->model->currentState());

        //asserting that the transition has been logged in the history
        $this->assertEquals(1, $this->model->history()->count());
        $this->assertNotNull($this->model->history()->first()->actor_id);

        //asserting that the 'last_state_change_at' column is updated
        $this->assertNotNull($this->model->last_state_change_at);

        // test transition 'quit'
        $result = $this->model->transition('quit');

        $this->assertTrue($result);
        $this->assertEquals(User::STATES['QUIT'], $this->model->currentState());
        $this->assertEquals(2, $this->model->history()->count());
    }

    public function test_it_knows_whether_it_logs_transitions()
    {
        $this->assertTrue($this->model->logsTransitions());
    }
}
