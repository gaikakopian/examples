<?php

namespace Modules\Core\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;
use App\Notifications\Enrollment\UserQuitNotification;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Notification;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;
use SM\StateMachine\StateMachine;

class EnrollmentStateMachineTest extends LaravelTest
{
    use DatabaseTransactions;

    /**
     * @var Enrollment
     */
    protected $model;


    protected $user;

    public function test_it_creates_a_state_machine_for_a_model()
    {
        $this->assertInstanceOf(StateMachine::class, $this->model->stateMachine());
    }

    public function test_it_retrieves_current_state()
    {
        $this->assertEquals('NEW', $this->model->currentState());
    }

    public function test_it_checks_if_a_transition_is_allowed()
    {
        $this->assertTrue($this->model->transitionAllowed('start_training'));

        $this->model->state = Enrollment::STATES['TRAINING'];
        $this->assertTrue($this->model->transitionAllowed('quit'));
    }

    public function test_start_training_with_training()
    {
        Notification::fake();
        $this->model->state = Enrollment::STATES['NEW'];
        $this->model->role = Role::ROLES['mentor'];

        /** @var Program $program */
        $program = $this->model->program;
        $program->has_mentor_webinar = true;
        $program->save();

        $result = $this->model->transition('start_training');
        $this->assertTrue($result);
        $this->assertEquals(Enrollment::STATES['TRAINING'], $this->model->currentState());
    }
    public function test_start_training_without_training()
    {
        Notification::fake();
        $this->model->state = Enrollment::STATES['NEW'];
        $this->model->role = Role::ROLES['mentee'];

        /** @var Program $program */
        $program = $this->model->program;
        $program->has_mentee_webinar = false;
        $program->save();

        $result = $this->model->transition('start_training');
        $this->assertTrue($result);
        $this->assertEquals(Enrollment::STATES['ACTIVE'], $this->model->currentState());
    }
    public function test_it_applies_a_transition()
    {
        Notification::fake();
        $basecount = $this->model->history()->count();

        $this->model->state = Enrollment::STATES['NEW'];

        $this->assertEquals('NEW', $this->model->currentState());
        $result = $this->model->transition('start_training');
        $this->assertTrue($result);

        //asserting that the transition has been logged in the history

        $this->assertNotNull($this->model->history()->first()->actor_id);

        //asserting that the 'last_state_change_at' column is updated
        $this->assertNotNull($this->model->last_state_change_at);

        // test transition 'quit'
        $result = $this->model->transition('quit');

        $this->assertTrue($result);
        $this->assertEquals(Enrollment::STATES['QUIT'], $this->model->currentState());

        //asserting that a cancel notification has been sent
//        Notification::assertSentTo(
//            $this->user,
//            UserQuitNotification::class
//        );
        //test that history is updated
    }

    public function test_it_knows_whether_it_logs_transitions()
    {
        $this->assertTrue($this->model->logsTransitions());
    }

    protected function setUp()
    {
        parent::setUp();


        //setting a fake_actor that represents the authenticated user who is applying changes

        $this->user = factory(User::class)->create(['accept_email' => true, 'accept_sms' => true, 'preferred_channel' => null]);


        $this->model = factory(Enrollment::class)->create(['user_id' => $this->user->id,'state' => 'NEW']);

        //setting a fake_actor that represents the authenticated user who is applying changes
        $fake_actor = factory(User::class)->create();
        $this->be($fake_actor);
    }
}
