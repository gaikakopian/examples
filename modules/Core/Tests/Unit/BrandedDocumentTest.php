<?php

namespace Modules\Core\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;
use Modules\Core\Domain\Services\ContentChooser\SearchObject;
use Modules\Core\Domain\Services\ContentChooser\ContentChooser;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\BrandedDocument;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Log;

/**
 * Test the functionality of the ContentChooser Service.
 */
class BrandedDocumentTest extends LaravelTest
{
    public function test_it_replaces_placeholders_with_data()
    {
        $user = factory(User::class)->create(['first_name' => 'Tommy', 'last_name' => 'Tester']);
        $organization = factory(Organization::class)->create(['name' => 'Super Company', 'type' => 'shop']);
        $program = factory(Program::class)->create(['title' => 'Nice Program']);
        $document = factory(BrandedDocument::class)->create(['subject' => 'Nice document for {{ $funny_placedholder_name_user->first_name }}', 'content' => 'Your name is {{ $user->first_name }} {{ $user->last_name }} and the parsed organization\'s name is {{ $the_organization->name }} ({{ $the_organization->type }}). Parsed program\'s title is {{ $funny_named_program_placeholder->title }}']);


        $parsed_subject = $document->parseSubject(['funny_placedholder_name_user' => $user]);
        $this->assertEquals('Nice document for Tommy', $parsed_subject);

        $parsed_content = $document->parseContent([
            'user' => $user,
            'the_organization' => $organization,
            'funny_named_program_placeholder' => $program
        ]);
        $this->assertEquals('Your name is Tommy Tester and the parsed organization\'s name is Super Company (shop). Parsed program\'s title is Nice Program', $parsed_content);
    }

    public function test_it_converts_content_markdown_with_placeholder_to_html_with_data()
    {
        $user = factory(User::class)->create(['first_name' => 'Tommy', 'last_name' => 'Tester']);
        $document = factory(BrandedDocument::class)->create(['content' => '@markdown
# Hello {{ $user->first_name }}! This **word** is bold!
@endmarkdown']);

        $parsed_content = $document->parseContent([
            'user' => $user,
        ]);

        $this->assertEquals('<h1>Hello Tommy! This <strong>word</strong> is bold!</h1>', $parsed_content);
    }

    public function test_it_can_handle_simple_strings_and_keeps_them_simple()
    {
        $document = factory(BrandedDocument::class)->create(['subject' => 'Hello', 'content' => 'I do not contain anything special.']);

        $parsed_subject = $document->parseSubject();
        $parsed_content = $document->parseContent();

        $this->assertEquals('Hello', $parsed_subject);
        $this->assertEquals('I do not contain anything special.', $parsed_content);
    }

    public function test_it_can_handle_null_values()
    {
        $document = factory(BrandedDocument::class)->create(['subject' => null, 'content' => 'I do not contain anything special.']);

        $parsed_subject = $document->parseSubject();

        $this->assertEquals('', $parsed_subject);
    }

    public function test_it_can_handle_html()
    {
        $document = factory(BrandedDocument::class)->create(['content' => 'How are <strong>you</strong>?']);
        $parsed_content = $document->parseContent();
        $this->assertEquals('How are <strong>you</strong>?', $parsed_content);
    }

    public function test_it_has_accessible_raw_data()
    {
        $document = factory(BrandedDocument::class)->create(['subject' => 'Hello {{ $user->first_name }}', 'content' => '@markdown
# Hello {{ $user->first_name }}! This **word** is bold!
@endmarkdown']);

        $this->assertEquals('Hello {{ $user->first_name }}', $document->subject);
        $this->assertEquals('@markdown
# Hello {{ $user->first_name }}! This **word** is bold!
@endmarkdown', $document->content);
    }

    public function test_it_finds_articles()
    {
        $articles = factory(BrandedDocument::class, 3)->create(['type' => 'article']);

        $this->assertTrue(BrandedDocument::articles()->count() >= 3);
    }

    public function test_it_writes_parse_error_to_logfile()
    {
        $this->markTestSkipped("different handling from now on");

        $document = factory(BrandedDocument::class)->create(['content' => 'Your name is {{ $user->first_name }}']);

        config(['database.log_queries' => false]);

        Log::shouldReceive('warning')
            ->once()
            ->with("[BrandedDocuments] Error while parsing the content of BrandedDocument #{$document->id}.", ['data' => []]);

        $parsed_content = $document->parseContent();
    }

    public function test_it_gets_a_list_of_branded_documents_from_classnames() {
        $keys =  BrandedDocument::getAllKnownKeys();
        $this->assertNotNull($keys);

        $keys =  BrandedDocument::getAllKnownKeysWithComments();
        $this->assertNotNull($keys);
    }
}
