<?php

namespace Modules\Core\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;
use Carbon\Carbon;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\GroupService;

class GroupServiceTest extends LaravelTest
{
    /**
     * @var GroupService
     */
    protected $groupService;

    public function setUp()
    {
        parent::setUp();

        $this->groupService = app(GroupService::class);
    }

    public function test_user_is_member_of_a_group()
    {
        /**
         * @var $user User
         * @var $group Group
         * @var $otherGroup Group
         */
        $user = factory(User::class)->create();
        $group = factory(Group::class)->create();
        $group->users()->attach($user, ['role' => 'member']);

        $otherGroup = factory(Group::class)->create();

        $this->assertTrue($this->groupService->isMemberOf($user, $group->id));
        $this->assertFalse($this->groupService->isMemberOf($user, $otherGroup->id));
    }

    public function test_it_generates_the_same_password_all_day_for_a_group()
    {
        $todayStart = Carbon::now()->startOfDay();
        $todayMid = $todayStart->copy()->addHours(10);
        $todayEnd = $todayStart->copy()->endOfDay();

        Carbon::setTestNow($todayStart);
        $startOfTodayPassword = $this->groupService->getTodaysPasswordForGroup(1);
        Carbon::setTestNow($todayMid);
        $midOfTodayPassword = $this->groupService->getTodaysPasswordForGroup(1);
        Carbon::setTestNow($todayEnd);
        $endOfTodayPassword = $this->groupService->getTodaysPasswordForGroup(1);

        $this->assertEquals($startOfTodayPassword, $midOfTodayPassword);
        $this->assertEquals($endOfTodayPassword, $midOfTodayPassword);
    }

    public function test_it_generates_a_different_password_on_different_days_for_a_group()
    {
        $today = Carbon::now()->startOfDay()->addHours(10);
        $tomorrow = $today->copy()->addDays(1);

        Carbon::setTestNow($today);
        $todayPassword = $this->groupService->getTodaysPasswordForGroup(1);
        Carbon::setTestNow($tomorrow);
        $tomorrowPassword = $this->groupService->getTodaysPasswordForGroup(1);

        $this->assertNotEquals($todayPassword, $tomorrowPassword);
    }

    public function test_it_generates_a_different_password_for_a_different_group()
    {
        $groupOnePassword = $this->groupService->getTodaysPasswordForGroup(1);
        $groupTwoPassword = $this->groupService->getTodaysPasswordForGroup(2);

        $this->assertNotEquals($groupOnePassword, $groupTwoPassword);
    }
}
