<?php

namespace Modules\Core\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class AddUserToGroupRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'is_supervisor' => 'sometimes|boolean',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function authorize()
    {
        // Authorization handled via Scope Middleware.
        return true;
    }
}
