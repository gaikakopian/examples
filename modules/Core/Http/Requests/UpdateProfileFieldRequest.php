<?php

namespace Modules\Core\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class UpdateProfileFieldRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules() : array
    {
        return [
            'data' => 'required'
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function authorize() : bool
    {
        return true;
    }
}
