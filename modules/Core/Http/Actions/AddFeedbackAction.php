<?php

namespace Modules\Core\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use App\Services\MultiTenant\Facades\MultiTenant;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\RegistrationCodeService;
use Modules\Core\Domain\Services\UserFeedbackService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Requests\AddFeedbackRequest;
use Modules\Core\Http\Requests\RegisterUserRequest;
use Modules\Core\Http\Resources\UserHttpResource;

class AddFeedbackAction extends Action
{

    protected $responder;
    protected $userFeedbackService;


    public function __construct(
        ResourceResponder $responder,
        UserFeedbackService $userFeedbackService

    )
    {
        $this->userFeedbackService = $userFeedbackService;
        $this->responder = $responder;
    }

    public function __invoke(AddFeedbackRequest $request)
    {

        $code = UserFeedback::CODES['OPEN_FEEDBACK'];

        $request->input('code');
        $userid = Auth::id();


        $this->userFeedbackService->create(
            [
                'question_code' => $code,
                'response_scalar' => $request->input('response_scalar'),
                'response_text' => $request->input('response_text'),
                'user_id' => $userid
            ]
        );

        return response()->json(['message' => 'ok']);

//        $feedback = new UserFeedback();
//        $feedback->fill([
//            'question_code' => $code,
//            'response_scalar' => $request->input('response_scalar'),
//            'response_text' => $request->input('response_text'),
//            'user_id' => $userid
//        ]);
//        $feedback->save();

        return response('', 200);

    }

}
