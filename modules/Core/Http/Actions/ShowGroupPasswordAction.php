<?php

namespace Modules\Core\Http\Actions;

use App\Exceptions\Client\NotAuthorizedException;
use App\Infrastructure\Http\Action;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Services\GroupService;

class ShowGroupPasswordAction extends Action
{
    protected $groupService;

    public function __construct(GroupService $groupService)
    {
        $this->groupService = $groupService;
    }

    public function __invoke(int $id, Request $request)
    {
        $group = $this->groupService->get($id);

        if (Auth::user()->cannot('supervise', $group)) {
            throw new NotAuthorizedException();
        }

        $todaysPassword = $this->groupService->getTodaysPasswordForGroup($id);

        return response($todaysPassword, 200);
    }
}
