<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Resources\KeyaccountHttpResource;
use Modules\Core\Http\Resources\UserHttpResource;

class ShowKeyaccountAction extends Action
{
    protected $userService;
    protected $responder;

    public function __construct(UserService $userService, ResourceResponder $responder)
    {
        $this->userService = $userService;
        $this->responder = $responder;
    }

    public function __invoke(Request $request)
    {
        $myId = Auth::id();
        /**
         * @var $me User
         * @var $organization Organization
         * @var $keyaccount User
         */

        $me = $this->userService->get($myId);
        $organization = $me->organization;
        $keyaccount = $organization->keyaccount;
        if (empty($keyaccount)) {
            // @todo: put this in config or make it brand dependend;
            $keyaccount = User::where('email', 'julia.winkler@volunteer-vision.com')->firstOrFail();
        }

        return $this->responder->send($keyaccount, KeyaccountHttpResource::class);
    }
}
