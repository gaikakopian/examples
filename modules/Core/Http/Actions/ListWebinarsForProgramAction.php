<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Services\WebinarService;
use Modules\Core\Http\Resources\WebinarHttpResource;

class ListWebinarsForProgramAction extends Action
{
    protected $webinarService;
    protected $responder;

    public function __construct(WebinarService $webinarService, ResourceResponder $responder)
    {
        $this->webinarService = $webinarService;
        $this->responder = $responder;
    }

    public function __invoke($id, Request $request)
    {
        $webinars = $this->webinarService->listForUserAndProgram(Auth::id(), $id);

        return $this->responder->send($webinars, WebinarHttpResource::class);
    }
}
