<?php

namespace Modules\Core\Http\Actions\Conference;

use App\Exceptions\Client\NotAuthorizedException;
use App\Infrastructure\Http\Action;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ConferenceService;
use Modules\Core\Domain\Services\UserService;

class AuthorizeUserAction extends Action
{
    /**
     * An instance of the ConferenceService.
     *
     * @var ConferenceService
     */
    protected $conferenceService;


    /**
     * @var UserService
     */
    protected $userService;

    /**
     * Construct an instance of the action.
     *
     * @param ConferenceService $conferenceService
     */
    public function __construct(ConferenceService $conferenceService, UserService $userService)
    {
        $this->conferenceService = $conferenceService;
        $this->userService = $userService;
    }

    /**
     * Handle the request.
     *
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request)
    {
        $token = $request->input('token');

        if (!$this->conferenceService->isValidConferenceToken($token)) {
            throw new NotAuthorizedException('The token provided is not authorized to log into the conference app.');
        }

        $userId = $this->conferenceService->getUserIdFromToken($token);

        /** @var User $user */
        $user = $this->userService->get($userId);
        $data = $this->getConferenceUserdata($user);
        return response()->json($data)->setStatusCode(200);
    }

    /**
     * @param User $user
     * @return array
     */
    private function getConferenceUserdata(User $user): array
    {

        /** interface
         *
         * // see confercenbackend /src/strategies/callback.js
         * externalUser.firstName,
         * externalUser.lastName,
         * externalUser.id,
         * externalUser.email,
         * externalUser.avatarUrl,
         * externalUser.extra
         * };
         */
        return [
            'id' => $user->id . "",
            'success' => 'The user may log into the conference app.',
            'firstName' => $user->first_name,
            'lastName' => $user->last_name,
            'email' => $user->email,
            'avatarUrl' => $user->avatar ? $user->avatar['small'] : null,
            //@todo: we need the room ID to find this!
            'role' => ($user->primary_role === User::ROLES['mentee'] ? User::ROLES['mentee'] : User::ROLES['mentor']),
            'extra' => [
                'phone' => $user->getQualifiedPhoneNumber(),
                'whatsapp' => $user->getQualifiedWhatsappNumber()
            ]
            // does not work like this..
        ];
    }
}
