<?php

namespace Modules\Core\Http\Actions\Conference;

use App\Exceptions\Client\NotAuthorizedException;
use App\Exceptions\Client\NotFoundException;
use App\Infrastructure\Http\Action;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\UserService;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GetUserAction extends Action
{

    /**
     * @var UserService
     */
    protected $userService;


    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Handle the request.
     *
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request, $id)
    {
        /** @var User $user */
        $user = $this->userService->get($id);
        $data = $this->getConferenceUserdata($user);
        return response()->json($data)->setStatusCode(200);
    }

    /**
     * @param User $user
     * @return array
     */
    private function getConferenceUserdata(User $user): array
    {

        /** interface
         *
         * // see confercenbackend /src/strategies/callback.js
         * externalUser.firstName,
         * externalUser.lastName,
         * externalUser.id,
         * externalUser.email,
         * externalUser.avatarUrl,
         * externalUser.extra
         * };
         */
        return [
            'id' => $user->id . "",
            'success' => 'The user may log into the conference app.',
            'firstName' => $user->first_name,
            'lastName' => $user->last_name,
            'email' => $user->email,
            'avatarUrl' => $user->avatar ? $user->avatar['small'] : null,
            //@todo: we need the room ID to find this!
            'role' => ($user->primary_role === User::ROLES['mentee'] ? User::ROLES['mentee'] : User::ROLES['mentor']),
            'extra' => [
                'phone' => $user->getQualifiedPhoneNumber(),
                'whatsapp' => $user->getQualifiedWhatsappNumber()
            ]
            // does not work like this..
        ];
    }

}
