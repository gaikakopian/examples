<?php

namespace Modules\Core\Http\Actions\Conference;

use App\Infrastructure\Http\Action;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Scheduling\Domain\Models\Appointment;
use Modules\Scheduling\Domain\Services\AppointmentService;

class AppointmentCallbackAction extends Action
{
    protected $matchService;
    protected $appointmentService;

    /**
     * Construct an instance of the action.
     */
    public function __construct(MatchService $matchService, AppointmentService $appointmentService)
    {
        $this->middleware('verify-conference-signature');
        $this->matchService = $matchService;
        $this->appointmentService = $appointmentService;

    }

    /**
     * Handle the request.
     *
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response
     */
    public function __invoke(Request $request, $id)
    {

        /** @var Match $match */
        $match = $this->matchService->getByExternalRoomId($id);
        $lectionCompleted = $request->input('currentLection');

        // @todo: finish current appointment first.


        /** @var Appointment $pastAppointment */
        $pastAppointment = $this->appointmentService->getCurrentAppointmentForMatch($match->id);
        if ($pastAppointment) {
            $pastAppointment->current_lecture = $lectionCompleted;
            $pastAppointment->save();
            if ($pastAppointment->transitionAllowed('finish')) {
                $pastAppointment->transition('finish');
            }
        } else {
            Log::info("[AppointmentCallbackAction] Could not find current Appointment for Match $match->id");
        }

        $appointment = $this->appointmentService->planFutureAppointmentForMatch($match->id, $request->get('date'));
        $appointment->state = Appointment::STATES['CONFIRMED'];
        $appointment->save();

        return response()->json([
            'id' => $appointment->id,
            'planned_start' => $appointment->planned_start->toIso8601String()
        ])->setStatusCode(200);
    }
}
