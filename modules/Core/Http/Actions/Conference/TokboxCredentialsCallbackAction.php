<?php

namespace Modules\Core\Http\Actions\Conference;

use App\Infrastructure\Http\Action;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Services\UserFeedbackService;
use Modules\External\Domain\Services\OpentokService;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Scheduling\Domain\Models\Appointment;
use Modules\Scheduling\Domain\Services\AppointmentService;
use OpenTok\OpenTok;

class TokboxCredentialsCallbackAction extends Action
{
    protected $matchService;

    protected $opentokService;


    /**
     * Construct an instance of the action.
     */
    public function __construct(MatchService $matchService, OpentokService $opentokService)
    {
        $this->middleware('verify-conference-signature');
        $this->matchService = $matchService;
        $this->opentokService = $opentokService;
    }

    /**
     * Handle the request.
     *
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response
     */
    public function __invoke(Request $request, $id)
    {

        /** @var Match $match */
        $match = $this->matchService->getByExternalRoomId($id);

        $userId = $request->header("X-VV-User-Id");

        $credentials = $this->opentokService->getCredentialsForMatch($match);

        return response()->json([
            'data' => $credentials
        ])->setStatusCode(200);
    }
}
