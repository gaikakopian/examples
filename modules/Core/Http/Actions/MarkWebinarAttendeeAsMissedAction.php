<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use Illuminate\Http\Request;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\WebinarService;

/**
 * Class MarkWebinarAttendeeAsMissedAction
 * @package Modules\Core\Http\Actions
 * @deprecated  should be in admin?
 */
class MarkWebinarAttendeeAsMissedAction extends Action
{
    protected $enrollmentService;
    protected $webinarService;

    public function __construct(EnrollmentService $enrollmentService, WebinarService $webinarService)
    {
        $this->enrollmentService = $enrollmentService;
        $this->webinarService = $webinarService;
    }

    public function __invoke(int $webinarId, int $enrollmentId, Request $request)
    {
        $webinar = $this->webinarService->get($webinarId);

        /** @var Enrollment $enrollment */
        $enrollment = $this->enrollmentService->get($enrollmentId);

        $this->webinarService->markAttended($enrollment->id, $webinar, false);

        if ($enrollment->transitionAllowed('missed_training')) {
            $enrollment->transition('missed_training');
        }


        return response('', 200);
    }
}
