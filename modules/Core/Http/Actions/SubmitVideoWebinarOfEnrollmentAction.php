<?php

namespace Modules\Core\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Webinar;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\WebinarService;
use Modules\Core\Http\Resources\EnrollmentHttpResource;
use Modules\Core\Http\Requests\EnrollToWebinarRequest;

class SubmitVideoWebinarOfEnrollmentAction extends Action
{
    protected $enrollmentService;
    protected $responder;

    public function __construct(
        EnrollmentService $enrollmentService,
        ResourceResponder $responder
    )
    {
        $this->enrollmentService = $enrollmentService;
        $this->responder = $responder;
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function __invoke(int $id)
    {
        /** @var Enrollment $enrollment */
        $enrollment = $this->enrollmentService->get($id);

        $user = Auth::user();

        if (!$enrollment->transitionAllowed('complete_video_training')) {
            throw new ClientException('This enrollment is not in training state');
        }


        $enrollment->transition('complete_video_training');


        return response()->json(['status' => 'ok']);


    }
}
