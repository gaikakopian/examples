<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class AcceptPoliciesAction extends Action
{

    public function __invoke(Request $request)
    {
        $user = Auth::user();
        $user->pp_accepted_at = Carbon::now();
        $user->tos_accepted_at = Carbon::now();
        $user->save();

        return response('ok', 200);
    }
}
