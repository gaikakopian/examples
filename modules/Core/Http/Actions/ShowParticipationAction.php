<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Services\ConferenceService;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Core\Http\Resources\ParticipationHttpResource;

class ShowParticipationAction extends Action
{
    protected $participationService;
    protected $responder;

    public function __construct(ParticipationService $participationService, ResourceResponder $responder)
    {
        $this->participationService = $participationService;
        $this->responder = $responder;
    }

    public function __invoke(int $id, Request $request)
    {
        $userId = Auth::id();
        $participations = $this->participationService->getForUser($userId, $id);
        // @todo: witch match to avoid n+1

        return $this->responder->send($participations, ParticipationHttpResource::class);
    }
}
