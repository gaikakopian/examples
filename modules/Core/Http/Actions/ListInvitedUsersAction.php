<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Services\UserInvitationService;
use Modules\Core\Http\Resources\UserInvitationHttpResource;

class ListInvitedUsersAction extends Action
{
    protected $userInvitationService;
    protected $responder;

    public function __construct(UserInvitationService $userInvitationService, ResourceResponder $responder)
    {
        $this->userInvitationService = $userInvitationService;
        $this->responder = $responder;
    }


    public function __invoke(Request $request)
    {
        $invited_users = $this->userInvitationService->getAllInvitedByUser(Auth::id());

        return $this->responder->send($invited_users, UserInvitationHttpResource::class);
    }
}
