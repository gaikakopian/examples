<?php

namespace Modules\Core\Http\Actions;

use App\Exceptions\Client\NotAuthorizedException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\UnauthorizedException;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\FileService;
use Modules\Core\Http\Resources\FileHttpResource;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class ListFilesForPlacementAction extends Action
{
    protected $enrollmentService;
    protected $fileService;
    protected $responder;

    public function __construct(
        EnrollmentService $enrollmentService,
        FileService $fileService,
        ResourceResponder $responder
    ) {
        $this->enrollmentService = $enrollmentService;
        $this->fileService = $fileService;
        $this->responder = $responder;
    }

    public function __invoke(string $placement, Request $request)
    {

        //  Currently we do not check if the user has the corrct rights.

        $programId = $request->query->get('programId', null);
        $audience = $request->query->get('targetAudience', null);
        $files = $this->fileService->listForProgramAndPlacement($placement, $programId, $audience, null);

        return $this->responder->send($files, FileHttpResource::class);
    }
}
