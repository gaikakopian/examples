<?php

namespace modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use Modules\Core\Domain\Services\StateMachineTransitionsService;

class ApplyTransitionOnModelAction extends Action
{
    protected $stateMachineTransitionsService;

    /**
     * ApplyTransitionOnModelAction constructor.
     * @param StateMachineTransitionsService $stateMachineTransitionsService
     */
    public function __construct(StateMachineTransitionsService $stateMachineTransitionsService)
    {
        $this->stateMachineTransitionsService = $stateMachineTransitionsService;
    }

    /**
     * @param string $model_name
     * @param int $id
     * @param string $transition
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $model_name, int $id, string $transition)
    {
        $model_instance = $this->stateMachineTransitionsService->getModelInstance($model_name, $id);

        $this->stateMachineTransitionsService->applyTransition($model_instance, $transition);

        return response('', 200);
    }
}
