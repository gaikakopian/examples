<?php

namespace Modules\Core\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use App\Services\MultiTenant\Facades\MultiTenant;
use Illuminate\Support\Carbon;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserLead;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\RegistrationCodeService;
use Modules\Core\Domain\Services\UserLeadService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Requests\RegisterUserRequest;
use Modules\Core\Http\Resources\UserHttpResource;

class RegisterUserAction extends Action
{
    protected $userService;
    protected $codeService;
    protected $enrollmentService;
    protected $registrationCodeService;
    protected $userLeadService;
    protected $responder;


    public function __construct(
        UserService $userService,
        RegistrationCodeService $codeService,
        RegistrationCodeService $registrationCodeService,
        EnrollmentService $enrollmentService,
        ResourceResponder $responder,
        UserLeadService $userLeadService

    )
    {
        $this->userService = $userService;
        $this->registrationCodeService = $registrationCodeService;
        $this->codeService = $codeService;
        $this->userLeadService = $userLeadService;

        $this->enrollmentService = $codeService;
        $this->responder = $responder;
    }

    /**
     * @param RegisterUserRequest $request
     * @return mixed
     * @throws ClientException
     */
    public function __invoke(RegisterUserRequest $request)
    {

        /** @var RegistrationCode $registrationCode */
        $registrationCode = $this->codeService->getByCode($request->input('code'));


        $data = $this->prepareData($registrationCode, $request);
        /** @var User $user */
        $user = $this->userService->create($data);

        $user->pp_accepted_at = Carbon::now();
        $user->tos_accepted_at = Carbon::now();
        $user->save();

        if ($user->primary_role === User::ROLES['supervisor']) {
            $user->groups()->attach($registrationCode->group_id, ['role' => User::ROLES['supervisor']]);

        }

        if ($user->primary_role !== User::ROLES['supervisor']) {
            $this->registrationCodeService->applyRegistrationCode($user, $registrationCode);
        }

        $this->userLeadService->eventEmailRegistered($user->email);

        return $this->responder->send($user, UserHttpResource::class);
    }

    /**
     * - Merge request data with brand- and organization_id
     *   from the RegistrationCode.
     * - Check whether we can send SMS to this User.
     * - Hash the password.
     *
     * @param RegisterUserRequest $request
     * @return array
     */
    private function prepareData(RegistrationCode $registrationCode, RegisterUserRequest $request)
    {
        if (empty($registrationCode->group)) {
            throw new ClientException('This registration code has no group!');
        }

        $data = array_merge(
            $request->all(),
            [
                'primary_role' => $registrationCode->primary_role,
                'brand_id' => $registrationCode->brand_id,
                'organization_id' => $registrationCode->group->organization_id,
                'registration_code_id' => $registrationCode->id,
                'tenant_id' => $registrationCode->tenant_id,
            ]
        );

        // If there is no phone number, we can't send SMSs...
        if (!array_key_exists('phone_number', $data)) {
            $data['accept_sms'] = false;
        }
        // Hmm... I'd die to know what's in there. But meh. Bye bye....
        $data['password'] = bcrypt($data['password']);

        return $data;
    }


    // moved to
//    private function processAutoEnrollment(RegistrationCode $registrationCode, User $user)
//    {
//        $program = $registrationCode->auto_enroll_in;
//
//        if ($program) {
//            $data = [
//                'user_id' => $user->id,
//                'program_id' => $program,
//                'role' => $registrationCode->primary_role
//            ];
//            $e = new Enrollment($data);
//            $e->save();
//            //   @todo         $this->enrollmentService->create($data); // This throws an random error :O
//        }
//    }
}
