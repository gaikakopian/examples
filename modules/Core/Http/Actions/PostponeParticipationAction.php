<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Core\Http\Requests\PostponeParticipationRequest;
use Modules\Core\Http\Resources\ParticipationHttpResource;

class PostponeParticipationAction extends Action
{
    protected $participationService;
    protected $responder;

    public function __construct(ParticipationService $participationService, ResourceResponder $responder)
    {
        $this->participationService = $participationService;
        $this->responder = $responder;
    }

    public function __invoke(int $id, PostponeParticipationRequest $request)
    {
        $until = $request->input('until');

        $participation = $this->participationService->update($id, [
            'start_matching_after' => $until
        ]);

        return $this->responder->send($participation, ParticipationHttpResource::class);
    }
}
