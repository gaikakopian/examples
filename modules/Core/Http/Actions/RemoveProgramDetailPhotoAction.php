<?php

namespace Modules\Core\Http\Actions;

use Modules\Core\Http\Actions\RemoveProgramImageAction;

class RemoveProgramDetailPhotoAction extends RemoveProgramImageAction
{
    protected $attachmentName = 'detail_photo';
}
