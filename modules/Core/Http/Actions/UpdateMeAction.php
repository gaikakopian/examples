<?php

namespace Modules\Core\Http\Actions;

use App\Exceptions\Client\WrongPasswordException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Requests\UpdateUserRequest;
use Modules\Core\Http\Resources\UserHttpResource;

class UpdateMeAction extends Action
{
    protected $userService;
    protected $responder;

    public function __construct(UserService $userService, ResourceResponder $responder)
    {
        $this->userService = $userService;
        $this->responder = $responder;
    }

    public function __invoke(UpdateUserRequest $request)
    {
        $myId = Auth::id();

//        $input = $request->all();

        $input = $request->validated();

        if (array_key_exists('email', $input) || array_key_exists('new_password', $input)) {

            $user = $this->userService->get($myId);

            if (!Hash::check($input['password'], $user->password)) { // @todo: should be in User Class;
                throw new WrongPasswordException();
            }

            if (array_key_exists('new_password', $input)) {
                $input['password'] = bcrypt($input['new_password']);
            }
        }


        $me = $this->userService->update($myId, $input);

        return $this->responder->send($me, UserHttpResource::class);
    }
}
