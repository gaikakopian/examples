<?php

namespace Modules\Core\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Exceptions\Server\ServerException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use App\Services\MultiTenant\Facades\MultiTenant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\RegistrationCodeService;
use Modules\Core\Domain\Services\UserFeedbackService;
use Modules\Core\Domain\Services\UserLeadService;
use Modules\Core\Http\Requests\AddFeedbackRequest;
use Modules\Core\Http\Requests\RegisterUserRequest;
use Modules\Core\Http\Resources\UserHttpResource;
use Modules\Scheduling\Domain\Services\AppointmentService;
use Symfony\Component\Finder\Exception\AccessDeniedException;

class UserLeadMailResponseAction extends Action
{

    protected $responder;
    protected $userleadLeadService;


    public function __construct(
        ResourceResponder $responder,
        UserLeadService $userleadLeadService
    )
    {
        $this->userLeadService = $userleadLeadService;
    }

    public function __invoke(Request $request, $action, $leadid, $secret)
    {

        /** @var User $userlead */
        $userlead = $this->userLeadService->get($leadid);

        $action = strtolower($action);

        if ($userlead->emailSecret($action) !== $secret) {
            throw new AccessDeniedException("The given Link was invalid. Please try again.");
        }

        switch ($action) {
            case 'activate':
                if ($userlead->transitionAllowed('activate')){
                    $userlead->transition('activate');
                }

                break;
            case 'disable':
                if ($userlead->transitionAllowed('disable')){
                    $userlead->transition('disable');
                }
                break;
            default:
                throw new ServerException("Unknown action:" . $action);
        }

//        $url = $userlead->brand->frontend_url;

        return view('responses.generalResponse');


    }

}
