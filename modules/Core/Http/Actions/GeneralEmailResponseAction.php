<?php

namespace Modules\Core\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Exceptions\Server\ServerException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use App\Services\MultiTenant\Facades\MultiTenant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\RegistrationCodeService;
use Modules\Core\Domain\Services\UserFeedbackService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Requests\AddFeedbackRequest;
use Modules\Core\Http\Requests\RegisterUserRequest;
use Modules\Core\Http\Resources\UserHttpResource;
use Modules\Scheduling\Domain\Services\AppointmentService;
use Symfony\Component\Finder\Exception\AccessDeniedException;

class GeneralEmailResponseAction extends Action
{

    protected $responder;
    protected $userService;


    public function __construct(
        ResourceResponder $responder,
        UserService $userService

    )
    {
        $this->userService = $userService;
    }

    public function __invoke(Request $request, $action, $userid, $secret)
    {

        /** @var User $user */
        $user = $this->userService->get($userid);

        $action = strtolower($action);
        App::setLocale($user->language);

        if ($user->emailSecret($action) !== $secret) {
            throw new AccessDeniedException("The given Link was invalid. Please try again.");
        }

        switch ($action) {
            case 'confirmemail':
                $user->email_optin_at = Carbon::now();
                $user->save();

                break;
            default:
                throw new ServerException("Unknown action" . $action);
        }

        $url = $user->brand->frontend_url;

        return view('responses.generalResponse', ['link' => $url]);


    }

}
