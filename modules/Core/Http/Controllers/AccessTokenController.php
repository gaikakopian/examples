<?php

namespace Modules\Core\Http\Controllers;

use App\Exceptions\Client\NotAuthenticatedException;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Passport\Http\Controllers\AccessTokenController as PassportAccessTokenController;
use League\OAuth2\Server\Exception\OAuthServerException;
use Modules\Admin\Domain\Services\TwoFactorAuthService;
use Modules\Core\Domain\Models\SecurityIncident;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\SecurityIncidentService;
use Modules\Partnerapi\Domain\Models\OrganizationSaml;
use Modules\Partnerapi\Domain\Services\SamlService;
use Psr\Http\Message\ServerRequestInterface;

use Zend\Diactoros\Response as Psr7Response;

/**
 * This request finds user's tenant_id without requiring it to be set.
 *
 *
 * Class AccessTokenController
 * @package Modules\Core\Http\Controllers
 */
class AccessTokenController extends PassportAccessTokenController
{
    /**
     * Find if a given username has special login conditions (e.g. SSO)
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginRedirectByEmail(Request $request)
    {
        $username = $request->input('email');
        if (empty($username)) {
            $responseJson = ['message' => 'none'];
            return response()->json($responseJson);
        }
        /** @var SamlService $samlService */
        $samlService = app(SamlService::class);

        /** @var OrganizationSaml $organizationSaml */
        $organizationSaml = $samlService->findSamlRedirectByEmail($username);
        if ($organizationSaml === null) {
            $responseJson = ['message' => 'none'];
            return response()->json($responseJson);
        }

        $loginRoute = route('saml.login', ['slug' => $organizationSaml->slug]);
        $responseJson = ['message' => 'redirect', 'redirectUrl' => $loginRoute];
        return response()->json($responseJson);
    }

    /**
     * Authorize a client to access the user's account.
     *
     * @param  ServerRequestInterface $request
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \League\OAuth2\Server\Exception\OAuthServerException
     */
    public function issueToken(ServerRequestInterface $request)
    {

        // first we have to find the scope;
        // alternative: public function findForPassport(string $username)

        $requestParameters = (array)$request->getParsedBody();

        $username = isset($requestParameters["username"]) ? $requestParameters["username"] : null;

        if ($username === null) {
            throw new NotAuthenticatedException('NO_EMAIL_OR_PHONENUMBER_PROVIDED');
        }

        $username = strtolower($username);

        /** @var User $user */
        $user =
            User::query()->where('email', '=', $username)
                ->first();

        if (!$user) {
            throw new NotAuthenticatedException('EMAIL_OR_PHONENUMBER_NOT_FOUND');
        }

        //
        // 2FA for admins and coordinators;
        //
        if ($user->requiresTwoFactorAuth()) {
            if (empty($requestParameters['validation_2fa'])) {
                throw new NotAuthenticatedException('TWO_FACTOR_AUTHENTICATION_REQUIRED');
            }
            /** @var TwoFactorAuthService $twoFAservice */
            $twoFAservice = app(TwoFactorAuthService::class);
            if (!$twoFAservice->verify($user, $requestParameters['validation_2fa'])) {
                throw new NotAuthenticatedException('TWO_FACTOR_AUTHENTCATION_INVALID');
            }

        }

        // check if account was removed or is disabled;
        $user->isAllowedToLogin();

        $request->username = $username;


        try {
            return $this->convertResponse(
                $this->server->respondToAccessTokenRequest($request, new Psr7Response)
            );
        } catch (OAuthServerException $exception) {

            $securityIncidentService = app(SecurityIncidentService::class);
            $securityIncidentService->saveSecurityIncident(
                SecurityIncident::TYPES['INVALID_LOGIN'],
                ['ip' => \Request::ip(), 'email' => $username],
                $user->id
            );

            return $this->withErrorHandling(function () use ($exception) {
                throw $exception;
            });
        }

    }
}
