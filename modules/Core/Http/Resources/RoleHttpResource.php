<?php

namespace Modules\Core\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class RoleHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'displayName' => empty($this->display_name) ? $this->name : $this->display_name
        ];
    }
}
