<?php

namespace Modules\Core\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class EnrollmentMinimalHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'program' => new ProgramHttpResource($this->whenLoaded('program')),
            'role' => $this->role,
            'state' => $this->state,
            'lastStateChange' => $this->last_state_change,
            'participations' => ParticipationHttpResource::collection($this->whenLoaded('participations')),
            'webinars' => WebinarHttpResource::collection($this->whenLoaded('webinars')),
        ];
    }
}
