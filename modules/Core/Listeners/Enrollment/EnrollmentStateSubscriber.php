<?php

namespace Modules\Core\Listeners\Enrollment;

use App\Infrastructure\Domain\EventSubscriberProxy;
use Symfony\Component\EventDispatcher\Event;

class EnrollmentStateSubscriber extends EventSubscriberProxy
{
    /** {@inheritDoc} */
    protected function mapEvents(): array
    {
        return [
            'Enrollment.start_training' => 'startTraining',
            'Enrollment.complete_training' => 'completeTraining',
            'Enrollment.participate' => 'participate',
            'Enrollment.enroll' => 'enroll',
            'Enrollment.postpone' => 'postpone',
            'Enrollment.resume' => 'resume',
            'Enrollment.quit' => 'quit',
            'Enrollment.return_to_available' => 'returnToAvailable',
            'Enrollment.return_to_training' => 'returnToTraining',
        ];
    }

    /** {@inheritDoc} */
    protected function chooseHandler(Event $event)
    {
        return new DefaultEnrollmentStateHandler();
    }
}
