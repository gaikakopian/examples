<?php

namespace Modules\Core\Reminder;

use App\Exceptions\Client\ClientException;


use App\Notifications\Match\MatchNotConfirmedReminder2Notification;
use App\Notifications\Match\MatchNotConfirmedReminder3Notification;
use App\Notifications\Match\MatchNotConfirmedReminderNotification;
use App\Notifications\Match\MatchPausedCheckinNotification;
use App\Notifications\Match\MatchRequestReminderNotification;
use App\Reminders\Handler\AbstractReminderHandler;
use Illuminate\Support\Carbon;
use Modules\Admin\Domain\Services\CoordinatorTodoService;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

class MatchPausedReminderHandler extends AbstractReminderHandler
{


    protected $coordinatorTodoService;

    public function __construct(CoordinatorTodoService $coordinatorTodoService)
    {
        parent::__construct();
        $this->coordinatorTodoService = $coordinatorTodoService;

        $this->setDateCallback(function ($match) {
            return $match->last_state_change_at;
        });

        $this->registerEvents();
    }

    public function registerEvents()
    {
        $this->registerEvent(14 * 24, [$this, 'firstReminder']);
        $this->registerEvent( 21 * 24, [$this, 'moveMatchToActive']);
    }

    /**
     * @param Match $match
     */
    public function firstReminder($match)
    {
        $this->sendReminder($match);
    }


    public function moveMatchToActive(Match $match)
    {

        try {
            $match->transition('auto_resume');
        } catch (ClientException $e) {
            report($e);
        }
    }

    private function sendReminder(Match $match)
    {

        /** @var Participation $participation */
        $participation = $match->getMentorParticipation();
        /** @var User $mentor */
        $mentor = $participation->enrollment->user;


        // tdb: needed?
        $mentor->last_reminder = Carbon::now();
        $mentor->save();

        $notification = new MatchPausedCheckinNotification($mentor->brand, $match, $participation->enrollment);
        $mentor->notify($notification);

    }
}