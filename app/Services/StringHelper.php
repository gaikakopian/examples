<?php

namespace App\Services;


class StringHelper
{

    static function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    static function trimOnlyLetters(string $string)
    {

        // @todo preg_amtch..
        return preg_replace('/\PL/u', '', trim($string));

    }

    static function startsWith(string $string, string $search)
    {
        return (strncmp($string, $search, strlen($search)) === 0);
    }

    static function dashesToCamelCase($string)
    {
        return join('', array_map(function ($w) {
            return ucfirst($w);
        }, explode('_', $string)));
    }

    static function strip_introduction($message)
    {
        $introductionValues = ['hallo', 'hello', 'hola', 'guten tag', 'hi'];

        $message = trim($message);
        $lowerMessage = strtolower($message);

        foreach ($introductionValues as $word) {
            if ($lowerMessage !== $word && self::startsWith($lowerMessage, $word)) {
                return trim(substr($message, strlen($word)));
            }
        }
        return $message;
    }

    static function snake_to_camel($input){
        return self::dashesToCamelCase($input);
    }
    static function camel_to_snake($input)
    {
        if (preg_match('/[A-Z]/', $input) === 0) {
            return $input;
        }
        $r = $input;

        $pattern = '/([A-Z0-9])([A-Z])/';
        $r = preg_replace_callback($pattern, function ($a) {
            return $a[1] . "_" . $a[2];
        }, $r);

        $pattern = '/([a-z])([A-Z0-9])/';
        $r = strtolower(preg_replace_callback($pattern, function ($a) {
            return $a[1] . "_" . (isset($a[2]) ? strtolower($a[2]) : '');
        }, $r));


        return $r;
    }

    static function classNameToDocumentKey($fqnClassName)
    {

        $namespace = explode("\\", $fqnClassName);
        $class = end($namespace);

        return self::camel_to_snake($class);

    }
}