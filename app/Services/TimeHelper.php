<?php

namespace App\Services;

use Carbon\Carbon;
use DateTimeZone;
use Modules\Core\Domain\Models\User;

class TimeHelper
{
    /**
     * Convert db time column into minutes as integer
     *
     * @param string $time , e.g. '16:00:00'
     * @return int
     */
    public static function timeColumnToMinutes(string $time): int
    {
        list($hours, $minutes, $seconds) = explode(':', $time);

        return ($hours * 60) + $minutes;
    }

    /**
     * Convert db minutes into timecalumn
     *
     * @param  int minutes of day;
     * @return string $time , e.g. '16:00:00'
     */
    public static function minutesToTimeColumn(int $time): string
    {

        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf("%02d:%02d:00", $hours, $minutes);
    }

    /**
     * Get timezone offset to UTC in minutes from a timezone string
     *
     * @param string $time_zone , e.g. 'Europe/Berlin'
     * @return int
     */
    public static function timezoneAsUtcOffsetInMinutes(string $time_zone): int
    {
        // Set UTC as default time zone.
        date_default_timezone_set('UTC');
        $utc = Carbon::now();

        // Calculate offset.
        $current = timezone_open($time_zone);
        $offset_s = timezone_offset_get($current, $utc); // seconds

        return $offset_s / 60;
    }

    public static function formatForUser(User $user, Carbon $date)
    {
        $timezone = 'Europe/Berlin';
        $locale = 'en';
        $format = '%d %B %Y, %H:%M';

        if ($user) {
            $timezone = $user->timezone ?: $timezone;
            $locale = $user->language ?: $locale;
        }
        $date->setTimezone(new DateTimeZone($timezone));
        Carbon::setLocale($locale);
        return $date->formatLocalized($format) . ' (' . $timezone . ' time)';
    }
}
