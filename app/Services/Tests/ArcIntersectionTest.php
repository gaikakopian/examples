<?php

namespace App\Services\Tests;

use App\Infrastructure\AbstractTests\LaravelTest;
use App\Services\ArcIntersection\Arc;
use App\Services\ArcIntersection\ArcIntersection;

class ArcIntersectionTest extends LaravelTest
{
    protected $service;

    /** {@inheritdoc} */
    protected function setUp()
    {
        parent::setUp();
        $this->service = new ArcIntersection(360);
    }

    public function test_it_computes_an_identical_intersection()
    {
        $arc = new Arc(0, 90);
        $this->assertEquals(90, $this->service->computeIntersection($arc, $arc));
    }

    public function test_it_doesnt_compute_an_intersection()
    {
        $arc1 = new Arc(0, 180);
        $arc2 = new Arc(180, 360);
        $this->assertEquals(0, $this->service->computeIntersection($arc1, $arc2));

        $arc3 = new Arc(270, 90);
        $arc4 = new Arc(90, 270);
        $this->assertEquals(0, $this->service->computeIntersection($arc3, $arc4));
    }

    public function test_it_computes_a_partial_intersection()
    {
        $arc1 = new Arc(0, 180);
        $arc2 = new Arc(90, 270);
        $this->assertEquals(90, $this->service->computeIntersection($arc1, $arc2));
    }

    public function test_it_computes_a_containing_intersection()
    {
        $arc1 = new Arc(0, 270);
        $arc2 = new Arc(90, 180);
        $this->assertEquals(90, $this->service->computeIntersection($arc1, $arc2));

        $arc3 = new Arc(270, 180);
        $arc4 = new Arc(0, 80);
        $this->assertEquals(80, $this->service->computeIntersection($arc3, $arc4));

        $arc5 = new Arc(90, 180);
        $arc6 = new Arc(0, 180);
        $this->assertEquals(90, $this->service->computeIntersection($arc5, $arc6));
    }
}
