<?php

namespace App\Services\Tests;

use App\Infrastructure\AbstractTests\LaravelTest;
use App\Services\ChatbotService;
use App\Services\TimeHelper;
use Carbon\Carbon;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;
use Twilio\Rest\Chat;

class ChatbotServiceTest extends LaravelTest
{

    /**
     * @var ChatbotService
     */
    protected $service;

    /** {@inheritdoc} */
    protected function setUp()
    {
        parent::setUp();
        $this->service = app(ChatbotService::class);
    }

    public function test_it_detects_confirm_message()
    {

//        $message = 'Guten Tag
//Ja bitte!
//Vielen Dank
//Beste Grüße';
//        $response = $this->service->convertTextIntoMessageType($message);
//        $this->assertEquals(ChatbotService::MESSAGE_TYPES['CONFIRM'], $response);


        $message = 'Yes, sure thank you!';
        $response = $this->service->convertTextIntoMessageType($message);
        $this->assertEquals(ChatbotService::MESSAGE_TYPES['CONFIRM'], $response);

        $message = 'Ja';
        $response = $this->service->convertTextIntoMessageType($message);
        $this->assertEquals(ChatbotService::MESSAGE_TYPES['CONFIRM'], $response);

//        $message = '👍';
//        $response = $this->service->convertTextIntoMessageType($message);
//        $this->assertEquals(ChatbotService::MESSAGE_TYPES['CONFIRM'], $response);

        $message = 'Si';
        $response = $this->service->convertTextIntoMessageType($message);
        $this->assertEquals(ChatbotService::MESSAGE_TYPES['CONFIRM'], $response);
    }

    public function test_it_detects_start()
    {
        $response = $this->service->convertTextIntoMessageType('Start Simon');
        $this->assertEquals(ChatbotService::MESSAGE_TYPES['HELLO'], $response);

        $response = $this->service->convertTextIntoMessageType('start!');
        $this->assertEquals(ChatbotService::MESSAGE_TYPES['HELLO'], $response);


        $response = $this->service->convertTextIntoMessageType('!start!');
        $this->assertEquals(ChatbotService::MESSAGE_TYPES['HELLO'], $response);

        $response = $this->service->convertTextIntoMessageType('!..start!');
        $this->assertEquals(ChatbotService::MESSAGE_TYPES['HELLO'], $response);

    }

    public function test_it_detects_no()
    {
        $message = 'Hallo 
Nein leider kann nicht mehr.
Vielen Dank 
Beste Grüße';
        $response = $this->service->convertTextIntoMessageType($message);
        $this->assertEquals(ChatbotService::MESSAGE_TYPES['REJECT'], $response);

        $response = $this->service->convertTextIntoMessageType('No Simon');
        $this->assertEquals(ChatbotService::MESSAGE_TYPES['REJECT'], $response);

        $response = $this->service->convertTextIntoMessageType('!!no');
        $this->assertEquals(ChatbotService::MESSAGE_TYPES['REJECT'], $response);


        $response = $this->service->convertTextIntoMessageType('nope');
        $this->assertEquals(ChatbotService::MESSAGE_TYPES['REJECT'], $response);

        $response = $this->service->convertTextIntoMessageType('Nein');
        $this->assertEquals(ChatbotService::MESSAGE_TYPES['REJECT'], $response);
    }


    public function it_confirms_the_whatsapp_status()
    {

        $user = factory(User::class)->create(['first_name' => 'Tommy', 'last_name' => 'Tester']);

        $this->service->handleMessageType($user, ChatbotService::MESSAGE_TYPES['HELLO']);

        $this->assertEquals($user->whatsapp_status, 'confirmed');


    }

    public function test_it_handles_appointment_confirmation()
    {

        $user = factory(User::class)->create(['first_name' => 'Tommy', 'last_name' => 'Tester']);

        $appointment = factory(Appointment::class)->create([
            'match_id' => 1,
            'state' => 'PLANNED',
        ]);

        $this->service->setUserState($user, ChatbotService::STATES['appointment_confirmation'], ['id' => $appointment->id]);
        $this->service->handleMessageType($user, ChatbotService::MESSAGE_TYPES['CONFIRM']);


        $this->assertEquals($user->whatsapp_status, 'exists');
        $this->assertDatabaseHas('appointments', [
            'id' => $appointment->id,
            'state' => Appointment::STATES['CONFIRMED']
        ]);

    }

    public function test_it_handles_match_confirmation()
    {
        $user = factory(User::class)->create(['first_name' => 'Tommy', 'last_name' => 'Tester']);
        $enrollment = factory(Enrollment::class)->create(['user_id' => $user->id, 'role' => 'mentee']);


        $match = factory(Match::class)->create(['state' => Match::STATES['REQUESTED']]);
        $participation = factory(Participation::class)->create(['match_id' => $match->id, 'enrollment_id' => $enrollment->id]);
        $this->service->setUserState($user, ChatbotService::STATES['match_request'], ['id' => $match->id]);
        $this->service->handleMessageType($user, ChatbotService::MESSAGE_TYPES['CONFIRM']);


        $newMatch = Match::query()->find($match->id);
        $this->assertEquals(Match::STATES['UNCONFIRMED'], $newMatch->state);


    }

    public function test_it_handles_reject_request()
    {
        $user = factory(User::class)->create(['first_name' => 'Tommy', 'last_name' => 'Tester']);
        $match = factory(Match::class)->create(['state' => Match::STATES['REQUESTED']]);

        $this->service->setUserState($user, ChatbotService::STATES['match_request'], ['id' => $match->id]);
        $this->service->handleMessageType($user, ChatbotService::MESSAGE_TYPES['REJECT']);


        $newMatch = Match::query()->find($match->id);
        $this->assertEquals(Match::STATES['REJECTED'], $newMatch->state);


    }


}
