<?php

namespace App\Services\Tests;

use App\Infrastructure\AbstractTests\LaravelTest;
use App\Services\TimeHelper;
use Carbon\Carbon;

class TimeHelperTest extends LaravelTest
{
    public function test_it_converts_db_column_time_to_minutes()
    {
        $this->assertEquals(960, TimeHelper::timeColumnToMinutes('16:00:00'));
        $this->assertEquals(0, TimeHelper::timeColumnToMinutes('00:00:00'));
        $this->assertEquals(0, TimeHelper::timeColumnToMinutes('00:00:10'));
        $this->assertEquals(1, TimeHelper::timeColumnToMinutes('00:01:10'));
    }
    public function test_it_converts_db_minutes_to_timecolumn()
    {
        $this->assertEquals('16:00:00', TimeHelper::minutesToTimeColumn(960));
        $this->assertEquals('00:00:00', TimeHelper::minutesToTimeColumn(0));
        $this->assertEquals('00:01:00', TimeHelper::minutesToTimeColumn(1));
        $this->assertEquals('00:00:00', TimeHelper::minutesToTimeColumn(0));
    }

    public function test_it_calculates_timezone_offset_as_minutes()
    {
        $this->assertEquals(480, TimeHelper::timezoneAsUtcOffsetInMinutes('Asia/Chongqing'));
    }
    public function test_tz_offset_summer()
    {

        $winerTime = Carbon::create(2017, 8, 01, 12);
        Carbon::setTestNow($winerTime);

        $this->assertEquals(120, TimeHelper::timezoneAsUtcOffsetInMinutes('Europe/Berlin'));
    }
    public function test_tz_offset_winter()
    {
        $winerTime = Carbon::create(2017, 02, 01, 12);
        Carbon::setTestNow($winerTime);

        $this->assertEquals(60, TimeHelper::timezoneAsUtcOffsetInMinutes('Europe/Berlin'));
    }
    public function test_tz_offset_gz()
    {
        $winerTime = Carbon::create(2017, 02, 01, 12);
        Carbon::setTestNow($winerTime);
        $this->assertEquals(0, TimeHelper::timezoneAsUtcOffsetInMinutes('Etc/Greenwich'));
    }
}
