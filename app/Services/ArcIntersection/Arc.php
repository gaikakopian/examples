<?php

namespace App\Services\ArcIntersection;

use App\Infrastructure\Contracts\Interval;

class Arc implements Interval
{
    /**
     * Start of the arc.
     *
     * @var int
     */
    protected $start;

    /**
     * End of the arc.
     *
     * @var int
     */
    protected $end;

    /**
     * Construct a new Arc instance.
     *
     * @param int $start
     * @param int $end
     */
    public function __construct(int $start, int $end)
    {
        $this->start = $start;
        $this->end = $end;
    }

    /** {@inheritDoc} */
    public function intervalStart() : int
    {
        return $this->start;
    }

    /** {@inheritDoc} */
    public function intervalEnd() : int
    {
        return $this->end;
    }
}
