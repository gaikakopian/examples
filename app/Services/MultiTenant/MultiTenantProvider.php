<?php

namespace App\Services\MultiTenant;

use Illuminate\Support\ServiceProvider;

class MultiTenantProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(TenantState::class, function () {
            return new TenantState();
        });
    }
}
