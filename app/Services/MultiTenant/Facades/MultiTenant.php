<?php

namespace App\Services\MultiTenant\Facades;

use App\Services\MultiTenant\TenantState;
use Illuminate\Support\Facades\Facade;

class MultiTenant extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return TenantState::class;
    }
}
