<?php

namespace App\Reminder\Test;

use App\Infrastructure\AbstractTests\LaravelTest;
use App\Infrastructure\Http\QueryParams;
use App\Reminders\Handler\DummyReminderHandler;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Modules\Admin\Domain\Services\CoordinatorTodoService;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Domain\Services\UserService;
use Modules\Matching\Domain\Models\Match;

class ReminderHandlerTest extends LaravelTest
{

    public function setUp()
    {
        parent::setUp();


    }

    public function test_it_fires_correct_events()
    {

        $collection = new Collection();

        // no reminder; event just happened; should not fire;

        $object = new SampleReminderObject();
        $object->id = 1;
        $object->last_reminder = null;
        $object->last_status_changed = Carbon::now();
        $collection->push($object);

        // no reminder; event happened 24hrs ago; should fire 24h
        $object = new SampleReminderObject();
        $object->id = 2;
        $object->last_reminder = null;
        $object->last_status_changed = Carbon::now()->subHours(25);
        $collection->push($object);

        // no reminder; event happened 48hrs ago; should fire 24h
        $object = new SampleReminderObject();
        $object->id = 3;
        $object->last_reminder = null;
        $object->last_status_changed = Carbon::now()->subHours(49);
        $collection->push($object);

        // no reminder; event happened 48hrs ago; should fire 48h
        $object = new SampleReminderObject();
        $object->id = 4;
        $object->last_reminder = Carbon::now()->subHours(5);
        $object->last_status_changed = Carbon::now()->subHours(49);
        $collection->push($object);


        $count = 0;

        $handler = App(DummyReminderHandler::class);
        $handler->registerEvent(24, function ($obj) {
            $this->assertTrue(in_array($obj->id, [2, 3]));
        });
        $handler->registerEvent(48, function ($obj) {
            $this->assertEquals($obj->id, 4);
        });

        /** @var $obj SampleReminderObject */
        $firedActions = $handler->applyActions($collection, function ($obj) {
            return $obj->last_status_changed;
        });

        $this->assertEquals($firedActions, 3);


    }

    public function it_does_not_fire_event_when_field_is_null()
    {

        $handler = new DummyReminderHandler();
        $handler->registerEvent(0, function ($match) {
            echo '!! EVENT FIRED! ERROR' . PHP_EOL;
        });

        // no reminder; event happened 24hrs ago
        $collection = new Collection();

        $object = new SampleReminderObject();
        $object->id = 3;
        $object->last_reminder = null;
        $object->last_status_changed = null;
        $collection->push($object);

        /** @var $obj SampleReminderObject */
        $amountFired = $handler->applyActions($collection, function ($obj) {
            return $obj->last_status_changed;
        });


        $this->assertEquals($amountFired, 0);


    }
    public function it_does_not_fire_when_last_reminder()
    {

        $handler = new DummyReminderHandler();
        $handler->registerEvent(0, function ($match) {
            echo '!! EVENT FIRED! ERROR' . PHP_EOL;
        });

        // no reminder; event happened 24hrs ago
        $collection = new Collection();

        $object = new SampleReminderObject();
        $object->id = 3;
        $object->last_reminder = Carbon::now()->subHours(1);
        $object->last_status_changed = Carbon::now()->subHours(60);
        $collection->push($object);

        $handler->registerEvent(24, function ($obj) {
            $this->assertTrue(false); // should not happend
        });
        $handler->registerEvent(48, function ($obj) {
            $this->assertTrue(false); // should not happend
        });

        /** @var $obj SampleReminderObject */
        $amountFired = $handler->applyActions($collection, function ($obj) {
            return $obj->last_status_changed;
        });


        $this->assertEquals($amountFired, 0);


    }

}
