<?php

namespace App\Http\Middleware;

use App\Exceptions\Client\NotAuthorizedException;
use App\Services\MultiTenant\Facades\MultiTenant;
use Closure;
use Illuminate\Auth\Middleware\Authenticate as AuthenticateMiddleware;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Role;

class Authenticate extends AuthenticateMiddleware
{
    /** {@inheritDoc} */
    public function handle($request, Closure $next, ...$guards)
    {
        $this->authenticate($guards);

        $this->setTenant($request);

        return $next($request);
    }

    /**
     * Set the global tenant_id for the request.
     *
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    protected function setTenant($request) : void
    {
        $user = Auth::user();

        // Super-Admins can override the tenant via the X-Tenant-Id header,
        if ($request->hasHeader('X-Tenant-Id')) {
            if (!$user->hasRole(Role::ROLES['superadmin'])){
                throw new NotAuthorizedException('Only superadmins are allowed to switch tenants');
            }
            $tenantId = $request->header('X-Tenant-Id');

        } else {
            // … while all other users are limited to their own tenant_id
            $tenantId = $user->tenant_id;
        }

        MultiTenant::setTenantId($tenantId);
    }
}
