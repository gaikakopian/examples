<?php

namespace App\Domain\Models;

/*
protected function asDateTime($value) {
*/

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model {

    protected function asDateTime($value)
    {
        if (is_string($value)) {
            // parses json zulu strings;
            $date = Carbon::parse($value);
            return $date;
        }
        return parent::asDateTime($value);
    }
}