<?php

namespace App\Services\Tests;

use App\Infrastructure\AbstractTests\LaravelTest;
use App\Infrastructure\Domain\EventSubscriberProxy;
use Illuminate\Support\Facades\Log;
use Mockery;
use Symfony\Component\EventDispatcher\Event;

/**
 * Test the functionality of the EventSubscriberProxy.
 */
class EventSubscriberProxyTest extends LaravelTest
{
    /**
     * A mocked subclass of the abstract EventSubscriberProxy.
     *
     * @var App\Infrastructure\Domain\EventSubscriberProxy
     */
    protected $mock;

    /** {@inheritdoc} */
    protected function setUp()
    {
        parent::setUp();

        $this->mock = new TestProxy();
    }

    public function test_it_defers_to_the_handler_given_by_chooseHandler_method()
    {
        $event = Mockery::mock(Event::class);
        $event->shouldReceive('setName')
            ->once()
            ->with('handledByTestHandler');

        $this->mock->handlerMethod($event);
    }

    public function test_it_logs_when_handler_method_doesnt_exist_on_handler()
    {
        Log::shouldReceive('warning')
            ->once()
            ->with('[EventSubscriberProxy] Chosen handler class does not implment required method', [
                'handler' => new TestHandler(),
                'method' => 'missingMethod',
            ]);

        $event = Mockery::mock(Event::class);
        $this->mock->missingMethod($event);
    }

    public function test_it_logs_when_event_not_listed_in_mapEvents()
    {
        Log::shouldReceive('warning')
            ->once()
            ->with("[EventSubscriberProxy] Possible event 'badMethod' unhandled.");

        $event = Mockery::mock(Event::class);
        $this->mock->badMethod($event);
    }
}
