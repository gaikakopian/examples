<?php

namespace App\Services\Tests;

use App\Infrastructure\Domain\EventSubscriberProxy;
use Symfony\Component\EventDispatcher\Event;

/**
 * Mock class for testing the EventSubscriberProxy.
 */
class TestProxy extends EventSubscriberProxy
{
    protected function mapEvents() : array
    {
        return [
            'FakeEvent' => 'handlerMethod',
            'AnotherEvent' => 'missingMethod',
        ];
    }

    protected function chooseHandler(Event $event)
    {
        return new TestHandler();
    }
}
