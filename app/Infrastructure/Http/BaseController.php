<?php

namespace App\Infrastructure\Http;

use Illuminate\Routing\Controller ;

/**
 * BaseController for API development
 */
abstract class BaseController extends Controller
{
    //
}
