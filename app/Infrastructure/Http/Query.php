<?php

namespace App\Infrastructure\Http;

use App\Infrastructure\Contracts\Query as QueryInterface;
use Illuminate\Http\Request;
use InvalidArgumentException;

/**
 * A Bruno-based implementation of the query interface.
 *
 * All the `parseXYZ` methods as well as the `options` method are carbon-copied from
 * https://github.com/esbenp/bruno/blob/master/src/LaravelController.php
 *
 * In the `parseSort` and `parseFilterGroups` methods, conversion from camelCase
 * to snake_case model attribute names was additionally implemented.
 */
class Query implements QueryInterface
{
    /**
     * The request from which to extract the query params.
     *
     * @var Request
     */
    protected $request;

    /**
     * Default values for query options.
     *
     * @var array
     */
    public static $defaults = [
        'includes' => [],
        'withCount' => [],
        'sort' => [],
        'limit' => 25,
        'page' => null,
        'mode' => 'embed',
        'filter_groups' => [],
        'filter' => null,
        'search' => null
    ];

    /**
     * Actual values parsed from the request.
     *
     * @var array
     */
    protected $parsed_options;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Parse sort options.
     *
     * @param array $sort
     * @return array
     */
    protected function parseSort(array $sort): array
    {
        return array_map(function ($sort) {
            if (!isset($sort['direction'])) {
                $sort['direction'] = 'asc';
            }
            if (isset($sort['key'])) {
                $sort['key'] = snake_case($sort['key']);
            }

            return $sort;
        }, $sort);
    }

    /**
     * Parse include strings into resource and modes arrays.
     *
     * @param  array $includes
     * @return array The parsed resources and their respective modes
     */
    protected function parseIncludes(array $includes): array
    {
        $return = [
            'includes' => [],
            'modes' => []
        ];

        foreach ($includes as $include) {
            $explode = explode(':', $include);
            if (!isset($explode[1])) {
                $explode[1] = self::$defaults['mode'];
            }
            $return['includes'][] = $explode[0];
            $return['modes'][$explode[0]] = $explode[1];
        }

        return $return;
    }

    /**
     * Parse filter group strings into filters array.
     *
     * Filters are formatted as key:operator(value)
     * Example: name:eq(esben)
     *
     * @param  array $filter_groups
     * @return array
     */
    protected function parseFilterGroups(array $filter_groups): array
    {
        $return = [];

        foreach ($filter_groups as $group) {
            if (!array_key_exists('filters', $group)) {
                throw new InvalidArgumentException('Filter group does not have the `filters` key.');
            }

            $filters = array_map(function ($filter) {
                if (!isset($filter['not'])) {
                    $filter['not'] = false;
                }
                if (isset($filter['key'])) {
                    $filter['key'] = snake_case($filter['key']);
                }

                return $filter;
            }, $group['filters']);

            $return[] = [
                'filters' => $filters,
                'or' => isset($group['or']) ? $group['or'] : false
            ];
        }

        return $return;
    }


    /**
     * Return a request parameter or its corresponding value form `$this->defaults`.
     *
     * @param  string $key Name of the parameter
     * @return mixed
     */
    protected function getFromRequestOrDefault(string $key)
    {
        return $this->request->get($key, self::$defaults[$key]);
    }

    /**
     * Parse GET parameters into resource options.
     *
     * @return array
     */
    public function options(): array
    {
        if ($this->parsed_options) {
            return $this->parsed_options;
        }

        $limit = $this->getFromRequestOrDefault('limit');
        $page = $this->getFromRequestOrDefault('page');
        $search = $this->getFromRequestOrDefault('search');

        if ($page !== null && $limit === null) {
            throw new InvalidArgumentException('Cannot use page option without limit option');
        }


        $includes = $this->parseIncludes($this->getFromRequestOrDefault('includes'));
        $withCount = $this->getFromRequestOrDefault('withCount');



        $sortParameter = $this->getFromRequestOrDefault('sort');
        if (is_string($sortParameter)) {
            $sortParameter = json_decode($sortParameter, true);
        }

        $sort = $this->parseSort($sortParameter);


        // allow json as parameter
        $filter_groups = $this->getFromRequestOrDefault('filter_groups');
        if (is_string($filter_groups)) {
            $filter_groups = json_decode($filter_groups, true);
        }
        $filter_groups = $this->parseFilterGroups($filter_groups);


        $this->parsed_options = [
            'limit' => $limit,
            'page' => $page,
            'includes' => $includes['includes'],
            'withCount' => $withCount,
            'modes' => $includes['modes'],
            'sort' => $sort,
            'filter_groups' => $filter_groups,
            'search' => $search
        ];

        return $this->parsed_options;
    }

    /**
     * Determine whether the request asks for a paginated response.
     *
     * @return bool
     */
    public function isPaginated(): bool
    {
        return $this->getFromRequestOrDefault('limit') || $this->getFromRequestOrDefault('page');
    }
}
