<?php

namespace App\Infrastructure\Http;

use Illuminate\Http\Resources\Json\Resource as BaseResource;

abstract class HttpResource extends BaseResource
{
    // ...
}
