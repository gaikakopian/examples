<?php

namespace App\Infrastructure\Http;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Collection;

class ResourceResponder
{
    public function send($data, string $resourceClass)
    {
        if (
            (is_a($data, Collection::class)  || is_a($data, \Illuminate\Database\Eloquent\Collection::class)) &&
            !is_a($resourceClass, ResourceCollection::class, true)
        ) {
            return $resourceClass::collection($data);
        }

        return new $resourceClass($data);
    }
}
