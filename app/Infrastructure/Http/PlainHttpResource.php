<?php

namespace App\Infrastructure\Http;

use App\Infrastructure\Http\HttpResource;

class PlainHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return $this->resource;
    }
}
