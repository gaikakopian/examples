<?php

namespace App\Infrastructure\Domain;

use DB;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Facades\Log;
use Symfony\Component\EventDispatcher\Event;

/**
 * Proxy class that can defer event handling to a dynamically selected implementation.
 *
 * `chooseHandler()` is where the magic happens: It must return a handler class instance,
 * which it can be dynamically determine based on arbitrary criteria. This can be used
 * to override event handlers, eg. when the event relates to a certain Program or
 * Organization, which might need custom event handling logic.
 */
abstract class EventSubscriberProxy
{
    /**
     * List all events that the implementation should subscribe to.
     *
     * Return an array that maps global event names to method names on the subscriber class.
     * Example:
     * ```
     * [
     *      'User.begin_registration' => 'beginRegistration',
     *      'User.complete_registration' => 'completeRegistration',
     * ]
     * ```
     *
     * @return array
     */
    abstract protected function mapEvents(): array;

    /**
     * Choose the right handler for given event.
     *
     * Usually, a default handler instance should be returned,
     * if the `$event` doesn't meet any special criteria.
     *
     * @param Symfony\Component\EventDispatcher\Event $event
     * @return mixed A handler instance that implements all methods returned by `mapEvents()`
     */
    abstract protected function chooseHandler(Event $event);

    /**
     * Map events to their handler methods.
     *
     * @param Illuminate\Events\Dispatcher $dispatcher
     * @return void
     */
    public function subscribe(Dispatcher $dispatcher): void
    {
        foreach ($this->mapEvents() as $eventName => $handlerMethod) {

            $handler = get_called_class() . '@' . $handlerMethod;
            $dispatcher->listen($eventName, $handler);
        }
    }

    /**
     * Handle an event on the subscriber by passing it to the right implementation.
     *
     * @param string $methodName
     * @param array $arguments
     * @return mixed
     */
    public function __call(string $methodName, array $arguments)
    {
        // Defer to another method for testing, as `__call()`
        // will be overridden when mocking this class.
        return $this->callMethod($methodName, $arguments);
    }

    /**
     * Handle an event on the subscriber by passing it to the right implementation.
     *
     * @param string $methodName
     * @param array $arguments
     * @return mixed
     */
    public function callMethod(string $methodName, array $arguments)
    {
        if (!in_array($methodName, $this->mapEvents())) {
            Log::warning("[EventSubscriberProxy] Possible event '$methodName' unhandled.");

            return;
        }

        $event = $arguments[0];
        $handler = $this->chooseHandler($event);

        if (!method_exists($handler, $methodName)) {
            Log::warning('[EventSubscriberProxy] Chosen handler class does not implment required method', [
                'handler' => $handler,
                'method' => $methodName,
            ]);

            return;
        }
        DB::beginTransaction();
        $return = null;
        try {
            $return = $handler->$methodName($event);
            DB::commit();
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }

        return $return;
    }
}
