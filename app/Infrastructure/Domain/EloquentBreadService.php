<?php

namespace App\Infrastructure\Domain;

use App\Infrastructure\Contracts\BreadService;
use App\Infrastructure\Contracts\Query;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

abstract class EloquentBreadService implements BreadService
{
    use AppliesQueryArgsToEloquent;

    /**
     * An instance of the eloquent model we're giving access to.
     *
     * @var Illuminate\Database\Eloquent\Model
     */
    protected $baseModel;
    protected $model;

    /**
     * The constructor must be declared to require a service via dependency injection.
     */
    public function __construct(Model $model)
    {
        $this->baseModel = $model;

        $this->copyBaseModel();

    }

    /**
     * Browse all entities.
     *
     * @param  App\Infrastructure\Contracts\Query
     * @return Illuminate\Support\Collection
     */
    public function list(Query $query = null): Collection
    {
        $builder = $this->model->newQuery();

        if ($query) {
            $this->apply($builder, $query);
            $this->applyWithCount($builder, $query);
            $this->applySearch($builder, $query);
        }

        return $builder->get();
    }

    /**
     * Browse all entities (paginated).
     *
     * @param  App\Infrastructure\Contracts\Query
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginate(Query $query): LengthAwarePaginator
    {
        $builder = $this->model->newQuery();
        $this->apply($builder, $query);
        $this->applySearch($builder, $query);
        $this->applyWithCount($builder, $query);


        return $builder->paginate($query->options()['limit']);
    }

    /**
     * Create a new entity by an array of attributes.
     *
     * Attributes missing from the array will be
     * set to their respective default values.
     *
     * @param  array $attributes Attributes of the new entity
     * @return Illuminate\Contracts\Support\Arrayable Entity created
     */
    public function create(array $attributes): Arrayable
    {
        $this->copyBaseModel();

        $this->model->fill($attributes)->save();

        return $this->model->fresh();
    }

    private function copyBaseModel()
    {
        // mock Models do not allow replicate
        if (strstr(get_class($this->baseModel), 'Mock') !== false) {
            $this->model = $this->baseModel;
            return;
        }
        $this->model = $this->baseModel->replicate();

    }

    /**
     * Retrieve a single entity by its id.
     *
     * @param  mixed $id Unique identifier of the entity
     * @param  App\Infrastructure\Contracts\Query Optional query for specifying includes
     * @return Illuminate\Contracts\Support\Arrayable
     */
    public function get($id, Query $query = null): Arrayable
    {
        $builder = $this->model->newQuery();

        if ($query) {
            $this->applyIncludes($builder, $query);
            $this->applyWithCount($builder, $query);
        }

        return $builder->findOrFail($id);
    }

    /**
     * Update a single entity by its id and an associative array of attributes.
     *
     * Attributes missing from the array will remain
     * untouched on the entity (partial update).
     *
     * @param  mixed $id Unique identifier of the entity
     * @param  array $attributes New attributes
     * @return Illuminate\Contracts\Support\Arrayable Updated entity
     */
    public function update($id, array $attributes): Arrayable
    {
        $record = $this->model->newQuery()->findOrFail($id);
        $record->update($attributes);

        return $record;
    }

    /**
     * Delete an existing entity by its unique id.
     *
     * @param  mixed $id Unique identifier of the entity
     * @return Illuminate\Contracts\Support\Arrayable Deleted entity
     */
    public function delete($id): Arrayable
    {
        $record = $this->model->newQuery()->findOrFail($id);
        $record->delete();

        return $record;
    }

    /**
     * Find Entitites,
     * hanging on a given state since to long :)
     *
     * However, this might be better a trait;
     *
     * @deprecated
     */
    public function listHangingOnStateWithoutReminder($state, $hours): Collection
    {
        return $this->model->newQuery()
            ->where('state', '=', $state)
            ->where(function (Builder $query) {
                return $query->whereNULL('last_reminder')
                    ->orWhereRaw('last_reminder < last_state_change_at');
            })
            ->whereNotNull('last_state_change_at')
            ->where('last_state_change_at', '<', Carbon::now()->subHours($hours))
            ->get();
    }

    public function listHangingOnState($state, $hours): Collection
    {
        return $this->model->newQuery()
            ->where('state', '=', $state)
            ->whereNotNull('last_state_change_at')
            ->where(function (Builder $query) use ($hours) {
                return $query->whereNULL('last_reminder')
                    ->orWhere('last_reminder', '<', Carbon::now()->subHours($hours));
            })
            ->where('last_state_change_at', '<', Carbon::now()->subHours($hours))
            ->get();
    }
}
