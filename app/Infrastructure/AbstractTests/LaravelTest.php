<?php

namespace App\Infrastructure\AbstractTests;

use App\Services\MultiTenant\Facades\MultiTenant;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestCase;

class LaravelTest extends TestCase
{
    use DatabaseTransactions;
    use CreatesApplication;


    /** {@inheritdoc} */
    protected function setUp()
    {
        parent::setUp();
        MultiTenant::setTenantId(1);
    }
    //
}
