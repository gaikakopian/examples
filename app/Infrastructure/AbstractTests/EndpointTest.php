<?php

namespace App\Infrastructure\AbstractTests;

use App\Services\MultiTenant\Facades\MultiTenant;
use Modules\Core\Domain\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

class EndpointTest extends LaravelTest
{

    /**
     * A regular, unprivileged user for testing api requests etc.
     *
     * @var User
     */
    private $user;

    /**
     * A privileged admin user for testing api requests etc.
     *
     * @var User
     */
    private $admin;

    /** {@inheritdoc} */
    protected function setUp()
    {
        parent::setUp();

    }

    /**
     * Load needed information on demand;
     *
     * @param $name
     * @return Model|User|static
     */
    public function __get($name)
    {

        if ($name === 'user') {
            if (!$this->user) {
                $this->user = User::where('email', 'user@volunteer-vision.com')->firstOrFail();
            }
            return $this->user;
        }
        if ($name === 'admin') {
            if (!$this->admin) {
                $this->admin = User::where('email', 'admin@volunteer-vision.com')->firstOrFail();
            }
            return $this->admin;
        }

    }


    protected function it_uploads_and_removes_an_image_attachment(
        string $uri,
        Model $model,
        string $attachmentName,
        string $folderPath,
        $testStyle = 'small'
    ) {
        Storage::fake('public');
        $img = UploadedFile::fake()->image('file.jpg');

        $response = $this->actingAs($this->__get('user'))->call(
            'PUT',
            $uri,
            [],
            [],
            [],
            [
                'CONTENT_TYPE' => 'image/jpeg',
                'HTTP_ACCEPT' => 'application/json'
            ],
            $img
        );

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


        $model->refresh();

        $response->assertJson([
            'data' => [
                'id' => $model->id,
                camel_case($attachmentName) => [
                    $testStyle => "/storage/uploads/{$folderPath}/{$model->partitionId($model->id)}/{$testStyle}/{$model->getAttributes()[$attachmentName]}"
                ],
            ]
        ]);

        $this->assertEquals('.jpeg', substr($model->getAttributes()[$attachmentName], -5));

        $response = $this->actingAs($this->__get('user'))->json('DELETE', $uri);
        $response->assertStatus(200);

        $model->refresh();

        if ($attachmentName == 'avatar') { // avatar has a default
            $this->assertNotNull($model->$attachmentName);
        } else {
            $this->assertEquals(null, $model->$attachmentName);
        }

    }
}
