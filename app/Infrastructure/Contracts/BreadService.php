<?php

namespace App\Infrastructure\Contracts;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;

/**
 * Describes a service that provides the basic BREAD operations.
 *
 * - Browse
 * - Read
 * - Edit
 * - Add
 * - Delete
 */
interface BreadService
{
    /**
     * Browse all entities.
     *
     * @param  App\Infrastructure\Contracts\Query
     * @return Illuminate\Support\Collection
     */
    public function list(Query $query = null) : Collection;

    /**
     * Browse all entities (paginated).
     *
     * @param  App\Infrastructure\Contracts\Query
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginate(Query $query) : LengthAwarePaginator;

    /**
     * Create a new entity by an array of attributes.
     *
     * Attributes missing from the array will be
     * set to their respective default values.
     *
     * @param  array  $attributes Attributes of the new entity
     * @return Illuminate\Contracts\Support\Arrayable Entity created
     */
    public function create(array $attributes) : Arrayable;

    /**
     * Retrieve a single entity by its id.
     *
     * @param  mixed $id Unique identifier of the entity
     * @param  App\Infrastructure\Contracts\Query Optional query for specifying includes
     * @return Illuminate\Contracts\Support\Arrayable
     */
    public function get($id, Query $query = null) : Arrayable;

    /**
     * Update a single entity by its id and an associative array of attributes.
     *
     * Attributes missing from the array will remain
     * untouched on the entity (partial update).
     *
     * @param  mixed $id          Unique identifier of the entity
     * @param  array  $attributes New attributes
     * @return Illuminate\Contracts\Support\Arrayable Updated entity
     */
    public function update($id, array $attributes) : Arrayable;

    /**
     * Delete an existing entity by its unique id.
     *
     * @param  mixed $id Unique identifier of the entity
     * @return Illuminate\Contracts\Support\Arrayable Deleted entity
     */
    public function delete($id) : Arrayable;
}
