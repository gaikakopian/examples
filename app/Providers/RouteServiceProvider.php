<?php

namespace App\Providers;

use DirectoryIterator;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Passport;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * Prefix for all module routes.
     *
     * @var string
     */
    private $routePrefix = 'api/v2';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        Passport::routes();
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapModuleRoutes();
    }

    /**
     * Iterate over all Modules present in `modules`, registering each one's routes.
     *
     * @return void
     */
    private function mapModuleRoutes()
    {
        $path = base_path('modules');

        foreach (new DirectoryIterator($path) as $file) {
            if ($file->isDir() && !$file->isDot()) {
                $module = $file->getFilename();
                $this->processModule($module);
            }
        }
    }

    /**
     * Load routes for a single module.
     *
     * @param  string $name
     * @return void
     */
    private function processModule(string $name)
    {
        Route::get('/', 'App\Http\Actions\IndexAction');
        $this->processRoutes($name, 'routes', 'api');
        $this->processRoutes($name, 'serviceapi', 'serviceapi');


    }

    private function processRoutes($name, $fileName, $middleware)
    {
        $routesFile = base_path('modules/' . $name . '/Http/' . $fileName . '.php');
        if (!file_exists($routesFile)) {
//            Log::notice('[Module] Module has no routes file', ['module' => $name]);
            return;
        }
        Route::middleware($middleware)
            ->namespace('Modules\\' . $name . '\Http')
            ->prefix($this->routePrefix)
            ->group($routesFile);

    }
}
