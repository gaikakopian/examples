<?php

namespace App\Providers;


use Illuminate\Support\ServiceProvider;
use Blade;


class BladeServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->prettyDateHelper();
        $this->buttonHelper();


    }


    private function buttonHelper()
    {
        $buttonTemplate = <<<'EOT'
<table class="action" align="center" width="100%%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <table width="100%%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <a href="%s" class="button button-{{ $color or 'blue' }}" target="_blank">%s</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
EOT;


        Blade::directive('button', function ($input) use ($buttonTemplate) {
            list($description, $link) = explode(',', $input, 2);
            return sprintf($buttonTemplate, $link, $description);
        });
    }


    private function miniHelper()
    {


        Blade::directive('frontendUrl', function ($variable) {

            return '<?php echo (isset($brand)? $brand->frontend_url : isset($user) && isset($user->brand) ? $user->brand->frontend_url : http://www.volunteer-vision.com ?>';


        });
        Blade::directive('keyaccount', function ($variable) {
            return '<?php echo (isset($user) && $user->organization && $user->organization->key_account ? 
                            $user->organization->key_account->first_name . " ". 
                            $user->organization->key_account->last_name . ", " .
                            $user->organization->key_account->email . ", "
                              : "organizations keyaccount not set" 
                              ?>';
        });


    }

    private function prettyDateHelper()
    {
        $getDateFormat = function ($format) {
            switch ($format) {
                case 'full':
                    return '%d %B %Y, %H:%M';
                case 'time':
                    return '%H:%M';

                case 'shortDateTime':
                    return '%d.%m.%Y, %H:%M';
                case 'default':
                    return '%d %B %Y, %H:%M';
                default:
                    throw new \Exception('Invalid Datetime Format : ' . $format . '. Please use full,time or default' . $format);
            }
        };


//
        Blade::directive('prettydate', function ($variable) use ($getDateFormat) {

            $format = 'default';
            if (strstr($variable, ',') !== false) {
                list($variable, $format) = explode(',', $variable, 2);
            }

            if (!starts_with($variable, '$')) {
                $variable = '$' . $variable;
            }

            $format = $getDateFormat($format);

            $template = <<<'EOT'
if (is_string($date)){
  $date = Carbon\Carbon::parse($date);
}
if ($date && $date instanceOf Carbon\Carbon) 
{
    $timezone = 'Europe/Berlin';
    $locale = 'en';
    
    if ($user)
    {
        $timezone = $user->timezone;
        $locale = $user->language || $locale;
    }
    $date->setTimezone(new DateTimeZone($timezone));
    Carbon\Carbon::setLocale($locale);
    echo $date->formatLocalized($format) . ' ('. $timezone .' time)'; 

} else {
    echo '(sorry, the date saved here was invalid. Please contact our support)!';
}

EOT;


            $response = '<?php ' . PHP_EOL .
                ' $date = ' . $variable . ';' . PHP_EOL .
                ' $format = "' . $format . '";' . PHP_EOL .
                $template . PHP_EOL
                . '?>';


            return $response;


        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
//
    }
}
