<?php

namespace App\Exceptions\Server;

use App\Exceptions\BaseException;

class ParseErrorException extends BaseException
{
    /** {@inheritDoc} */
    protected $httpStatus = 500;

    /** {@inheritDoc} */
    protected $code = 'server.parseError';

    /**
     * Get an array representation of the Exception.
     *
     * @return array
     */
    public function toArray() : array
    {
        $return = parent::toArray() ;
        return $return;

    }


}
