<?php

namespace App\Exceptions\Client;

use App\Exceptions\BaseException;
use Exception;
use Throwable;

class ValidationException extends ClientException
{
    /** {@inheritDoc} */
    protected $httpStatus = 422;

    /** {@inheritDoc} */
    protected $code = 'client.validation';

    /**
     * Fields on which validation failed.
     *
     * @var array
     */
    protected $validationErrors = [];

    public function __construct(
        string $message,
        $code = null,
        Throwable $previous = null,
        array $validationErrors = []
    ) {
        parent::__construct($message, null, $previous);

        $this->validationErrors = $validationErrors;
    }

    /** {@inheritDoc} */
    public function toArray() : array
    {
        $data = parent::toArray();
        $data['errors'] = $this->validationErrors;

        return $data;
    }

    /**
     * Create an App exception from a Laravel ValidationException.
     *
     * @param Exception $exception
     * @return App\Exceptions\Client\ValidationException
     */
    public static function fromBase(Exception $exception) : BaseException
    {
        $errors = static::arrayKeysToCamelCase($exception->errors());

        return new static($exception->getMessage(), null, null, $errors);
    }

    /**
     * Convert an arrays' keys to camelCase.
     *
     * @param array $input
     * @return array
     */
    protected static function arrayKeysToCamelCase(array $input) : array
    {
        $result = [];
        foreach ($input as $key => $value) {
            $result[camel_case($key)] = $value;
        }

        return $result;
    }
}
