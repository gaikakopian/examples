<?php

namespace App\Exceptions;

use App\Exceptions\Client\NotAuthenticatedException;
use App\Exceptions\Client\NotAuthorizedException;
use App\Exceptions\Client\NotFoundException;
use App\Exceptions\Client\ValidationException;
use App\Exceptions\Client\ClientException;
use App\Exceptions\Server\ServerException;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException as IlluminateValidationException;
use League\OAuth2\Server\Exception\OAuthServerException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        OAuthServerException::class,
        ClientException::class

    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        if (app()->environment('production') && app()->bound('sentry') && $this->shouldReport($exception)) {
            app('sentry')->captureException($exception);
        }

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // This is for local development only ...
        if (env('DEBUG_EXCEPTIONS')) {
            return parent::render($request, $exception);
        }

        // Validation exceptions need special treatment
        if ($exception instanceof IlluminateValidationException) {
            $exception = ValidationException::fromBase($exception);

            // Symfony's NotFoundHttpException and Laravel's ModelNotFoundException
            // are thrown programatically and therefore need to be translated
        } elseif ($exception instanceof NotFoundHttpException) {
            $exception = NotFoundException::fromBase($exception);
        } elseif ($exception instanceof ModelNotFoundException) {
            // @Todo: also show which model was not found;
            $exception = NotFoundException::fromBase($exception);
            // Handle Passport/League OAuth Server errors
        } elseif ($exception instanceof OAuthServerException) {
            $exception = OauthException::fromBase($exception);
            // Handle Auth Guard Exception
        } elseif ($exception instanceof AuthenticationException) {
            $exception = NotAuthenticatedException::fromBase($exception);

            // Handle Auth Guard Exception
        } elseif ($exception instanceof AuthorizationException) {
            $exception = NotAuthorizedException::fromBase($exception);

            // Everything else, that's not a child class of our custom BaseException
            // will be wrapped in a ServerException for correct formatting.
        } elseif (!($exception instanceof BaseException)) {
            $exception = ServerException::fromBase($exception);
        }

        if (!$request->expectsJson() && $request->acceptsHtml()) {

            $errorclass = class_basename($exception);
            $statusCode = $this->getStatusCode($exception);

            if (view()->exists("errors.{$errorclass}")) {
                return response()->view("errors.{$errorclass}", ['exception' => $exception], $statusCode);
            }
            // toodo get status code if vaialble
            return response()->view('errors.default', ['exception' => $exception], $statusCode);
        }



        return parent::render($request, $exception);
    }

    private function getStatusCode(Exception $exception)
    {
        if (isset($exception->status)) {
            return $exception->status;
        }

        if ($exception instanceof ClientException) {
            return $exception->getHttpStatus();
        }

        return 500;


    }
}
