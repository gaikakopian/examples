<?php

namespace App\Console\Commands;

use App\Notifications\Webinar\WebinarChoiceReminderNotification;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Core\Domain\Models\User;

/**
 * Class ParticipationWaitingSenderCommand
 * Wrong content!!
 * @package App\Console\Commands
 */
class ParticipationWaitingSenderCommand extends AbstractCronjobCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:reminer:participation:waiting';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();

        $query = $this->enrollmentService->findWithoutFutureTraining()
            ->where(function ($query) {
                $query->where('last_reminder', '<', Carbon::now()->subDays(10))
                    ->orWhereNull('last_reminder');
            });

        $enrollments = $query->get();
        $i = 0;

        /**
         * @var Collection $appointments
         * @var Appointment $appointment
         * @var User $user
         * @var Collection $enrollments
         * @var Enrollment $enrollment
         */

//        foreach ($enrollments as $enrollment) {
//            $enrollment->last_reminder = Carbon::now();
//            $enrollment->save();
//
//            $user = $enrollment->user;
//            $webinars = $this->webinarService->listForEnrollment($enrollment);
//            if ($webinars->count() === 0) {
//                $this->informProgramManager($enrollment->program, $user);
//                continue;
//            }
//
//            if ($user->state === User::STATES['INMIGRATION']) {
//                continue;
//            }
//
//            $user->notify(new WebinarChoiceReminderNotification($user->brand, $user, $enrollment, $webinars));
//            $i++;
//        }


        $this->end('Send %s appoinemtnts ', $i);
    }
}
