<?php

namespace App\Console\Commands;

use App\Services\WaboboxService;
use Illuminate\Console\Command;
use Laravel\Passport\Bridge\UserRepository;
use Modules\Core\Domain\Models\User;

class CheckWhatsappStatusOfUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'whatsapp:checkstatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    protected $userRepository;
    protected $waboxService;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepository, WaboboxService $waboboxService)
    {
        parent::__construct();
        $this->waboxService = $waboboxService;
        $this->userRepository = $userRepository;

    }


    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \App\Exceptions\Server\ServerException
     */
    public function handle()
    {

        $status = $this->waboxService->getWhatsappDeviceStatus();

        $this->info(sprintf('[whatsapp] device status  %s', $status));
        return;
    }
}
