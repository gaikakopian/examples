<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Core\Domain\Services\ConferenceService;

class createConferenceRoom extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'conference:room';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a new confernece room';



    protected $conferenceService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ConferenceService $conferenceService)
    {
        parent::__construct();

        $this->conferenceService = $conferenceService;

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        $roomId = $this->conferenceService->createRoom('estart');
        echo 'created Room Id: '. $roomId . PHP_EOL;

    }
}
