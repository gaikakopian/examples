<?php

namespace App\Console\Commands;

use App\Notifications\User\SmsSampleNotification;
use Illuminate\Console\Command;
use Modules\Core\Domain\Models\User;

class SendSampleSmsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:sample:sms';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Sample SMS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $number = '1776706937';

        /** @var User $user */
        $user = User::where('phone_number','=',$number)
            ->firstOr(function() use ($number) {
                return Factory(User::class)->create( ['phone_number' => $number]);
            });

        $user->accept_sms = true;
        $user->save();


        $this->info('Sending sample sms to  ' . $user->qualifiedPhoneNumber);

        $user->notify(new SmsSampleNotification($user->brand, $user));

        $this->info('done');

    }
}
