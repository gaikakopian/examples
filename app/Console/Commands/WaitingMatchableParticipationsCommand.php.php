<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Admin\Domain\Models\CoordinatorTodo;
use Modules\Admin\Domain\Services\CoordinatorTodoService;
use Modules\Core\Domain\Services\ParticipationService;

/**
 * Class WaitingMatchableParticipationsCommand
 * @package App\Console\Commands
 * @deprecated
 */
class WaitingMatchableParticipationsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:reminder:matchableParticipation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    protected $participationService;
    protected $coordinatorTodoService;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ParticipationService $participationService, CoordinatorTodoService $coordinatorTodoService)
    {
        parent::__construct();
        $this->participationService = $participationService;
        $this->coordinatorTodoService = $coordinatorTodoService;

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $participations = $this->participationService->findMatchableParticipationsOlderThan( 42 * 24); // 42 days
//        $found = 0;
//
//        foreach($participations as $participation){
//
//
//            if ($participation->enrollment->last_reminder > Carbon::now()->subDays(7)){
//                continue;
//            }
//
//            $participation->enrollment->last_reminder = Carbon::now();
//            $participation->enrollment->save();
//            // @todo: send email here.
//
//            $found++;
//
//            $user = $participation->enrollment->user;
//            $todo = new CoordinatorTodo(
//                [
//                    'customer_id' => $user->id,
//                    'description' => 'This person is waiting for more than 42 days for a match. Please find a solution',
//                    'meta' => ['enrollment_id' => $participation->enrollment->id],
//                    'type' => CoordinatorTodo::TYPES['WAITING_FOR_MATCH'],
//                    'duedate' => Carbon::now()
//                ]);
//
//            $this->coordinatorTodoService->createTodoIfNotExistsWithinDays($todo, 14);
//
//        }
//
//        $this->info("[WaitingParticipations Reminder] Found {$found} participations to work on");


        //
    }
}
