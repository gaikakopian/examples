<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Mail\Message;

class SendOutlookInviteTestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'outlook:invite';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $from;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

//        $to = 'simon@fakir-it.de'
//        $subject = 'sample';
////        $attachments = $row->attachment;
//        $cc = ''
//        $body = 'Hello1';
//$from = 'test@fakir.it';
//        $calendar_invitation = $this->getBody($from,$from,'Simon', $to, )$row->calendar_invitation;

        $this->from = 'info@volunteer-vision.com';

        $data = [
            'start_date' => time(),
            'texto' => 'hello',
            'titulo' => 'appointment',
//            'destinatario' => 'simon@fakir-it.de'
            'destinatario' => 'outbox2@volunteer-vision.com'
        ];


        \Mail::send('emails.sample', $data, function ($message) use ($data) {


            $vcal = $this->getBody();
            $message->attachData($vcal, 'invite.ics', [
                'mime' => 'text/calendar;charset=UTF-8;method=REQUEST',
                'content-disposition' => 'attachment; filename=invite2.ics'
            ]);


//            $message->setBody($mail, 'text/calendar; charset="utf-8"; method=REQUEST');
            $message->subject($data['titulo']  . date('g:i'));
            $message->to($data['destinatario']);
//            $message->attach($filename, array('mime' => "text/calendar"));
        });

        echo 'ok';


//        \Mail::send(
//            'emails.sample',
//            ['emailBody' => $row->body],
//            function (Message $message) use ($to, $subject, $cc, $body, $calendar_invitation) {
//
//
//                $message->from('test@fakir.it', '');
////                $message->replyTo($companyEmail, 'Email Agent Evmeetings');
//                $message->to($to, '')->subject($subject);
//                $file = fopen("invite.ics", "w");
//                echo fwrite($file, $calendar_invitation);
//                fclose($file);
//                $message->attach('invite.ics', array('mime' => "text/calendar"));
//                $message->from($companyEmail, '');
//                $message->replyTo($companyEmail, 'Email Agent Evmeetings');
//                $message->to($to, '')->subject($subject);
//                $message->setBody($calendar_invitation, 'text/calendar; charset="utf-8"; method=REQUEST');
//                $message->addPart($body, "text/html");
//
//
//            });


    }

    private function getBody()
    {

        $var =  '
BEGIN:VCALENDAR
VERSION:2.0
METHOD:PUBLISH
BEGIN:VTIMEZONE
TZID:Pacific Time (US & Canada)
BEGIN:STANDARD
DTSTART:20061105T020000
RRULE:FREQ=YEARLY;BYDAY=1SU;BYMONTH=11
TZOFFSETFROM:-0700
TZOFFSETTO:-0800
END:STANDARD
BEGIN:DAYLIGHT
DTSTART:20070311T020000
RRULE:FREQ=YEARLY;BYDAY=2SU;BYMONTH=3
TZOFFSETFROM:-0800
TZOFFSETTO:-0700
TZNAME:Daylight Savings Time
END:DAYLIGHT
END:VTIMEZONE
BEGIN:VEVENT
DTSTART;TZID="Pacific Time (US & Canada)":20110405T120000
DTEND;TZID="Pacific Time (US & Canada)":20110405T130000
LOCATION;ENCODING=QUOTED-PRINTABLE:Location of the event
UID:100000000034201184
DTSTAMP:20110328T124055Z
DESCRIPTION:1. Click this link to join the Webinar:\n\n   https://www2.gotomeeting.com/join/000000000/000000000\n\n\n2. Choose one of the following audio options:\n\n   TO USE YOUR COMPUTERS AUDIO:\n   When the Webinar begins, you will be connected to audio using your computers microphone and speakers (VoIP). A headset is recommended.\n\n\n
SUMMARY;ENCODING=QUOTED-PRINTABLE:Subject of the event
END:VEVENT
END:VCALENDAR
';
    return $var;
        $domain = 'volunteer-vision.com';
        $from_address = $this->from;

        $startTime = time();
        $endTime = time() + 3600;
        $from_name = $to_name = 'Simon';
        $subject = 'Hello appointment';
        $location = 'here';
        $to_address = 'simon@fakir-it.de';



        $ical = 'BEGIN:VCALENDAR' . "\r\n" .
            'PRODID:-//Microsoft Corporation//Outlook 10.0 MIMEDIR//EN' . "\r\n" .
            'VERSION:2.0' . "\r\n" .
            'METHOD:REQUEST' . "\r\n" .
            'BEGIN:VTIMEZONE' . "\r\n" .
            'TZID:Eastern Time' . "\r\n" .
            'BEGIN:STANDARD' . "\r\n" .
            'DTSTART:20091101T020000' . "\r\n" .
            'RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=1SU;BYMONTH=11' . "\r\n" .
            'TZOFFSETFROM:-0400' . "\r\n" .
            'TZOFFSETTO:-0500' . "\r\n" .
            'TZNAME:EST' . "\r\n" .
            'END:STANDARD' . "\r\n" .
            'BEGIN:DAYLIGHT' . "\r\n" .
            'DTSTART:20090301T020000' . "\r\n" .
            'RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=2SU;BYMONTH=3' . "\r\n" .
            'TZOFFSETFROM:-0500' . "\r\n" .
            'TZOFFSETTO:-0400' . "\r\n" .
            'TZNAME:EDST' . "\r\n" .
            'END:DAYLIGHT' . "\r\n" .
            'END:VTIMEZONE' . "\r\n" .
            'BEGIN:VEVENT' . "\r\n" .
            'ORGANIZER;CN="' . $from_name . '":MAILTO:' . $from_address . "\r\n" .
            'ATTENDEE;CN="' . $to_name . '";ROLE=REQ-PARTICIPANT;RSVP=TRUE:MAILTO:' . $to_address . "\r\n" .
            'LAST-MODIFIED:' . date("Ymd\TGis") . "\r\n" .
            'UID:' . date("Ymd\TGis", strtotime($startTime)) . rand() . "@" . $domain . "\r\n" .
            'DTSTAMP:' . date("Ymd\TGis") . "\r\n" .
            'DTSTART;TZID="Eastern Time":' . date("Ymd\THis", strtotime($startTime)) . "\r\n" .
            'DTEND;TZID="Eastern Time":' . date("Ymd\THis", strtotime($endTime)) . "\r\n" .
            'TRANSP:OPAQUE' . "\r\n" .
            'SEQUENCE:1' . "\r\n" .
            'SUMMARY:' . $subject . "\r\n" .
            'LOCATION:' . $location . "\r\n" .
            'CLASS:PUBLIC' . "\r\n" .
            'PRIORITY:5' . "\r\n" .
            'BEGIN:VALARM' . "\r\n" .
            'TRIGGER:-PT15M' . "\r\n" .
            'ACTION:DISPLAY' . "\r\n" .
            'DESCRIPTION:Reminder' . "\r\n" .
            'END:VALARM' . "\r\n" .
            'END:VEVENT' . "\r\n" .
            'END:VCALENDAR' . "\r\n";

            return $ical;



    }
}
