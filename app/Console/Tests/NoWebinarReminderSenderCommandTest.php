<?php

namespace Modules\Supervisor\Test\Http;

use App\Console\Commands\NoWebinarReminderSenderCommand;
use App\Infrastructure\AbstractTests\EndpointTest;
use App\Notifications\Webinar\WebinarChoiceReminderNotification;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Notification;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Webinar;

/**
 * Test the functionality of the `/api/v1/users/me/groups` endpoint.
 */
class NoWebinarReminderSenderCommandTest extends EndpointTest
{
    /** @test */
    public function test_it_sends_webinar_info_if_not_available()
    {

        Notification::fake();


        $resultAsText = Artisan::output();

        $this->assertTrue(true);
        // Assert a notification was sent to the given users...

        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'state' => Enrollment::STATES['TRAINING'],
            'program_id' => 1,
            'last_reminder' => null
        ]);

        Artisan::call('vv:reminder:nowebinar');

        Notification::assertSentTo(
            [$this->user], WebinarChoiceReminderNotification::class
        );


    }


    public function test_it_sends_webinar_info_if_webinar_is_in_past()
    {

        Notification::fake();


        $resultAsText = Artisan::output();

        $this->assertTrue(true);
        // Assert a notification was sent to the given users...

        $pastWebinar = Webinar::query()->where('program_id', 1)
            ->where('starts_at', '<', Carbon::now())->firstOrFail();


        /** @var Enrollment $enrollment */
        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'state' => Enrollment::STATES['TRAINING'],
            'program_id' => 1,
            'last_reminder' => null
        ]);

        $enrollment->webinars()->attach($pastWebinar);


        Artisan::call('vv:reminder:nowebinar');

        Notification::assertSentTo(
            [$this->user], WebinarChoiceReminderNotification::class
        );


    }

    public function test_it_does_not_send_on_future_webinars()
    {

        Notification::fake();


        $resultAsText = Artisan::output();

        $this->assertTrue(true);
        // Assert a notification was sent to the given users...

        $pastWebinar = Webinar::query()->where('program_id', 1)
            ->where('starts_at', '>', Carbon::now())->firstOrFail();


        /** @var Enrollment $enrollment */
        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'state' => Enrollment::STATES['TRAINING'],
            'program_id' => 1,
            'last_reminder' => null
        ]);

        $enrollment->webinars()->attach($pastWebinar);


        Artisan::call('vv:reminder:nowebinar');

        Notification::assertNotSentTo(
            [$this->user], WebinarChoiceReminderNotification::class
        );


    }
}


