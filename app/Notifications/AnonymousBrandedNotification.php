<?php

namespace App\Notifications;

use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Brand;
use App\Services\BrandedDocumentService;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\Core\Domain\Services\ContentChooser\ContentChooser;
use Modules\Core\Domain\Services\ContentChooser\SearchObject;
use NotificationChannels\Twilio\TwilioSmsMessage;

abstract class AnonymousBrandedNotification extends BaseNotification
{
    /**
     * The key of the database documents
     *
     * @var String
     */
    protected $document_key;

    protected $language = 'en';

    protected $role = null;


    /**
     * The brand for which a template will be searched
     *
     * @var Modules\Core\Domain\Models\Brand
     */
    protected $brand;

    public function __construct(Brand $brand)
    {
        $this->brand = $brand;
    }

    /** {@inheritDoc} */
    public function toDatabase(User $user): array
    {
        return [
            'user' => $user,
            'brand' => $this->brand
        ];
    }

    /** {@inheritDoc} */
    public function toMail(): MailMessage
    {
        $message = new BrandedMailMessage($this->brand);

        $data = $this->getOutputFor('email');

        return $message->subject($data['subject'])
            ->line($data['content']);
    }

    /** {@inheritDoc} */
    public function toTwilio(): TwilioSmsMessage
    {
        $data = $this->getOutputFor('sms');
        return (new TwilioSmsMessage())->content($data['content']);
    }

    /**
     * Grabs a template and parses subject and content with placeholder data
     *
     * @param User $user
     * @param String $type
     * @return array
     */
    protected function getOutputFor(String $type)
    {
        $template = $this->getTemplateDocument($type);
        $placeholder_data = $this->placeholderData();

        $subject = $template->parseSubject($placeholder_data);
        $content = $template->parseContent($placeholder_data);

        return [
            'subject' => $subject,
            'content' => $content
        ];
    }

    /**
     * Makes use of the content chooser to find the best template for content and subject
     *
     * @param User $user
     * @param String $type
     */
    protected function getTemplateDocument(String $type)
    {
        $search_by = new SearchObject($this->document_key, $type, $this->language, $this->brand);
        if ($this->role) {
            $search_by->setRole($this->role);
        }
        $document = ContentChooser::findDocument($search_by);
        return $document;
    }

    /**
     * Decides which data is passed as a nameable var to the view file
     *
     * @return array
     */
    protected function placeholderData()
    {
        return [
            'brand' => $this->brand
        ];
    }
}
