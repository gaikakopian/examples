<?php

namespace App\Notifications\Enrollment;

use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\UserService;

/**
 * Class EnrollmentCompleteNotification
 * @deprecated
 * @package App\Notifications\Enrollment
 */
class EnrollmentNoreplyFinalNotification extends EnrollmentNotification
{
    public static function getComment()
    {
        return '???';
    }


}