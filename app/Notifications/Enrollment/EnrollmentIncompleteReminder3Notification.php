<?php

namespace App\Notifications\Enrollment;

class EnrollmentIncompleteReminder3Notification extends EnrollmentNotification
{

    public static function getComment()
    {
        return '20days Enrollment State = NEW';
    }
}