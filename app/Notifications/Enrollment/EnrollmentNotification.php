<?php

namespace App\Notifications\Enrollment;

use App\Notifications\BrandedNotification;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;

class EnrollmentNotification extends BrandedNotification
{
    /**
     *
     * @var Enrollment
     */
    protected $enrollment;

    public function __construct(Brand $brand, Enrollment $enrollment)
    {
        $this->brand = $brand;
        $this->enrollment = $enrollment;
        $this->program = $enrollment->program;
        $this->role = $enrollment->role;
    }

    /** {@inheritDoc} */
    protected function placeholderData(User $user)
    {
        return [
            'user' => $user,
            'brand' => $this->brand,
            'enrollment' => $this->enrollment,
        ];
    }

    public static function samplePlaceholderData($user)
    {
        return [
            'user' => $user,
            'brand' => $user->brand,
            'enrollment' => Enrollment::query()->where('state', Enrollment::STATES['ACTIVE'])->firstOrFail()
        ];

    }

}