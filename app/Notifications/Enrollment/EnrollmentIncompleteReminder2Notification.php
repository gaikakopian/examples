<?php

namespace App\Notifications\Enrollment;

class EnrollmentIncompleteReminder2Notification extends EnrollmentNotification
{
    public static function getComment()
    {
        return '10days Enrollment State = NEW';
    }

}