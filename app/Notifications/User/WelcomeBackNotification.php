<?php

namespace App\Notifications\User;

class WelcomeBackNotification extends UserNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'user_welcome_back_notification';
}