<?php

namespace App\Notifications\User;

use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\UserService;

class UserRegistrationMobileConfirmationNotification extends UserNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'user_registration_mobile_confirmation_notification';

    protected $available_channels = ['whatsapp'];
    protected $additional_mobile_channels = ['whatsapp'];


}