<?php

namespace App\Notifications\Match;

use App\Notifications\BrandedNotification;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

class MatchUserLeftAMessageNotification extends BrandedNotification
{


    protected $document_key = 'match_user_left_a_message_notification';
    /**
     *
     * @var Match
     */
    protected $match;
    protected $enrollment;
    protected $sender;
    protected $message;
    protected $showContactDetails;

    protected $additional_mobile_channels = ['whatsapp'];

    public function __construct(Brand $brand, User $sender, $showContactDetails, $message)
    {
        $this->brand = $brand;
        $this->sender = $sender;
        $this->message = $message;
        $this->showContactDetails = $showContactDetails;
    }

    /** {@inheritDoc} */
    protected function placeholderData(User $user)
    {
        return [
            'user' => $user,
            'sender' => $this->sender,
            'brand' => $this->brand,
            'match' => $this->match,
            'text' => $this->message,
            'showContactDetails' => $this->showContactDetails,
            'enrollment' => $this->enrollment,
        ];
    }

    public static function samplePlaceholderData($user)
    {

        /** @var Match $match */
        $match = Match::query()->where('state', Match::STATES['ACTIVE'])->firstOrFail();

        $enrollment = $match->getMentorParticipation()->enrollment;

        return [
            'user' => $enrollment->user,
            'sender' => $match->getMenteeParticipation()->enrollment->user,
            'brand' => $enrollment->user->brand,
            'match' => $match,
            'text' => 'Hello world!',
            'showContactDetails' => true,
            'enrollment' => $enrollment
        ];
    }


}