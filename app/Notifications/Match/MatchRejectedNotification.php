<?php

namespace App\Notifications\Match;

use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

class MatchRejectedNotification extends MatchNotification
{

    /** {@inheritDoc} */
    protected $document_key = 'match_rejected_notification';

    public static function getComment()
    {
        return 'Match was rejected';
    }

    /**
     *
     * @var Match
     */
    protected $match;
    protected $enrollment;

    public function __construct(Brand $brand, Match $match)
    {
        $this->brand = $brand;
        $this->match = $match;
    }

    /** {@inheritDoc} */
    protected function placeholderData(User $user)
    {
        return [
            'user' => $user,
            'brand' => $this->brand,
            'match' => $this->match,
        ];
    }


    public static function samplePlaceholderData($user)
    {
        /** @var Match $match */
        $match = Match::query()->where('state', Match::STATES['ACTIVE'])->firstOrFail();
        $enrollment = $match->getMentorParticipation()->enrollment;

        return [
            'user' => $enrollment->user,
            'brand' => $enrollment->user->brand,
            'match' => $match
        ];
    }


}