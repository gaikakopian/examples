<?php

namespace App\Notifications\Match;

class MatchNotConfirmedReminder3Notification extends MatchNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'match_not_confirmed_reminder_3_notification';

    protected $available_channels = ['whatsapp'];
    protected $additional_mobile_channels = ['whatsapp'];

    public static function getComment()
    {
        return '9 days match unconfirmed';
    }
}