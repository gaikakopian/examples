<?php

namespace App\Notifications\Match;

use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

class MatchCancellationInfoNotification extends MatchNotification
{

    /** {@inheritDoc} */
    protected $document_key = 'match_cancellation_info_notification';

}