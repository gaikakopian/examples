<?php

namespace App\Notifications;

use App\Exceptions\Server\ParseErrorException;
use App\Services\MultiTenant\Facades\MultiTenant;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Models\SmsLog;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ContentChooser\ContentChooser;
use Modules\Core\Domain\Services\ContentChooser\SearchObject;
use NotificationChannels\Twilio\TwilioSmsMessage;
use Twilio\TwiML\Voice\Sms;

class BrandedNotification extends BaseNotification
{
    /**
     * The key of the database documents
     *
     * @var String
     */
    protected $document_key;

    // optional to be  overwriten in subclass, when the role/program is known
    protected $role = null;
    protected $program = null;



    public function __construct(Brand $brand)
    {
        $this->brand = $brand;
    }

    /** {@inheritDoc} */
    public function toDatabase(User $user): array
    {
        return [
            'user' => $user,
            'brand' => $this->brand
        ];
    }

    /** {@inheritDoc} */
    public function toMail($user): MailMessage
    {
        $message = new BrandedMailMessage($this->brand);

        try {
            $data = $this->getOutputFor($user, 'email');
            $lines = explode("\n", $data['content']);
            $message->subject($data['subject']);
            foreach ($lines as $line) {
                $message->line($line);
            }

        } catch (ParseErrorException $exception) {

            $message->bcc('simon.fakir@volunteer-vision.com'); // @todo: move this to .env
            $message->subject('Oh! Something went wrong!');
            $message->line('This email could not be delivered. Please talk to use using the following code: ' . $this->document_key);
            $message->line('exception: ' . $exception->getMessage());


        }

        return $message;
    }

    /** {@inheritDoc} */
    public function toTwilio(User $user): TwilioSmsMessage
    {
        Log::info("Sending twilio SMS to userid: {$user->id} ");
        $data = $this->getOutputFor($user, 'sms');
        if (strlen($data['content']) > 160) {
            Log::warning(sprintf("[SMS] Longer than 160 letters; cutting shortmessage (%s ; %s) ", $data['content'],
                strlen($data['content'])));
            $data['content'] = substr($data['content'], 0, 160);
        }
        $this->saveSmsLog($user, $data['content']);

        return (new TwilioSmsMessage())->content($data['content']);
    }

    /** {@inheritDoc}
     * @throws \App\Exceptions\Client\NotFoundException
     */
    public function toWhatsapp(User $user): string
    {
        Log::info("Sending twilio SMS to userid: {$user->id} ");
        $data = $this->getOutputFor($user, 'whatsapp');

        return str_replace("<br>", PHP_EOL, $data['content']);
    }

    protected function saveSmsLog(User $user, string $content)
    {
        $log = new SmsLog();
        $log->user_id = $user->id;
        $log->from = 'outgoing';
        $log->channel = 'sms';
        $log->direction = 'outgoing';
        $log->to = $user->routeNotificationForTwilio();
        $log->body = $content;
        $log->type = $this->document_key ?: 'outgoing';
        $log->save();

    }

    /**
     * Grabs a template and parses subject and content with placeholder data
     *
     * @param User $user
     * @param String $type
     * @return array
     * @throws \App\Exceptions\Client\NotFoundException
     */
    protected function getOutputFor($user, String $type)
    {
        /** @var BrandedDocument $template */
        $template = $this->getTemplateDocument($user, $type);

        App::setLocale($template->language);

        $placeholder_data = $this->placeholderData($user);
        $placeholder_data = array_merge($placeholder_data, $this->getDefaultVariables($user));

        $subject = $template->parseSubject($placeholder_data);
        $content = $template->parseContent($placeholder_data); // @todo: improve error handling;

        return [
            'subject' => $subject,
            'content' => $content
        ];
    }





    /**
     * Makes use of the content chooser to find the best template for content and subject
     *
     * @param User $user
     * @param String $type
     *
     * @return BrandedDocument
     * @throws \App\Exceptions\Client\NotFoundException
     */
    protected function getTemplateDocument(User $user, String $type)
    {

        $key = $this->getDocumentKey();

        $search_by = new SearchObject($key, $type, $user->getPreferredLanguageAttribute(), $this->brand, $this->program, $this->role);

        $document = ContentChooser::findDocument($search_by);

        return $document;
    }

    /**
     ** Decides which data is passed as a nameable var to the view file
     * @param User $user
     * @return array
     */
    protected function placeholderData(User $user)
    {
        return [
            'user' => $user,
            'brand' => $this->brand
        ];
    }

    public static function samplePlaceholderData($user)
    {
        return [
            'user' => $user,
            'brand' => $user->brand
        ];

    }

}
