<?php

namespace App\Notifications\Program;

use App\Notifications\BrandedNotification;

use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;

/**
 * Class ProgramManagerNoWebinarNotification
 * @package App\Notifications\Program
 *
 */
class ProgramManagerNoWebinarNotification extends BrandedNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'program_manager_no_webinar_notification';

    var $program;

    public function __construct(Brand $brand, Program $program, User $user)
    {
        $this->brand = $brand;
        $this->user = $user;
        $this->program = $program;

    }

    /** {@inheritDoc} */
    protected function placeholderData(User $user)
    {
        return [
            'brand' => $this->brand,
            'user' => $user,
            'program' => $this->program
        ];
    }

    public static function samplePlaceholderData($user)
    {
        $program = Program::query()->inRandomOrder()->firstOrFail();

        return [
            'user' => $user,
            'brand' => $user->brand,
            'program' => $program,


        ];
    }


}