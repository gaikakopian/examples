<?php

namespace App\Notifications\UserInvitation;

use App\Notifications\AnonymousBrandedNotification;
use App\Notifications\BrandedNotification;
use Illuminate\Support\Facades\App;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserInvitation;
use Modules\Core\Domain\Models\UserLead;

abstract class UserLeadAbstractNotification extends AnonymousBrandedNotification
{
    /**
     * to notify invited users on the sysem
     */
    protected $userLead;



    public function __construct(Brand $brand, UserLead $userLead)
    {
        $this->brand = $brand;
        $this->userLead = $userLead;
        $this->language = $userLead->language;
        $this->role = $userLead->registrationCode->primary_role;

    }


    /** {@inheritDoc} */
    protected function placeholderData()
    {

        App::setLocale($this->userLead->language);

        $notification = [
            'brand' => $this->brand,
            'userLead' => $this->userLead,
            'link' => $this->userLead->registrationCode->getRegistrationLink($this->brand),
            'disableLead' => self::generateLink($this->userLead, 'disable'),
            'activateLead' => self::generateLink($this->userLead, 'activate'),
        ];

        return array_merge($notification, $this->getDefaultVariables($this->userLead));
    }

    public static function samplePlaceholderData($user)
    {

        $lead = UserLead::query()->where('state', UserLead::STATES['CONTACTED'])->firstOrFail();

        return [
            'brand' => $user->brand,
            'userLead' => $lead,
            'link' => $lead->registrationCode->getRegistrationLink($user->brand),
            'disableLead' => self::generateLink($lead, 'disable'),
            'activateLead' => self::generateLink($lead, 'activate'),

        ];

    }

    private static function generateLink(UserLead $userLead, $action){
        $secret = $userLead->emailSecret($action);
        $route = route('email.response.userlead',
            [
                'id' => $userLead->id,
                'action' => $action,
                'secret' => $secret,
            ]
        );

        return $route;
    }


    /** {@inheritDoc} */
    public function via($notifiable)
    {
        return ['mail'];
    }
}