<?php

namespace App\Notifications\Appointment;

class LastAppointmentPlannedNotification extends AppointmentNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'appointment_planned_last_notification';
}