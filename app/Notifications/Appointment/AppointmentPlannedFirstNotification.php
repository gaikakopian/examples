<?php

namespace App\Notifications\Appointment;

use Modules\Core\Domain\Models\User;

/**
 * Class AppointmentPlannedFirstNotification
 * @package App\Notifications\Appointment
 *
 */
class AppointmentPlannedFirstNotification extends AppointmentNotification
{

    protected $additional_mobile_channels = ['whatsapp'];

    public static function getComment()
    {
        return 'first app. was planned; only Mentee!';
    }


    /** {@inheritDoc} */
    protected function placeholderData(User $user)
    {
        $data = parent::placeholderData($user);
        $data['technicalInfo'] = $user->organization->technical_usage_comment;
        return $data;

    }
    public static function samplePlaceholderData($user)
    {
        $data = parent::samplePlaceholderData($user);
        $data['technicalInfo'] = "Please use Google Chrome";
        return $data;
    }

}