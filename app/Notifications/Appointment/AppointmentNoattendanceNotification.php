<?php

namespace App\Notifications\Appointment;

use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;
use Modules\Scheduling\Domain\Models\Appointment;

class AppointmentNoattendanceNotification extends AppointmentNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'appointment_noattendance_notification';

    public static function getComment()
    {
        return 'if no one came online';
    }


    public function __construct(Brand $brand, Appointment $appointment, Enrollment $enrollment)
    {
        parent::__construct($brand, $appointment, $enrollment);

    }

    /** {@inheritDoc} */
    protected function placeholderData(User $user)
    {
        $data = parent::placeholderData($user);

        $data = self::generateLinks($user, $this->appointment, $data);

        return $data;
    }

    private static function generateLinks(User $user, Appointment $appointment, $data)
    {

        $secret = $user->emailSecret('RESPONSE_APPOINTMENT');

        $linkParameters = [
            'id' => $appointment->id,
            'userid' => $user->id,
            'secret' => $secret,
        ];

        $linkParameters['action'] = 'finish_offline';
        $data['linkOffline'] = route('email.appointment.response', $linkParameters);

        $linkParameters['action'] = 'finish_offline';
        $data['linkOffline'] = route('email.appointment.response', $linkParameters);

        $linkParameters['action'] = 'email_noshow';
        $data['linkNoshow'] = route('email.appointment.response', $linkParameters);

        $linkParameters['action'] = 'report_technicalissues';
        $data['linkTechnical'] = route('email.appointment.response', $linkParameters);

        $linkParameters['action'] = 'cancel_after';
        $data['linkNoMeeting'] = route('email.appointment.response', $linkParameters);

        $linkParameters['action'] = 'finish_after';
        $data['linkSuccessNotTracked'] = route('email.appointment.response', $linkParameters);

        return $data;

    }

    public static function samplePlaceholderData($user)
    {
        $data = parent::samplePlaceholderData($user);

        /** @var Appointment $appointment */
        $appointment = Appointment::query()->inRandomOrder()->firstOrFail();
        $data = self::generateLinks($user, $appointment, $data);
        return $data;
    }
}