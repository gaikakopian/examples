<?php

namespace App\Notifications\Match;

use App\Notifications\BrandedNotification;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;

class ParticipationSearchingForAMatchNotification extends BrandedNotification
{

    protected $participation;
    protected $enrollment;

    public function __construct(Brand $brand, Participation $participation)
    {
        $this->brand = $brand;
        $this->participation = $participation;

    }

    /** {@inheritDoc} */
    protected function placeholderData(User $user)
    {
        return [
            'user' => $user,
            'brand' => $this->brand,
            'participation' => $this->participation,
            'cancelParticipation' => self::generateLink($user, $this->participation->id, 'disable'),
            'postponeParticipation' => self::generateLink($user, $this->participation->id, 'postpone')
        ];
    }

    private static function generateLink(User $user, $participationid, $action)
    {
        $secret = $user->emailSecret($action);
        $route = route('email.response.participation',
            [
                'userid' => $user->id,
                'participationid' => $participationid,
                'action' => $action,
                'secret' => $secret,
            ]
        );

        return $route;
    }


    public static function samplePlaceholderData($user)
    {

        $participation = Participation::query()->inRandomOrder()->firstOrFail();

        return [
            'user' => $user,
            'brand' => $user->brand,
            'participation' => $participation,
            'cancelParticipation' => self::generateLink($user, $participation->id, 'disable'),
            'postponeParticipation' => self::generateLink($user, $participation->id, 'postpone')
        ];
    }


}