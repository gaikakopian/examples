<?php

namespace App\Notifications\Webinar;

use App\Notifications\BrandedNotification;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Webinar;

class WebinarNotification extends BrandedNotification
{
    /**
     * @var Webinar
     */
    protected $webinar;
    protected $program;

    public function __construct(Brand $brand, Webinar $webinar)
    {
        $this->brand = $brand;
        $this->webinar = $webinar;
    }

    /** {@inheritDoc} */
    protected function placeholderData(User $user)
    {
        return [
            'user' => $user,
            'brand' => $this->brand,
            'webinar' => $this->webinar,
        ];
    }

    public static function samplePlaceholderData($user)
    {
        $webinar = Webinar::query()->where('state', Webinar::STATES['PLANNED'])->firstOrFail();

        return [
            'user' => $user,
            'brand' => $user->brand,
            'webinar' => $webinar
        ];
    }

}