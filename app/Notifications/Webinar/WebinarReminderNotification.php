<?php

namespace App\Notifications\Webinar;

class WebinarReminderNotification extends WebinarNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'webinar_reminder';
}