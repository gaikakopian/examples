<?php

namespace App\Notifications\Webinar;

class WebinarCanceledNotification extends WebinarNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'webinar_canceled_notification';
}